package insuide.wisefly.wiseflypoc.sensors;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.opengl.Matrix;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import insuide.wisefly.wiseflypoc.helpers.Kalman;
import insuide.wisefly.wiseflypoc.manager.PreferencesManager;

public class StepAndAzimutDetector implements SensorEventListener2 {

    private static final float SHAKE_THRESHOLD = 800;
    public static float[] gravity = new float[3];
    public static float[] linear = new float[4];
    public static float[] linearRemapped = new float[4];
    public static float[] magn = new float[4];
    public static float[] magnuncal = new float[4];
    public static float[] magn_remapped = new float[4];
    public static float[] uncalmagn_remapped = new float[4];
    public static double startLat;
    public static double startLon;
    public static int stepCounter = 0;
    public static double azimuth;
    public static int altitude = 150;
    public static double distanceLongitude;
    public static float stepLength;
    public static String version;
    public static int units = 0;
    private static double oldAzimuth = 0;
    private double turnDegrees = 0;
    private static float frequency;
    private static boolean stepBegin = false;
    private static float[] iMatrix = new float[9];
    private static float[] RMatrix = new float[16];
    private static float[] RMatrixRemapped = new float[16];
    private static float[] RMatrixTranspose = new float[16];
    private static float[] orientation = new float[3];
    private static double deltaLat;
    private static double deltaLon;
    private static float iStep = 1;
    private static float ugainA;
    private static float ugainM;
    private static double[] xa0 = new double[4];
    private static double[] ya0 = new double[4];
    private static double[] xa1 = new double[4];
    private static double[] ya1 = new double[4];
    private static double[] xa2 = new double[4];
    private static double[] ya2 = new double[4];
    private static float[] tpA = new float[3];
    private static float[] tpM = new float[3];
    private static double[] xm0 = new double[4];
    private static double[] ym0 = new double[4];
    private static double[] xm1 = new double[4];
    private static double[] ym1 = new double[4];
    private static double[] xm2 = new double[4];
    private static double[] ym2 = new double[4];
    private static float stepThreshold = 1.0f;
    private static float decl = 0;
    private static boolean initialStep;
    private static boolean newStepDetected = false;
    private static long startTime;
    public boolean gyroExists = false;
    private AdvancedOrientationSensorProvider mOrientationProvider;
    private SensorManager mSensorManager;
    private Kalman magx;
    private Kalman magy;
    private Kalman magz;
    private int magnUnits;
    private int aclUnits;
    private onStepUpdateListener stepUpdateListener;
    private onCalibratedMagneticUpdateListener calibMagneticUpdateListener;
    private onUncalibratedMagneticUpdateListener uncalibratedMagneticUpdateListener;
    public static boolean  isPositionSet = false;
    private boolean isAccurate = false;
    private Context context;
    int axisX;
    int axisY;
    private float hardIronOffsetX,hardIronOffsetY,hardIronOffsetZ = 0f;
    private float softIronScaleX,softIronScaleY,softIronScaleZ = 1f;
    private int axis = 2;

    private static double[] xm0uncal = new double[4];
    private static double[] ym0uncal = new double[4];
    private static double[] xm1uncal = new double[4];
    private static double[] ym1uncal = new double[4];
    private static double[] xm2uncal = new double[4];
    private static double[] ym2uncal = new double[4];

    private float pressAltitude;
    private onAltitudeChangedListener mAltitudeChangedListener;
    private long gyroTime = 0;
    private double currVal = 0.0;
    private double previousKnowVal = 0.0;
    private float mAccelLast;
    private float mAccelCurrent;
    private float mAccel;
    private float ALPHA_NOISE = 0.25f;
    int prevAxis = 0;
    private boolean shake;

    public StepAndAzimutDetector(Context cont, Fragment context) {
        this.context = cont;
        if (context instanceof onStepUpdateListener) {
            stepUpdateListener = (onStepUpdateListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        if (context instanceof onCalibratedMagneticUpdateListener) {
            calibMagneticUpdateListener = (onCalibratedMagneticUpdateListener) context;
        }

        if (context instanceof onUncalibratedMagneticUpdateListener) {
            uncalibratedMagneticUpdateListener = (onUncalibratedMagneticUpdateListener) context;
        }

        if (context instanceof onAltitudeChangedListener) {
            mAltitudeChangedListener = (onAltitudeChangedListener) context;
        }



        hardIronOffsetX     = PreferencesManager.getFloat(cont,"hardironx", 0f);
        hardIronOffsetY     = PreferencesManager.getFloat(cont,"hardirony", 0f);
        hardIronOffsetZ     = PreferencesManager.getFloat(cont, "hardironz", 0f);

        softIronScaleX      = PreferencesManager.getFloat(cont,"softironx", 1.0f);
        softIronScaleY      = PreferencesManager.getFloat(cont,"softirony", 1.0f);
        softIronScaleZ      = PreferencesManager.getFloat(cont,"softironz", 1.0f);

        stepCounter = 0;
        initialStep = true;

        magn[0] = magn[1] = magn[2] = gravity[0] = gravity[1] = 0;
        magnuncal[0] = magnuncal[1] = magnuncal[2] = 0;
        gravity[2] = 9.81f;
        ugainM = ugainA = 154994.3249f;
        tpA[0] = tpM[0] = 0.9273699683f;
        tpA[1] = tpM[1] = -2.8520278186f;
        tpA[2] = tpM[2] = 2.9246062355f;



        mSensorManager = (SensorManager) cont.getSystemService(Context.SENSOR_SERVICE);
        axisX = SensorManager.AXIS_X;
        axisY = SensorManager.AXIS_Y;


        if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            gyroExists = true;
            mOrientationProvider = new AdvancedOrientationSensorProvider((SensorManager) cont.getSystemService(Context.SENSOR_SERVICE), cont);
//            if (gyroExists) {
//                //use gyroscope with impovedOrientationProvider
//                mOrientationProvider.start();
//            }
        }
        boolean hasBarometer = cont.getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_BAROMETER);
        System.out.print("xxx");

        //initOrientationProvider();
        activateNow();
        aclUnits = 0;
        magnUnits = 0;
        startTime = System.nanoTime();

    }






    /**
     * Initializing
     *
     * @param startLat
     * @param startLon
     * @param distanceLongitude
     */
    public static  void initialize(double startLat, double startLon, double distanceLongitude, double altitude) {
        StepAndAzimutDetector.startLat = startLat;
        StepAndAzimutDetector.startLon = startLon;
        StepAndAzimutDetector.distanceLongitude = distanceLongitude;
        StepAndAzimutDetector.altitude = (int) altitude;
        //StepAndAzimutDetector.lastErrorGPS = lastErrorGPS;
        trueNorth();
    }

    public void activateNow(){
        try {
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_GAME);
            if(calibMagneticUpdateListener != null) {
                // mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI);
            }
            if(uncalibratedMagneticUpdateListener != null){
                mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED), SensorManager.SENSOR_DELAY_GAME);
            }
            //mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_GAME);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_FASTEST);
            if(gyroExists == true){
                mSensorManager.registerListener(StepAndAzimutDetector.this,
                        mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (gyroExists) {
            //use gyroscope with impovedOrientationProvider
            mOrientationProvider.start();
        }
    }

    public static  void setLocation(double lat, double lon) {
        startLat = lat;
        startLon = lon;
    }

    private static  void trueNorth() {
        long time = System.currentTimeMillis();
        GeomagneticField geo = new GeomagneticField((float) startLat, (float) startLon, altitude, time);
        decl = geo.getDeclination();
        //decl =0;
    }



    public  void startSensors(boolean initialStep) {
        isPositionSet = initialStep;
    }





    public  void reactivateSensors() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(StepAndAzimutDetector.this);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_GAME);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 1);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 1);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_GAME);
            if (gyroExists) {
                //use gyroscope with impovedOrientationProvider
                mOrientationProvider.start();
            }
        }
    }

    public  void pauseSensors() {
        try {
            mSensorManager.unregisterListener(this);
            //new orientation provider
            mOrientationProvider.stop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public  void imbaMagnetic(float[] magnetic) {
        // LowPass 0.5Hz for alpha0

        xm0[0] = xm0[1];
        xm0[1] = xm0[2];
        xm0[2] = xm0[3];
        xm0[3] = magnetic[0] / ugainM;
        ym0[0] = ym0[1];
        ym0[1] = ym0[2];
        ym0[2] = ym0[3];
        ym0[3] = (xm0[0] + xm0[3]) + 3 * (xm0[1] + xm0[2]) + (tpM[0] * ym0[0]) + (tpM[1] * ym0[1]) + (tpM[2] * ym0[2]);
        magn[0] = (float) ym0[3];

        // LowPass 0.5Hz for alpha1
        xm1[0] = xm1[1];
        xm1[1] = xm1[2];
        xm1[2] = xm1[3];
        xm1[3] = magnetic[1] / ugainM;
        ym1[0] = ym1[1];
        ym1[1] = ym1[2];
        ym1[2] = ym1[3];
        ym1[3] = (xm1[0] + xm1[3]) + 3 * (xm1[1] + xm1[2]) + (tpM[0] * ym1[0]) + (tpM[1] * ym1[1]) + (tpM[2] * ym1[2]);
        magn[1] = (float) ym1[3];

        // LowPass 0.5Hz for alpha2
        xm2[0] = xm2[1];
        xm2[1] = xm2[2];
        xm2[2] = xm2[3];
        xm2[3] = magnetic[2] / ugainM;
        ym2[0] = ym2[1];
        ym2[1] = ym2[2];
        ym2[2] = ym2[3];
        ym2[3] = (xm2[0] + xm2[3]) + 3 * (xm2[1] + xm2[2]) + (tpM[0] * ym2[0]) + (tpM[1] * ym2[1]) + (tpM[2] * ym2[2]);
        magn[2] = (float) ym2[3];

        double val             = Math.sqrt(magnetic[0]*magnetic[0] + magnetic[1]*magnetic[1]+magnetic[2]*magnetic[2]);
        double aftermagnitude = Math.sqrt(magn[0]*magn[0] + magn[1]*magn[1]+magn[2]*magn[2]);


        // magneticStable = true;

        SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magn);
        Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
        Matrix.multiplyMV(magn_remapped, 0, RMatrixTranspose, 0, magn, 0);
        if(calibMagneticUpdateListener != null) {
            calibMagneticUpdateListener.onCalibratedMagneticUpdate(magn_remapped.clone(), aftermagnitude);
        }
    }

    public  void imbaUnCalibratedMagnetic(float[] magnetic) {
        // LowPass 0.5Hz for alpha0
        if(calibMagneticUpdateListener != null){
            hardIronOffsetX = magnetic[3];
            hardIronOffsetY = magnetic[4];
            hardIronOffsetZ = magnetic[5];
            softIronScaleX = 1;
            softIronScaleY = 1;
            softIronScaleZ = 1;
//            if(magx == null){
//                magx = new Kalman(magnetic[0], 1e-3d,0.75);
//            }else{
//                double[] predictedDistance = magx.estimatedFilter(new ArrayRealVector(1, magnetic[0]));
//                magnetic[0] = (float) predictedDistance[0];
//            }
//
//            if(magy == null){
//                magy = new Kalman(magnetic[1], 1e-3d,0.75);
//            }else{
//                double[] predictedDistance = magy.estimatedFilter(new ArrayRealVector(1, magnetic[1]));
//                magnetic[1] = (float) predictedDistance[0];
//            }
//
//            if(magz == null){
//                magz = new Kalman(magnetic[2], 1e-3d,0.75);
//            }else{
//                double[] predictedDistance = magz.estimatedFilter(new ArrayRealVector(1, magnetic[2]));
//                magnetic[2] = (float) predictedDistance[0];
//            }
//            magnuncal[0] = (magnetic[0] - hardIronOffsetX)*1;
//            magnuncal[1] = (magnetic[1] - hardIronOffsetY)*1;
//            magnuncal[2] = (magnetic[2] - hardIronOffsetZ)*1;
        }




        xm0uncal[0] = xm0uncal[1];
        xm0uncal[1] = xm0uncal[2];
        xm0uncal[2] = xm0uncal[3];
        xm0uncal[3] = magnetic[0] / ugainM;
        ym0uncal[0] = ym0uncal[1];
        ym0uncal[1] = ym0uncal[2];
        ym0uncal[2] = ym0uncal[3];
        ym0uncal[3] = (xm0uncal[0] + xm0uncal[3]) + 3 * (xm0uncal[1] + xm0uncal[2]) + (tpM[0] * ym0uncal[0]) + (tpM[1] * ym0uncal[1]) + (tpM[2] * ym0uncal[2]);
        magnuncal[0] = (float) ym0uncal[3];

        // LowPass 0.5Hz for alpha1
        xm1uncal[0] = xm1uncal[1];
        xm1uncal[1] = xm1uncal[2];
        xm1uncal[2] = xm1uncal[3];
        xm1uncal[3] = magnetic[1] / ugainM;
        ym1uncal[0] = ym1uncal[1];
        ym1uncal[1] = ym1uncal[2];
        ym1uncal[2] = ym1uncal[3];
        ym1uncal[3] = (xm1uncal[0] + xm1uncal[3]) + 3 * (xm1uncal[1] + xm1uncal[2]) + (tpM[0] * ym1uncal[0]) + (tpM[1] * ym1uncal[1]) + (tpM[2] * ym1uncal[2]);
        magnuncal[1] = (float) ym1uncal[3];

        // LowPass 0.5Hz for alpha2
        xm2uncal[0] = xm2uncal[1];
        xm2uncal[1] = xm2uncal[2];
        xm2uncal[2] = xm2uncal[3];
        xm2uncal[3] = magnetic[2] / ugainM;
        ym2uncal[0] = ym2uncal[1];
        ym2uncal[1] = ym2uncal[2];
        ym2uncal[2] = ym2uncal[3];
        ym2uncal[3] = (xm2uncal[0] + xm2uncal[3]) + 3 * (xm2uncal[1] + xm2uncal[2]) + (tpM[0] * xm2uncal[0]) + (xm2uncal[1] * ym2uncal[1]) + (xm2uncal[2] * ym2uncal[2]);
        magnuncal[2] = (float) ym2uncal[3];


        magnuncal[0] = (magnuncal[0] - hardIronOffsetX)*softIronScaleX;
        magnuncal[1] = (magnuncal[1] - hardIronOffsetY)*softIronScaleY;
        magnuncal[2] = (magnuncal[2] - hardIronOffsetZ)*softIronScaleZ;
        magnuncal[3] = magnUnits;

        // magneticStable = true;
        SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magnuncal);
        //float matrix[] = mOrientationProvider.getRotationMatrixArray();
        Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
        Matrix.multiplyMV(uncalmagn_remapped, 0, RMatrixTranspose, 0, magnuncal, 0);
        if(uncalibratedMagneticUpdateListener != null) {
            uncalibratedMagneticUpdateListener.onUncalibratedMagneticUpdate(uncalmagn_remapped.clone(), magnuncal);
        }
    }

    public  void imbaGravity(float[] accel) {
        // LowPass 0.5Hz for alpha0
        double magnitude = Math.sqrt(accel[0]*accel[0] + accel[1]*accel[1]+accel[2]*accel[2]);
        xa0[0] = xa0[1];
        xa0[1] = xa0[2];
        xa0[2] = xa0[3];
        xa0[3] = accel[0] / ugainA;
        ya0[0] = ya0[1];
        ya0[1] = ya0[2];
        ya0[2] = ya0[3];
        ya0[3] = (xa0[0] + xa0[3]) + 3 * (xa0[1] + xa0[2]) + (tpA[0] * ya0[0]) + (tpA[1] * ya0[1]) + (tpA[2] * ya0[2]);
        gravity[0] = (float) ya0[3];

        // LowPass 0.5Hz for alpha1
        xa1[0] = xa1[1];
        xa1[1] = xa1[2];
        xa1[2] = xa1[3];
        xa1[3] = accel[1] / ugainA;
        ya1[0] = ya1[1];
        ya1[1] = ya1[2];
        ya1[2] = ya1[3];
        ya1[3] = (xa1[0] + xa1[3]) + 3 * (xa1[1] + xa1[2]) + (tpA[0] * ya1[0]) + (tpA[1] * ya1[1]) + (tpA[2] * ya1[2]);
        gravity[1] = (float) ya1[3];

        // LowPass 0.5Hz for alpha2
        xa2[0] = xa2[1];
        xa2[1] = xa2[2];
        xa2[2] = xa2[3];
        xa2[3] = accel[2] / ugainA;
        ya2[0] = ya2[1];
        ya2[1] = ya2[2];
        ya2[2] = ya2[3];
        ya2[3] = (xa2[0] + xa2[3]) + 3 * (xa2[1] + xa2[2]) + (tpA[0] * ya2[0]) + (tpA[1] * ya2[1]) + (tpA[2] * ya2[2]);
        gravity[2] = (float) ya2[3];
    }

    public  void imbaLinear(float[] accel) {
//        linear[0] = accel[0] - gravity[0];
//        linear[1] = accel[1] - gravity[1];
//        linear[2] = accel[2] - gravity[2];
          linear = lowPass(accel, linear);
          linearRemapped[0] = linear[0];
          linearRemapped[1] = linear[1];
          linearRemapped[2] = linear[2];
    }



    public int getVerticalAxis(){
        return axis;
    }



    public  void calculateAzimuth() {

        int axis = getVerticalAxis();
        int x_world = axisX,y_world = axisY;
        if(axis == 2){
            x_world = SensorManager.AXIS_X;
            y_world = SensorManager.AXIS_Y;
        }else if(axis ==1){
            x_world = SensorManager.AXIS_X;
            y_world = SensorManager.AXIS_Z;
        }else if(axis == 0){
            x_world = SensorManager.AXIS_Z;
            y_world = SensorManager.AXIS_X;
        }


        if (gyroExists) {
            float orientationValues[] = mOrientationProvider.getAzimuth(decl, SensorManager.AXIS_X, SensorManager.AXIS_Y);
            azimuth = orientationValues[0];

        }else {
            SensorManager.remapCoordinateSystem(RMatrix, x_world, y_world, RMatrixRemapped);
            SensorManager.getOrientation(RMatrixRemapped, orientation);
            if (orientation[0] >= 0) {
                // Azimuth-Calculation (rad in degree)
                azimuth = (orientation[0] * 57.29577951f + decl);
            } else {
                // Azimuth-Calculation (rad in degree) +360
                azimuth = (orientation[0] * 57.29577951f + 360 + decl);
            }

            if (azimuth >= 360) {
                azimuth -= 360;
            }

        }

        RMatrix = mOrientationProvider.getRotationMatrixArray();
        //SensorManager.remapCoordinateSystem(RMatrix,x_world, y_world,RMatrixRemapped);
        Matrix.transposeM(RMatrix, 0, RMatrixRemapped, 0);
        //Matrix.multiplyMV(linearRemapped, 0, RMatrixTranspose, 0, linear, 0);

    }



    public  void stepDetection() {
//        if(shake){
//            return;
//        }
        float value = linearRemapped[2];
        ArrayList<Float> currentAccel = new ArrayList<>();;


        boolean intimate = false;
        if (initialStep && value >= stepThreshold) {
            // Introduction of a step
            initialStep = false;
            stepBegin = true;
        } else if(!stepBegin) {
            if (oldAzimuth - azimuth > 5 || oldAzimuth - azimuth < -5) {
                //invoke step (only interface, not a real step), because orientation of user has changed more than X degree
                //so a step is necessary to update users position marker and respective orientation
                //at this position in code it means: no step is being awaited and therefore check orientation change
                //sendOrientationChange();
                intimate = true;
            }
        }
        if (stepBegin && iStep / frequency >= 0.24f && iStep / frequency <= 0.8f) {
            // Timeframe for step between minTime and maxTime
            // Check for negative peak
            if (value < -stepThreshold) {
                // TimeFrame correct AND Threshold of reverse side reached
//
                //distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz,2));
                stepCounter++;
                stepBegin = false;
                iStep = 1;
                initialStep = true;
                newStepDetected = true;


                newStep();

                //save old azimith for possibly necessary orientation change, in case no steps are detected and users orientation changes strong enough
                oldAzimuth = azimuth;

            } else {
                // TimeFrame correct but negative Threshold is too low
                iStep++;
                //sendOrientationChange();
            }
        } else if (stepBegin && iStep / frequency < 0.24f) {
            // TimeFrame for step too small, so wait and iStep++
            iStep++;
            //sendOrientationChange();
            turnDegrees = azimuth - oldAzimuth;
////
//            stepUpdateListener.onStepUpdate(2,turnDegrees, azimuth);
        } else if (stepBegin && iStep / frequency > 0.8f) {
            // TimeFrame for step too long
            stepBegin = false;
            initialStep = true;
            iStep = 1;
//            turnDegrees = azimuth - oldAzimuth;
//
//            stepUpdateListener.onStepUpdate(2,turnDegrees, azimuth);
        }
        if(!newStepDetected ){
            //if(!newStepDetected){
            sendOrientationChange();
        }



    }



    private void sendOrientationChange(){
        turnDegrees = azimuth - oldAzimuth;
        double diff = currVal - previousKnowVal;
//        if(Math.abs(diff) < 5){
//            diff = 0;
//        }
        if(Math.abs(diff) > 5) {

            stepUpdateListener.onStepUpdate(0, turnDegrees, azimuth, diff, linearRemapped, axis);
            oldAzimuth = azimuth;
            previousKnowVal = currVal;
        }
    }
    public void setGyroReadings(){
        previousKnowVal = 0;
        currVal = 0;
    }
    private  void newStep() {
//        if(shake){
//            newStepDetected = false;
//            return;
//        }
//        double winkel = azimuth;
//        double winkel2 = winkel * 0.01745329252;
//
//        deltaLat = Math.cos(winkel2) * 0.000008984725966 * stepLength;
//        // 100cm for a step will be calculated according to angle on lat
//        deltaLon = Math.sin(winkel2) / (distanceLongitude * 1000) * stepLength;
//        // 100cm for a step will be calculated according to angle on lon
//
//        deltaLat = Math.abs(deltaLat);
//        deltaLon = Math.abs(deltaLon);
//        // made by Christian Henke
//        if (startLat > 0) {
//            // User is on northern hemisphere, Latitude bigger than 0
//            if (winkel > 270 || winkel < 90) { // Movement towards north
//                startLat += deltaLat;
//            } else {
//                // Movement towards south
//                startLat -= deltaLat;
//            }
//        } else if (startLat < 0) {
//            // User is on southern hemisphere, Latitude smaller than 0
//            if (winkel > 270 || winkel < 90) {
//                // Movement towards north
//                startLat += deltaLat;
//            } else {
//                // Movement towards south
//                startLat -= deltaLat;
//            }
//        }
//        if (winkel < 180) {
//            // Movement towards east
//            startLon += deltaLon;
//        } else {
//            // Movement towards west
//            startLon -= deltaLon;
//        }


        turnDegrees = azimuth - oldAzimuth;
//        if(Math.abs(turnDegrees) < 5){
//            turnDegrees = 0;
//        }
        double diff = currVal - previousKnowVal;
//        if(Math.abs(diff) < ){
//            diff = 0;
//        }
        previousKnowVal = currVal;
        if(isPositionSet ) {
            stepUpdateListener.onStepUpdate(1, turnDegrees, azimuth, diff, linearRemapped, axis);
        }
        newStepDetected = false;
    }

    public  void changeDelay(int freq, int sensor) {
        // LowPassFilter 3. Order - Corner frequency all at 0.3 Hz

        //Initializing on 50Hz
        float ugain = 154994.3249f;
        float tp0 = 0.9273699683f;
        float tp1 = -2.8520278186f;
        float tp2 = 2.9246062355f;

        // Values according to actual frequency
        if (freq >= 125) {    //130
            ugain = 2662508.633f;
            tp0 = 0.9714168814f;
            tp1 = -2.9424208232f;
            tp2 = 2.9710009372f;
        } else if (freq <= 124 && freq >= 115) { //120
            ugain = 2096647.970f;
            tp0 = 0.9690721133f;
            tp1 = -2.9376603253f;
            tp2 = 2.9685843964f;
        } else if (freq <= 114 && freq >= 105) { //110
            ugain = 1617241.715f;
            tp0 = 0.9663083052f;
            tp1 = -2.9320417512f;
            tp2 = 2.9657284993f;
        } else if (freq <= 104 && freq >= 95) { //100
            ugain = 1217122.860f;
            tp0 = 0.9630021159f;
            tp1 = -2.9253101348f;
            tp2 = 2.9623014461f;
        } else if (freq <= 94 && freq >= 85) { //90
            ugain = 889124.3983f;
            tp0 = 0.9589765397f;
            tp1 = -2.9170984005f;
            tp2 = 2.9581128632f;
        } else if (freq <= 84 && freq >= 75) { //80
            ugain = 626079.3215f;
            tp0 = 0.9539681632f;
            tp1 = -2.9068581408f;
            tp2 = 2.9528771997f;
        } else if (freq <= 74 && freq >= 65) { //70
            ugain = 420820.6222f;
            tp0 = 0.9475671238f;
            tp1 = -2.8937318862f;
            tp2 = 2.9461457520f;
        } else if (freq <= 64 && freq >= 55) { //60
            ugain = 266181.2926f;
            tp0 = 0.9390989403f;
            tp1 = -2.8762997235f;
            tp2 = 2.9371707284f;
        } else if (freq <= 54 && freq >= 45) {  //50
            ugain = 154994.3249f;
            tp0 = 0.9273699683f;
            tp1 = -2.8520278186f;
            tp2 = 2.9246062355f;
        } else if (freq <= 44 && freq >= 35) { //40
            ugain = 80092.71123f;
            tp0 = 0.9100493001f;
            tp1 = -2.8159101079f;
            tp2 = 2.9057609235f;
        } else if (freq <= 34 && freq >= 28) { //30
            ugain = 34309.44333f;
            tp0 = 0.8818931306f;
            tp1 = -2.7564831952f;
            tp2 = 2.8743568927f;
        } else if (freq <= 27 && freq >= 23) { //25
            ugain = 20097.49869f;
            tp0 = 0.8599919781f;
            tp1 = -2.7096291328f;
            tp2 = 2.8492390952f;
        } else if (freq <= 22 && freq >= 15) { //20
            ugain = 10477.51171f;
            tp0 = 0.8281462754f;
            tp1 = -2.6404834928f;
            tp2 = 2.8115736773f;
        } else if (freq <= 14) { //10
            ugain = 1429.899908f;
            tp0 = 0.6855359773f;
            tp1 = -2.3146825811f;
            tp2 = 2.6235518066f;
        }

        // Set values for specific sensor
        if (sensor == 0) {
            //  Accelerometer
            frequency = freq;
            ugainA = ugain;
            tpA[0] = tp0;
            tpA[1] = tp1;
            tpA[2] = tp2;
        } else if (sensor == 1) {
            // Magnetic Field
            // here not: frequency = freq; otherwise value is wrong for step detection
            //that value has to be specified by accelerometer
            ugainM = ugain;
            tpM[0] = tp0;
            tpM[1] = tp1;
            tpM[2] = tp2;
        }
    }

    public  void shutdown(Context mContext) {
        pauseSensors();
    }

    @Override
    public  void onSensorChanged(SensorEvent event) {

        if (event.accuracy == SensorManager.SENSOR_STATUS_NO_CONTACT || event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE || event.accuracy == SensorManager.SENSOR_STATUS_ACCURACY_LOW) {
            return;
        }
        switch (event.sensor.getType()) {

            case Sensor.TYPE_PRESSURE:
                float presure = event.values[0];
                pressAltitude = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, presure);
                if(mAltitudeChangedListener != null){
                    mAltitudeChangedListener.onAltitudeChanged(pressAltitude);
                }
                break;

            case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                float[] unvalues = event.values.clone();
                imbaUnCalibratedMagnetic(unvalues);
                magnUnits++;
                break;

            case Sensor.TYPE_MAGNETIC_FIELD:

                float[] values = event.values.clone();
                imbaMagnetic(values);

                magnUnits++;
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                float newvals[] = event.values.clone();
                float vals[] = newvals;

                if(axis == 2){
                    //do nothing
                }else if(axis ==1){
                    //
                    vals[2] = newvals[1];
                    vals[1] = newvals[2];
                }else{
                    vals[2] = newvals[0];
                    vals[0] = newvals[1];
                    vals[1] = newvals[2];
                }
//                float x = vals[0];
//                float y = vals[1];
//                float z = vals[2];
//                mAccelLast = mAccelCurrent;
//                mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
//                float delta = mAccelCurrent - mAccelLast;
//                mAccel = mAccel * 0.9f + delta;
//                if (mAccel > 12) {
//                    Toast.makeText(context, "Shake event detected", Toast.LENGTH_SHORT).show();
//                    shake = true;
//
//                    vals[2] = 0;
//                    vals[0] = 0;
//                    vals[1] = 0;
//
//                    linear[2] = 0;
//                    linear[0] = 0;
//                    linear[1] = 0;
//
//                }else{
//                    shake = false;
//                }
                if(prevAxis != axis){
                    linear = vals;
                }

                prevAxis = axis;
                imbaLinear(vals);
                break;
            case Sensor.TYPE_ACCELEROMETER:

                long currTime = System.nanoTime();
                long time = System.nanoTime() - startTime;
                aclUnits++;
                units++;
                //shake detection working code ignore these values when shake detected
//                    if(timePassed > 100 * 1000000){
//                        lasttime = currTime;
//                        float[] values_new = event.values.clone();
//                        float x = values_new[0];
//                        float y = values_new[1];
//                        float z = values_new[2];
//                        timePassed /= 1000000;
//                        float speed = Math.abs(x+y+z - last_x - last_y - last_z) / timePassed * 10000;
//
//                        if (speed > SHAKE_THRESHOLD) {
//                            Log.d("sensor", "shake detected w/ speed: " + speed);
//                            Toast.makeText((Context) stepUpdateListener, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
//                        }
//                        last_x = x;
//                        last_y = y;
//                        last_z = z;
//                    }
                float[] values_new = event.values.clone();



                if (time >= 2000000000) { // every 2sek
                    changeDelay(aclUnits / 2, 0);
                    changeDelay(magnUnits / 2, 1);

                    startTime = System.nanoTime();
                    aclUnits = magnUnits = 0;
                }
                calculateVerticalAxis(event.values.clone());
                imbaGravity(event.values.clone());
                //imbaLinear(event.values.clone());

                if (isPositionSet) {
                    calculateAzimuth();
                    stepDetection();
                }




                break;

            case Sensor.TYPE_GYROSCOPE:
//                if(shake){
//                    return;
//                }
                if(gyroTime == 0){
                    gyroTime = System.nanoTime();
                    return;
                };
                double axiazi = 0;
                //int axis = getVerticalAxis();
                if(axis == 2){
                    axiazi = event.values[2];
                }else if(axis == 1){
                    axiazi = event.values[1];
                }else{
                    axiazi = event.values[0];
                }

                long diff    = System.nanoTime() - gyroTime;
                double zdiff = -1* axiazi * diff * Math.pow(10, -9);
                currVal += Math.toDegrees(zdiff);
                if(currVal > 360){
                    currVal %= 360;
                }else if(currVal < 0){
                    currVal += 360;
                }
                gyroTime = System.nanoTime();

        }
    }

    private void calculateVerticalAxis(float[] clone) {
        double x = Math.abs(1.0/clone[0]);
        double y = Math.abs(1.0/clone[1]);
        double z = Math.abs(1.0/clone[2]);

        if(x <= z){
            if(x <= y){
                axis = 0;
            }else{
                axis = 1;
            }
        }else if(x <= y){
            if(x <= z){
                axis = 0;
            }else{
                axis = 2;
            }
        }else{
            if(y <= z){
                axis = 1;
            }else {
                axis = 2;
            }
        }
    }



    @Override
    public  void onAccuracyChanged(Sensor sensor, int accuracy) {
        if(sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
            if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_HIGH){
                isAccurate = true;
            }else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM){
                isAccurate = true;
            }else {
                isAccurate = false;
            }
        }
    }

    @Override
    public void onFlushCompleted(Sensor sensor) {

    }

    public interface onStepUpdateListener {
        void onStepUpdate(int event, double turnDegrees, double heading, double gyroTurn, float values[], int axis);
    }
    public interface onCalibratedMagneticUpdateListener{
        void onCalibratedMagneticUpdate(float values[], double magnitude);
    }

    public interface onUncalibratedMagneticUpdateListener{
        void onUncalibratedMagneticUpdate(float coordvalues[], float deviceValues[]);
    }

    public interface onAltitudeChangedListener{
        void onAltitudeChanged(float altitude);
    }

    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) return input;
        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA_NOISE * (input[i] - output[i]);
        }
        return output;
    }






}