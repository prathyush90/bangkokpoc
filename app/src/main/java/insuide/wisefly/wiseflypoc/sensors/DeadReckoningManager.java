package insuide.wisefly.wiseflypoc.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import androidx.fragment.app.Fragment;

import static android.content.Context.SENSOR_SERVICE;

public class DeadReckoningManager {
    private final onStepUpdateNotifier stepUpdateListener;
    private Context context;
    private SensorManager sensorManager;
    private Sensor gyroscopeSensor;
    private Sensor linearAccelerationSensor;
    private Sensor accelerationSensor;
    private SensorEventListener gyroscopeSensorListener;
    private SensorEventListener linearAccelSensorListener;
    private SensorEventListener accelSensorListener;
    private int axis = 2;
    private boolean initDone;
    private int aclUnits = 0;
    private static long startTime;
    private int frequency;
    private float linear[] = new float[3];
    private float ALPHA_NOISE = 0.5f;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    private long mShakeTimestamp;
    private static float stepThreshold = 1.0f;
    private double MAX_MAG_ACCEL = 0.75;
    LinkedList<Float> mAccelHistory;
    private static final int PEAK_HUNT = 0;
    private static final int VALLEY_HUNT = 1;
    private int mState;
    private long prevSteptimestamp;
    private static final int MAX_HISTORY_SIZE = 10;
    private ArrayList<Float>angularVelocity = new ArrayList<Float>();
    private double pitchChange = 0;
    private double rollChange = 0;
    private double yawChange = 0;

    private double previousPitchChange = 0;
    private double previousRollChange = 0;
    private double previousYawChange = 0;
    long gyroTime = 0;
    private Object obj = new Object();
    int movementAxis = 1;
    private float valleyValue = 0;
    private long prevStepTime = 0l;
    public DeadReckoningManager(Fragment context, Context cont) {
        this.context = cont;
        if (context instanceof onStepUpdateNotifier) {
            stepUpdateListener = (onStepUpdateNotifier) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mAccelHistory = new LinkedList<>();
        initSensors();
        setListeners();
    }

    private void initSensors() {
        sensorManager =
                (SensorManager) this.context.getSystemService(SENSOR_SERVICE);
        gyroscopeSensor =
                sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        linearAccelerationSensor =
                sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        accelerationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    private void setListeners(){
        gyroscopeSensorListener = new SensorEventListener() {

            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(!initDone){
                    return;
                }
                if(gyroTime == 0){
                    gyroTime = System.nanoTime();
                    return;
                };

                float deltaTime = (System.nanoTime() - gyroTime)*0.000000001f;
                float changeOrientation[] = new float[4];

                changeOrientation[0] = (sensorEvent.values.clone()[0]*deltaTime)*57.2957795131f;
                changeOrientation[1] = (sensorEvent.values.clone()[1]*deltaTime)*57.2957795131f;
                changeOrientation[2] = (sensorEvent.values.clone()[2]*deltaTime)*57.2957795131f;

                if(axis == 2){
                    //zaxis vertical
                    pitchChange -= changeOrientation[0];
                    rollChange  -= changeOrientation[1];
                    yawChange   -= changeOrientation[2];
                    angularVelocity.add(sensorEvent.values.clone()[2]);
                }else if(axis ==1){
                    pitchChange -= changeOrientation[0];
                    rollChange  += changeOrientation[2];
                    yawChange   -=  changeOrientation[1];
                    angularVelocity.add(sensorEvent.values.clone()[1]);
                }else{
                    pitchChange -= changeOrientation[1];
                    rollChange  += changeOrientation[2];
                    yawChange   += changeOrientation[0];
                    angularVelocity.add(sensorEvent.values.clone()[0]);
                }
                if(angularVelocity.size() >25){
                    angularVelocity.remove(0);
                }

                //sendOrientationChange();
                gyroTime = System.nanoTime();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        accelSensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                calculateVerticalAxis(event.values.clone());
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        linearAccelSensorListener = new SensorEventListener(){
            @Override
            public void onSensorChanged(SensorEvent event) {
                if(!initDone){
                    return;
                }

                aclUnits++;

                long time = System.nanoTime() - startTime;
                if (time >= 2000000000) { // every 2sek
                    changeDelay(aclUnits / 2);
                    startTime = System.nanoTime();
                    aclUnits = 0;
                }
                float previousLinear[] = linear.clone();

                linear = lowPass(event.values.clone(), linear);
                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt((double) (linear[0]*linear[0] + linear[1]*linear[1] + linear[2]*linear[2]));
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta;
                if (mAccel > 3.0) {
                    mShakeTimestamp = System.currentTimeMillis();
                    //sendOrientationChange();
                    return;
                    //Toast.makeText(getApplicationContext(), "Shake event detected", Toast.LENGTH_SHORT).show();
                }
                long shakediff = System.currentTimeMillis() - mShakeTimestamp;
                if(shakediff < 500){
                    //sendOrientationChange();
                    return;
                }


                findStepAndDirectionVector();

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    };


    private void findStepAndDirectionVector() {

        boolean isDetected = false;
        if(axis == 2){
            stepThreshold = 0.75f;
            final double linearAccel = Math.sqrt(Math.pow(linear[0], 2)+Math.pow(linear[1], 2) );

            if(linearAccel < MAX_MAG_ACCEL){
                return;
            }
            isDetected = stepDetectionPeak(linear[2]);
        }else if(axis ==1){
            stepThreshold = 0.75f;
            final double linearAccel = Math.sqrt(Math.pow(linear[0], 2)+ Math.pow(linear[2], 2));

            if(linearAccel < MAX_MAG_ACCEL){
                return;
            }
            isDetected = stepDetectionPeak(linear[1]);
        }else{
            //gravity vector along x
            stepThreshold = 0.75f;
            final double linearAccel = Math.sqrt(Math.pow(linear[1], 2) + Math.pow(linear[2], 2));
            if(linearAccel < MAX_MAG_ACCEL){
                return;
            }
            isDetected = stepDetectionPeak(linear[0]);
        }
        if(!isDetected){
            sendOrientationChange();
        }
    }






    private void changeDelay(int freq) {
        frequency = freq;
    }

    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) return input;
        for ( int i=0; i<input.length; i++ ) {
            output[i] = ALPHA_NOISE*output[i] + (1-ALPHA_NOISE)*input[i];
        }
        return output;
    }

    private void registerListeners(){
        sensorManager.registerListener(gyroscopeSensorListener,
                gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(linearAccelSensorListener,
                linearAccelerationSensor, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(accelSensorListener,
                accelerationSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void registerForDeadReckoning(){
        registerListeners();
        initDone = true;

        mAccelHistory.add(0f);
        mAccelHistory.add(0f);
    }

    private void unRegisterListeners(){
        sensorManager.unregisterListener(gyroscopeSensorListener);
        sensorManager.unregisterListener(linearAccelSensorListener);
        sensorManager.unregisterListener(accelSensorListener);
    }

    public void unRegisterFromDeadReckoning(){
        unRegisterListeners();
        mAccelHistory.clear();
    }

    public interface onStepUpdateNotifier{
        void onStepUpdate(int event, double pitchChange, double rollChange, double yawChange, int axis);
    }

    private void calculateVerticalAxis(float[] clone) {
        double x = Math.abs(1.0/clone[0]);
        double y = Math.abs(1.0/clone[1]);
        double z = Math.abs(1.0/clone[2]);

        if(x <= z){
            if(x <= y){
                axis = 0;
            }else{
                axis = 1;
            }
        }else if(x <= y){
            if(x <= z){
                axis = 0;
            }else{
                axis = 2;
            }
        }else{
            if(y <= z){
                axis = 1;
            }else {
                axis = 2;
            }
        }
    }

    private float calculateAverage(List<Float> marks) {
        float sum = 0;
        if(!marks.isEmpty()) {
            for (Float mark : marks) {
                sum += mark;
            }
            return sum/ marks.size();
        }
        return sum;
    }

    public boolean stepDetectionPeak(float value){
        boolean isDetected = false;

            synchronized(mAccelHistory) {
                ArrayList<Float> anVel = (ArrayList<Float>) angularVelocity.clone();

                float avgAngularVel = calculateAverage(anVel);
                long timestamp = System.nanoTime();
                if(prevSteptimestamp == 0){
                    prevSteptimestamp = timestamp;
                }
                float s0 = mAccelHistory.get(mAccelHistory.size()-2), s1 = mAccelHistory.get(mAccelHistory.size()-1), s2 = value;
                // Count peaks and valleys

                if((s2 - s1)*(s1 - s0) < 0) {
                    if(s2 - s1 < 0 && s1 > 0) { // Peak Found
                        if(mState == PEAK_HUNT) {
                            long time = System.nanoTime() - prevSteptimestamp;
                            if(time < 350 *1000000){
                                //time difference too low for a valley and peak
                                //and ignore the current val
                                return false;
                            }
                            else if(time > 1500 * 1000000){
                                //time difference too big so change status to valley hunt and consider this false step
                                mState = VALLEY_HUNT;
                                return false;
                            }
                            double peakHeight = Math.abs(s2-valleyValue);
                            if(peakHeight < stepThreshold){
                                //too low value to consider peak
                                return false;
                            }
                            if(Math.abs(avgAngularVel) > 1.1f){
                                //remove this step. Formed from too quick movement of the phone
                                mState = VALLEY_HUNT;

                                return false;
                            }


                            isDetected = true;
                            newStep();
                            prevStepTime = System.nanoTime();


                            mState = VALLEY_HUNT;
                        }

                    } else if (s2 - s1 > 0 && s1 < 0) { // Valley Found
                        if(Math.abs(avgAngularVel) > 1.1f){
                            //false valley
                            return false;
                        }
                        valleyValue = s2;
                        if(mState == VALLEY_HUNT) {
                            mState = PEAK_HUNT;
                        }
                        prevSteptimestamp = System.nanoTime();
                    }
                }
                mAccelHistory.add(value);
                if(mAccelHistory.size() > MAX_HISTORY_SIZE) {
                    mAccelHistory.removeFirst();
                }
            }

//        if(!isDetected){
//            sendOrientationChange();
//        }
        return isDetected;
    }

    private void sendOrientationChange() {
        //synchronized (obj) {
            //send euler angles here. Event 0
            if (Math.abs(previousYawChange - yawChange) >= 10) {
                double pChange = pitchChange - previousPitchChange;
                double rChange = rollChange - previousRollChange;
                double yChange = yawChange - previousYawChange;
                stepUpdateListener.onStepUpdate(0, pChange, rChange, yChange, movementAxis);
                previousPitchChange = pitchChange;
                previousRollChange = rollChange;
                previousYawChange = yawChange;
            }
        //}

    }

    private void newStep() {
        //synchronized (obj) {
            double directVector[] = new double[3];
            if (axis == 2) {
                movementAxis = 1;
                directVector[1] = 0.65;
            } else if (axis == 1) {
                movementAxis = -2;
                directVector[2] = -0.65;
            } else {
                movementAxis = -2;
                directVector[2] = -0.65;
            }


            //send step detection here. Event 1
            double pChange = pitchChange - previousPitchChange;
            double rChange = rollChange - previousRollChange;
            double yChange = yawChange - previousYawChange;
            stepUpdateListener.onStepUpdate(1, pChange, rChange, yChange, movementAxis);
            previousPitchChange = pitchChange;
            previousRollChange = rollChange;
            previousYawChange = yawChange;
        }
    //}

    public  void shutdown(Context mContext) {
        unRegisterFromDeadReckoning();
    }
    public  void startSensors(boolean initialStep) {
        registerForDeadReckoning();
    }
}
