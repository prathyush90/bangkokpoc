package insuide.wisefly.wiseflypoc.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FloorPlanPojo  implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("actual")
    @Expose
    private Integer actual;
    @SerializedName("maxzoom")
    @Expose
    private Integer maxzoom;
    @SerializedName("lockpoints")
    @Expose
    private List<List<String>> lockpoints = null;
    @SerializedName("minzoom")
    @Expose
    private Integer minzoom;
    @SerializedName("sliceid")
    @Expose
    private String sliceid;
    @SerializedName("slicename")
    @Expose
    private String slicename;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("floorplan_id")
    @Expose
    private String floorplanId;
    @SerializedName("ia_secret_key")
    @Expose
    private String iaSecretKey;
    @SerializedName("ia_key")
    @Expose
    private String iaKey;
    @SerializedName("mapboxurl")
    @Expose
    private String mapboxurl;
    @SerializedName("base_url")
    @Expose
    private String baseUrl;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("appname")
    @Expose
    private String appname;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("lat")
    @Expose
    private Double lat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getActual() {
        return actual;
    }

    public void setActual(Integer actual) {
        this.actual = actual;
    }

    public Integer getMaxzoom() {
        return maxzoom;
    }

    public void setMaxzoom(Integer maxzoom) {
        this.maxzoom = maxzoom;
    }

    public List<List<String>> getLockpoints() {
        return lockpoints;
    }

    public void setLockpoints(List<List<String>> lockpoints) {
        this.lockpoints = lockpoints;
    }

    public Integer getMinzoom() {
        return minzoom;
    }

    public void setMinzoom(Integer minzoom) {
        this.minzoom = minzoom;
    }

    public String getSliceid() {
        return sliceid;
    }

    public void setSliceid(String sliceid) {
        this.sliceid = sliceid;
    }

    public String getSlicename() {
        return slicename;
    }

    public void setSlicename(String slicename) {
        this.slicename = slicename;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFloorplanId() {
        return floorplanId;
    }

    public void setFloorplanId(String floorplanId) {
        this.floorplanId = floorplanId;
    }

    public String getIaSecretKey() {
        return iaSecretKey;
    }

    public void setIaSecretKey(String iaSecretKey) {
        this.iaSecretKey = iaSecretKey;
    }

    public String getIaKey() {
        return iaKey;
    }

    public void setIaKey(String iaKey) {
        this.iaKey = iaKey;
    }

    public String getMapboxurl() {
        return mapboxurl;
    }

    public void setMapboxurl(String mapboxurl) {
        this.mapboxurl = mapboxurl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeValue(this.v);
        dest.writeString(this.level);
        dest.writeValue(this.actual);
        dest.writeValue(this.maxzoom);
        dest.writeList(this.lockpoints);
        dest.writeValue(this.minzoom);
        dest.writeString(this.sliceid);
        dest.writeString(this.slicename);
        dest.writeString(this.userid);
        dest.writeString(this.floorplanId);
        dest.writeString(this.iaSecretKey);
        dest.writeString(this.iaKey);
        dest.writeString(this.mapboxurl);
        dest.writeString(this.baseUrl);
        dest.writeString(this.url);
        dest.writeString(this.terminal);
        dest.writeString(this.appname);
        dest.writeValue(this.lng);
        dest.writeValue(this.lat);
    }

    public FloorPlanPojo() {
    }

    protected FloorPlanPojo(Parcel in) {
        this.id = in.readString();
        this.v = (Integer) in.readValue(Integer.class.getClassLoader());
        this.level = in.readString();
        this.actual = (Integer) in.readValue(Integer.class.getClassLoader());
        this.maxzoom = (Integer) in.readValue(Integer.class.getClassLoader());
        this.lockpoints = new ArrayList<>();
        in.readList(this.lockpoints, String.class.getClassLoader());
        this.minzoom = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sliceid = in.readString();
        this.slicename = in.readString();
        this.userid = in.readString();
        this.floorplanId = in.readString();
        this.iaSecretKey = in.readString();
        this.iaKey = in.readString();
        this.mapboxurl = in.readString();
        this.baseUrl = in.readString();
        this.url = in.readString();
        this.terminal = in.readString();
        this.appname = in.readString();
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<FloorPlanPojo> CREATOR = new Creator<FloorPlanPojo>() {
        @Override
        public FloorPlanPojo createFromParcel(Parcel source) {
            return new FloorPlanPojo(source);
        }

        @Override
        public FloorPlanPojo[] newArray(int size) {
            return new FloorPlanPojo[size];
        }
    };
}
