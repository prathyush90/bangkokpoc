package insuide.wisefly.wiseflypoc.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BeaconsPojo implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("geo")
    @Expose
    private List<Double> geo = null;
    @SerializedName("nodes")
    @Expose
    private List<Object> nodes = null;
    @SerializedName("battery")
    @Expose
    private Integer battery;
    @SerializedName("insertedby")
    @Expose
    private String insertedby;
    @SerializedName("venue")
    @Expose
    private String venue;
    @SerializedName("appname")
    @Expose
    private String appname;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("txPower")
    @Expose
    private Integer txPower;
    @SerializedName("major")
    @Expose
    private String major;
    @SerializedName("minor")
    @Expose
    private String minor;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("sliceid")
    @Expose
    private String sliceid;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("proximity")
    @Expose
    private String proximity;
    @SerializedName("uniqueId")
    @Expose
    private String uniqueId;
    @SerializedName("namespace")
    @Expose
    private String namespace;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("userlng")
    @Expose
    private Integer userlng;
    @SerializedName("userlat")
    @Expose
    private Integer userlat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("interval")
    @Expose
    private Integer interval;
    @SerializedName("instanceId")
    @Expose
    private String instanceId;
    @SerializedName("firmware")
    @Expose
    private String firmware;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("enabled")
    @Expose
    private boolean enabled = false;
    @SerializedName("alias")
    @Expose
    private Object alias;
    @SerializedName("access")
    @Expose
    private String access;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Double> getGeo() {
        return geo;
    }

    public void setGeo(List<Double> geo) {
        this.geo = geo;
    }

    public List<Object> getNodes() {
        return nodes;
    }

    public void setNodes(List<Object> nodes) {
        this.nodes = nodes;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public String getInsertedby() {
        return insertedby;
    }

    public void setInsertedby(String insertedby) {
        this.insertedby = insertedby;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTxPower() {
        return txPower;
    }

    public void setTxPower(Integer txPower) {
        this.txPower = txPower;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getSliceid() {
        return sliceid;
    }

    public void setSliceid(String sliceid) {
        this.sliceid = sliceid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProximity() {
        return proximity;
    }

    public void setProximity(String proximity) {
        this.proximity = proximity;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserlng() {
        return userlng;
    }

    public void setUserlng(Integer userlng) {
        this.userlng = userlng;
    }

    public Integer getUserlat() {
        return userlat;
    }

    public void setUserlat(Integer userlat) {
        this.userlat = userlat;
    }

    public double getLng() {
        if(lng == null){
            lng = "0";
        }
        return Double.parseDouble(lng);
    }

    public void setLng(String lng) {
        if (lng == null) {
            lng = "0";
        }
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Object getAlias() {
        return alias;
    }

    public void setAlias(Object alias) {
        this.alias = alias;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    protected BeaconsPojo(Parcel in) {
        id = in.readString();
        if (in.readByte() == 0) {
            battery = null;
        } else {
            battery = in.readInt();
        }
        insertedby = in.readString();
        venue = in.readString();
        appname = in.readString();
        status = in.readString();
        if (in.readByte() == 0) {
            txPower = null;
        } else {
            txPower = in.readInt();
        }
        major = in.readString();
        minor = in.readString();
        terminal = in.readString();
        proximity = in.readString();
        uniqueId = in.readString();
        namespace = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            userlng = null;
        } else {
            userlng = in.readInt();
        }
        if (in.readByte() == 0) {
            userlat = null;
        } else {
            userlat = in.readInt();
        }
        if (in.readByte() == 0) {
            lng = null;
        } else {
            lng = in.readString();
        }
        if (in.readByte() == 0) {
            lat = null;
        } else {
            lat = in.readDouble();
        }
        if (in.readByte() == 0) {
            interval = null;
        } else {
            interval = in.readInt();
        }
        instanceId = in.readString();
        firmware = in.readString();
        deviceType = in.readString();
        byte tmpEnabled = in.readByte();
        enabled = tmpEnabled == 0 ? null : tmpEnabled == 1;
        access = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        if (battery == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(battery);
        }
        dest.writeString(insertedby);
        dest.writeString(venue);
        dest.writeString(appname);
        dest.writeString(status);
        if (txPower == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(txPower);
        }
        dest.writeString(major);
        dest.writeString(minor);
        dest.writeString(terminal);
        dest.writeString(proximity);
        dest.writeString(uniqueId);
        dest.writeString(namespace);
        dest.writeString(name);
        if (userlng == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userlng);
        }
        if (userlat == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userlat);
        }
        if (lng == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Double.parseDouble(lng));
        }
        if (lat == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(lat);
        }
        if (interval == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(interval);
        }
        dest.writeString(instanceId);
        dest.writeString(firmware);
        dest.writeString(deviceType);
        dest.writeByte((byte) (enabled  ? 0 : enabled ? 1 : 2));
        dest.writeString(access);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BeaconsPojo> CREATOR = new Creator<BeaconsPojo>() {
        @Override
        public BeaconsPojo createFromParcel(Parcel in) {
            return new BeaconsPojo(in);
        }

        @Override
        public BeaconsPojo[] newArray(int size) {
            return new BeaconsPojo[size];
        }
    };

}
