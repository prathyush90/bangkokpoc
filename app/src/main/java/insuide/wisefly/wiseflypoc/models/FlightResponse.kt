package com.netpiper.sia.model

import insuide.wisefly.wiseflypoc.models.PoiDetail

/**
 * Created by Furqan on 18-01-2018.
 */
data class FlightResponse(
        val success: Boolean,
        val status: Int,
        val message: String,
        val flights: List<Object>,
        val data: List<PoiDetail>
)