package insuide.wisefly.wiseflypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VenueAndAssetsPojo {


    @SerializedName("success")
    @Expose

    private Boolean success;
    @SerializedName("beaconsdata")
    @Expose
    private ArrayList<BeaconsPojo> bData;
    @SerializedName("message")
    @Expose
    private String error;

    public ArrayList<GeoFencePojo> getGeofenceData() {
        return geofenceData;
    }

    public void setGeofenceData(ArrayList<GeoFencePojo> geofenceData) {
        this.geofenceData = geofenceData;
    }

    @SerializedName("floorplandata")
    @Expose
    private ArrayList<FloorPlanPojo> fData;

    @SerializedName("geofence")
    @Expose
    private ArrayList<GeoFencePojo> geofenceData;



    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<BeaconsPojo> getbData() {
        return bData;
    }

    public void setbData(ArrayList<BeaconsPojo> bData) {
        this.bData = bData;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<FloorPlanPojo> getfData() {
        return fData;
    }

    public void setfData(ArrayList<FloorPlanPojo> fData) {
        this.fData = fData;
    }

}
