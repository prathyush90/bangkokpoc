package insuide.wisefly.wiseflypoc.models;

/**
 * Created by prathyush on 01/07/17.
 */
public class FilteredBeacon {
    double rssi;
    int txPower;
    double coveriError;
    double kalmanDistance;

    public double getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(double lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    double lastUpdateTime;

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    double kalmanRssi;
    int major;
    int minor;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    double distance;

    public double getKalmanDistance() {
        return kalmanDistance;
    }

    public void setKalmanDistance(double kalmanDistance) {
        this.kalmanDistance = kalmanDistance;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getTxPower() {
        return txPower;
    }

    public void setTxPower(int txPower) {
        this.txPower = txPower;
    }

    public double getRssi() {
        return rssi;
    }

    public void setRssi(double rssi) {
        this.rssi = rssi;
    }

    public double getKalmanRssi() {
        return kalmanRssi;
    }

    public void setKalmanRssi(double kalmanRssi) {
        this.kalmanRssi = kalmanRssi;
    }

    String uniqueId;


    @Override
    public String toString() {
        return "Unique Id " + uniqueId + "," + coveriError + "";
    }

    public double getCoveriError() {
        return coveriError;
    }

    public void setCoveriError(double coveriError) {
        this.coveriError = coveriError;
    }
}
