package com.netpiper.sia.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import java.io.Serializable

/**
 * Created by Furqan on 26-06-2018.
 */
data class PoiMultilingual(
        val ar: ShjLocal?,
        val en: ShjLocal,
        val ru: ShjLocal?,
        val hi: ShjLocal?
) : Serializable, Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readParcelable(ShjLocal::class.java.classLoader),
            parcel.readParcelable(ShjLocal::class.java.classLoader)!!,
            parcel.readParcelable(ShjLocal::class.java.classLoader),
            parcel.readParcelable(ShjLocal::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(ar, flags)
        parcel.writeParcelable(en, flags)
        parcel.writeParcelable(ru, flags)
        parcel.writeParcelable(hi, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PoiMultilingual> {
        override fun createFromParcel(parcel: Parcel): PoiMultilingual {
            return PoiMultilingual(parcel)
        }

        override fun newArray(size: Int): Array<PoiMultilingual?> {
            return arrayOfNulls(size)
        }
    }
}

data class ShjLocal(
        @Json(name = "location_text")
        var locationText: String? = null,
        var description: String? = null,
        var name: String? = null,
        var title: String? = null,
        var desc: String? = null
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(locationText)
        parcel.writeString(description)
        parcel.writeString(name)
        parcel.writeString(title)
        parcel.writeString(desc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ShjLocal> {
        override fun createFromParcel(parcel: Parcel): ShjLocal {
            return ShjLocal(parcel)
        }

        override fun newArray(size: Int): Array<ShjLocal?> {
            return arrayOfNulls(size)
        }
    }
}