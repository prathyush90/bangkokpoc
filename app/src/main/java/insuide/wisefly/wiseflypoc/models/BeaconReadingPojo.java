package insuide.wisefly.wiseflypoc.models;

public class BeaconReadingPojo {
    private String uniqueId;
    private double rssi;

    public BeaconReadingPojo(String uniqueId, double rssi) {
        this.uniqueId = uniqueId;
        this.rssi = rssi;
    }

    public double getRssi() {
        return rssi;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setRssi(double rssi) {
        this.rssi = rssi;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }
}
