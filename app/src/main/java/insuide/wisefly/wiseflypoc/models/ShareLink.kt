package com.netpiper.sia.model

/**
 * Created by Furqan on 06-06-2018.
 */
data class ShareLink(
        val success: Boolean,
        val url: String)