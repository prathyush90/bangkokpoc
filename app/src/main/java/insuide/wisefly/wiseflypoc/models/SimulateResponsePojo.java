package insuide.wisefly.wiseflypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SimulateResponsePojo {
   @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("weights")
        @Expose
        private List<Double> weights = null;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public List<Double> getWeights() {
            return weights;
        }

        public void setWeights(List<Double> weights) {
            this.weights = weights;
        }


}
