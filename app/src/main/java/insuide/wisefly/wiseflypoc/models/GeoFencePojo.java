package insuide.wisefly.wiseflypoc.models;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GeoFencePojo {

    @SerializedName("_id")
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSlice() {
        return slice;
    }

    public void setSlice(String slice) {
        this.slice = slice;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public ArrayList<ArrayList<ArrayList<Double>>> getGeofence() {
        return geofence;
    }

    public void setGeofence(ArrayList<ArrayList<ArrayList<Double>>> geofence) {
        this.geofence = geofence;
    }

    @SerializedName("appname")
    @Expose
    private String appname;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("slice")
    @Expose
    private String slice;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("geofence")
    @Expose
    private ArrayList<ArrayList<ArrayList<Double>>> geofence = null;
}
