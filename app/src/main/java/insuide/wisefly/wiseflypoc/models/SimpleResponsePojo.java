package insuide.wisefly.wiseflypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SimpleResponsePojo {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("message")
    private String error;

    @SerializedName("data")
    private ArrayList<MappingPointPojo> data;

    @SerializedName("point")
    private MappingPointPojo point;

    public MappingPointPojo getPoint() {
        return point;
    }

    public void setPoint(MappingPointPojo point) {
        this.point = point;
    }

    public ArrayList<MappingPointPojo> getData() {
        return data;
    }

    public void setData(ArrayList<MappingPointPojo> data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @SerializedName("url")
    @Expose
    private String url;

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
