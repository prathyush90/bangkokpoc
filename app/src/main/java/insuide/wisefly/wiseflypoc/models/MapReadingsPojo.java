package insuide.wisefly.wiseflypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MapReadingsPojo {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("altitude")
    @Expose
    private double altitude;
    @SerializedName("has_wifi_reading")
    @Expose
    private boolean hasWifiReading;
    @SerializedName("wifi_std_rssi")
    @Expose
    private double wifiStdRssi;
    @SerializedName("wifi_mean_rssi")
    @Expose
    private double wifiMeanRssi;
    @SerializedName("wifi_unique")
    @Expose
    private ArrayList<String> wifiUnique = null;
    @SerializedName("has_beacon_reading")
    @Expose
    private boolean hasBeaconReading;
    @SerializedName("beacon_std_rssi")
    @Expose
    private double beaconStdRssi;
    @SerializedName("beacon_mean_rssi")
    @Expose
    private double beaconMeanRssi;
    @SerializedName("beacon_unique")
    @Expose
    private ArrayList<String> beaconUnique = null;
    @SerializedName("beacon_std_dist")
    @Expose
    private double beaconStdDist;
    @SerializedName("beacon_mean_dist")
    @Expose
    private double beaconMeanDist;
    @SerializedName("beacon_rssi_z")
    @Expose
    private double beaconRssiZ;
    @SerializedName("beacon_rssi_y")
    @Expose
    private double beaconRssiY;
    @SerializedName("beacon_rssi_x")
    @Expose
    private double beaconRssiX;
    @SerializedName("beacon_dist_z")
    @Expose
    private double beaconDistZ;
    @SerializedName("beacon_dist_y")
    @Expose
    private double beaconDistY;
    @SerializedName("beacon_dist_x")
    @Expose
    private double beaconDistX;
    @SerializedName("mag_z")
    @Expose
    private double magZ;
    @SerializedName("mag_y")
    @Expose
    private double magY;
    @SerializedName("mag_x")
    @Expose
    private double magX;
    @SerializedName("posz")
    @Expose
    private double posz;
    @SerializedName("posy")
    @Expose
    private double posy;
    @SerializedName("posx")
    @Expose
    private double posx;
    @SerializedName("milliseconds")
    @Expose
    private long milliseconds;
    @SerializedName("lng")
    @Expose
    private double lng;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("sliceid")
    @Expose
    private String sliceid;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("appname")
    @Expose
    private String appname;
    @SerializedName("__v")
    @Expose
    private int v;
    @SerializedName("beacon_data")
    @Expose
    private String beacon_data ="[]";
    @SerializedName("wifi_data")
    @Expose
    private String wifi_data="[]";

    @SerializedName("magnitude_mean")
    @Expose
    private double magnitude_mean;
    @SerializedName("magnitude_std")
    @Expose
    private double magnitude_std;

    @SerializedName("mag_x_mean")
    @Expose
    private double mag_x_mean;
    @SerializedName("mag_x_std")
    @Expose
    private double mag_x_std;

    @SerializedName("mag_y_mean")
    @Expose
    private double mag_y_mean;
    @SerializedName("mag_y_std")
    @Expose
    private double mag_y_std;

    @SerializedName("mag_z_mean")
    @Expose
    private double mag_z_mean;
    @SerializedName("mag_z_std")
    @Expose
    private double mag_z_std;

    @SerializedName("beacon_dist_x_mean")
    @Expose
    private double beacon_dist_x_mean;
    @SerializedName("beacon_dist_x_std")
    @Expose
    private double beacon_dist_x_std;

    @SerializedName("beacon_dist_y_mean")
    @Expose
    private double beacon_dist_y_mean;
    @SerializedName("beacon_dist_y_std")
    @Expose
    private double beacon_dist_y_std;

    @SerializedName("orientation")
    @Expose
    private double orientationDegrees;

    public double getMagnitude_mean() {
        return magnitude_mean;
    }

    public void setMagnitude_mean(double magnitude_mean) {
        this.magnitude_mean = magnitude_mean;
    }

    public double getMagnitude_std() {
        return magnitude_std;
    }

    public void setMagnitude_std(double magnitude_std) {
        this.magnitude_std = magnitude_std;
    }

    public double getMag_x_mean() {
        return mag_x_mean;
    }

    public void setMag_x_mean(double mag_x_mean) {
        this.mag_x_mean = mag_x_mean;
    }

    public double getMag_x_std() {
        return mag_x_std;
    }

    public void setMag_x_std(double mag_x_std) {
        this.mag_x_std = mag_x_std;
    }

    public double getMag_y_mean() {
        return mag_y_mean;
    }

    public void setMag_y_mean(double mag_y_mean) {
        this.mag_y_mean = mag_y_mean;
    }

    public double getMag_y_std() {
        return mag_y_std;
    }

    public void setMag_y_std(double mag_y_std) {
        this.mag_y_std = mag_y_std;
    }

    public double getMag_z_mean() {
        return mag_z_mean;
    }

    public void setMag_z_mean(double mag_z_mean) {
        this.mag_z_mean = mag_z_mean;
    }

    public double getMag_z_std() {
        return mag_z_std;
    }

    public void setMag_z_std(double mag_z_std) {
        this.mag_z_std = mag_z_std;
    }

    public double getBeacon_dist_x_mean() {
        return beacon_dist_x_mean;
    }

    public void setBeacon_dist_x_mean(double beacon_dist_x_mean) {
        this.beacon_dist_x_mean = beacon_dist_x_mean;
    }

    public double getBeacon_dist_x_std() {
        return beacon_dist_x_std;
    }

    public void setBeacon_dist_x_std(double beacon_dist_x_std) {
        this.beacon_dist_x_std = beacon_dist_x_std;
    }

    public double getBeacon_dist_y_mean() {
        return beacon_dist_y_mean;
    }

    public void setBeacon_dist_y_mean(double beacon_dist_y_mean) {
        this.beacon_dist_y_mean = beacon_dist_y_mean;
    }

    public double getBeacon_dist_y_std() {
        return beacon_dist_y_std;
    }

    public void setBeacon_dist_y_std(double beacon_dist_y_std) {
        this.beacon_dist_y_std = beacon_dist_y_std;
    }

    public double getBeacon_dist_z_mean() {
        return beacon_dist_z_mean;
    }

    public void setBeacon_dist_z_mean(double beacon_dist_z_mean) {
        this.beacon_dist_z_mean = beacon_dist_z_mean;
    }

    public double getBeacon_dist_z_std() {
        return beacon_dist_z_std;
    }

    public void setBeacon_dist_z_std(double beacon_dist_z_std) {
        this.beacon_dist_z_std = beacon_dist_z_std;
    }

    @SerializedName("beacon_dist_z_mean")
    @Expose
    private double beacon_dist_z_mean;
    @SerializedName("beacon_dist_z_std")
    @Expose
    private double beacon_dist_z_std;




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public boolean getHasWifiReading() {
        return hasWifiReading;
    }

    public void setHasWifiReading(boolean hasWifiReading) {
        this.hasWifiReading = hasWifiReading;
    }

    public double getWifiStdRssi() {
        return wifiStdRssi;
    }

    public void setWifiStdRssi(double wifiStdRssi) {
        this.wifiStdRssi = wifiStdRssi;
    }

    public double getWifiMeanRssi() {
        return wifiMeanRssi;
    }

    public void setWifiMeanRssi(double wifiMeanRssi) {
        this.wifiMeanRssi = wifiMeanRssi;
    }

    public ArrayList<String> getWifiUnique() {
        return wifiUnique;
    }

    public void setWifiUnique(ArrayList<String> wifiUnique) {
        this.wifiUnique = wifiUnique;
    }

    public boolean getHasBeaconReading() {
        return hasBeaconReading;
    }

    public void setHasBeaconReading(boolean hasBeaconReading) {
        this.hasBeaconReading = hasBeaconReading;
    }

    public double getBeaconStdRssi() {
        return beaconStdRssi;
    }

    public void setBeaconStdRssi(double beaconStdRssi) {
        this.beaconStdRssi = beaconStdRssi;
    }

    public double getBeaconMeanRssi() {
        return beaconMeanRssi;
    }

    public void setBeaconMeanRssi(int beaconMeanRssi) {
        this.beaconMeanRssi = beaconMeanRssi;
    }

    public ArrayList<String> getBeaconUnique() {
        return beaconUnique;
    }

    public void setBeaconUnique(ArrayList<String> beaconUnique) {
        this.beaconUnique = beaconUnique;
    }

    public double getBeaconStdDist() {
        return beaconStdDist;
    }

    public void setBeaconStdDist(int beaconStdDist) {
        this.beaconStdDist = beaconStdDist;
    }

    public double getBeaconMeanDist() {
        return beaconMeanDist;
    }

    public void setBeaconMeanDist(int beaconMeanDist) {
        this.beaconMeanDist = beaconMeanDist;
    }

    public double getBeaconRssiZ() {
        return beaconRssiZ;
    }

    public void setBeaconRssiZ(int beaconRssiZ) {
        this.beaconRssiZ = beaconRssiZ;
    }

    public double getBeaconRssiY() {
        return beaconRssiY;
    }

    public void setBeaconRssiY(int beaconRssiY) {
        this.beaconRssiY = beaconRssiY;
    }

    public double getBeaconRssiX() {
        return beaconRssiX;
    }

    public void setBeaconRssiX(int beaconRssiX) {
        this.beaconRssiX = beaconRssiX;
    }

    public double getBeaconDistZ() {
        return beaconDistZ;
    }

    public void setBeaconDistZ(int beaconDistZ) {
        this.beaconDistZ = beaconDistZ;
    }

    public double getBeaconDistY() {
        return beaconDistY;
    }

    public void setBeaconDistY(int beaconDistY) {
        this.beaconDistY = beaconDistY;
    }

    public double getBeaconDistX() {
        return beaconDistX;
    }

    public void setBeaconDistX(int beaconDistX) {
        this.beaconDistX = beaconDistX;
    }

    public double getMagZ() {
        return magZ;
    }

    public void setMagZ(double magZ) {
        this.magZ = magZ;
    }

    public double getMagY() {
        return magY;
    }

    public void setMagY(double magY) {
        this.magY = magY;
    }

    public double getMagX() {
        return magX;
    }

    public void setMagX(double magX) {
        this.magX = magX;
    }

    public double getPosz() {
        return posz;
    }

    public void setPosz(double posz) {
        this.posz = posz;
    }

    public double getPosy() {
        return posy;
    }

    public void setPosy(double posy) {
        this.posy = posy;
    }

    public double getPosx() {
        return posx;
    }

    public void setPosx(double posx) {
        this.posx = posx;
    }

    public long getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getSliceid() {
        return sliceid;
    }

    public void setSliceid(String sliceid) {
        this.sliceid = sliceid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public ArrayList<BeaconReadingPojo> getBeacon_data() {
        ArrayList<BeaconReadingPojo>array = new ArrayList<>();
        try {
            JSONArray arr = new JSONArray(beacon_data);
            for(int k=0;k<arr.length();k++){
                JSONObject obj = arr.getJSONObject(k);
                BeaconReadingPojo pojo = new BeaconReadingPojo(obj.get("uniqueId").toString(),Double.parseDouble(obj.get("rssi").toString()));
                array.add(pojo);
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return array;

    }

    public void setBeacon_data(String beacon_data) {
        this.beacon_data = beacon_data;
    }

    public ArrayList<BeaconReadingPojo> getWifi_data() {
        ArrayList<BeaconReadingPojo>array = new ArrayList<>();
        try {
            JSONArray arr = new JSONArray(wifi_data);
            for(int k=0;k<arr.length();k++){
                JSONObject obj = arr.getJSONObject(k);
                BeaconReadingPojo pojo = new BeaconReadingPojo(obj.get("uniqueId").toString(),Double.parseDouble(obj.get("rssi").toString()));
                array.add(pojo);
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return array;
    }

    public void setWifi_data(String wifi_data) {
        this.wifi_data = wifi_data;
    }

    public double getOrientationDegrees() {
        return orientationDegrees;
    }

    public void setOrientationDegrees(double orientationDegrees) {
        this.orientationDegrees = orientationDegrees;
    }
}
