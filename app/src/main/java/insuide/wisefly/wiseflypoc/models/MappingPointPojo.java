package insuide.wisefly.wiseflypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MappingPointPojo {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("geo")
    @Expose
    private List<Double> geo = null;
    @SerializedName("lng")
    @Expose
    private double lng;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("sliceid")
    @Expose
    private String sliceid;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("appname")
    @Expose
    private String appname;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Double> getGeo() {
        return geo;
    }

    public void setGeo(List<Double> geo) {
        this.geo = geo;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getSliceid() {
        return sliceid;
    }

    public void setSliceid(String sliceid) {
        this.sliceid = sliceid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }
}
