package insuide.wisefly.wiseflypoc.models


import android.os.Parcel
import android.os.Parcelable
import com.netpiper.sia.model.PoiMultilingual
import com.squareup.moshi.Json
import java.io.Serializable

/**
 * Created by Furqan on 11-04-2018.
 */

data class PoiDetail(
        @Json(name = "user_id")
        var userId: String? = null,

        var keywords: List<String>? = null,
        var subcategory: String? = null,
        var category: String? = null,
        @Json(name = "poi_url")
        var poiUrl: String? = null,
        var appname: String? = null,
        var sliceid: String? = null,
        var levelid: String? = null,
        var updated: Boolean? = null,
        var lng: Double? = null,
        var lat: Double? = null,
        @Json(name = "location_text")
        var locationText: String? = null,
        var description: String? = null,
        var name: String? = null,
        @Json(name = "_id")
        internal var id: String,
        var section: String? = null,
        var sectionBtn: Int = 0,
        var temperature: Double = 0.0,
        var weather: String? = null,
        var weatherIcon: String? = null,
        @Json(name = "poi_multilingual")
        var poiMultilingual: PoiMultilingual? = null,
        val mobile_number: String = "",
        val email: String = ""



) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()!!,
            parcel.readString(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(PoiMultilingual::class.java.classLoader),
            parcel.readString()!!,
            parcel.readString()!!) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userId)
        parcel.writeStringList(keywords)
        parcel.writeString(subcategory)
        parcel.writeString(category)
        parcel.writeString(poiUrl)
        parcel.writeString(appname)
        parcel.writeString(sliceid)
        parcel.writeString(levelid)
        parcel.writeValue(updated)
        parcel.writeValue(lng)
        parcel.writeValue(lat)
        parcel.writeString(locationText)
        parcel.writeString(description)
        parcel.writeString(name)
        parcel.writeString(id)
        parcel.writeString(section)
        parcel.writeInt(sectionBtn)
        parcel.writeDouble(temperature)
        parcel.writeString(weather)
        parcel.writeString(weatherIcon)
        parcel.writeParcelable(poiMultilingual, flags)
        parcel.writeString(mobile_number)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PoiDetail> {
        override fun createFromParcel(parcel: Parcel): PoiDetail {
            return PoiDetail(parcel)
        }

        override fun newArray(size: Int): Array<PoiDetail?> {
            return arrayOfNulls(size)
        }
    }
}
