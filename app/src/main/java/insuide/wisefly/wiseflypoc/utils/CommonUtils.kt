package insuide.wisefly.wiseflypoc.utils

import android.content.Context
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netpiper.sia.manager.LocaleManager
import com.netpiper.sia.model.PoiMultilingual

/**
 * Created by Furqan on 21-03-2018.
 */
class CommonUtils {
    companion object {

        fun dpFromPx(context: Context, px: Float): Float {
            return px / context.resources.displayMetrics.density
        }

        fun pxFromDp(context: Context, dp: Float): Float {
            return dp * context.resources.displayMetrics.density
        }

        fun getDistanceFromLatLonInMeters(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
            val r = 6371.0 // Radius of the earth in km
            val dLat = deg2rad(lat2 - lat1)  // deg2rad below
            val dLon = deg2rad(lon2 - lon1)
            val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon / 2) * Math.sin(dLon / 2)
            val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
            val d = r * c // Distance in km
            return d * 1000
        }

        private fun deg2rad(deg: Double): Double {
            return deg * (Math.PI / 180)
        }

        fun setCatName(context: Context, currentView: TextView, multiLingualData: PoiMultilingual?) {

            when (LocaleManager.getLanguage(context)) {
                LocaleManager.LANGUAGE_ENGLISH -> {
                    currentView.text = multiLingualData?.en?.name
                }
                LocaleManager.LANGUAGE_ARABIC -> {
                    multiLingualData?.ar?.let {
                        currentView.text = it.name
                    } ?: run {
                        currentView.text = multiLingualData?.en?.name
                    }
                }
                LocaleManager.LANGUAGE_RUSSSIAN -> {
                    multiLingualData?.ru?.let {
                        currentView.text = it.name
                    } ?: run {
                        currentView.text = multiLingualData?.en?.name
                    }
                }
                LocaleManager.LANGUAGE_HINDI -> {
                    multiLingualData?.hi?.let {
                        currentView.text = it.name
                    } ?: run {
                        currentView.text = multiLingualData?.en?.name
                    }
                }
                else -> {
                    currentView.text = multiLingualData?.en?.name
                }
            }
        }

        fun hideKeyboard(context: Context, view: View) {
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }




    }
}