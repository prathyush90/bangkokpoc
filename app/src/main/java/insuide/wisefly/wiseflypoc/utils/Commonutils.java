package insuide.wisefly.wiseflypoc.utils;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Created by Furqan on 28-09-2019.
 */
public class Commonutils {


    public static double[] distance(double[] from, double distance, double heading) {
        distance /= 6371000;
        heading = Math.toRadians(heading);
        double fromLat = Math.toRadians(from[0]);
        double fromLng = Math.toRadians(from[1]);
        double cosDistance = Math.cos(distance);
        double sinDistance = Math.sin(distance);
        double sinFromLat = Math.sin(fromLat);
        double cosFromLat = Math.cos(fromLat);
        double sinLat = cosDistance * sinFromLat + sinDistance * cosFromLat * Math.cos(heading);
        double dLng = Math.atan2(
                sinDistance * cosFromLat * Math.sin(heading),
                cosDistance - sinFromLat * sinLat);
        return new double[]{Math.toDegrees(Math.asin(sinLat)), Math.toDegrees(fromLng + dLng)};
    }

    public static double[] convertCartesianToSpherical(double[] cartesian) {
        double r = Math.sqrt(cartesian[0] * cartesian[0] + cartesian[1] * cartesian[1] + cartesian[2] * cartesian[2]);
        double lat = Math.toDegrees(Math.asin(cartesian[2] / r));
        double lon = Math.toDegrees(Math.atan2(cartesian[1], cartesian[0]));
        return new double[]{lat, lon};
    }

    public static double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = Math.toRadians(latitude);
        double lon = Math.toRadians(longitude);
        double x = earthRadius * cos(lat) * cos(lon);
        double y = earthRadius * cos(lat) * sin(lon);
        double z = earthRadius * sin(lat);
        return new double[]{x, y, z};
    }

    public static double angleFromCoordinate(double lat1, double long1, double lat2, double long2) {
        lat1 = Math.toRadians(lat1);
        long1 = Math.toRadians(long1);
        lat2 = Math.toRadians(lat2);
        long2 = Math.toRadians(long2);
        double dLon = (long2 - long1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);
        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        return brng;
    }
}
