package insuide.wisefly.wiseflypoc.utils

import androidx.recyclerview.widget.DiffUtil
import insuide.wisefly.wiseflypoc.models.PoiDetail


/**
 * Created by Furqan on 22-03-2018.
 */
class PoiDiffUtilCallback(val oldList: List<PoiDetail>, val newList: List<PoiDetail>) : DiffUtil.Callback() {


    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newList[newItemPosition].id == oldList[oldItemPosition].id
    }

    override fun getOldListSize() = oldList.size


    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newList[newItemPosition] == oldList[oldItemPosition]
    }

}