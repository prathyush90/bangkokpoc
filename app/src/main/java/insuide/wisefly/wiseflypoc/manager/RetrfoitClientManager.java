package insuide.wisefly.wiseflypoc.manager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrfoitClientManager {


        private static Retrofit retrofit;
        private static Retrofit retrofit_analytics;
        private static Retrofit wisemapper_instance;
        public static final String ASSET_INPUT_URL = "http://development.wisefly.in";
        private static final String MAPPING_URL = "TBD";
        private static final String ANALYTICS_URL = "http://54.200.248.60:3003";
        private static final String WISEMAPPER_URL = "http://62.171.147.128";

        public static Retrofit getAssetManagerRetrofitInstance() {
            if (retrofit == null) {

                OkHttpClient client = new OkHttpClient.Builder()

                        .build();
                retrofit = new Retrofit.Builder()
                        .baseUrl(ASSET_INPUT_URL)
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }

    public static Retrofit getAnalyticsManagerInstance() {
        if (retrofit_analytics == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .build();
            retrofit_analytics = new Retrofit.Builder()
                    .baseUrl(ANALYTICS_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit_analytics;
    }

    public static Retrofit getWiseMapperInstance() {
        if (wisemapper_instance == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .build();
            wisemapper_instance = new Retrofit.Builder()
                    .baseUrl(WISEMAPPER_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return wisemapper_instance;
    }


    public static <S> S getClient(Class<S> serviceClass) {

        OkHttpClient client = new OkHttpClient.Builder()
                .build();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ASSET_INPUT_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(serviceClass);
    }
}

