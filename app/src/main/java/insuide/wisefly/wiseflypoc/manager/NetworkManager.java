package insuide.wisefly.wiseflypoc.manager;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import insuide.wisefly.wiseflypoc.interfaces.AnalyticsService;
import insuide.wisefly.wiseflypoc.interfaces.DataService;
import insuide.wisefly.wiseflypoc.interfaces.LoginResponseInterface;
import insuide.wisefly.wiseflypoc.interfaces.ReadingsResponseInterface;
import insuide.wisefly.wiseflypoc.interfaces.SimpleNetworkInterface;
import insuide.wisefly.wiseflypoc.interfaces.VenueAssetResponseInterface;
import insuide.wisefly.wiseflypoc.models.LoginPojo;
import insuide.wisefly.wiseflypoc.models.SimpleResponsePojo;
import insuide.wisefly.wiseflypoc.models.VenueAndAssetsPojo;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkManager {


    public static void authenticateThisHuman(String username, String password, final LoginResponseInterface mInterface) throws Exception {
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("email", username);
        obj.put("password", password);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<LoginPojo> call = dataService.authenticateAssetInputUser(body);
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {

                LoginPojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.getSuccess()) {
                        JSONObject data = null;

                        data = (JSONObject) pojo.getData();


                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public static void getVenuesAndAssets(String appname, String token, final VenueAssetResponseInterface mInterface) throws Exception {
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("appname", appname);
        obj.put("token", token);

        RequestBody body = RequestBody
                .create(MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<VenueAndAssetsPojo> call = dataService.getVenuesAndAssets(body);
        call.enqueue(new Callback<VenueAndAssetsPojo>() {
            @Override
            public void onResponse(Call<VenueAndAssetsPojo> call, Response<VenueAndAssetsPojo> response) {

                VenueAndAssetsPojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.getSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<VenueAndAssetsPojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }


    public static void updateBeacon( String token, JSONObject beacon, final SimpleNetworkInterface mInterface) throws Exception {
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("beacon", beacon);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.updateBeacon(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public static void updatePOI( String token, JSONObject poi, final SimpleNetworkInterface mInterface) throws Exception {
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("poi", poi);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.updatePOI(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public static void uploadImage(String imageBase64, String token, final SimpleNetworkInterface mInterface) throws Exception {
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("img", imageBase64);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());

        Call<SimpleResponsePojo> call = dataService.uploadImage(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {

                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public static void addWayPoint(String token, JSONObject waypointObject, final SimpleNetworkInterface mInterface) throws Exception{
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("waypoint", waypointObject);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.addWayPoint(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public static void getAllWayPoints(String token, String terminal, String level, String sliceid, String appname, final SimpleNetworkInterface mInterface) throws Exception{
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("termina1", terminal);
        obj.put("slice", sliceid);
        obj.put("level", level);
        obj.put("appname", appname);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.getAllWayPoints(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static void removeWaypoint(String token, String id,  final SimpleNetworkInterface mInterface) throws Exception{
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();

        obj.put("id", id);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.deleteWayPoint(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static void insertReading(String token, JSONObject reading,  final SimpleNetworkInterface mInterface) throws Exception{
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();

        obj.put("reading", reading);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.insertFingerPrintReading(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static void getReadingsFromServer(String token, String appname, long time,  final ReadingsResponseInterface mInterface) throws Exception{
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();

        obj.put("appname", appname);
        obj.put("token", token);
        obj.put("prevtime", time);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<JsonElement> call = dataService.getReadingsFromServer(body);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                JsonElement abs = response.body();
                try {
                    JsonObject gsonJsonObj = abs.getAsJsonObject();
                    JSONObject pojo = new JSONObject(gsonJsonObj.toString());
                    boolean success = pojo.getBoolean("success");
                    if(success){
                        mInterface.onSuccess(pojo);
                    }else{
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("error", pojo.get("message").toString());
                            if(mInterface != null) {
                                mInterface.onError(obj);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //mInterface.onError();
                    }
                }catch (JSONException e){
                    mInterface.onError(e);
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public static void sendUserLocation(  JSONObject data, final SimpleNetworkInterface mInterface) throws Exception {
        AnalyticsService dataService     = RetrfoitClientManager.getAnalyticsManagerInstance().create(AnalyticsService.class);
        JSONObject obj = new JSONObject();
        obj.put("track_data", data);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.sendLocData(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public static void sendPositionData(  String token, JSONObject data, final SimpleNetworkInterface mInterface) throws Exception {
        DataService dataService     = RetrfoitClientManager.getAssetManagerRetrofitInstance().create(DataService.class);
        JSONObject obj = new JSONObject();
        obj.put("data", data);
        obj.put("token", token);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.updatePositionData(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }



    public static void sendNotificationAnalytics(  JSONObject data, final SimpleNetworkInterface mInterface) throws Exception {
        AnalyticsService dataService     = RetrfoitClientManager.getAnalyticsManagerInstance().create(AnalyticsService.class);
        JSONObject obj = new JSONObject();
        obj.put("data", data);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(obj).toString());
        Call<SimpleResponsePojo> call = dataService.sendNotificationData(body);
        call.enqueue(new Callback<SimpleResponsePojo>() {
            @Override
            public void onResponse(Call<SimpleResponsePojo> call, Response<SimpleResponsePojo> response) {

                SimpleResponsePojo pojo = response.body();
                if(pojo != null) {
                    if (pojo.isSuccess()) {
                        if (mInterface != null) {
                            mInterface.onSuccess(pojo);
                        }
                    } else {
                        //handle error event
                        if (mInterface != null) {
                            mInterface.onError(pojo);
                        }
                    }
                }else {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("error", "no response");
                        if(mInterface != null) {
                            mInterface.onError(obj);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponsePojo> call, Throwable t) {
                //throw new Exception("Error occured while making a network call in WiseMap. Maybe internet permission is turned off");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("error", t.getLocalizedMessage());
                    if(mInterface != null) {
                        mInterface.onError(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }




}
