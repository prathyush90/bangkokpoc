package com.netpiper.sia.manager

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by Furqan on 11-01-2018.
 */
class ApiManager {

    companion object {

        private const val BASE_URL: String = "http://development.wisefly.in"//"http://35.164.199.65/"
        private const val BASE_URL_SHJ: String = "http://development.wisefly.in"
        private const val BASE_URL_Dev: String = "http://development.wisefly.in/"
        private const val BASE_URL_GOOGLE_MAPS: String = "https://maps.googleapis.com"
        private const val OPEN_WEATHER_URL = "http://api.openweathermap.org/data/2.5/"
        private const val BASE_URL_ADS: String = "http://adscms.wisefly.in/"
        private const val BASE_URL_REGION: String = "http://ip-api.com/"


        private var retrofit: Retrofit? = null

        fun <S> getClient(context: Context, serviceClass: Class<S>): S {


            val cacheSize = 10 * 1024 * 1024 // 10 MB
            //val cache = Cache(getCacheDir(), cacheSize.toLong())
            val interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            val client = OkHttpClient.Builder()
                    .addNetworkInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    //.cache(cache)
                    .build()
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(client)
                        .addConverterFactory(MoshiConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
            }
            return retrofit!!.create(serviceClass)
        }

        fun <S> getClientSharjaIp(context: Context, serviceClass: Class<S>): S {

            //val interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            val client = OkHttpClient.Builder()
                    //.addNetworkInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
//                    .cache(cache)
                    .build()

            val retrofitAe = Retrofit.Builder()
                    .baseUrl(BASE_URL_SHJ)
                    .client(client)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            return retrofitAe.create(serviceClass)
        }

        fun <S> getClientDev(serviceClass: Class<S>): S {

            //val interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            val client = OkHttpClient.Builder()
                    //.addNetworkInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()
            val retrofitAe = Retrofit.Builder()
                    .baseUrl(BASE_URL_Dev)
                    .client(client)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            return retrofitAe.create(serviceClass)
        }



    }
}