package insuide.wisefly.wiseflypoc.manager;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class PreferencesManager {
    private final static String PREF_NAME = "wisemapper";
    public static void addStringKey(Context mContext,String key, String value){
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
        editor.apply();

    }

    public static String getStringKey(Context mContext, String key, String defaultVal){
        String val = "";
        SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        val = prefs.getString(key, defaultVal);
        return val;

    }

    public static void addBooleanKey(Context mContext, String key, boolean value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(key, value);
        editor.commit();
        editor.apply();
    }

    public static boolean getBooleanKey(Context mContext, String key, boolean defaultVal){
        boolean val = false;
        SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        val = prefs.getBoolean(key, defaultVal);
        return val;

    }

    public static long getLongKey(Context mContext, String key, long defaultValue){
        long val=0l;
        SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        val = prefs.getLong(key, defaultValue);
        return val;
    }

    public static void addLongKey(Context mContext, String key, long value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
        editor.putLong(key, value);
        editor.commit();
        editor.apply();
    }

    public static float getFloat(Context mContext, String key, float defaultVal){
        float val=0f;
        SharedPreferences prefs = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        val = prefs.getFloat(key, defaultVal);
        return val;
    }

    public static void addFloatKey(Context mContext, String key, float value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
        editor.putFloat(key, value);
        editor.commit();
        editor.apply();
    }
}
