package insuide.wisefly.wiseflypoc.manager;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import insuide.wisefly.wiseflypoc.DemoActivity;
import insuide.wisefly.wiseflypoc.interfaces.LocationUpdaterInterface;
import insuide.wisefly.wiseflypoc.models.FilteredBeacon;
import insuide.wisefly.wiseflypoc.models.MapReadingsPojo;


/**
 * Created by prathyush on 02/12/18.
 */

public class LocationManager {

    static {
        System.loadLibrary("slamlib");
    }

    HandlerThread handlerThread;
    Handler messenger;
    private static final int INIT_KDTREE   = 0;
    private static final int SIMULATE_ROBOT_WORLD  = 1;
    private static final int SEND_INITIAL_LOCATION =  2;

    private LocationUpdaterInterface updater;
    private int dimensions = 3;
    //this for mumbai airport
    //double[][] geofence = {{1772612.5964885126-5, 1772947.8879604405+5}, {5753319.554939809-5, 5753471.8145920625+5},{2084466.295558238-5, 2084623.9072973288+5}};
    //this is for office ecostar
    double[][] geofence;
//    = {{1774184.2793695577-1, 1774196.7543790387+1}, {
//            5750321.7180962-1, 5750331.520549943+1},{2091789.3776727167-1, 2091819.0163994082+1}};

    private int particleSize = 1500;

    private long pointerAddress;
    private long sessionId;
    private Context mContext;
    private int maxParticleSize = 2000;
    int i=0;
    private boolean initDone;
    private ArrayList<ArrayList<ArrayList<Double>>> walkregion = new ArrayList<>();
    private NavigationManager navManager;
    private JSONObject floorPlanObject = new JSONObject();
    private final int INVALIDATE_THREAD = 4;
    private boolean shouldUseSingleBeacon = false;
    private boolean converged = false;

    //private ParticleManager particleManager;
    public LocationManager(Context context, double [][]geofence, JSONObject walkablearea, Fragment cont) {

        if (cont instanceof LocationUpdaterInterface) {
            updater = (LocationUpdaterInterface) cont;
        } else {
            throw new RuntimeException(cont.toString()
                    + " must implement Neigbours updater interface");
        }
        initDone = false;
        this.geofence = geofence;
        this.mContext = context;
        handlerThread = new HandlerThread("neighboursthread", Thread.MAX_PRIORITY);
        handlerThread.start();
        messenger = new Handler(handlerThread.getLooper(), callback);

        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        long totalMemory = memInfo.totalMem;

//        if(totalMemory <= 5500000000l){
//            particleSize = 800;
//        }

//
        if(totalMemory <= 4800000000l){
            //shouldUseSingleBeacon = true;
            particleSize = 700;
        }
//        if(totalMemory <= 2800000000l){
//            particleSize = 350;
//        }
//        try {
//            JSONArray regionaccessible = walkablearea.getJSONArray("geofence");
//            for(int k=0;k<regionaccessible.length();k++){
//                JSONArray polygon = regionaccessible.getJSONArray(k);
//                ArrayList<ArrayList<Double>>shape = new ArrayList<>();
//                for(int i=0;i<polygon.length();i++){
//                    ArrayList<Double>latLng = new ArrayList<>();
//                    JSONArray point = polygon.getJSONArray(i);
//                    latLng.add(point.getDouble(0));
//                    latLng.add(point.getDouble(1));
//                    shape.add(latLng);
//                }
//                walkregion.add(shape);
//
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        System.out.print("xxx");
//        initLocProvider();
        navManager = new NavigationManager();
        navManager.init(context);

    }

    private void initLocProvider() {
        this.pointerAddress = nativeInitlocProvider();
    }

    public void invalidate(){
        /*
            Remove all present messages here. Required when floor change happens. Redundant information
         */
        if(messenger != null){
            messenger.removeCallbacksAndMessages(null);
        }
        Message msg = new Message();
        msg.what    = INVALIDATE_THREAD;
        msg.obj     = null;
        if(messenger != null) {
            messenger.sendMessage(msg);
        }

    }

    public void invalidateJNI(){
        if(messenger != null){
            messenger.removeCallbacksAndMessages(null);
        }
        if(handlerThread != null && handlerThread.isAlive()){
            handlerThread.quit();
        }
        if(messenger != null){
            messenger = null;
        }

        deleteNativeReference(this.pointerAddress);
        sessionId = 0l;
        navManager = null;
    }

    private Handler.Callback callback = new Handler.Callback(){
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case INIT_KDTREE:
                    ArrayList<MapReadingsPojo> readings = (ArrayList<MapReadingsPojo>) msg.obj;
                    initKdTree(readings);
                    break;
                case SIMULATE_ROBOT_WORLD:
                    simulateRobotWorld(msg.obj);
                    break;
                case SEND_INITIAL_LOCATION:
                    sendMeanLocation();
                    break;
                case 3:
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(msg.obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    nativeInitialize(obj);
                    break;
                case INVALIDATE_THREAD:
                    invalidateJNI();
            }
            return false;
        }
    };

    private void simulateRobotWorld(Object obj){
        System.out.println("---> java move called");

        try{
            JSONObject message_obj = new JSONObject(obj.toString());
            double pitch = message_obj.getDouble("pitch");
            double roll  = message_obj.getDouble("roll");
            double yaw   = message_obj.getDouble("yaw");
            int axis = message_obj.getInt("movevector");
            int event = message_obj.getInt("event");
            boolean isCached = message_obj.getBoolean("iscached");
            JSONArray bdata             = new JSONArray(message_obj.get("beacon_data").toString());
//
            Gson gson = new Gson();
            Type type = new TypeToken<List<FilteredBeacon>>(){}.getType();
            ArrayList<FilteredBeacon> bData = gson.fromJson(bdata.toString(), type);

            JSONArray magneticObs          = new JSONArray(message_obj.get("magnetic").toString());
            JSONArray wifiObs              = new JSONArray(message_obj.get("wifi").toString());
            JSONArray beaconObs            = new JSONArray(message_obj.get("beacon").toString());
//            JSONArray movevectorObs           = new JSONArray(message_obj.get("movevector").toString());
            boolean isWifi                 = Boolean.parseBoolean(message_obj.get("isWifi").toString());
            boolean isBeacon               = Boolean.parseBoolean(message_obj.get("isBeacon").toString());
            int size                       = message_obj.getInt("beaconsize");
            double magObserved = Double.parseDouble(message_obj.get("magnitude").toString());
            double magneticobservations[]  = new double[magneticObs.length()+1];
            double magnitude = 0;
            for(int k=0;k<magneticObs.length();k++){
                magneticobservations[k] = Double.parseDouble(magneticObs.get(k).toString());
                magnitude += Math.pow(magneticobservations[k], 2);
            }
            magneticobservations[3]  = magObserved;


            double wifiobservations[]  = new double[wifiObs.length()];
            for(int k=0;k<wifiObs.length();k++){
                wifiobservations[k] = Double.parseDouble(wifiObs.get(k).toString());
            }

            double bluetoothobservations[]  = new double[beaconObs.length()];
            for(int k=0;k<beaconObs.length();k++){
                bluetoothobservations[k]    = Double.parseDouble(beaconObs.get(k).toString());
            }

            double distance = event == 0 ? 0 : 0.6;
            double returnedlocation[] = nativeMoveSlaves(distance,yaw, magneticobservations, this.pointerAddress, magneticobservations.length, bluetoothobservations, isBeacon, wifiobservations, isWifi, size, isCached);
            double particlesLocation[][] = getParticlesLocations(this.pointerAddress, maxParticleSize);
            this.updater.onNeighboursLocation(returnedlocation);

            JSONObject message = new JSONObject();
            JSONArray particles = new JSONArray();

            for(int i =0;i<particlesLocation.length; i++){
                JSONArray particledata = new JSONArray();
                for(int k= 0;k<particlesLocation[0].length;k++){
                    try {
                        particledata.put(particlesLocation[i][k]);
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
                particles.put(particledata);
            }

            try {
                message.put("particlesdata", particles);
                message.put("time", System.currentTimeMillis());
                message.put("beacon_x", bluetoothobservations[0]);
                message.put("beacon_y", bluetoothobservations[1]);
                message.put("beacon_z", bluetoothobservations[2]);
                message.put("magnitude", magObserved);
                message.put("turn", yaw);
                message.put("distance", distance);
                message.put("mag_x", magneticobservations[0]);
                message.put("mag_y", magneticobservations[1]);
                message.put("mag_z", magneticobservations[2]);
                message.put("user_x", returnedlocation[0]);
                message.put("user_y", returnedlocation[1]);
                message.put("user_z", returnedlocation[2]);
                message.put("user_ori", returnedlocation[3]);
                message.put("max_weight", returnedlocation[4]);
                message.put("sessionid", sessionId);
//                //message.put("neighbors", new JSONArray());
                if (!converged) {
                    if (returnedlocation[4] > 0) {
                        converged = true;

                    }
                }
                if(distance > 0) {
                    passOntoServer(message);
                }

                if(i>50){
                    i=0;
                    System.gc();
                }else {
                    i++;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }





    public void initKdTree(ArrayList<MapReadingsPojo> readings){
        Context context = (Context) this.mContext;
        if(context != null) {

            initNativeLocalizerAndMapper(this.pointerAddress, dimensions, readings, particleSize, geofence,walkregion, shouldUseSingleBeacon);
        }
        //particleManager = new ParticleManager(dimensions, readings);
    }

    public void initializeParticleFilterFromGuessLocation(double geofence[][], double heading, double b_x, double b_y, double b_z, int beaconsSize){
        JSONArray fence = new JSONArray();
        for (int k=0;k<geofence.length;k++){
            JSONArray json = new JSONArray();
            for(int j=0;j<geofence[0].length;j++){
                try {
                    json.put(geofence[k][j]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            fence.put(json);
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("geofence",fence);
            obj.put("heading", heading);
            obj.put("b_x", b_x);
            obj.put("b_y", b_y);
            obj.put("b_z", b_z);
            obj.put("bsize", beaconsSize);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Message msg = new Message();
        msg.what    = 3;
        msg.obj     = obj;
        messenger.sendMessage(msg);

    }

    public void nativeInitialize(JSONObject obj){
        JSONArray geof = null;
        double geofence[][] = new double[3][3];
        try {
            geof = obj.getJSONArray("geofence");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        double heading = 0;
        double b_x = 0;
        double b_y = 0;
        double b_z = 0;
        int size = 0;
        try {
            heading = obj.getDouble("heading");
            b_x = obj.getDouble("b_x");
            b_y = obj.getDouble("b_y");
            b_z = obj.getDouble("b_z");

            size = obj.getInt("bsize");

            for(int i=0;i<geof.length();i++){
                for(int k=0;k<geof.getJSONArray(0).length();k++){
                    geofence[i][k] = geof.getJSONArray(i).getDouble(k);
                }
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nativeInitParticles(pointerAddress, geofence,heading, b_x, b_y, b_z, size);
        double particlesLocation[][] = getParticlesLocations(this.pointerAddress, particleSize);
//
//        this.updater.particlesLocation(particlesLocation);
        sessionId = System.currentTimeMillis();
        JSONObject message = new JSONObject();
        JSONArray particles = new JSONArray();
        double x_mean = (geofence[0][1] + geofence[0][0]) /2;
        double y_mean = (geofence[1][1] + geofence[1][0]) /2;
        double z_mean = (geofence[2][1] + geofence[2][0]) /2;
        for(int i =0;i<particlesLocation.length; i++){
            JSONArray particledata = new JSONArray();
            for(int k= 0;k<particlesLocation[0].length;k++){
                try {
                    particledata.put(particlesLocation[i][k]);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
            particles.put(particledata);
        }
        try {
            message.put("particlesdata", particles);
            message.put("time", System.currentTimeMillis());
            message.put("beacon_x", 0);
            message.put("beacon_y", 0);
            message.put("beacon_z", 0);
            message.put("magnitude", 0);
            message.put("turn", 0);
            message.put("distance", 0);
            message.put("mag_x", 0);
            message.put("mag_y", 0);
            message.put("mag_z", 0);
            message.put("user_x", x_mean);
            message.put("user_y", y_mean);
            message.put("user_z", z_mean);
            message.put("user_ori", 0);
            message.put("max_weight", 0);
            message.put("sessionid", sessionId);
            //message.put("neighbors", new JSONArray());
            passOntoServer(message);
        }catch (JSONException e){
            e.printStackTrace();
        }
        initDone = true;
    }




    public void passInitMessage(ArrayList<MapReadingsPojo> readings, JSONArray readingsJson){
        Message msg = new Message();
        msg.what    = INIT_KDTREE;
        msg.obj     = readings;
        messenger.sendMessage(msg);
        //particleManager = new ParticleManager(dimensions, readings);
    }

    public void passMeanLocationMessage() {
        Message message = new Message();
        message.what    = SEND_INITIAL_LOCATION;
        messenger.sendEmptyMessage(message.what);
        //initializeParticleFilterFromGuessLocation(geofence, Math.random()*360);
        sendMeanLocation();
    }

    public void sendMeanLocation(){
        double x_mean = (geofence[0][1] + geofence[0][0]) /2;
        double y_mean = (geofence[1][1] + geofence[1][0]) /2;
        double z_mean = (geofence[2][1] + geofence[2][0]) /2;
        this.updater.onInitialLocation(new double[]{x_mean, y_mean, z_mean, 0.0, 0.0});
    }



    public void passSimulateMessage(int event, int axis, double pitchChange, double rollChange, double yawChange, double magObservations[], double[] wifiObservations, double[] beaconObservations, boolean isWifi, boolean isBeacon, int size, ArrayList<FilteredBeacon>beacon, double magnitudeObserved, boolean isCached){
//        if(!initDone){
//            return;
//        }
        Message msg = new Message();
        msg.what    = SIMULATE_ROBOT_WORLD;
        JSONObject obj = new JSONObject();
        try{
            obj.put("pitch", pitchChange);
            obj.put("roll", rollChange);
            obj.put("yaw", yawChange);
            obj.put("event", event);
            obj.put("iscached", isCached);


            JSONArray magneticObs = new JSONArray();
            for(int i=0;i<magObservations.length;i++){
                magneticObs.put(magObservations[i]);
            }

            JSONArray wifiObs = new JSONArray();
            for(int i=0;i<wifiObservations.length;i++){
                wifiObs.put(wifiObservations[i]);
            }

            JSONArray beaconObs = new JSONArray();
            for(int i=0;i<beaconObservations.length;i++){
                beaconObs.put(beaconObservations[i]);
            }

            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(beacon, new TypeToken<List<FilteredBeacon>>() {
            }.getType());
            if (!element.isJsonArray()) {

                System.out.print("check");
            }

            obj.put("beacon_data", element.getAsJsonArray());

            obj.put("magnetic", magneticObs);
            obj.put("wifi", wifiObs);
            obj.put("beacon", beaconObs);
            obj.put("isWifi", isWifi);
            obj.put("isBeacon", isBeacon);
            obj.put("beaconsize", size);
            obj.put("magnitude", magnitudeObserved);
            obj.put("movevector", axis);

            msg.obj = obj;
            messenger.sendMessage(msg);

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

//    public void passSimulateMessage(double distance, double turn, double[] magObservations, double[] wifiObservations, double[] beaconObservations, boolean isWifi, boolean isBeacon, int size, ArrayList<FilteredBeacon>beacon, double magnitudeObserved) {
//       if(!initDone){
//           return;
//       }
//        Message msg     = new Message();
//        msg.what        = SIMULATE_ROBOT_WORLD;
//        JSONObject obj  = new JSONObject();
//        try {
//            obj.put("distance", distance);
//            obj.put("turn", turn);
//
//            JSONArray magneticObs = new JSONArray();
//            for(int i=0;i<magObservations.length;i++){
//                magneticObs.put(magObservations[i]);
//            }
//
//            JSONArray wifiObs = new JSONArray();
//            for(int i=0;i<wifiObservations.length;i++){
//                wifiObs.put(wifiObservations[i]);
//            }
//
//            JSONArray beaconObs = new JSONArray();
//            for(int i=0;i<beaconObservations.length;i++){
//                beaconObs.put(beaconObservations[i]);
//            }
//
//            Gson gson = new Gson();
//
//                JsonElement element = gson.toJsonTree(beacon, new TypeToken<List<FilteredBeacon>>() {
//                }.getType());
//                if (!element.isJsonArray()) {
//
//                    System.out.print("check");
//                }
//
//
//            obj.put("beacon_data", element.getAsJsonArray());
//
//            obj.put("magnetic", magneticObs);
//            obj.put("wifi", wifiObs);
//            obj.put("beacon", beaconObs);
//            obj.put("isWifi", isWifi);
//            obj.put("isBeacon", isBeacon);
//            obj.put("beaconsize", size);
//            obj.put("magnitude", magnitudeObserved);
//
//            msg.obj = obj;
//            messenger.sendMessage(msg);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    public double[][] getGeoFenceTest(){
        return new double[][]{{geofence[0][0],geofence[0][1]}, {geofence[1][0], geofence[1][1]}, {geofence[2][0], geofence[2][1]}};
    }

    public void setFloorPlanObject(JSONObject floorPlanObject){
        this.floorPlanObject = floorPlanObject;
    }

    public void passOntoServer(JSONObject data){
        DemoActivity act = (DemoActivity) mContext;
        String url = null;
        try {
            url = floorPlanObject.getString("base_url");
            double lat = floorPlanObject.getDouble("lat");
            double lng = floorPlanObject.getDouble("lng");
            data.put("floorplanurl", url);
            data.put("center_lat", lat);
            data.put("center_lng", lng);
            data.put("appname", "bkk");
            act.emitSocketMessage(data, "positiontesterdata");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void removeAllPending() {
        if(messenger != null){
            messenger.removeCallbacksAndMessages(null);
        }
    }

    public JSONArray getShortestRoute(LatLng source, LatLng destination, String sourcefloor, String destinationfloor){
        return navManager.getShortestPath(source, destination, sourcefloor, destinationfloor);
    }

    private native long nativeInitlocProvider();

    private native void deleteNativeReference(long address);

    private native void initNativeLocalizerAndMapper(long address, int dimensions, ArrayList<MapReadingsPojo> measurements,int particleSize, double[][] geofence, ArrayList<ArrayList<ArrayList<Double>>> walkregion, boolean shouldUseSingleBeacon);

    private native double[] nativeMoveSlaves(double distance, double turn, double[] magobservations, long address, int maglength, double []beaconsobservations, boolean isBeacon, double []wifiobservations, boolean isWifi, int size,boolean isCached);

    //private native double[] nativeMoveSlaves(int event, int axis, double pitchChange, double rollChange, double yawChange, double[] magobservations, long address, int maglength, double []beaconsobservations, boolean isBeacon, double []wifiobservations, boolean isWifi, int size);

    private native double[][] getParticlesLocations(long address, int particleSize);

    private native void nativeInitParticles(long address, double coords[][], double heading, double b_x, double b_y, double b_z, int size);


}
