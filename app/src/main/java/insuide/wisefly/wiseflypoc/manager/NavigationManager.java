package insuide.wisefly.wiseflypoc.manager;

import android.content.Context;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import insuide.wisefly.wiseflypoc.utils.FileUtils;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class NavigationManager {
    JSONObject verticesMap = new JSONObject();
    JSONObject edgesMap     = new JSONObject();

    public void init(Context context){
        String jsonstring = FileUtils.loadJSONFromAsset(context, "routes.json");
        try {
            JSONObject resp = new JSONObject(jsonstring);
            JSONArray wayPoints = resp.getJSONArray("markers");
            JSONArray wayEdges  = resp.getJSONArray("edges");
            addVertices(wayPoints);
            addEdges(wayEdges);
//            JSONArray edges = edgesMap.getJSONArray("a6bfd7e8-3abc-a58e-69c8-eedfca90c6c5");
//            System.out.print("sss");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addVertices(JSONArray points){
        for(int i=0;i<points.length();i++){
            try {
                JSONObject point = points.getJSONObject(i);
                verticesMap.put(point.getString("_id"), point);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private void addEdges(JSONArray edges){
        for(int k=0;k<edges.length();k++){
            try {
                JSONObject edge = edges.getJSONObject(k);
                String from_id  = edge.getString("from_point");
                if(!edgesMap.has(from_id)){
                    edgesMap.put(from_id, new JSONArray());
                }
                JSONArray edgesList = edgesMap.getJSONArray(from_id);
                edgesList.put(edge);
                edgesMap.put(from_id, edgesList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public JSONArray getShortestPath(LatLng sourceLatLng, LatLng destinationLat, String sourceFloor, String destinationFloor){
        JSONArray unsmoothedPath = astarShortestPath(sourceLatLng, destinationLat, sourceFloor, destinationFloor);
        //smoothing pending
//        JSONArray unsmoothedCartesian = new JSONArray();
//        for(int k=0;k<unsmoothedPath.length();k++){
//            try {
//                JSONObject point = unsmoothedPath.getJSONObject(k);
//                double lat = point.getDouble("lat");
//                double lng = point.getDouble("lng");
//                double xyz[] = convertSphericalToCartesian(lat, lng);
//                JSONArray coords = new JSONArray();
//                coords.put(xyz[0]);
//                coords.put(xyz[1]);
//                coords.put(xyz[2]);
//                unsmoothedCartesian.put(coords);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        JSONArray smoothedPathUnTransformed = smoothPath(unsmoothedCartesian, 0.1, 0.1, 0.000001);
//        JSONArray smoothedPath = new JSONArray();
//        for(int i=0; i<smoothedPathUnTransformed.length();i++){
//            try {
//                JSONArray coords = smoothedPathUnTransformed.getJSONArray(i);
//                double latLng[]  = convertCartesianToSpherical(new double[]{coords.getDouble(0), coords.getDouble(1), coords.getDouble(2)});
//                JSONObject insideObj = new JSONObject();
//                insideObj.put("lat", latLng[0]);
//                insideObj.put("lng", latLng[1]);
//                insideObj.put("level", unsmoothedPath.getJSONObject(i).getString("level"));
//                smoothedPath.put(insideObj);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        }
        return unsmoothedPath;
    }


    //TO DO
    public JSONArray smoothPath(JSONArray unsmoothePath, double weight_data, double weight_smooth, double tolerance){
        JSONArray smoothedPath = null;
        try {
            smoothedPath =    new JSONArray(unsmoothePath.toString());
            double converge =100;
            while (converge >= tolerance){
                converge  = 0.0;
                for(int i=1;i<unsmoothePath.length()-1;i++){
                    for(int k=0;k<3;k++){
                        double coord = smoothedPath.getJSONArray(i).getDouble(k);
                        double newcoord = weight_data * (unsmoothePath.getJSONArray(i).getDouble(k) - smoothedPath.getJSONArray(i).getDouble(k)) +
                        weight_smooth * (smoothedPath.getJSONArray(i-1).getDouble(k) + smoothedPath.getJSONArray(i+1).getDouble(k) - 2.0 * smoothedPath.getJSONArray(i).getDouble(k));
                        smoothedPath.getJSONArray(i).put(k, newcoord);
                        converge += Math.abs(coord - newcoord);
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return smoothedPath;
    }

    public JSONArray astarShortestPath(LatLng sourceLatLng, LatLng destinationLat, String sourceFloor, String destinationFloor){
        JSONArray closedSet   = new JSONArray();
        JSONArray openSet   = new JSONArray();
        JSONObject cameFrom = new JSONObject();
        JSONObject wscoreFromSource = new JSONObject();
        JSONObject fscoreFromSource = new JSONObject();
        boolean found  = false;
        JSONArray result = new JSONArray();
        Iterator<String> keys = verticesMap.keys();

        JSONObject nearestSource = getNearestPoint(sourceLatLng, sourceFloor);
        JSONObject nearestDestination = getNearestPoint(destinationLat, destinationFloor);
        if(nearestSource == null || nearestDestination == null){
            return new JSONArray();
        }
        while (keys.hasNext()){
            String key = keys.next();
            try {
                wscoreFromSource.put(key, Double.MAX_VALUE);
                fscoreFromSource.put(key, Double.MAX_VALUE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String sourceId = null;
        try {
            sourceId = nearestSource.getString("_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            wscoreFromSource.put(sourceId, 0);
            fscoreFromSource.put(sourceId, getHeuristiceDistance(nearestSource.getDouble("lat"), nearestSource.getDouble("lng"),sourceFloor, nearestDestination.getDouble("lat"), nearestDestination.getDouble("lng"), destinationFloor));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        openSet.put(nearestSource);
        try {
            cameFrom.put(sourceId, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {


            while (openSet.length() > 0 && !found) {
                JSONObject currNode = returnProbableNode(openSet, fscoreFromSource);
                String name = currNode.getString("name");
                String floor = currNode.getString("level");
                System.out.println("<----->"+name);
                System.out.println("<----->"+floor);
                if (currNode == null) {
                    System.out.println("You are fucked !!!");
                    currNode = openSet.getJSONObject(0);
                }
                if(currNode.getString("_id").equals(nearestDestination.getString("_id"))){
                    found = true;
                    result = reconstructPath(cameFrom, currNode.getString("_id"));
                    break;
                }
                int index =currNode.getInt("indexinclosedset");
                if(index > -1){
                    openSet.remove(index);
                }
                closedSet.put(currNode);

                JSONArray edges = edgesMap.getJSONArray(currNode.getString("_id"));
                if(edges.length() == 0){
                    continue;
                }

                for(int i=0;i<edges.length();i++){
                    JSONObject edge = edges.getJSONObject(i);
                    JSONObject neighbour = verticesMap.getJSONObject(edge.getString("to_point"));
                    boolean inClosed = searchInArray(closedSet, neighbour.getString("_id"));
                    if(inClosed){
                        continue;
                    }

                    boolean inopen = searchInArray(openSet, neighbour.getString("_id"));
                    if(!inopen){
                        openSet.put(neighbour);
                    }

                    double presentScore = wscoreFromSource.getDouble(currNode.getString("_id"))+getWScore(edge);
                    double alreadyCalculatedScore = wscoreFromSource.getDouble(neighbour.getString("_id"));
                    if(presentScore >= alreadyCalculatedScore){
                        continue;
                    }
                    cameFrom.put(neighbour.getString("_id"), currNode.getString("_id"));
                    wscoreFromSource.put(neighbour.getString("_id"), presentScore);
                    double fscore = wscoreFromSource.getDouble(neighbour.getString("_id"))+getHeuristiceDistance(neighbour.getDouble("lat"), neighbour.getDouble("lng"),neighbour.getString("level"), nearestDestination.getDouble("lat"), nearestDestination.getDouble("lng"),nearestDestination.getString("level"));
                    fscoreFromSource.put(neighbour.getString("_id"), fscore);


                }

            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    private double getWScore(JSONObject edge) {
        double distanceFac = 0;
        try {
            double lng2 = edge.getDouble("to_point_lng");
            double lat2 = edge.getDouble("to_point_lat");

            double lng1 = edge.getDouble("from_point_lng");
            double lat1 = edge.getDouble("from_point_lat");
            double R = 6371000;
            double dLat = Math.toRadians(lat2-lat1);  // deg2rad below
            double dLon = Math.toRadians(lng2-lng1);
            double a =
                    Math.sin(dLat/2) * Math.sin(dLat/2) +
                            Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                                    Math.sin(dLon/2) * Math.sin(dLon/2)
                    ;
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            double d = R * c;
            distanceFac = d;
        }catch (JSONException e){
            e.printStackTrace();
        }
        return distanceFac;
    }

    private boolean searchInArray(JSONArray array, String id){
        boolean found = false;
        for(int k=0;k<array.length();k++){
            try {
                JSONObject obj = array.getJSONObject(k);
                if (obj.getString("_id").equals(id)) {
                    found = true;
                    return found;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        return found;

    }

    private JSONArray reconstructPath(JSONObject cameFrom, String id){
        JSONArray result = new JSONArray();
        try {

            result.put(verticesMap.getJSONObject(id));
//            Iterator<String> keys = cameFrom.keys();
//            while (keys.hasNext()){
//                String key = keys.next();
//                if(key != null) {
//                    result.put(verticesMap.getJSONObject(key));
//                }
//            }
            String key = cameFrom.getString(id);
            while (key != null){
                result.put(verticesMap.getJSONObject(key));
                key = cameFrom.getString(key);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        return result;

    }

    private JSONObject returnProbableNode(JSONArray openset, JSONObject fscore){
        double min = Double.MAX_VALUE;
        JSONObject node = null;

        for(int i=0;i<openset.length();i++){
            try {
                JSONObject point = openset.getJSONObject(i);
                double f_score = fscore.getDouble(point.getString("_id"));
                if(f_score < min){
                    min = f_score;
                    node = point;
                    node.put("indexinclosedset", i);
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        return node;


    }

    private JSONObject getNearestPoint(LatLng sourceLatLng, String sourceFloor) {
        double minimum = Double.MAX_VALUE;
        JSONObject val = null;
        Iterator<String> keys = verticesMap.keys();
        while (keys.hasNext()){
            String key = keys.next();
            try {
                JSONObject point = verticesMap.getJSONObject(key);
                if(point.getString("level").equals(sourceFloor)){
                    double distance = getHeuristiceDistance(sourceLatLng.getLatitude(), sourceLatLng.getLongitude(), sourceFloor,point.getDouble("lat"), point.getDouble("lng"), point.getString("level"));
                    if(distance < minimum){
                        val = point;
                        minimum = distance;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    private double getHeuristiceDistance(double lat1, double lng1, String level1, double lat2, double lng2, String level2){
        double R = 6371000;
        double dLat = Math.toRadians(lat2-lat1);  // deg2rad below
        double dLon = Math.toRadians(lng2-lng1);
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in metres
        double level_diff = Math.abs(Integer.parseInt(level1) - Integer.parseInt(level2))*50;
        return d+level_diff;
    }
    private double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = Math.toRadians(latitude);
        double lon = Math.toRadians(longitude);
        double x = earthRadius * cos(lat) * cos(lon);
        double y = earthRadius * cos(lat) * sin(lon);
        double z = earthRadius * sin(lat);
        return new double[]{x, y, z};
    }
    private double[] convertCartesianToSpherical(double[] cartesian) {
        double r = Math.sqrt(cartesian[0] * cartesian[0] + cartesian[1] * cartesian[1] + cartesian[2] * cartesian[2]);
        double lat = Math.toDegrees(Math.asin(cartesian[2] / r));
        double lon = Math.toDegrees(Math.atan2(cartesian[1], cartesian[0]));

        return new double[]{lat, lon};
    }
}
