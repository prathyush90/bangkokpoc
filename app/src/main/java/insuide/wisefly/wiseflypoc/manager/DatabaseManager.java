package insuide.wisefly.wiseflypoc.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import insuide.wisefly.wiseflypoc.utils.FileUtils;


public class DatabaseManager extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "wisemapper.db";
    public static final String BEACONS_TABLE_NAME = "beacons";
    public static final String FLOORPLANS_TABLE_NAME = "floorplans";
    public static final String GEOFENCE_TABLE_NAME = "geofence";
    public static final String BEACONS_COLUMN_ID = "appname";
    public static final String FLOORPLANS_COLUMN_ID = "appname";
    public static final String BEACONS_COLUMN_DATA = "data";
    public static final String FLOORPLANS_COLUMN_DATA = "data";
    public static final int version = 6;

    public static final String TABLE_READINGS = "readings";
    public static final String READINGS_COLUMN_ID = "appname";
    public static final String READINGS_COLUMN_TERMINAL = "terminal";
    public static final String READINGS_COLUMN_LEVEL = "level";
    public static final String READINGS_COLUMN_SLICE = "slice";
    public static final String READINGS_COLUMN_TIME = "milliseconds";
    public static final String READINGS_COLUMN_DATA = "data";

    public static final String TABLE_MAP = "mapgenerated";
    public static final String MAP_COLUMN_APP = "appname";
    public static final String MAP_COLUMN_TERMINAL = "terminal";
    public static final String MAP_COLUMN_LEVEL = "level";
    public static final String MAP_COLUMN_SLICE = "slice";
    public static final String MAP_COLUMN_VALUES = "data";


    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "create table beacons " +
                        "(appname string primary key, data)"
        );

        db.execSQL(
                "create table floorplans " +
                        "(appname string primary key, data)"
        );

        db.execSQL(
                "create table geofence " +
                        "(appname string primary key, data)"
        );

        String CREATE_READINGS_TABLE = "CREATE TABLE " + TABLE_READINGS + "("
                + READINGS_COLUMN_ID + " VARCHAR ," + READINGS_COLUMN_TERMINAL + " TEXT,"
                +READINGS_COLUMN_SLICE + " TEXT,"
                +READINGS_COLUMN_TIME + " TEXT PRIMARY KEY,"
                +READINGS_COLUMN_DATA + " TEXT,"
                + READINGS_COLUMN_LEVEL + " TEXT" + ")";

        String CREATE_MAP_TABLE = "CREATE TABLE " + TABLE_MAP + "("
                +"id INTEGER PRIMARY KEY AUTOINCREMENT,"
                +MAP_COLUMN_APP + " TEXT,"
                +MAP_COLUMN_LEVEL + " TEXT,"
                +MAP_COLUMN_TERMINAL + " TEXT,"
                +MAP_COLUMN_SLICE + " TEXT,"
                + MAP_COLUMN_VALUES + " TEXT" + ")";


        db.execSQL(
                CREATE_READINGS_TABLE
        );
        db.execSQL(
                CREATE_MAP_TABLE
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS beacons");
        db.execSQL("DROP TABLE IF EXISTS floorplans");
        db.execSQL("DROP TABLE IF EXISTS readings");
        db.execSQL("DROP TABLE IF EXISTS mapgenerated");
        db.execSQL("DROP TABLE IF EXISTS geofence");
        onCreate(db);
    }

    public boolean insertBeaconData (String app, String data) {
        deleteBeaconData(app);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BEACONS_COLUMN_ID, app);
        contentValues.put(BEACONS_COLUMN_DATA, data);
        db.insert(BEACONS_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertFloorPlanData (String app, String data) {
        deleteFloorPlanData(app);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLOORPLANS_COLUMN_ID, app);
        contentValues.put(FLOORPLANS_COLUMN_DATA, data);
        db.insert(FLOORPLANS_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertGeofencePlanData (String app, String data) {
        deleteGeofencePlanData(app);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLOORPLANS_COLUMN_ID, app);
        contentValues.put(FLOORPLANS_COLUMN_DATA, data);
        db.insert(GEOFENCE_TABLE_NAME, null, contentValues);
        return true;
    }

    public String getBeaconsData(String app) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from beacons where appname="+"'"+app+"'"+"", null );
        res.moveToFirst();
        String data = "";
        if(res.moveToFirst()) {
            data = res.getString(res.getColumnIndex(BEACONS_COLUMN_DATA));
        }

        return data;
    }

    public String getGeofenceData(String app) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from geofence where appname="+"'"+app+"'"+"", null );
        res.moveToFirst();
        String data = "";
        try {
            data = res.getString(res.getColumnIndex(BEACONS_COLUMN_DATA));
        }catch (Exception e){
            e.printStackTrace();
        }

        return data;
    }

    public String getFloorPlansData(String app) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from floorplans where appname="+"'"+app+"'"+"", null );
        res.moveToFirst();
        String data = "";
        try {
            data = res.getString(res.getColumnIndex(BEACONS_COLUMN_DATA));
        }catch (Exception e){
            e.printStackTrace();
        }

        return data;
    }



    public int deleteBeaconData (String app) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("beacons",
                "appname = ? ",
                new String[]{app});
    }

    public int deleteFloorPlanData (String app) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("floorplans",
                "appname = ? ",
                new String[]{app});
    }

    public int deleteGeofencePlanData (String app) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("geofence",
                "appname = ? ",
                new String[]{app});
    }
    public boolean updateBeaconData(String app, String data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BEACONS_COLUMN_ID, app);
        contentValues.put(BEACONS_COLUMN_DATA, data);

        db.update(BEACONS_TABLE_NAME, contentValues, "appname = ? ", new String[] {app} );
        return true;
    }

    public boolean updateFloorPlanData(String app, String data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLOORPLANS_COLUMN_ID, app);
        contentValues.put(FLOORPLANS_COLUMN_DATA, data);

        db.update(FLOORPLANS_TABLE_NAME, contentValues, "appname = ? ", new String[] {app} );
        return true;
    }

    public JSONArray getPendingReadings(String app){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from readings where appname="+"'"+app+"'"+"", null );
        JSONArray readings = new JSONArray();
        if (cursor.moveToFirst()) {
            do {
                try{
                    JSONObject reading = new JSONObject();
                    reading.put("appname", app);
                    reading.put("terminal", cursor.getString(cursor.getColumnIndex(READINGS_COLUMN_TERMINAL)));
                    reading.put("level", cursor.getString(cursor.getColumnIndex(READINGS_COLUMN_LEVEL)));
                    reading.put("slice", cursor.getString(cursor.getColumnIndex(READINGS_COLUMN_SLICE)));
                    reading.put("milliseconds", Long.parseLong(cursor.getString(cursor.getColumnIndex(READINGS_COLUMN_TIME))));
                    reading.put("data",new JSONArray(cursor.getString(cursor.getColumnIndex(READINGS_COLUMN_DATA))));
                    readings.put(reading);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        return readings;
    }

    public JSONArray getPendingForLevelAndTerminal(String app, String terminal, String level, String slice){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from readings where appname="+"'"+app+"'"+" AND terminal="+"'"+terminal+"'"+" AND level="+"'"+level+"'"+" AND slice="+"'"+slice+"'"+"", null );
        cursor.moveToFirst();
        JSONArray readings = new JSONArray();
        if (cursor.moveToFirst()) {
            do {
                try{
                    JSONObject reading = new JSONObject();
                    reading.put("result",new JSONArray(cursor.getString(cursor.getColumnIndex(READINGS_COLUMN_DATA))));
                    readings.put(reading);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        return readings;
    }

    public int deleteReadingsData (String app, String terminal, String level, String slice, String time) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("readings",
                "appname=? AND terminal = ? AND level = ? AND slice = ? AND milliseconds = ?",
                new String[]{app, terminal, level, slice, time});
    }


    public boolean insertReadingsData (String app, String terminal, String level, String slice, String time, String data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(READINGS_COLUMN_ID, app);
        contentValues.put(READINGS_COLUMN_TERMINAL, terminal);
        contentValues.put(READINGS_COLUMN_LEVEL, level);
        contentValues.put(READINGS_COLUMN_SLICE, slice);
        contentValues.put(READINGS_COLUMN_TIME, time);
        contentValues.put(READINGS_COLUMN_DATA, data);
        db.insert(TABLE_READINGS, null, contentValues);
        return true;
    }

    public int getPendingCount(String app) {
        String countQuery = "select * from readings where appname="+"'"+app+"'"+"";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public boolean insertMapData (JSONObject data, Context mContext) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            long milliseconds = System.currentTimeMillis();
            String level = data.get("level").toString();
            String filename = FileUtils.saveFile(mContext, data.get("data").toString(), String.valueOf(milliseconds)+"-"+String.valueOf(level));
            contentValues.put(MAP_COLUMN_APP, data.get("appname").toString());
            contentValues.put(MAP_COLUMN_TERMINAL, data.get("terminal").toString());
            contentValues.put(MAP_COLUMN_LEVEL, data.get("level").toString());
            contentValues.put(MAP_COLUMN_SLICE, data.get("sliceid").toString());
            contentValues.put(MAP_COLUMN_VALUES, filename);
            db.insert(TABLE_MAP, null, contentValues);
            return true;
        }catch (JSONException e){
            e.printStackTrace();
            return false;
        }

    }

    public int deleteMapData (String app) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("mapgenerated",
                "appname=?",
                new String[]{app});
    }

    public String[] getFilesForApp(String app){
        String countQuery = "select * from mapgenerated where appname="+"'"+app+"'"+"";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);
        String files[] = new String[cursor.getCount()];
        int ind = 0;
        while (cursor.moveToNext()){
            String filename = cursor.getString(cursor.getColumnIndex(MAP_COLUMN_VALUES));
            files[ind] = filename;
            ind++;

        }
        cursor.close();

        // return count
        return files;
    }

    public String getMapData(String app, String level, String slice, String terminal, Context mContext) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from mapgenerated where appname="+"'"+app+"'"+" AND terminal="+"'"+terminal+"'"+" AND level="+"'"+level+"'"+" AND slice="+"'"+slice+"'"+"", null );
        res.moveToFirst();
        String data = "";
        try {
            String namefile = res.getString(res.getColumnIndex(MAP_COLUMN_VALUES));
            data = FileUtils.loadJSONFromFile(mContext, namefile);

        }catch (Exception e){
            e.printStackTrace();
        }

        return data;
    }




}
