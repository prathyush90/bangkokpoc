package insuide.wisefly.wiseflypoc.manager;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import insuide.wisefly.wiseflypoc.interfaces.LocationUpdaterInterface;
import insuide.wisefly.wiseflypoc.interfaces.WisemapperService;
import okhttp3.RequestBody;
import retrofit2.Call;

public class LocalizationManager {
    int particleSize = 1500;
    static {
        System.loadLibrary("slamlib");
    }

    HandlerThread handlerThread;
    Handler messenger;
    private static final int INIT_SLAM_MANAGER = 0;
    private static final int INIT_PARTICLE_FILTER = 3;
    private static final int MOVE_ROBOTS_AND_RESAMPLE  = 1;
    private static final int MOVE_ROBOTS  = 5;
    private static final int SEND_INITIAL_LOCATION =  2;
    private final int INVALIDATE_THREAD = 4;
    public static int fromServer = 0;

    private LocationUpdaterInterface updater;

    double[][] geofence;
    private long pointerAddress;
    private long sessionId;
    private Context mContext;
    int i=0;
    private ArrayList<ArrayList<ArrayList<Double>>> walkregion = new ArrayList<>();
    private NavigationManager navManager;



    public LocalizationManager(Context context, double [][]geofence, JSONObject walkablearea, Fragment cont) {

        if (cont instanceof LocationUpdaterInterface) {
            updater = (LocationUpdaterInterface) cont;
        } else {
            throw new RuntimeException(cont.toString()
                    + " must implement Neigbours updater interface");
        }
        this.geofence = geofence;
        this.mContext = context;
        handlerThread = new HandlerThread("neighboursthread", Thread.MAX_PRIORITY);
        handlerThread.start();
        messenger = new Handler(handlerThread.getLooper(), callback);

//        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
//        actManager.getMemoryInfo(memInfo);
//        long totalMemory = memInfo.totalMem;
//
////
//        if(totalMemory <= 4800000000l){
//            particleSize = 500;
//        }
        try {
            JSONArray regionaccessible = walkablearea.getJSONArray("geofence");
            for(int k=0;k<regionaccessible.length();k++){
                JSONArray polygon = regionaccessible.getJSONArray(k);
                ArrayList<ArrayList<Double>>shape = new ArrayList<>();
                for(int i=0;i<polygon.length();i++){
                    ArrayList<Double>latLng = new ArrayList<>();
                    JSONArray point = polygon.getJSONArray(i);
                    latLng.add(point.getDouble(0));
                    latLng.add(point.getDouble(1));
                    shape.add(latLng);
                }
                walkregion.add(shape);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initLocProvider();
        navManager = new NavigationManager();
        navManager.init(context);

    }

    private Handler.Callback callback = new Handler.Callback(){

        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case INIT_SLAM_MANAGER:
                    initParticleFilter();
                    break;
                case MOVE_ROBOTS_AND_RESAMPLE:
                    moveAndResampleRobots(msg.obj);
                    break;
                case MOVE_ROBOTS:
                    moveRobots(msg.obj);
                    break;
                case SEND_INITIAL_LOCATION:
                    sendMeanLocation();
                    break;
                case INIT_PARTICLE_FILTER:
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(msg.obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    nativeInitialize(obj);
                    break;
                case INVALIDATE_THREAD:
                    invalidateJNI();
            }
            return false;
        }
    };

    private void moveAndResampleRobots(Object obj) {
        try {
            JSONObject message_obj = new JSONObject(obj.toString());
            double yaw = message_obj.getDouble("yaw");
            int axis = message_obj.getInt("movevector");
            int event = message_obj.getInt("event");
            String level = message_obj.getString("level");
            JSONArray beaconObs            = new JSONArray(message_obj.get("beacon").toString());
            double returnedlocation[] = nativeMoveRobots(pointerAddress, yaw, axis, event);
            this.updater.onNeighboursLocation(returnedlocation);
            double particles[][] = getParticlesLocations(pointerAddress, particleSize);
            JSONArray particlesJSON = new JSONArray();
            for(int i=0;i<particleSize;i++){
                JSONArray point = new JSONArray();
                for(int k=0;k<particles[0].length;k++){
                    try {
                        point.put(particles[i][k]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                particlesJSON.put(point);
            }
            JSONObject dataToServer = new JSONObject();
            dataToServer.put("level", level);
            dataToServer.put("beacon_dist_x_mean", beaconObs.getDouble(0));
            dataToServer.put("beacon_dist_y_mean", beaconObs.getDouble(1));
            dataToServer.put("std_dev", 3.0);
            dataToServer.put("weight_exp", 0.1);
            dataToServer.put("particles", particlesJSON);
            WisemapperService dataService     = RetrfoitClientManager.getWiseMapperInstance().create(WisemapperService.class);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(dataToServer).toString());
            Call<JsonElement> call = dataService.sendLocData(body);
            JsonElement val = call.execute().body();
            JsonObject jobj = val.getAsJsonObject();
            JSONObject returnVal = new JSONObject(jobj.toString());
            JSONArray weights = returnVal.getJSONArray("weights");
            double weightsArray[] = new double[weights.length()];
            for(int i=0;i<weights.length();i++){
                weightsArray[i] = weights.getDouble(i);
            }
            nativeResampleRobots(pointerAddress, weightsArray);
            fromServer += 1;
        }catch (JSONException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void moveRobots(Object obj) {
        try {
            JSONObject message_obj = new JSONObject(obj.toString());
            double yaw = message_obj.getDouble("yaw");
            int axis = message_obj.getInt("movevector");
            int event = message_obj.getInt("event");
            double returnedlocation[] = nativeMoveRobots(pointerAddress, yaw, axis, event);
            this.updater.onNeighboursLocation(returnedlocation);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void initParticleFilter() {
        Context context = (Context) this.mContext;
        if(context != null) {
            initNativeLocalizerAndMapper(this.pointerAddress,particleSize, geofence,walkregion);
        }
    }

    private void initLocProvider() {
        this.pointerAddress = nativeInitlocProvider();
    }

    public void invalidate(){
        /*
            Remove all present messages here. Required when floor change happens. Redundant information
         */
        if(messenger != null){
            messenger.removeCallbacksAndMessages(null);
        }
        Message msg = new Message();
        msg.what    = INVALIDATE_THREAD;
        msg.obj     = null;
        if(messenger != null) {
            messenger.sendMessage(msg);
        }

    }

    public void invalidateJNI(){
        if(messenger != null){
            messenger.removeCallbacksAndMessages(null);
        }
        if(handlerThread != null && handlerThread.isAlive()){
            handlerThread.quit();
        }
        if(messenger != null){
            messenger = null;
        }

        deleteNativeReference(this.pointerAddress);
        sessionId = 0l;
        navManager = null;
    }

    public void passInitMessage(){
        Message msg = new Message();
        msg.what    = INIT_SLAM_MANAGER;
        messenger.sendMessage(msg);
        //particleManager = new ParticleManager(dimensions, readings);
    }

    public void passMeanLocationMessage() {
        Message message = new Message();
        message.what    = SEND_INITIAL_LOCATION;
        messenger.sendEmptyMessage(message.what);
        sendMeanLocation();
    }


    public void nativeInitialize(JSONObject obj){
        JSONArray geof = null;
        double geofence[][] = new double[3][3];
        try {
            geof = obj.getJSONArray("geofence");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        double heading = 0;
        double b_x = 0;
        double b_y = 0;
        double b_z = 0;
        int size = 0;
        try {
            heading = obj.getDouble("heading");
            b_x = obj.getDouble("b_x");
            b_y = obj.getDouble("b_y");
            b_z = obj.getDouble("b_z");

            size = obj.getInt("bsize");

            for(int i=0;i<geof.length();i++){
                for(int k=0;k<geof.getJSONArray(0).length();k++){
                    geofence[i][k] = geof.getJSONArray(i).getDouble(k);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nativeInitParticles(pointerAddress, geofence,heading, b_x, b_y, b_z, size);
        sessionId = System.currentTimeMillis();
    }


    public void sendMeanLocation(){
        double x_mean = (geofence[0][1] + geofence[0][0]) /2;
        double y_mean = (geofence[1][1] + geofence[1][0]) /2;
        double z_mean = (geofence[2][1] + geofence[2][0]) /2;
        this.updater.onInitialLocation(new double[]{x_mean, y_mean, z_mean, 0.0, 0.0});
    }

    //please add level don't forget here
    public void passSimulateMessage(int event, int axis,  double yawChange,  double[] beaconObservations, boolean isBeacon, int size, String level){
        Message msg = new Message();
        if(event ==1 && isBeacon && size > 1){
            msg.what    = MOVE_ROBOTS_AND_RESAMPLE;
        }else{
            msg.what    = MOVE_ROBOTS;
        }

        JSONObject obj = new JSONObject();
        try{
            obj.put("yaw", yawChange);
            obj.put("event", event);
            JSONArray beaconObs = new JSONArray();
            for(int i=0;i<beaconObservations.length;i++){
                beaconObs.put(beaconObservations[i]);
            }
            obj.put("beacon", beaconObs);
            obj.put("isBeacon", isBeacon);
            obj.put("beaconsize", size);
            obj.put("movevector", axis);
            obj.put("level", level);

            msg.obj = obj;
            messenger.sendMessage(msg);

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public JSONArray getShortestRoute(LatLng source, LatLng destination, String sourcefloor, String destinationfloor){
        return navManager.getShortestPath(source, destination, sourcefloor, destinationfloor);
    }

    public void initializeParticleFilterFromGuessLocation(double geofence[][], double heading, double b_x, double b_y, double b_z, int beaconsSize){
        JSONArray fence = new JSONArray();
        for (int k=0;k<geofence.length;k++){
            JSONArray json = new JSONArray();
            for(int j=0;j<geofence[0].length;j++){
                try {
                    json.put(geofence[k][j]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            fence.put(json);
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("geofence",fence);
            obj.put("heading", heading);
            obj.put("b_x", b_x);
            obj.put("b_y", b_y);
            obj.put("b_z", b_z);
            obj.put("bsize", beaconsSize);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Message msg = new Message();
        msg.what    = 3;
        msg.obj     = obj;
        messenger.sendMessage(msg);

    }




    private native long nativeInitlocProvider();
    private native void deleteNativeReference(long address);
    private native void initNativeLocalizerAndMapper(long address,int particleSize, double[][] geofence, ArrayList<ArrayList<ArrayList<Double>>> walkregion);
    private native void nativeInitParticles(long address, double coords[][], double heading, double b_x, double b_y, double b_z, int size);
    private native double[] nativeMoveRobots(long address, double yaw, int axis, int event);
    private native double[][] getParticlesLocations(long address, int particleSize);
    private native void nativeResampleRobots(long address, double weights[]);
}
