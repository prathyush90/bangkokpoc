package insuide.wisefly.wiseflypoc.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.gson.Gson
import com.kontakt.sdk.android.ble.configuration.ScanMode
import com.kontakt.sdk.android.ble.configuration.ScanPeriod
import com.kontakt.sdk.android.ble.manager.ProximityManager
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener
import com.kontakt.sdk.android.common.profile.IBeaconDevice
import com.kontakt.sdk.android.common.profile.IBeaconRegion
import insuide.wisefly.wiseflypoc.DemoActivity
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.fragments.BangkokMapFragment
import insuide.wisefly.wiseflypoc.manager.DatabaseManager
import insuide.wisefly.wiseflypoc.manager.NetworkManager
import insuide.wisefly.wiseflypoc.manager.PreferencesManager
import insuide.wisefly.wiseflypoc.models.BeaconsPojo
import insuide.wisefly.wiseflypoc.models.FilteredBeacon
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Furqan on 24-05-2019.
 */
class CheckNearByBeacon(val context: Context) {
    var proximityManager: ProximityManager? = null
    private var deviceId: String? = null
    private var beaconsList:ArrayList<FilteredBeacon> = ArrayList();
    internal var beaconsHashMap = HashMap<String, BeaconsPojo>()
    private var beacons:JSONArray = JSONArray()
    private var dbManager: DatabaseManager? = null;
    init {
        setupProximityManager()
        startScanning()
        dbManager = DatabaseManager(context)
        getDataForBeacons()
    }

    private fun getDataForBeacons() {
        val beaconsVal = dbManager?.getBeaconsData("bkk")

        try {
            beacons = JSONArray(beaconsVal)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        for (i in 0 until beacons.length()) {
            try {
                val obj = beacons.getJSONObject(i)
                val g = Gson()
                val pojo = g.fromJson<BeaconsPojo>(obj.toString(), BeaconsPojo::class.java)
                val uniqueid = obj.getString("uniqueId")
                if (pojo.getEnabled()) {
                    beaconsHashMap.put(uniqueid, pojo)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }

        //startScanning();

    }

    private fun setupProximityManager() {
        // Create proximity manager instance

        proximityManager = ProximityManagerFactory.create(context.applicationContext)

        // Configure proximity manager basic options
        proximityManager!!.configuration()
                .scanPeriod(ScanPeriod.RANGING)
                .scanMode(ScanMode.BALANCED)
                .deviceUpdateCallbackInterval(350)
        proximityManager?.setIBeaconListener(createIBeaconListener())
        deviceId = Settings.Secure.getString(context.contentResolver,
                Settings.Secure.ANDROID_ID);
    }

    fun checkIsWorking() {
//        setupProximityManager()
//        startScanning()
    }

    private fun startScanning() {
        System.out.println("scan about to start")
        proximityManager?.let { pm ->
            pm.connect {
                pm.startScanning()
                //Toast.makeText(context, "Scanning service started.", Toast.LENGTH_SHORT).show()
            }
        }

    }

    fun stopScanning() {
        proximityManager?.let {
            it.stopScanning()
        }
    }
//pROg central second
    //ZMqq isetan second
    //nCwE Karl LaggerField
    private fun createIBeaconListener(): IBeaconListener {
        return object : SimpleIBeaconListener() {
            override fun onIBeaconDiscovered(ibeacon: IBeaconDevice?, region: IBeaconRegion?) {
                Log.d("TAG", " onIBeaconDiscovered ${ibeacon?.uniqueId}")
//                onDeviceDiscovered(ibeacon!!)
                try {
                    ibeacon?.let {
                        val uniqueid: String = it.uniqueId!!;
//                        System.out.println("scan " + uniqueid)
//                        if (uniqueid.equals("pROg")) {
//                            //central second floor
//                            sendNotification("Welcome to Central", "Check out our exciting offers from Central just for you", "1")
//                        } else if (uniqueid.equals("ZMqq")) {
//                            //isetan second floor
//                            sendNotification("Welcome to Isetan", "Check out our exciting offers from Isetan just for you", "2")
//                        } else if (uniqueid.equals("nCwE")) {
//                            //Karl Lagerfield
//                            sendNotification("Welcome to Karl Lagerfeld", "Check out our exciting offers from Karl Lagerfeld just for you", "3")
//                        }
                        var fBeacon:FilteredBeacon = FilteredBeacon()
                        fBeacon.uniqueId = ibeacon.uniqueId
                        fBeacon.distance = ibeacon.distance
                        beaconsList.add(fBeacon)

                    }
                    calculateLocation()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onIBeaconsUpdated(ibeacons: MutableList<IBeaconDevice>?, region: IBeaconRegion?) {
                try {
                    for (ibeacon: IBeaconDevice in ibeacons!!) {
                        ibeacon?.let {
                           val uniqueid: String = it.uniqueId!!;
//                            System.out.println("scan " + uniqueid)
//                            if (uniqueid.equals("pROg")) {
//                                //central second floor
//                                sendNotification("Welcome to Central", "Check out our exciting offers from Central just for you", "1")
//                            } else if (uniqueid.equals("ZMqq")) {
//                                //isetan second floor
//                                sendNotification("Welcome to Isetan", "Check out our exciting offers from Isetan just for you", "2")
//                            } else if (uniqueid.equals("nCwE")) {
//                                //Karl Lagerfield
//                                sendNotification("Welcome to Karl Lagerfeld", "Check out our exciting offers from Karl Lagerfeld just for you", "3")
//                            }
                            var fBeacon:FilteredBeacon = FilteredBeacon()
                            //fBeacon.kalmanDistance = ibeacon.distance
                            fBeacon.uniqueId = ibeacon.uniqueId
                            fBeacon.distance = ibeacon.distance
                            beaconsList.add(fBeacon)
                        }
                    }
                    calculateLocation()
                }catch (e:Exception){

                    e.printStackTrace()
                }

            }

            override fun onIBeaconLost(ibeacon: IBeaconDevice?, region: IBeaconRegion?) {
                try{
                    val uniqueid:String = ibeacon!!.uniqueId
                    val clone = beaconsList.clone() as ArrayList<FilteredBeacon>
                    val index = -1;
                    for(fbacon:FilteredBeacon in clone){
                        val id = fbacon.uniqueId
                        if(id.equals("uniqueid")){
                            beaconsList.remove(fbacon)
                            break
                        }
                    }
                    calculateLocation()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }
        }
    }

//    private fun onDeviceDiscovered(device: IBeaconDevice) {
//
//        //TODO set notification condition base on beacons
//        //sendNotification("Detected ", "beacon id ${device.uniqueId}")
//        try{
//            val uniqueid:String = device.uniqueId
//            val clone = beaconsList.clone() as ArrayList<FilteredBeacon>
//            val index = -1;
//            for(fbacon:FilteredBeacon in clone){
//                val id = fbacon.uniqueId
//                if(id.equals("uniqueid")){
//                    beaconsList.remove(fbacon)
//                    break
//                }
//            }
//            calculateLocation()
//        }catch (e:Exception){
//            e.printStackTrace()
//        }
//    }

    private fun sendDataToServer(latitude: Double, longitude: Double, headDirection: Double, deviceId: String, time: Long, confidence: Double, measurementAccuracy: Double,level:String) {
        val obj: JSONObject = JSONObject();
        obj.put("lat", latitude)
        obj.put("lng", longitude)
        obj.put("heading", headDirection)
        obj.put("deviceid", deviceId)
        obj.put("timestamp", time)
        obj.put("confidenceinmetres", confidence)
        obj.put("level", level)


        NetworkManager.sendUserLocation(obj, null);
    }

    private fun calculateLocation(){
        try {
            Collections.sort(beaconsList) { filteredBeacon, t1 -> java.lang.Double.compare(filteredBeacon.distance, t1.distance) }
           System.out.println("--- calling")
            if(beaconsList.size > 0 && !BangkokMapFragment.isRunning) {
               System.out.println("--- calculating location")
               val uniqueid = beaconsList[0].uniqueId
               val pojo:BeaconsPojo? = beaconsHashMap.get(uniqueid)
               val lati:Double = pojo!!.lat
               val long:Double = pojo!!.lng
                Toast.makeText(context, "-->"+lati.toString()+"-->"+long.toString()+"--->"+beacons.length(), Toast.LENGTH_SHORT).show()
               //sendNotification("Location", lati.toString()+"-->"+long.toString(), "")
               sendDataToServer(lati, long, 0.0, deviceId!!, System.currentTimeMillis(), 3.0, 10.0, pojo!!.level)
                System.out.println("----- sent to server")
           }
        }catch (e:Exception){
            System.out.println("----- errrrrrr")
            System.out.println(e.toString())
            e.printStackTrace()
        }
    }


    private fun sendNotification(title: String, msgBody: String, offerId: String = "") {
        val lastUpdateTime:Long = PreferencesManager.getLongKey( context, offerId+"->notif", 0L);
//        if(!(System.currentTimeMillis() - lastUpdateTime >= 10 *60 *1000)){
//            return
//        }
        PreferencesManager.addLongKey(context, offerId+"->notif", System.currentTimeMillis());
        //send to analytics here
        //sendNotificationAnalytics(deviceId!!, offerId, System.currentTimeMillis(), msgBody, title)
        val CHANNEL_ID = "channel_01"
        val intent = Intent(context, DemoActivity::class.java)
        intent.putExtra("msg_text", msgBody)
        intent.putExtra(Intent.EXTRA_TEXT, offerId)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val notificationManager = context.getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID,
                    context.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
        val pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)
        val notificationBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(Color.GREEN)
                .setContentTitle(title)
                .setContentText(msgBody)
                .setContentIntent(pendingIntent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID) // Channel ID
        }
        notificationBuilder.setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle().bigText(msgBody))
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        notificationManager.notify(Random().nextInt(10), notificationBuilder.build())

    }

    private fun sendNotificationAnalytics(deviceid:String, offerid:String, time:Long, notificationText:String,notificationTitle:String) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val obj: JSONObject = JSONObject();

        obj.put("deviceid", deviceid)
        obj.put("timestamp", time)
        obj.put("offerid", offerid)
        obj.put("notificationbody", notificationText)
        //obj.put("measurementaccuracypercentage", measurementAccuracy)
        obj.put("notificationtitle", notificationTitle)

        NetworkManager.sendNotificationAnalytics(obj, null);
    }




}