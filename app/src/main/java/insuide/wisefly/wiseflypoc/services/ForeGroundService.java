package insuide.wisefly.wiseflypoc.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import insuide.wisefly.wiseflypoc.DemoActivity;
import insuide.wisefly.wiseflypoc.R;
import insuide.wisefly.wiseflypoc.fragments.BangkokMapFragment;
import insuide.wisefly.wiseflypoc.manager.DatabaseManager;
import insuide.wisefly.wiseflypoc.manager.NetworkManager;
import insuide.wisefly.wiseflypoc.models.BeaconsPojo;
import insuide.wisefly.wiseflypoc.models.FilteredBeacon;
import insuide.wisefly.wiseflypoc.utils.ConversionUtils;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class ForeGroundService extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private Handler handler;
    private Runnable runnable;
    public static boolean isRunning = false;
    private ScanCallback mScanCallBack;
    private HashMap<String, FilteredBeacon>detectedDevices = new HashMap<>();
    private DatabaseManager dbManager;
    HashMap<String, BeaconsPojo> beaconsHashMap = new HashMap<>();
    private long sessionId;
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String input = "WiseFlyPoc is running in the background to find offers best suited for you.";
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, DemoActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_log_out)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        dbManager = new DatabaseManager(getApplicationContext());
        sessionId = System.currentTimeMillis();
        getDataForBeacons();
        isRunning = true;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                try{
                    BluetoothLeScanner bleScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
                    //bleScanner.flushPendingScanResults(mScanCallBack);
                    bleScanner.stopScan(mScanCallBack);
                }catch (Exception e){
                    e.printStackTrace();
                }
                getUserLocation();
                removeOldBeacons();
                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(runnable, 1000);

        boolean shouldStop = intent.getBooleanExtra("shouldstop", false);
        if(shouldStop){
            stopSelf();
        }
        //do heavy work on a background thread
        //stopSelf();
        mScanCallBack = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                //Toast.makeText(getApplicationContext(), "scan single", Toast.LENGTH_SHORT).show();
                readBeacon(result, callbackType);
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                super.onBatchScanResults(results);
                //Toast.makeText(getApplicationContext(), "scan multiple", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                Toast.makeText(getApplicationContext(), "scan failed"+errorCode, Toast.LENGTH_SHORT).show();
            }
        };
        startScan();
        return START_NOT_STICKY;
    }

    private void removeOldBeacons() {
        HashMap<String, FilteredBeacon>newcache = (HashMap<String, FilteredBeacon>) detectedDevices.clone();
        for (Map.Entry mapElement : newcache.entrySet()) {
            FilteredBeacon val = (FilteredBeacon) mapElement.getValue();
            //make it 5 seconds maybe
            if(System.currentTimeMillis() - val.getLastUpdateTime() >= 10*1000){
                detectedDevices.remove(mapElement.getKey());
            }
        }
    }

    private void getDataForBeacons() {
        String beaconsVal = dbManager.getBeaconsData("bkk");
        JSONArray beacons = new JSONArray();
        try {
            beacons = new JSONArray(beaconsVal);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < beacons.length(); i++) {
            try {
                JSONObject obj = beacons.getJSONObject(i);
                Gson g = new Gson();
                BeaconsPojo pojo = g.fromJson(obj.toString(), BeaconsPojo.class);
                String major = pojo.getMajor();
                String minor = pojo.getMinor();
                String key = major+"-->"+minor;
                if (pojo.getEnabled()) {
                    beaconsHashMap.put(key, pojo);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    private void getUserLocation() {
        if(BangkokMapFragment.Companion.getIsRunning()){
            detectedDevices.clear();
            startScan();
            return;
        }
        BeaconsPojo nearest = null;
        double distance_min = 99999999;
        double positions[][] = new double[detectedDevices.size()][3];
        double distances[] = new double[detectedDevices.size()];
        int index = 0;
        for (Map.Entry mapElement : detectedDevices.entrySet()) {
            FilteredBeacon beacon = (FilteredBeacon) mapElement.getValue();
            if(beacon != null){
                int power = getTransmissionPower(3);
                double expPart = (power - beacon.getRssi()) / 20.0;
                double distance = Math.pow(10.0, expPart);
                if(distance < distance_min){
                    distance_min = distance;
                    nearest = beaconsHashMap.get(mapElement.getKey());
                }

            }
        }



        if(nearest != null){
            //Toast.makeText(getApplicationContext(),String.valueOf(detectedDevices.size()), Toast.LENGTH_SHORT).show();
            String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            //Toast.makeText(getApplicationContext(), "->"+nearest.getLat()+"->"+nearest.getLng(), Toast.LENGTH_SHORT).show();
            sendDataToServer(nearest.getLat(), nearest.getLng(), 0.0, deviceId, System.currentTimeMillis(), 5.0, 2.5, nearest.getLevel());
//            JSONObject message = new JSONObject();
//            try {
//                double xyz[] = convertSphericalToCartesian(nearest.getLat(), nearest.getLng());
//                message.put("particlesdata", new JSONArray());
//                message.put("time", System.currentTimeMillis());
//                message.put("beacon_x", nearest.getMajor());
//                message.put("beacon_y", nearest.getMinor());
//                message.put("beacon_z", 0);
//                message.put("magnitude", 0);
//                message.put("turn", 0);
//                message.put("distance", 0);
//                message.put("mag_x", 0);
//                message.put("mag_y", 0);
//                message.put("mag_z", 0);
//                message.put("user_x", xyz[0]);
//                message.put("user_y", xyz[1]);
//                message.put("user_z", xyz[2]);
//                message.put("user_ori", 0);
//                message.put("max_weight", 0);
//                message.put("sessionid", sessionId);
//                //message.put("neighbors", new JSONArray());
//                passOntoServer(message, nearest.getLevel());
//            }catch (JSONException e){
//                e.printStackTrace();
//            }


        }else{
            //Toast.makeText(getApplicationContext(), "catch this", Toast.LENGTH_SHORT).show();
        }
        //detectedDevices.clear();
        startScan();
    }

    private double[] convertSphericalToCartesian(double latitude, double longitude)

    {
        double earthRadius = 6371000.0; //radius in m
        double lat = Math.toRadians(latitude);
        double lon = Math.toRadians(longitude);
        double x = earthRadius * cos(lat) * cos(lon);
        double y = earthRadius * cos(lat) * sin(lon);
        double z = earthRadius * sin(lat);
        return new double[]{x, y, z};
    }

    private void sendDataToServer(double latitude, double longitude, double headDirection, String deviceId, long time, double confidence, double measurementAccuracy,String level) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("lat", latitude);
            obj.put("lng", longitude);
            obj.put("heading", headDirection);
            obj.put("deviceid", deviceId);
            obj.put("timestamp", time);
            obj.put("confidenceinmetres", confidence);
            obj.put("level", level);


            NetworkManager.sendUserLocation(obj, null);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private int getTransmissionPower(int txPower) {
        if (txPower == 0) {
            return -115;
        } else if (txPower == 1) {
            return -84;
        } else if (txPower == 2) {
            return -81;
        } else if (txPower == 3) {
            return -77;
        } else if (txPower == 4) {
            return -72;
        } else if (txPower == 5) {
            return -69;
        } else if (txPower == 6) {
            return -65;
        } else if (txPower == 7) {
            return -59;
        }
        return -77;
    }

    private void startScan() {
        try {
            BluetoothLeScanner bleScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
            bleScanner.startScan(mScanCallBack);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void readBeacon(ScanResult scanResult, int callback){
        if(callback == ScanSettings.CALLBACK_TYPE_MATCH_LOST){
            Toast.makeText(getApplicationContext(),"lost", Toast.LENGTH_SHORT).show();
        }
        if (scanResult.getScanRecord() != null)
        {
            final byte[] scanRecord = scanResult.getScanRecord().getBytes();
            int rssi = scanResult.getRssi();
            int startByte = 2;
            boolean patternFound = false;
            while (startByte <= 5)
            {
                if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && // identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15)
                {
                    // identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }

            if (patternFound)
                {
                    // get the UUID from the hex result
                    final byte[] uuidBytes = new byte[16];
                    System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                    final UUID uuid = ConversionUtils.bytesToUuid(uuidBytes);

                    // get the major from hex result
                    final int major = ConversionUtils.byteArrayToInteger(Arrays.copyOfRange(scanRecord, startByte + 20, startByte + 22));

                    // get the minor from hex result
                    final int minor = ConversionUtils.byteArrayToInteger(Arrays.copyOfRange(scanRecord, startByte + 22, startByte + 24));
                    FilteredBeacon beacon = new FilteredBeacon();
                    beacon.setRssi(rssi);
                    beacon.setMajor(major);
                    beacon.setMinor(minor);
                    beacon.setLastUpdateTime(System.currentTimeMillis());
                    //Toast.makeText(getApplicationContext(), major+"--->"+minor+"--->"+rssi, Toast.LENGTH_SHORT).show();
                    String key = major+"-->"+minor;
                    if(beaconsHashMap.get(key) != null) {
                        detectedDevices.put(key, beacon);
                    }
                }
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public void passOntoServer(JSONObject data, String level){
        double centre_lat = 0.0;
        double centre_lng = 0.0;
        String base_url ="";
        if(level.equals("1")){
            centre_lat=13.746828;
            centre_lng=100.539123;
            base_url = "http://mapwarper.net/maps/tile/45245/";
        }else{
            centre_lat=13.746828;
            centre_lng=100.539123;
            base_url = "http://mapwarper.net/maps/tile/45246/";
        }
        try {
            String url = base_url;
            double lat = centre_lat;
            double lng = centre_lng;
            data.put("floorplanurl", url);
            data.put("center_lat", lat);
            data.put("center_lng", lng);
            data.put("appname", "bkk");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            NetworkManager.sendPositionData(getString(R.string.token), data, null);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }
}
