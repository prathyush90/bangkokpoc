package insuide.wisefly.wiseflypoc;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import insuide.wisefly.wiseflypoc.interfaces.VenueAssetResponseInterface;
import insuide.wisefly.wiseflypoc.manager.DatabaseManager;
import insuide.wisefly.wiseflypoc.manager.NetworkManager;
import insuide.wisefly.wiseflypoc.manager.PreferencesManager;
import insuide.wisefly.wiseflypoc.models.BeaconsPojo;
import insuide.wisefly.wiseflypoc.models.Download;
import insuide.wisefly.wiseflypoc.models.FloorPlanPojo;
import insuide.wisefly.wiseflypoc.models.GeoFencePojo;
import insuide.wisefly.wiseflypoc.models.VenueAndAssetsPojo;
import insuide.wisefly.wiseflypoc.utils.FileUtils;


public class SplashScreen extends AppCompatActivity implements VenueAssetResponseInterface {
    String appname = "mnl";
    String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNTk1Zjc1NmY1NGVkYWYyMzA4MjYzMTlhIiwiaWF0IjoxNDk5ODc2OTM4LCJleHAiOjE1NTE3MTY5Mzh9.9vFu_xjGMNb_w_1i-qIuxI9pUkdAMFkRqvmHmJAByHk";
    DatabaseManager dbManager;
    private boolean isRequesting = false;
    JSONArray beacons = new JSONArray();
    JSONArray floorplans = new JSONArray();
    ArrayList<FloorPlanPojo> fPlaneData = new ArrayList<>();
    ArrayList<BeaconsPojo> beaconDataList = new ArrayList<>();
    ArrayList<GeoFencePojo> mappedPathsList = new ArrayList<>();
    private JSONArray walkableAreas = new JSONArray();

    private boolean assetsAcquired = false;
    private boolean readingsAcquired = false;

    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int PERMISSION_REQUEST_CODE = 1;
    private ProgressBar mProgressBar;
    private TextView mProgressText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbManager = new DatabaseManager(SplashScreen.this);
        appname = getString(R.string.appname);
        token = getString(R.string.token);
        mProgressBar = findViewById(R.id.progress);
        mProgressText = findViewById(R.id.progress_text);
        mProgressBar.setVisibility(View.GONE);

        registerReceiver();
    }


//    private void getReadingsFromServer() {
//        long prevTime = PreferencesManager.getLongKey(SplashScreen.this, "prevtime", 0l);
//        try {
//            NetworkManager.getReadingsFromServer(token, appname, prevTime, this);
//        } catch (final Exception e) {
//            e.printStackTrace();
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(SplashScreen.this, e.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
//    }

    public void getAssetsDataIfNeeded() {
        boolean isUpdated = PreferencesManager.getBooleanKey(SplashScreen.this, "isdataupdated", false);
        if (!isUpdated) {
            try {
                if (!isRequesting) {
                    NetworkManager.getVenuesAndAssets(appname, token, this);
                    isRequesting = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(SplashScreen.this, e.toString(), Toast.LENGTH_LONG).show();
            }
        } else {
            getDataForFloorPlansAndBeacons();
            assetsAcquired = true;

        }
    }

    private void getDataForFloorPlansAndBeacons() {
        String beaconsVal = dbManager.getBeaconsData(appname);
        String floorPlansVal = dbManager.getFloorPlansData(appname);
        String mappedregion = dbManager.getGeofenceData(appname);
        if (beaconsVal.equals("[]") || floorPlansVal.equals("[]") || mappedregion.equals("[]")) {
            PreferencesManager.addBooleanKey(SplashScreen.this, "isdataupdated", false);
            getAssetsDataIfNeeded();
            return;
        }
        try {
            beacons = new JSONArray(beaconsVal);
            //FileUtils.saveFile(SplashScreen.this, beacons.toString(), "beacons_bkk.json");
            floorplans = new JSONArray(floorPlansVal);
            //FileUtils.saveFile(SplashScreen.this, floorplans.toString(), "floorplans_bkk.json");
            Gson gson = new GsonBuilder().create();
            Type listType = new TypeToken<List<FloorPlanPojo>>() {
            }.getType();
            fPlaneData = gson.fromJson(floorPlansVal, listType);
            Type beaconListType = new TypeToken<List<BeaconsPojo>>() {
            }.getType();
            beaconDataList = gson.fromJson(beaconsVal, beaconListType);

            walkableAreas = new JSONArray(mappedregion);
            //FileUtils.saveFile(SplashScreen.this, walkableAreas.toString(), "geofences_bkk.json");

            //tryDownloadingData();
            proceedToNextActivity();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSuccess(VenueAndAssetsPojo model) {
        ArrayList<FloorPlanPojo> fData = model.getfData();
        fPlaneData = fData;
        ArrayList<BeaconsPojo> bData = model.getbData();
        beaconDataList = bData;
        mappedPathsList = model.getGeofenceData();

        beacons = new JSONArray();
        for (int i = 0; i < bData.size(); i++) {
            String strObj = new Gson().toJson(bData.get(i));
            try {
                JSONObject onj = new JSONObject(strObj);
                beacons.put(onj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dbManager.insertBeaconData(appname, beacons.toString());
            }
        });

        floorplans = new JSONArray();
        for (int i = 0; i < fData.size(); i++) {
            String strObj = new Gson().toJson(fData.get(i));
            try {
                JSONObject onj = new JSONObject(strObj);
                floorplans.put(onj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        walkableAreas = new JSONArray();
        for (int i = 0; i < mappedPathsList.size(); i++) {
            String strObj = new Gson().toJson(mappedPathsList.get(i));
            try {
                JSONObject onj = new JSONObject(strObj);
                walkableAreas.put(onj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dbManager.insertFloorPlanData(appname, floorplans.toString());
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dbManager.insertGeofencePlanData(appname, walkableAreas.toString());
            }
        });
        PreferencesManager.addBooleanKey(SplashScreen.this, "isdataupdated", true);


        isRequesting = false;
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(SplashScreen.this, "Data Fetched", Toast.LENGTH_LONG).show();
//            }
//        });
        assetsAcquired = true;
        //tryDownloadingData();
        proceedToNextActivity();
    }

    private void tryDownloadingData() {
        String filenames[] = dbManager.getFilesForApp(getString(R.string.appname));
        boolean present = true;
        if (filenames.length == 0) {
            present = false;
        } else {
            for (int i = 0; i < filenames.length; i++) {
                String name = filenames[i];
                File file = new File(name);
                if (!file.exists()) {
                    present = false;
//                    getFromServer();
                    //return;
                }
            }
        }
        if (!present) {
            getFromServer();
        } else {
            proceedToNextActivity();
        }
    }

    private void getFromServer() {
        mProgressBar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(getApplicationContext(), DownloadService.class);
        String url = "/wisemapperroute/downloadallmapreadings?token=" + getString(R.string.token) + "&appname=" + getString(R.string.appname);
        //String url = "https://download.learn2crack.com/files/Node-Android-Chat.zip";
        intent.putExtra("url", url);
        startService(intent);
    }

//    @Override
//    public void onSuccess(JSONObject model) {
//        try {
//            int status = model.getInt("status");
//            if (status == 1) {
//                //use the data already present
//                //getValuesForLevelAndSlice();
//                readingsAcquired = true;
//            } else {
//                //new data
//                dbManager.deleteMapData(appname);
//                JSONArray data = model.getJSONArray("data");
//                for (int i = 0; i < data.length(); i++) {
//                    JSONObject floor = new JSONObject();
//                    JSONObject obj = data.getJSONObject(i);
//                    JSONObject floormeta = obj.getJSONObject("_id");
//                    JSONArray values = obj.getJSONArray("result");
//
//                    floor.put("terminal", floormeta.get("terminal"));
//                    floor.put("level", floormeta.get("level"));
//                    floor.put("sliceid", floormeta.get("slice"));
//                    floor.put("appname", appname);
//                    floor.put("data", values);
//                    //insert into database
//                    dbManager.insertMapData(floor, getApplicationContext());
//
//                }
////                getValuesForLevelAndSlice();
//                PreferencesManager.addLongKey(SplashScreen.this, "prevtime", System.currentTimeMillis());
//                readingsAcquired = true;
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if(assetsAcquired && readingsAcquired){
//            proceedToNextActivity();
//        }
//    }

    @Override
    public void onError(Object error) {
        Toast.makeText(SplashScreen.this, "Error while getting data", Toast.LENGTH_SHORT).show();
    }

    public void proceedToNextActivity() {
        //pois should be fetched
        Intent i = new Intent(SplashScreen.this, DemoActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            } else {
                //already permission given
                getAssetsDataIfNeeded();
                //getReadingsFromServer();
            }
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result_fine_location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result_coarse_location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int result_write_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result_read_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result_background_location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);

        int result_wifi_access = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);
        int result_wifi_change_access = ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE);
        if (result_fine_location == PackageManager.PERMISSION_GRANTED &&
                result_coarse_location == PackageManager.PERMISSION_GRANTED &&
                result_background_location == PackageManager.PERMISSION_GRANTED &&
                result_write_storage == PackageManager.PERMISSION_GRANTED &&
                result_read_storage == PackageManager.PERMISSION_GRANTED &&
                result_wifi_access == PackageManager.PERMISSION_GRANTED &&
                result_wifi_change_access == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void requestForSpecificPermission() {
        String[] permsList = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_STATE};
        ActivityCompat.requestPermissions(this, permsList, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                    getAssetsDataIfNeeded();
                    //getReadingsFromServer();
                } else {
                    //not granted
                    Toast.makeText(getApplicationContext(), "Permissions denied", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void registerReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

                Download download = intent.getParcelableExtra("download");
                mProgressBar.setProgress(download.getProgress());
                if (download.getProgress() == 100) {

                    mProgressText.setText("File Download Complete");
                    mProgressText.setText("Extracting data");
                    //extract data and store in sql
                    //String filename = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()+"file_insuide.zip";
                    unpackZip(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), "file.zip");
                    //savetodb

                    //removezip
                    deleteFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + "file.zip");
                    proceedToNextActivity();
                } else {

                    mProgressText.setText(String.format("Downloaded (%d/%d) MB", download.getCurrentFileSize(), download.getTotalFileSize()));

                }
            }
        }
    };

    public boolean deleteFile(String filename) {
        File fdelete = new File(filename);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + filename);
            } else {
                System.out.println("file not Deleted :" + filename);
                return false;
            }
        }
        return true;
    }

    ;

    private boolean unpackZip(String path, String zipname) {
        dbManager.deleteMapData(appname);
        InputStream is;
        ZipInputStream zis;
        try {
            String filename;
            is = new FileInputStream(path + "/" + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();

                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    File fmd = new File(path + filename);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(path + "/" + filename);

                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();

                String json = FileUtils.loadJSONFromFile(SplashScreen.this, path + "/" + filename);
                try {
                    JSONObject obj = new JSONObject(json);
                    JSONObject idobj = obj.getJSONObject("_id");
                    JSONArray readings = obj.getJSONArray("result");
                    String appname = getString(R.string.appname);
                    String terminal = idobj.getString("terminal");
                    String level = idobj.getString("level");
                    String slice = idobj.getString("slice");
                    JSONObject floor = new JSONObject();
                    floor.put("terminal", terminal);
                    floor.put("level", level);
                    floor.put("sliceid", slice);
                    floor.put("appname", appname);
                    floor.put("data", readings);
                    dbManager.insertMapData(floor, getApplicationContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                deleteFile(path + "/" + filename);
            }

            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
