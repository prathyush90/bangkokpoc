package insuide.wisefly.wiseflypoc.helpers;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.HashMap;

public final class AppConstant {
    private static final String PACKAGE_NAME = "insuide.wisefly.wiseflypoc";

    public static final String GEOFENCES_ADDED_KEY = PACKAGE_NAME + ".GEOFENCES_ADDED_KEY";

    /**
     * Used to set an expiration time for a geofence. After this amount of time Location Services
     * stops tracking the geofence.
     */
    private static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;

    /**
     * For this sample, geofences expire after twelve hours.
     */
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS =
            GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 1609; // 1 mile, 1.6 km

    /**
     * Map for storing information about airports in the San Francisco bay area.
     */
    public static final HashMap<String, LatLng> AREA_LANDMARKS = new HashMap<>();

    static {
        // WiseMAll
        AREA_LANDMARKS.put("CPN", new LatLng(13.746495, 100.539441));

        // Office.
        AREA_LANDMARKS.put("ECO_STAR", new LatLng(19.167416, 72.853239));
    }
}
