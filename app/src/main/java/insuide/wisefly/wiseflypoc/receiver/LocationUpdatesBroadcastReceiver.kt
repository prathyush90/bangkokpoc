package insuide.wisefly.wiseflypoc.receiver

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ProcessLifecycleOwner
import com.google.android.gms.location.LocationResult
import insuide.wisefly.wiseflypoc.services.CheckNearByBeacon


/**
 * Created by Furqan on 23-09-2018.
 */
class LocationUpdatesBroadcastReceiver : BroadcastReceiver() {
    private val TAG = "LocationUpdatesBroadcastReceiver"

    companion object {
        var checkNearByBeacon: CheckNearByBeacon? = null
        val ACTION_PROCESS_UPDATES = "com.google.android.gms.location.sample.backgroundlocationupdates.action" + ".PROCESS_UPDATES"
    }


    override fun onReceive(context: Context, intent: Intent?) {

        if (intent != null) {
            val action = intent.action
            Log.d(TAG,"onReceive ${intent.action}")
            if (ACTION_PROCESS_UPDATES == action) {
                Log.d(TAG,"onReceive  inside${intent.action}")
                val result = LocationResult.extractResult(intent)
                if (result != null) {
                    val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val locations = result.locations
                    System.out.println("---->")
//                    val cochinLocation = Location("cochin")
//                    cochinLocation.latitude = AppConstant.CENTRAL_LAT
//                    cochinLocation.longitude = AppConstant.CENTRAL_LONG
//                    if (locations[0].distanceTo(cochinLocation) < AppConstant.GEOFENCE_RADIUS_IN_METERS) {

                    // check app is in background
                    //val background = true;//ProcessLifecycleOwner.get().lifecycle.currentState == Lifecycle.State.CREATED
                        if (checkNearByBeacon?.proximityManager == null && mBluetoothAdapter.isEnabled
                                ) {
                            checkNearByBeacon = CheckNearByBeacon(context)

                        }
                        if (checkNearByBeacon?.proximityManager != null ) {
                            checkNearByBeacon?.checkIsWorking()
                        }

//                    if (checkNearByBeacon?.proximityManager != null ) {
//                        checkNearByBeacon?.stopScanning()
//                        checkNearByBeacon?.proximityManager = null
//                    }

                   /* } else {
                        if (checkNearByBeacon?.proximityManager != null) {
                            checkNearByBeacon?.stopScanning()
                            checkNearByBeacon = null

                        }
                        *//*offerNotificationService?.let {

                            //                            it.stopForeground(true)
                            it.stopSelf()
                            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                            notificationManager.cancelAll()
                            OfferNotificationService.isRunning = false
                            offerNotificationService = null
                        }*//*

                    }*/
                }
            }

        }

    }
}