package insuide.wisefly.wiseflypoc.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;
import insuide.wisefly.wiseflypoc.services.ForeGroundService;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!ForeGroundService.isRunning) {
            Intent serviceIntent = new Intent(context, ForeGroundService.class);
            serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
            ContextCompat.startForegroundService(context, serviceIntent);
        }
        //Toast.makeText(context, "reboot hua", Toast.LENGTH_SHORT).show();
    }
}
