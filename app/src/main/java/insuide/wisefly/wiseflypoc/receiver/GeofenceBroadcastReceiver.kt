package insuide.wisefly.wiseflypoc.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import insuide.wisefly.wiseflypoc.services.GeofenceTransitionsJobIntentService

class GeofenceBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        GeofenceTransitionsJobIntentService.enqueueWork(context, intent)
    }
}