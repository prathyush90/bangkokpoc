package insuide.wisefly.wiseflypoc.fragments

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.inflate
import insuide.wisefly.wiseflypoc.utils.CommonUtils
import kotlinx.android.synthetic.main.item_contact.view.*

/**
 * Created by Furqan on 15-10-2019.
 */
class PoiContactAdapter(val stringList: List<Pair<Int, String>>) :
        RecyclerView.Adapter<PoiContactAdapter.PoiContactViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PoiContactViewHolder {
        val view = parent.inflate(R.layout.item_contact)
        return PoiContactViewHolder(view)
    }

    override fun getItemCount() = stringList.size
    override fun onBindViewHolder(viewHolder: PoiContactViewHolder, p1: Int) {
        viewHolder.bind(stringList[p1])
    }

    class PoiContactViewHolder(val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        lateinit var pair: Pair<Int, String>

        init {
            view.setOnClickListener(this)
            view.setBackgroundResource(R.drawable.layout_selector)
            view.elevation = CommonUtils.dpFromPx(view.context, 2f)
        }

        fun bind(data: Pair<Int, String>) {
            this.pair = data
            view.txtContact.text = data.second
            view.imgCall.setImageResource(data.first)
        }

        override fun onClick(p0: View?) {
            when (pair.first) {
                R.drawable.ic_phone -> {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:${pair.second}")
                    itemView.context.startActivity(intent)
                }
                R.drawable.ic_mail -> {
                    val intent = Intent(Intent.ACTION_SENDTO).apply {
                        data = Uri.parse("mailto:") // only email apps should handle this
                        putExtra(Intent.EXTRA_EMAIL, arrayOf(pair.second))
                    }
//                    if (intent.resolveActivity(packageManager) != null) {
                    itemView.context.startActivity(intent)
//                    }

                }
            }

        }
    }
}