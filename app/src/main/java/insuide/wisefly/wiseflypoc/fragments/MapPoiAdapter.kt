package insuide.wisefly.wiseflypoc.fragments

import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.fragments.PoiDetailDialog
import insuide.wisefly.wiseflypoc.inflate
import insuide.wisefly.wiseflypoc.models.PoiDetail
import insuide.wisefly.wiseflypoc.utils.CommonUtils
import insuide.wisefly.wiseflypoc.utils.PoiDiffUtilCallback
import kotlinx.android.synthetic.main.item_poi.view.*


/**
 * Created by Furqan on 19-04-2018.
 */
class MapPoiAdapter(val poiFragment: Fragment, var poiList: ArrayList<PoiDetail>) : RecyclerView.Adapter<MapPoiAdapter.PoiViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PoiViewHolder {
        val view = parent.inflate(R.layout.item_poi)
        return PoiViewHolder(view)
    }

    override fun getItemCount(): Int = poiList.size

    fun updateList(poiList: ArrayList<PoiDetail>) {
//      this.poiList = poiList
//        notifyDataSetChanged()
        val diffResult = DiffUtil.calculateDiff(PoiDiffUtilCallback(this.poiList, poiList))
        this.poiList.clear()
        this.poiList.addAll(poiList)
        diffResult.dispatchUpdatesTo(this)

    }

    override fun onBindViewHolder(holder: PoiViewHolder, position: Int) {
        holder.bindItem(poiList[position])
//        holder.bindItem()
    }

    inner class PoiViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        val view = v
        lateinit var poiDetail: PoiDetail
        var drawableId: Int = R.drawable.ic_services

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            if (poiFragment is PoiFragment) {
                val poiDetailDialog = PoiDetailDialog.newInstance(poiFragment, poiDetail)
                poiDetailDialog.show(poiFragment.fragmentManager!!, "PoiDetailDialog")
                /*     val bottomSheetDialog = BottomSheetDialog(itemView.context)
                     val poiDetailView = poiFragment.layoutInflater.inflate(R.layout.poi_detail, null)
                     bottomSheetDialog.setContentView(poiDetailView)
                     val params = (poiDetailView.parent as View)
                             .layoutParams as CoordinatorLayout.LayoutParams
     //                val behavior = params.behavior

                     val view = poiDetailView.parent as View
                     view.post {
                         val parent = view?.parent as View
                         val behavior = params.behavior
                         val bottomSheetBehavior = behavior as BottomSheetBehavior
                         bottomSheetBehavior.peekHeight = view.measuredHeight
                         parent.setBackgroundColor(Color.TRANSPARENT)
                     }
                     (poiDetailView.parent as View).setBackgroundColor(Color.TRANSPARENT)


                     val pairList = ArrayList<Pair<Int, String>>()
                     if (poiDetail.mobile_number.isNotEmpty() && poiDetail.mobile_number.length > 1) {
                         val mobileNo = poiDetail.mobile_number.split(",")
                         mobileNo.forEach { pairList.add(Pair(R.drawable.ic_phone, it)) }
                     }
                     if (poiDetail.email.isNotEmpty()) {
                         pairList.add(Pair(R.drawable.ic_mail, poiDetail.email))
                     }
                     if (pairList.isEmpty()) {
                         poiDetailView.recyclerViewPoiInfo.visibility = View.GONE
                     } else {
                         poiDetailView.recyclerViewPoiInfo.layoutManager = LinearLayoutManager(itemView.context)
                         poiDetailView.recyclerViewPoiInfo.adapter = PoiContactAdapter(pairList)
                     }

                     val pairText = when (LocaleManager.getLanguage(itemView.context)) {
                         LocaleManager.LANGUAGE_ENGLISH -> {
                             Pair(poiDetail.poiMultilingual?.en?.name,
                                     poiDetail.poiMultilingual?.en?.description)
                         }
                         LocaleManager.LANGUAGE_ARABIC -> {
                             Pair(poiDetail.poiMultilingual?.ar?.name,
                                     poiDetail.poiMultilingual?.ar?.description)
                         }
                         LocaleManager.LANGUAGE_HINDI -> {
                             Pair(poiDetail.poiMultilingual?.hi?.name,
                                     poiDetail.poiMultilingual?.hi?.description)
                         }
                         LocaleManager.LANGUAGE_RUSSSIAN -> {
                             Pair(poiDetail.poiMultilingual?.ru?.name,
                                     poiDetail.poiMultilingual?.ru?.description)
                         }
                         else -> {
                             Pair(poiDetail.poiMultilingual?.en?.name,
                                     poiDetail.poiMultilingual?.en?.description)
                         }
                     }

                     pairText.first?.let {
                         view.dialogTxtShopName.text = it
                     } ?: kotlin.run {
                         view.dialogTxtShopName.text = poiDetail.poiMultilingual?.en?.name
                     }
                     pairText.second?.let {
                         view.dialogTxtDesc.text = it
                     } ?: kotlin.run {
                         view.dialogTxtDesc.text = poiDetail.poiMultilingual?.en?.description
                     }

                     poiDetailView.dialogTxtCategoryName.text = poiDetail.category
                     poiDetailView.txt_Show_map.text = itemView.context.getString(R.string.route_preview)
                     GlideApp.with(itemView.context).load(this.poiDetail.poiUrl)
                             .into(poiDetailView.imgShop)
                     when (poiDetail.category?.toLowerCase()) {
                         "food & drink" -> {
                             poiDetailView.digImgCategory.setImageResource(R.drawable.ic_food)
                             poiDetailView.imgFacility.visibility = View.INVISIBLE
                         }
                         "shopping" -> {
                             poiDetailView.digImgCategory.setImageResource(R.drawable.ic_shop)
                             poiDetailView.imgFacility.visibility = View.INVISIBLE
                         }
                         else -> {

                             if (poiDetail.poiUrl.isNullOrEmpty()) {
                                 poiDetailView.imgFacility.visibility = View.VISIBLE
                                 try {
                                     poiDetailView.imgFacility.setImageResource(drawableId)
                                 } catch (e: NumberFormatException) {
                                     e.printStackTrace()
                                 }

                             } else {
                                 poiDetailView.imgFacility.visibility = View.INVISIBLE
                             }
                             poiDetailView.digImgCategory.visibility = View.INVISIBLE
                             poiDetailView.dialogTxtCategoryName.visibility = View.GONE
                         }
                     }
                     poiDetailView.btnShowMap.setOnClickListener {
                         bottomSheetDialog.dismiss()
                         poiFragment.drawPreview(poiDetail)
                     }
                     bottomSheetDialog.show()
                     poiFragment.collapse(poiDetail)*/
            }

        }

        fun bindItem(poiDetail: PoiDetail) {
            this.poiDetail = poiDetail

            CommonUtils.setCatName(itemView.context, view.txtPoi, poiDetail.poiMultilingual)

            when (poiDetail.levelid) {
                "0" -> {
                    view.txtDis.text = itemView.context.getString(R.string.groundFloor)
                }
                "1" -> {
                    view.txtDis.text = itemView.context.getString(R.string.firstFloor)
                }

            }
            when (poiDetail.category?.toLowerCase()) {
                "food & drink" -> {
                    drawableId = R.drawable.food
                    view.imgPoi.setImageResource(R.drawable.food)
                }
                "shopping" -> {
                    drawableId = R.drawable.shopping
                    view.imgPoi.setImageResource(R.drawable.shopping)
                }
                "utilities" -> {
                    when (poiDetail.subcategory?.toLowerCase()) {
                        "escalator" -> {
                            drawableId = R.drawable.escalator
                            view.imgPoi.setImageResource(R.drawable.escalator)
                        }
                        "lift" -> {
                            drawableId = R.drawable.lift
                            view.imgPoi.setImageResource(R.drawable.lift)
                        }
                        "staircase" -> {
                            drawableId = R.drawable.stairs
                            view.imgPoi.setImageResource(R.drawable.stairs)
                        }
                        "drinking water" -> {
                            drawableId = R.drawable.water
                            view.imgPoi.setImageResource(R.drawable.water)
                        }
                        "toilet" -> {
                            drawableId = R.drawable.toilet
                            view.imgPoi.setImageResource(R.drawable.toilet)
                        }
                        "prayer room" -> {
                            drawableId = R.drawable.prayer_room
                            view.imgPoi.setImageResource(R.drawable.prayer_room)
                        }
                        "smoking room" -> {
                            drawableId = R.drawable.smoking
                            view.imgPoi.setImageResource(R.drawable.smoking)
                        }
                        "baby changing room" -> {
                            drawableId = R.drawable.baby
                            view.imgPoi.setImageResource(R.drawable.baby)
                        }
                        "service desk" -> {
                            drawableId = R.drawable.info_helpdesk
                            view.imgPoi.setImageResource(R.drawable.info_helpdesk)
                        }
                        "help desk" -> {
                            drawableId = R.drawable.info_helpdesk
                            view.imgPoi.setImageResource(R.drawable.info_helpdesk)
                        }
                        "fids" -> {
                            drawableId = R.drawable.fids
                            view.imgPoi.setImageResource(R.drawable.fids)
                        }
                        "customs" -> {
                            drawableId = R.drawable.customs
                            view.imgPoi.setImageResource(R.drawable.customs)
                        }
                        "baggage claim" -> {
                            drawableId = R.drawable.baggage
                            view.imgPoi.setImageResource(R.drawable.baggage)
                        }
                        "passport control" -> {
                            drawableId = R.drawable.passport
                            view.imgPoi.setImageResource(R.drawable.passport)
                        }
                        "security screening" -> {
                            drawableId = R.drawable.security
                            view.imgPoi.setImageResource(R.drawable.security)
                        }
                        "check-in counters" -> {
                            drawableId = R.drawable.check_in
                            view.imgPoi.setImageResource(R.drawable.check_in)

                        }
                        "gates" -> {
                            drawableId = R.drawable.gate
                            view.imgPoi.setImageResource(R.drawable.gate)
                        }
                        "transit" -> {
                            drawableId = R.drawable.transit
                            view.imgPoi.setImageResource(R.drawable.transit)
                        }
                        "lost and found" -> {
                            drawableId = R.drawable.lost_found
                            view.imgPoi.setImageResource(R.drawable.lost_found)
                        }
                        "charging" -> {
                            drawableId = R.drawable.power
                            view.imgPoi.setImageResource(R.drawable.power)
                        }
                        "exit" -> {
                            drawableId = R.drawable.exit
                            view.imgPoi.setImageResource(R.drawable.exit)
                        }
                        "seating" -> {
                            drawableId = R.drawable.seating
                            view.imgPoi.setImageResource(R.drawable.seating)
                        }
                        "bus" -> {
                            drawableId = R.drawable.bus
                            view.imgPoi.setImageResource(R.drawable.bus)
                        }
                        "car" -> {
                            drawableId = R.drawable.car
                            view.imgPoi.setImageResource(R.drawable.car)
                        }
                        "metro" -> {
                            drawableId = R.drawable.metro
                            view.imgPoi.setImageResource(R.drawable.metro)
                        }
                        "train" -> {
                            drawableId = R.drawable.metro
                            view.imgPoi.setImageResource(R.drawable.metro)
                        }
                        "medical" -> {
                            drawableId = R.drawable.medical
                            view.imgPoi.setImageResource(R.drawable.medical)
                        }
                        "disabled toilet" -> {
                            drawableId = R.drawable.disabled_toilet
                            view.imgPoi.setImageResource(R.drawable.disabled_toilet)
                        }
                        "female toilet" -> {
                            drawableId = R.drawable.female_toilet
                            view.imgPoi.setImageResource(R.drawable.female_toilet)
                        }
                    }
                }
                "services" -> {
                    when (poiDetail.subcategory?.toLowerCase()) {
                        "lounge" -> {
                            drawableId = R.drawable.lounge
                            view.imgPoi.setImageResource(R.drawable.lounge)
                        }
                        "currency exchange" -> {
                            drawableId = R.drawable.exchange
                            view.imgPoi.setImageResource(R.drawable.exchange)

                        }
                        "ticket counter" -> {
                            drawableId = R.drawable.ticket
                            view.imgPoi.setImageResource(R.drawable.ticket)
                        }
                        "children's play area" -> {
                            drawableId = R.drawable.play
                            view.imgPoi.setImageResource(R.drawable.play)
                        }
                        "atm" -> {
                            drawableId = R.drawable.atm
                            view.imgPoi.setImageResource(R.drawable.atm)
                        }
                        "telephone" -> {
                            drawableId = R.drawable.phone
                            view.imgPoi.setImageResource(R.drawable.phone)
                        }
                        "car" -> {
                            drawableId = R.drawable.car
                            view.imgPoi.setImageResource(R.drawable.car)
                        }
                    }
                }
            }
        }
    }
}