package insuide.wisefly.wiseflypoc.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.netpiper.sia.manager.LocaleManager
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.models.PoiDetail
import kotlinx.android.synthetic.main.poi_detail.*
import kotlinx.android.synthetic.main.poi_detail.view.*

/**
 * Created by Furqan on 22-10-2019.
 */
class PoiDetailDialog : BottomSheetDialogFragment() {
    lateinit var poiDetail: PoiDetail
    private lateinit var poiFragment: Fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            poiDetail = it.getParcelable(Intent.EXTRA_TEXT)!!

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.poi_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        if (poiFragment is PoiFragment) {
        val pairList = ArrayList<Pair<Int, String>>()
        if (poiDetail.mobile_number.isNotEmpty() && poiDetail.mobile_number.length > 1) {
            val mobileNo = poiDetail.mobile_number.split(",")
            mobileNo.forEach { pairList.add(Pair(R.drawable.ic_phone, it)) }
        }
        if (poiDetail.email.isNotEmpty()) {
            pairList.add(Pair(R.drawable.ic_mail, poiDetail.email))
        }
        if (pairList.isEmpty()) {
            view.recyclerViewPoiInfo.visibility = View.GONE
        } else {
            view.recyclerViewPoiInfo.layoutManager = LinearLayoutManager(context)
            view.recyclerViewPoiInfo.adapter = PoiContactAdapter(pairList)
        }

        val pairText = when (LocaleManager.getLanguage(context!!)) {
            LocaleManager.LANGUAGE_ENGLISH -> {
                Pair(poiDetail.poiMultilingual?.en?.name,
                        poiDetail.poiMultilingual?.en?.description)
            }
            LocaleManager.LANGUAGE_ARABIC -> {
                Pair(poiDetail.poiMultilingual?.ar?.name,
                        poiDetail.poiMultilingual?.ar?.description)
            }
            LocaleManager.LANGUAGE_HINDI -> {
                Pair(poiDetail.poiMultilingual?.hi?.name,
                        poiDetail.poiMultilingual?.hi?.description)
            }
            LocaleManager.LANGUAGE_RUSSSIAN -> {
                Pair(poiDetail.poiMultilingual?.ru?.name,
                        poiDetail.poiMultilingual?.ru?.description)
            }
            else -> {
                Pair(poiDetail.poiMultilingual?.en?.name,
                        poiDetail.poiMultilingual?.en?.description)
            }
        }

        pairText.first?.let {
            view.dialogTxtShopName.text = it
        } ?: kotlin.run {
            view.dialogTxtShopName.text = poiDetail.poiMultilingual?.en?.name
        }
        pairText.second?.let {
            view.dialogTxtDesc.text = it
        } ?: kotlin.run {
            view.dialogTxtDesc.text = poiDetail.poiMultilingual?.en?.description
        }

        view.dialogTxtCategoryName.text = poiDetail.category
        view.txt_Show_map.text = getString(R.string.route_preview)
        Glide.with(context!!).load(this.poiDetail.poiUrl)
                .into(view.imgShop)
        when (poiDetail.category?.toLowerCase()) {
            "food & drink" -> {
                view.digImgCategory.setImageResource(R.drawable.ic_food)
                view.imgFacility.visibility = View.INVISIBLE
            }
            "shopping" -> {
                view.digImgCategory.setImageResource(R.drawable.ic_shop)
                view.imgFacility.visibility = View.INVISIBLE
            }
            else -> {

                if (poiDetail.poiUrl.isNullOrEmpty()) {
                    view.imgFacility.visibility = View.VISIBLE
                    try {
                        view.imgFacility.setImageResource(R.drawable.ic_services)
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                } else {
                    view.imgFacility.visibility = View.INVISIBLE
                }
                view.digImgCategory.visibility = View.INVISIBLE
                view.dialogTxtCategoryName.visibility = View.GONE
            }
        }
        view.btnShowMap.setOnClickListener {
            dismiss()
            when (poiFragment) {
                is PoiFragment -> {
                    (poiFragment as PoiFragment).drawPreview(poiDetail)
                }
                is BangkokMapFragment -> {
                    (poiFragment as BangkokMapFragment).preview(poiDetail)
                }
            }
        }

        /* } else
             if (poiFragment is MapFragment) {
             (poiFragment as MapFragment).showPoi(poiDetail)

         }*/

        imgHeart.setOnClickListener {


        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog

        if (dialog != null) {
            val bottomSheet = dialog.findViewById<FrameLayout>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        }
        view?.post {
            val parent = view?.parent as View
            val params = (parent).layoutParams as CoordinatorLayout.LayoutParams
            val behavior = params.behavior
            val bottomSheetBehavior = behavior as BottomSheetBehavior
            bottomSheetBehavior.peekHeight = view!!.measuredHeight
            parent.setBackgroundColor(Color.TRANSPARENT)
        }
    }


    companion object {

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param adsType @class AdsType
         * @return A new instance of fragment AdsFragment.
         */
        @JvmStatic
        fun newInstance(poiFragment: Fragment, poiDetail: PoiDetail) =
                PoiDetailDialog().apply {
                    this.poiFragment = poiFragment
                    arguments = Bundle().apply {
                        putParcelable(Intent.EXTRA_TEXT, poiDetail)

                    }
                }

    }


}