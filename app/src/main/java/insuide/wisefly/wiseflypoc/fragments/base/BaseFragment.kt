package com.netpiper.sia.base


import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment


/**
 * A simple [Fragment] subclass.
 */
abstract class BaseFragment<in V : BaseMvpView, T : BaseMvpPresenter<V>> : Fragment(), BaseMvpView {

    protected abstract var mPresenter: T
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this as V)

    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView();
    }

    override fun showLoading() {

    }

    override fun hideLoading() {
    }

    override fun onError(resId: Int) {
    }

    override fun onError(message: String) {
    }

    override fun showMessage(message: String) {

    }

    override fun showMessage(resId: Int) {
    }

    override fun isNetworkConnected(): Boolean {
        return false
    }

    override fun getContext(): Context = super.getContext()!!
}// Required empty public constructor
