package com.netpiper.sia.ui.map

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.media.AudioManager
import android.os.Build
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.style.expressions.Expression.*
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.Property.ICON_ROTATION_ALIGNMENT_MAP
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.CannotAddSourceException
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.netpiper.sia.base.BaseMvpPresenterImpl
import com.netpiper.sia.manager.ApiManager
import com.netpiper.sia.manager.LocaleManager
import com.netpiper.sia.model.FlightResponse
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.fragments.ApiMap
import insuide.wisefly.wiseflypoc.models.MapContractor
import insuide.wisefly.wiseflypoc.utils.CommonUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.set

/**
 * Created by Furqan on 22-01-2018.
 */
class MapPresenter : BaseMvpPresenterImpl<MapContractor.View>(), MapContractor.Presenter {

    override fun loadPoi(token: String, appname: String) {
        val hashMapRequest = HashMap<String, String>()
        hashMapRequest["appname"] = appname
        hashMapRequest["token"] = token
        val apiMap: ApiMap = ApiManager.getClient(mView!!.getContext(), ApiMap::class.java)
        apiMap.fetchPois(hashMapRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : DisposableObserver<FlightResponse>() {
                    override fun onNext(flightResponse: FlightResponse) {
                        try {
                            if (flightResponse.success) {
                                mView?.showAllPoi(flightResponse.data)
                            } else {
                                if (flightResponse.status == -1 &&
                                        flightResponse.message.equals("Failed to authenticate token.", true)) {
                                    mView?.reload()
                                }
                            }
                        } catch (npe: KotlinNullPointerException) {
                            npe.printStackTrace()
                        } catch (ise: IllegalArgumentException) {
                            ise.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        try {
                            mView?.showMessage(e.message!!)
                        } catch (npe: KotlinNullPointerException) {
                            npe.printStackTrace()
                        }

                    }

                    override fun onComplete() {

                    }
                })
    }



    override fun poiImageMap(): IntArray {
        val intArray = intArrayOf(R.drawable.atm, R.drawable.baby, R.drawable.baggage,
                R.drawable.bus, R.drawable.car, R.drawable.check_in,
                R.drawable.customs, R.drawable.emergency_exit, R.drawable.entry,
                R.drawable.escalator, R.drawable.exchange, R.drawable.exit,
                R.drawable.female_toilet, R.drawable.fids, R.drawable.food,
                R.drawable.gate, R.drawable.disabled_toilet, R.drawable.info_helpdesk,
                R.drawable.lift, R.drawable.lost_found, R.drawable.lounge,
                R.drawable.medical, R.drawable.male_toilet, R.drawable.metro,
                R.drawable.passport, R.drawable.phone, R.drawable.play,
                R.drawable.power, R.drawable.prayer_room, R.drawable.seating,
                R.drawable.security, R.drawable.shopping, R.drawable.smoking,
                R.drawable.stairs, R.drawable.ticket, R.drawable.toilet,
                R.drawable.transit, R.drawable.water

        )

        return intArray
    }

    fun filterSymbolLayerPoi(mapboxMap: MapboxMap, layerId: String, filter: String, visibility: Boolean) {
        try {
            val poiSymbolLayer = mapboxMap.style?.getLayer(layerId) as SymbolLayer
            if (visibility) {
                val filterLayer = poiSymbolLayer.withFilter(all(neq(get("color"), filter)))
                filterLayer.setProperties(visibility(Property.VISIBLE))
            } else {
                val filterLayer = poiSymbolLayer.withFilter(all(neq(get("level"), filter)))
                filterLayer.setProperties(visibility(Property.VISIBLE))
            }
            val position = CameraPosition.Builder()
                    .target(LatLng(0.0,0.0)) // Sets the new camera position
                    .build() // Creates a CameraPosition from the builder

            mapboxMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position))
        } catch (e: TypeCastException) {
            e.printStackTrace()
        }
//        notFilterLayer.setProperties(visibility(Property.VISIBLE))
    }
//
    fun updatePeerLocLayer(mapboxMap: MapboxMap, sourceId: String, layerId: String, features: ArrayList<Feature>) {

        val featureCollection = FeatureCollection.fromFeatures(features)
        val geoJsonSource = mapboxMap.style?.getSourceAs<GeoJsonSource>(sourceId)
        geoJsonSource?.setGeoJson(featureCollection)
        val layer = mapboxMap.style?.getLayer(layerId)
        if (layer != null) {
            val poiSymbolLayer = layer as SymbolLayer
            poiSymbolLayer.withProperties(
                    iconImage("{image}"))
        }

//        notFilterLayer.setProperties(visibility(Property.VISIBLE))
    }

    override fun addSymbolLayer(mapboxMap: MapboxMap, sourceId: String, layerId: String, features: ArrayList<Feature>) {
        if (mapboxMap.style?.getLayer(layerId) != null) {
            mapboxMap.style?.removeLayer(layerId)
            mapboxMap.style?.removeSource(sourceId)
        }
        val context = mView!!.getContext()
        val options = GeoJsonOptions()
                .withCluster(true)
                .withClusterRadius(30)
        val foodFeatureCollection = FeatureCollection.fromFeatures(features)
        val geoJsonFoodSource = GeoJsonSource(sourceId, foodFeatureCollection, options)
        mapboxMap.style?.addSource(geoJsonFoodSource)
        val language: String? = LocaleManager.getLanguage(context)
        val markersFoodNDrink = SymbolLayer(layerId, sourceId)
        markersFoodNDrink.withProperties(
                textField(
                        when (language) {
                            LocaleManager.LANGUAGE_ARABIC -> "{name_ar}"
                            LocaleManager.LANGUAGE_HINDI -> "{name_hi}"
                            LocaleManager.LANGUAGE_RUSSSIAN -> "{name_ru}"
                            else -> "{name}"
                        }),
                textOffset(if (language == "ar") {
                    arrayOf(0f, -1.5f)
                } else {
                    arrayOf(0f, -1.2f)
                }),
                textLineHeight(0.9f),
                textHaloColor(ContextCompat.getColor(context, R.color.symbol_halo_color)),
                textHaloWidth(1f),
                textJustify(Property.TEXT_JUSTIFY_CENTER),
                textAnchor(Property.TEXT_ANCHOR_BOTTOM),
                textIgnorePlacement(false),
                iconImage("{image}"),
                iconSize(interpolate(linear(), zoom(),
                        stop(17f, 0f),
                        stop(18f, 1f))),
                iconOpacity(interpolate(linear(), zoom(),
                        stop(17f, 0f),
                        stop(18f, 1f))),
                iconAllowOverlap(false),
                textAllowOverlap(false),
                textSize(interpolate(linear(),
                        zoom(), stop(17f, 4f),
                        stop(18f, 16f))),
                textOptional(true),
                textColor(match(get("color"), color(Color.BLACK),
                        stop("service", color(ContextCompat.getColor(context, R.color.colorBlueDark))),
                        stop("utility", color(ContextCompat.getColor(context, R.color.colorBloodyDry))),
                        stop("food", color(ContextCompat.getColor(context, R.color.colorFood))),
                        stop("shop", color(ContextCompat.getColor(context, R.color.colorShopping))))),
                textFont(arrayOf("Work Sans SemiBold", "Noto Sans Arabic Bold", "Roboto Regular",
                        "Devanagari Sangam MN Regular")),
                textOpacity(interpolate(linear(), zoom(),
                        stop(17f, 0f),
                        stop(18f, 1f)))
        )
        mapboxMap.style?.addLayer(markersFoodNDrink)
    }


    fun addPeerSymbolLayer(mapboxMap: MapboxMap, sourceId: String, layerId: String, features: ArrayList<Feature>) {
        if (mapboxMap.style?.getLayer(layerId) != null) {
            mapboxMap.style?.removeLayer(layerId)
            mapboxMap.style?.removeSource(sourceId)
        }
        val context = mView!!.getContext()
        val foodFeatureCollection = FeatureCollection.fromFeatures(features)
        val geoJsonFoodSource = GeoJsonSource(sourceId, foodFeatureCollection)
        try {
            mapboxMap.style?.addSource(geoJsonFoodSource)
            val markersFoodNDrink = SymbolLayer(layerId, sourceId)
            markersFoodNDrink.withProperties(
                    textField("{name}"),
                    textOffset(arrayOf(0f, -1.2f)),
                    iconRotationAlignment(ICON_ROTATION_ALIGNMENT_MAP),
                    textLineHeight(0.9f),
                    textHaloColor(ContextCompat.getColor(context, R.color.symbol_halo_color)),
                    textHaloWidth(1f),
                    textJustify(Property.TEXT_JUSTIFY_CENTER),
                    textAnchor(Property.TEXT_ANCHOR_BOTTOM),
                    iconImage("{image}"),
                    iconAllowOverlap(true),
                    textAllowOverlap(true),
                    textOptional(false),
                    textColor(color(ContextCompat.getColor(context, R.color.colorInsuidePurple))),
                    textFont(arrayOf("Work Sans SemiBold", "Noto Sans Arabic Bold", "Roboto Regular",
                            "Devanagari Sangam MN Regular"))

            )
            mapboxMap.style?.addLayer(markersFoodNDrink)
        } catch (ue: CannotAddSourceException) {
            ue.printStackTrace()
        }
    }

    fun addDottedLine(mapboxMap: MapboxMap, sourceId: String, layerId: String, points: ArrayList<Point>) {

        if (mapboxMap.style?.getLayer(layerId) != null) {
            mapboxMap.style?.removeLayer(layerId)
        }
        val lineString = LineString.fromLngLats(points)
        val featureCollection = FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(lineString)))

        val geoJsonSource = GeoJsonSource(sourceId, featureCollection)
        try {
            geoJsonSource.setGeoJson(featureCollection.toJson())
            mapboxMap.style?.addSource(geoJsonSource)
        } catch (e: CannotAddSourceException) {
            e.printStackTrace()
        }
        val dotedLineLayer = LineLayer(layerId, sourceId)

        // The layer properties for our line. This is where we make the line dotted, set the
        // color, etc.
        dotedLineLayer.setProperties(
                lineDasharray(arrayOf(1.5f, 1f)),
                lineCap(Property.LINE_CAP_SQUARE),
                lineJoin(Property.LINE_JOIN_BEVEL),
                lineWidth(8f),
                lineOpacity(0.5f),
                lineColor(Color.parseColor("#9577D1"))
        )
        mapboxMap.style?.addLayer(dotedLineLayer)
    }

    fun animate(view: View, translate: Int): ValueAnimator {
        val valueAnim = ValueAnimator.ofInt(view.translationY.toInt(), translate)
        valueAnim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            view.translationY = animatedValue.toFloat()
        }
        valueAnim?.duration = 150
        valueAnim?.start()
        return valueAnim
    }

    fun animateTutorial(view: View, translate: Int, topTranslation: Boolean = false): ValueAnimator {
        val translatePx = CommonUtils.pxFromDp(view.context, translate.toFloat())
        val valueAnim = ValueAnimator.ofInt(if (topTranslation) view.translationY.toInt() else view.translationX.toInt(),
                translatePx.toInt())
        valueAnim.interpolator = DecelerateInterpolator()
        valueAnim.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            if (topTranslation) {
                view.translationY = animatedValue.toFloat()
            } else
                view.translationX = animatedValue.toFloat()
            view.alpha = 1 - (animatedValue / translatePx)
        }
        valueAnim?.duration = 2000
        valueAnim.repeatCount = ValueAnimator.INFINITE
        valueAnim?.start()
        return valueAnim
    }



    fun setMaxVolume() {
        val am = mView?.getContext()!!.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, 0)
        } else {
            am.setStreamMute(AudioManager.STREAM_MUSIC, false)
        }
        val amStreamMusicMaxVol = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        try {
            am.setStreamVolume(AudioManager.STREAM_MUSIC, amStreamMusicMaxVol, 0)
        } catch (se: SecurityException) {
            se.printStackTrace()
        }
    }

    fun muteVolume() {
        val am = mView?.getContext()!!.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0)
        } else {
            am.setStreamMute(AudioManager.STREAM_MUSIC, true)
        }
    }




    fun updateAccuracyLayer(mapboxMap: MapboxMap, sourceId: String, features: ArrayList<Feature>) {
        val featureCollection = FeatureCollection.fromFeatures(features)
        val geoJsonSource = mapboxMap.style?.getSourceAs<GeoJsonSource>(sourceId)
        geoJsonSource?.setGeoJson(featureCollection)
    }
}