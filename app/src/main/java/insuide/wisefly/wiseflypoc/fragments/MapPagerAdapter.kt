package com.netpiper.sia.ui.map

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter


/**
 * Created by Furqan on 30-04-2018.
 */
class MapPagerAdapter(fragmentManager: FragmentManager, private val fragmentList: List<Fragment>) :
        FragmentStatePagerAdapter(fragmentManager) {

    val tabTitle: Array<String> = arrayOf("All places", " Food & Drink ", " Shopping ", " Service ", " Facility ")
    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int = fragmentList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitle[position]
    }

   override fun getItemPosition(any: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

}