package com.netpiper.sia.base

/**
 * Created by Furqan on 11-01-2018.
 */
interface BaseMvpPresenter<in V : BaseMvpView> {
    fun attachView(view: V)
    fun detachView()
}