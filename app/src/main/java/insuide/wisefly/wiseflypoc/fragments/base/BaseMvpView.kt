package com.netpiper.sia.base

import android.content.Context
import androidx.annotation.StringRes


/**
 * Created by Furqan on 11-01-2018.
 */
interface BaseMvpView {
    fun showLoading()

    fun hideLoading()

    fun onError(@StringRes resId: Int)

    fun onError(message: String)

    fun showMessage(message: String)

    fun showMessage(@StringRes resId: Int)

    fun isNetworkConnected(): Boolean

    fun getContext(): Context
}