package com.netpiper.sia.ui.map

import org.json.JSONObject

/**
 * Created by Furqan on 07-06-2018.
 */
interface PeerLocationUpdate {
    fun onTrackUser(jsonObj: JSONObject)
    fun stopTrack(status: Int)

}