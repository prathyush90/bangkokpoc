package insuide.wisefly.wiseflypoc.fragments


import android.Manifest
import android.animation.ObjectAnimator
import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Rect
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.speech.tts.TextToSpeech
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import biz.laenger.android.vpbs.BottomSheetUtils
import biz.laenger.android.vpbs.ViewPagerBottomSheetBehavior
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.indooratlas.android.sdk.IALocation
import com.indooratlas.android.sdk.IALocationListener
import com.indooratlas.android.sdk.IALocationManager
import com.indooratlas.android.sdk.IALocationRequest
import com.kontakt.sdk.android.ble.configuration.ScanMode
import com.kontakt.sdk.android.ble.configuration.ScanPeriod
import com.kontakt.sdk.android.ble.manager.ProximityManager
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener
import com.kontakt.sdk.android.common.profile.IBeaconDevice
import com.kontakt.sdk.android.common.profile.IBeaconRegion
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.mapboxsdk.annotations.*
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.netpiper.sia.base.BaseFragment
import com.netpiper.sia.manager.LocaleManager
import com.netpiper.sia.model.PoiMultilingual
import com.netpiper.sia.model.ShjLocal
import com.netpiper.sia.ui.map.MapPagerAdapter
import com.netpiper.sia.ui.map.MapPresenter
import com.netpiper.sia.ui.map.PeerLocationUpdate
import insuide.wisefly.wiseflypoc.DemoActivity
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.helpers.Kalman
import insuide.wisefly.wiseflypoc.interfaces.LocationUpdaterInterface
import insuide.wisefly.wiseflypoc.manager.DatabaseManager
import insuide.wisefly.wiseflypoc.manager.LocationManager
import insuide.wisefly.wiseflypoc.manager.NetworkManager
import insuide.wisefly.wiseflypoc.models.BeaconsPojo
import insuide.wisefly.wiseflypoc.models.FilteredBeacon
import insuide.wisefly.wiseflypoc.models.MapContractor
import insuide.wisefly.wiseflypoc.models.PoiDetail
import insuide.wisefly.wiseflypoc.sensors.DeadReckoningManager
import insuide.wisefly.wiseflypoc.sensors.StepAndAzimutDetector
import insuide.wisefly.wiseflypoc.utils.CommonUtils
import insuide.wisefly.wiseflypoc.utils.Commonutils
import insuide.wisefly.wiseflypoc.utils.GeoCentricCordinates
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.bottom_sheet_map.*
import kotlinx.android.synthetic.main.bottom_sheet_navigation.*
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.layout_eta.*
import kotlinx.android.synthetic.main.layout_msg.*
import kotlinx.android.synthetic.main.poi_detail.view.*
import kotlinx.android.synthetic.main.tab.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.Math.*
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.math.pow


/**
 * A simple [Fragment] subclass.
 */
internal var PERMISSIONS = arrayOf(Manifest.permission.CHANGE_WIFI_STATE,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION)

class BangkokMapFragment : BaseFragment<MapContractor.View, MapPresenter>(), MapContractor.View,
        OnMapReadyCallback, MapboxMap.OnMapClickListener, View.OnClickListener,
        TextToSpeech.OnInitListener, MapboxMap.OnCameraMoveStartedListener,
        PeerLocationUpdate, LocationUpdaterInterface, DeadReckoningManager.onStepUpdateNotifier, IALocationListener {
    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
        //nothing to do
    }


    private var arrowValueAnimator: ValueAnimator? = null
    var mapboxMap: MapboxMap? = null
    private lateinit var pagerAdapter: MapPagerAdapter
    private lateinit var landingActivity: DemoActivity
    private lateinit var mapBottomSheet: ViewPagerBottomSheetBehavior<LinearLayout>
    private lateinit var shareLocationBottomSheet: BottomSheetBehavior<ConstraintLayout>
    private lateinit var bottomSheetBehaviorNavigation: BottomSheetBehavior<FrameLayout>
    private val flightSearchPublishSubject = PublishSubject.create<String>()
    private lateinit var mapView: MapView
    private var translateHeight = 0f
    private lateinit var iaMarkerIcon: Icon
    private lateinit var shareMarkerIcon: Icon
    private lateinit var iaArrowMarker: Icon
    private var styleLoaded: Boolean = false
    private var floorPlanObject: JSONObject? = null
    private var mapUrl = ""

    var poiList = ArrayList<PoiDetail>()
    private var level0Features = ArrayList<Feature>()
    private var level1Features = ArrayList<Feature>()
    private var level2Features = ArrayList<Feature>()
    private var iaLatLng: LatLng? = null
    private var destinationLatLng: LatLng? = null
    private var steps = 0
    private var proximityManager: ProximityManager? = null
    private var tts: TextToSpeech? = null
    private lateinit var poiDetailRouting: PoiDetail
    private var isAnimateCamera = false
    private var isStartNavigation = false
    var dist: Double = 0.0
    private var dbManager: DatabaseManager? = null
    private var appname: String = ""
    private var beacons = JSONArray()

    companion object {
        var isRunning: Boolean = false
        fun getIsRunning(): Boolean {
            return isRunning
        }
    }

    internal var beaconsHashMap = HashMap<String, BeaconsPojo>()
    private var readyToMapBeacons: Boolean = false
    private var floorplans = JSONArray()
    private var walkableAreas = JSONArray()
    private val beaconsFilterHashMap = HashMap<String, Kalman>()
    private var level = ""
    private var slice = ""
    private var terminal = ""
    private var regionDataAvailable = false
    private var modifiedList = java.util.ArrayList<FilteredBeacon>()
    private var walkablePaths = JSONObject()
    private var converged = false
    private lateinit var logText: TextView
    private lateinit var beaconSignalImageView: ImageView
    private var initialPointSet: Boolean = false

    private var locManager: LocationManager? = null

    //private var sensorController: StepAndAzimutDetector? = null
    private val AIRCRAFT_MARKER_ICON_ID = "usermarker"

    private var wifiManager: WifiManager? = null
    private val magValues = java.util.ArrayList<DoubleArray>()
    private var heading = 0.0
    private val AIRCRAFT_LAYER_ID = "usermarkerlayer"
    private val SWEEP_LAYER_ID = "sweeplayer"
    private val GEOJSON_SOURCE_ID = "usermarkersource"
    private val POLYGON_SOURCE_ID = "headingerrorsource"

    private var geoJsonSource: GeoJsonSource? = null
    private var headingErrorSource: GeoJsonSource? = null
    private var layer: SymbolLayer? = null
    private var sweepLayer: FillLayer? = null
    private var circlePolygon: com.mapbox.mapboxsdk.annotations.Polygon? = null
    private var latLngUser: LatLng? = null
    private var currHeading: Double = Double.NEGATIVE_INFINITY
    private var trailId: Long = 0
    private var sensorController: DeadReckoningManager? = null
    private var textView: TextView? = null
    private var beaconBar: Int = 5
    private lateinit var floorChangeFrame: FrameLayout
    private var showing: Int = 1
    private var lastBeaconUpdate: Long = 0
    private var obj: Object = Object()
    private lateinit var mIALocationManager: IALocationManager
    private var wayPointIcon: Icon? = null
    private var iaMarker: Marker? = null
    private var deviceId: String? = null
    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onDetach() {
        super.onDetach()
        if (sensorController != null) {
            sensorController!!.shutdown(this.context)
        }
        if (locManager != null) {
            locManager!!.invalidate()
        }

        if (proximityManager != null) {
            proximityManager!!.disconnect()
            proximityManager = null
        }


    }

    override fun onLocationChanged(p0: IALocation?) {
        val latitude = p0!!.latitude
        val longitude = p0.longitude
        val floor = p0.floorLevel

        if (level != floor.toString()) {
            latLngUser = LatLng(latitude, longitude)
            level = floor.toString()
            //Changed due to client feedback
//            changeFloorPlan()
        } else {
            latLngUser = LatLng(latitude, longitude)
            //updateMarkerPosition(latitude, longitude, 0.0)
            if (iaMarker == null) {
                iaMarker = mapboxMap!!.addMarker(MarkerOptions()
                        .position(latLngUser)
                        .icon(wayPointIcon))
            } else {
                iaMarker!!.position = latLngUser
            }
        }
        val conf: Double = random() * 3
        sendDataToServer(latitude, longitude, 0.0, deviceId!!, System.currentTimeMillis(), conf, 999.0)

        if (isStartNavigation) {
            //calculateEta()
            calculateEta(poiDetailRouting)
            reRouting(latLngUser)

            val distance = CommonUtils.getDistanceFromLatLonInMeters(latLngUser?.latitude!!, latLngUser?.longitude!!,
                    poiDetailRouting.lat!!, poiDetailRouting.lng!!)
            if (distance <= 3) {
                activity?.runOnUiThread {
                    Toast.makeText(this.context, "You have reached your destination", Toast.LENGTH_SHORT).show()
                    stopNavigation()
                }

            }
        }
    }

    private fun changeFloorPlan() {
        if (level == "1") {
            mapUrl = getString(R.string.mapbox_style_bkk_first_floor)
            mapboxMap?.setStyle(mapUrl, Style.OnStyleLoaded { })
            logText.text = "1F"
        } else {
            mapUrl = getString(R.string.mapbox_style_bkk_second_floor)
            mapboxMap?.setStyle(mapUrl)
            logText.text = "2F"
        }
        mPresenter.loadPoi(getString(R.string.token), getString(R.string.appname))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //throw Exception()
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        tts = TextToSpeech(context, this)
        appname = getString(R.string.appname)
        mPresenter.setMaxVolume()
        isRunning = true
        mapView = view.findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)
        if (context is DemoActivity) {
            landingActivity = context as DemoActivity
        }

        logText = view.findViewById(R.id.logText)
        textView = view.findViewById(R.id.beaconsText)
        beaconSignalImageView = view.findViewById(R.id.bSignal)
        floorChangeFrame = view.findViewById(R.id.floorLayout)
//        mapView.getMapAsync(this)
        //mPresenter.loadBeaconFile(context, "beacons_sarjah.json")
        iaMarkerIcon = IconFactory.getInstance(context).fromResource(R.drawable.ia_dot)
        shareMarkerIcon = IconFactory.getInstance(context).fromResource(R.drawable.ia_arrow_dot)
        iaArrowMarker = IconFactory.getInstance(context).fromResource(R.drawable.ia_arrow_dot)

        wifiManager = this.context.getSystemService(Context.WIFI_SERVICE) as WifiManager?
        proximityManager = ProximityManagerFactory.create(context)
        proximityManager!!.configuration()
                .scanPeriod(ScanPeriod.RANGING)
                .scanMode(ScanMode.LOW_LATENCY)
                .deviceUpdateCallbackInterval(350)
        proximityManager?.setIBeaconListener(createIBeaconListener())
        sensorController = DeadReckoningManager(this, this.context)
        //sensorController!!.startSensors(false)
        dbManager = DatabaseManager(context)
        getDataForBeacons()
        getFloorPlansForApp(appname)
        getGeoFencesForApp(appname)
        mIALocationManager = IALocationManager.create(this.activity!!.applicationContext)
        mIALocationManager.requestLocationUpdates(IALocationRequest.create(), this)
        floorChangeFrame.setOnClickListener {
            if (mapUrl == "") {
                showing = if (showing == 1) {
                    showSecondFloor()
                    2
                } else {
                    showFirstFloor()
                    1
                }
            }
        }
        mapView.getMapAsync(this)

        trailId = System.currentTimeMillis()
        val iconFactory = IconFactory.getInstance(this.activity!!.applicationContext)
        wayPointIcon = iconFactory.fromResource(R.mipmap.ia_dot)
        locManager = LocationManager(this.context, arrayOf(doubleArrayOf(0.0, 0.0), doubleArrayOf(0.0, 0.0), doubleArrayOf(0.0, 0.0)), walkablePaths, this)
        deviceId = Settings.Secure.getString(context.contentResolver,
                Settings.Secure.ANDROID_ID)
        return view
    }

    private fun getGeoFencesForApp(appname: String) {
        val mappedRegion = dbManager?.getGeofenceData(appname)
        try {
            walkableAreas = JSONArray(mappedRegion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }


    private fun getDataForBeacons() {
        val beaconsVal = dbManager?.getBeaconsData(appname)
        try {
            beacons = JSONArray(beaconsVal)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        for (i in 0 until beacons.length()) {
            try {
                val obj = beacons.getJSONObject(i)
                val g = Gson()
                val pojo = g.fromJson(obj.toString(), BeaconsPojo::class.java)
                val uniqueId = obj.getString("uniqueId")
                if (pojo.enabled) {
                    beaconsHashMap[uniqueId] = pojo
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
//        Log.d("TAG", "getDataForBeacons: ${beaconsHashMap.size}")
//        beaconsHashMap.forEach { t, u ->
//            Log.d("TAG", "getDataForBeacons: $t  ${u.uniqueId}")
//        }
        readyToMapBeacons = true
        startScanning()

    }

    private fun getFloorPlansForApp(appname: String) {
        val floorPlansVal = dbManager?.getFloorPlansData(appname)
        try {
            floorplans = JSONArray(floorPlansVal)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }


    private val layerIds = ArrayList<String>()

    // second dot
    override fun onTrackUser(jsonObj: JSONObject) {
        val featureList = ArrayList<Feature>()
//        for (i in 0..jsonObj.length()) {
        val floor = jsonObj.getInt("floor")
        val floorStr = if (floor == 0) {
            "Ground floor"
        } else {
            "1st floor"
        }
        val userName = jsonObj.getString("name").split(" ")[0]

        val userId = jsonObj.getString("user_id")
        val feature = Feature.fromGeometry(Point.fromLngLat(jsonObj.getDouble("longitude"),
                jsonObj.getDouble("latitude")))
        feature.addStringProperty("name", userName + "\n" + floorStr)
        feature.addStringProperty("color", "peer")
        feature.addStringProperty("image", R.drawable.peer_dot.toString())
        featureList.add(feature)
//        }
        if (mapboxMap != null) {
            activity?.runOnUiThread {
                val peerLayer = mapboxMap?.style?.getLayer(userId)
                if (peerLayer != null) {
                    layerIds.add(userId)
                    mPresenter.updatePeerLocLayer(mapboxMap!!, "source_peer_loc$userId", userId, featureList)
                } else {
                    mapboxMap?.style?.addImage(R.drawable.peer_dot.toString(), getMarkerIcon(R.drawable.peer_dot))
                    mPresenter.addPeerSymbolLayer(mapboxMap!!, "source_peer_loc$userId", userId, featureList)
                }
            }

        }

    }

    override fun stopTrack(status: Int) {
    }


    private fun createIBeaconListener(): IBeaconListener? {
        return object : IBeaconListener {
            override fun onIBeaconDiscovered(iBeacon: IBeaconDevice?, region: IBeaconRegion?) {
                Log.d("TAG", "discovered ${iBeacon?.uniqueId}")
                iBeacon?.let {
                    val uniqueId: String? = iBeacon.uniqueId
                    if (uniqueId == null || beaconsHashMap[iBeacon.uniqueId] == null) {
                        return
                    }
                    val filteredBeacon = FilteredBeacon()
                    filteredBeacon.rssi = iBeacon.rssi.toDouble()
                    filteredBeacon.txPower = iBeacon.txPower
                    filteredBeacon.distance = iBeacon.distance
                    filteredBeacon.uniqueId = iBeacon.uniqueId
                    //d = 10 ^ ((TxPower - RSSI) / 20)
                    val power = getTransmissionPower(filteredBeacon.txPower)
                    val expPart = (power - iBeacon.rssi.toDouble()) / 20.0
                    val distance = 10.0.pow(expPart)
                    filteredBeacon.kalmanDistance = distance

                    if (iBeacon.uniqueId != null && beaconsHashMap[iBeacon.uniqueId] != null) {
                        modifiedList.add(filteredBeacon)

                    }
                    lastBeaconUpdate = System.currentTimeMillis()
                }


            }

            override fun onIBeaconLost(iBeacon: IBeaconDevice?, region: IBeaconRegion?) {
                Log.d("TAG", "onIBeaconLost:${iBeacon!!.uniqueId} ")

                val uniqueId: String? = iBeacon.uniqueId
                val newModifiedList: ArrayList<FilteredBeacon> = modifiedList.clone() as ArrayList<FilteredBeacon>
                for (filteredBeacon in newModifiedList) {
                    val removeId: String? = filteredBeacon.uniqueId
                    if (removeId == uniqueId) {
                        modifiedList.remove(filteredBeacon)
                    }
                }
                lastBeaconUpdate = System.currentTimeMillis()

            }

            override fun onIBeaconsUpdated(iBeacons: MutableList<IBeaconDevice>, region: IBeaconRegion?) {
                textView?.text = "beacons ${iBeacons?.size}"
                val beaconsInfo = ArrayList<BeaconsPojo>()
                for (iBeacon in iBeacons) {
                    Log.d("TAG", "onIBeaconsUpdated: ${iBeacon.uniqueId}")
                    val uniqueId: String? = iBeacon.uniqueId
                    if (uniqueId == null || beaconsHashMap[iBeacon.uniqueId] == null) {
                        continue
                    }
                    beaconsHashMap[iBeacon.uniqueId]?.let {
                        beaconsInfo.add(it)
                    }

                    var found = false
                    val filteredBeacon = FilteredBeacon()
                    filteredBeacon.rssi = iBeacon.rssi.toDouble()
                    filteredBeacon.txPower = iBeacon.txPower
                    filteredBeacon.distance = iBeacon.distance
                    filteredBeacon.uniqueId = iBeacon.uniqueId
                    //d = 10 ^ ((TxPower - RSSI) / 20)
                    val power = getTransmissionPower(filteredBeacon.txPower)
                    val expPart = (power - iBeacon.rssi.toDouble()) / 20.0
                    val distance = 10.0.pow(expPart)
                    filteredBeacon.kalmanDistance = distance
                    val newModifiedList = modifiedList.clone() as ArrayList<FilteredBeacon>

                    for (fBeacon in newModifiedList) {
                        val removeId: String? = fBeacon.uniqueId
                        if (removeId == uniqueId) {
                            modifiedList[modifiedList.indexOf(fBeacon)] = filteredBeacon
                            found = true
                        }
                    }
                    if (!found) {
                        modifiedList.add(filteredBeacon)
                    }
                }

                textView?.text = "beacons info ${beaconsInfo.size}"
                if (beaconsInfo.size > 0) {

                    val detectedLevel1 = beaconsInfo.filter {
                        it.level == "1"
                    }.size
                    val detectedLevel2 = beaconsInfo.filter {
                        it.level == "2"
                    }.size

                    val intArray = intArrayOf(detectedLevel1, detectedLevel2)
                    if (intArray.max()!! > 1) {
                        when (intArray.max()) {
                            detectedLevel1 -> {
                                if (logText.text != "1F")
                                    showFirstFloor()
                            }
                            else -> {
                                if (logText.text != "2F")
                                    showSecondFloor()
                            }
                        }
                    }
                }
                lastBeaconUpdate = System.currentTimeMillis()
            }
        }
    }

    private fun showSecondFloor() {
        logText.text = "2F"
//        mPresenter.loadPoi(getString(R.string.token), getString(R.string.appname))
        mapboxMap?.setStyle(getString(R.string.mapbox_style_bkk_second_floor), Style.OnStyleLoaded {
            addImagesToMap()
            mPresenter.addSymbolLayer(mapboxMap!!, "source_floor_2", "layer_floor_2", level2Features)
        })
    }

    @SuppressLint("WrongConstant")
    private fun showFirstFloor() {
        logText.text = "1F"
//        mPresenter.loadPoi(getString(R.string.token), getString(R.string.appname))
        mapboxMap?.setStyle(getString(R.string.mapbox_style_bkk_first_floor), Style.OnStyleLoaded {
            addImagesToMap()
            mPresenter.addSymbolLayer(mapboxMap!!, "source_floor_1", "layer_floor_1", level1Features)

        })

    }

    private fun startScanning() {
        proximityManager?.connect { proximityManager?.startScanning() }
        Log.d("TAG", "startScanning: ")
    }


    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTabIcons()
        setupMapBottomSheet()
        setNavigationBottomSheet()
        searchInputListener()
        configureSearch()

        firstFloor.setOnClickListener(this)
        groundFloor.setOnClickListener(this)
        logText.setOnClickListener(this)

        imgCancelMap.setOnClickListener {
            etSearchMap.setText("")
            searchText("")
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }
        var isGlobal = false
        view.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            view.getWindowVisibleDisplayFrame(r)
            val heightDiff = view.rootView.height - (r.bottom - r.top)
            if (heightDiff > 400) {
                // keyboard open
                if (isGlobal) {
                    isGlobal = false
                    if (mapBottomSheet.state == ViewPagerBottomSheetBehavior.STATE_EXPANDED) {
                        searchWeight(1f)
                    } else {
                        searchWeight(1f)
                        bottomMapGroup.visibility = View.GONE
                        filterGroup.visibility = View.GONE
                        llBottom.translationY = translateHeight
                        llBottom.setBackgroundResource(R.drawable.bg_search_with_keyboard)

                    }
                }
            } else {

                isGlobal = true
                if (!bottomMapGroup.isShown) {
                    searchWeight(0f)
                    llBottom.translationY = 0f
                    if (!startNavigationLayout.isShown && !isStartNavigation)


                        llBottom.setBackgroundResource(R.drawable.filter_bg_round_shap)
                    bottomMapGroup.visibility = View.VISIBLE
                    filterGroup.visibility = View.VISIBLE
                } else if (mapBottomSheet.state == ViewPagerBottomSheetBehavior.STATE_EXPANDED) {
                    searchWeight(0f)
                    llBottom.setBackgroundResource(R.drawable.filter_bg_round_shap)

                }

            }
        }

        //click listener
        imgCancel.setOnClickListener { hideNavigationLayout() }
        btnStartNavigation.setOnClickListener { startNavigation() }
        btnMute.setOnClickListener { stopSpeak() }
        btnSpeak.setOnClickListener { startSpeak() }
        btnStopNavigation.setOnClickListener { stopNavigation() }
        fabRecenter.setOnClickListener {
            if (dist > 1200) {
                // outside airport
                navigateToMap()
            } else
                animateCamera()
            isAnimateCamera = true
            fabRecenter.visibility = View.GONE
        }
        shareLayout.setOnClickListener {
            if (progressBarShare.isShown) {
                return@setOnClickListener
            }

        }

        bottomSheetLayoutMap.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (mapBottomSheet.state == BottomSheetBehavior.STATE_EXPANDED) {
                        arrowValueAnimator = startArrow(-15)
                    } else if (mapBottomSheet.state == BottomSheetBehavior.STATE_COLLAPSED) {
                        arrowValueAnimator = startArrow(15)
                    }
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_UP -> {
                    removeArrrowAnimatoin()
                    if (mapBottomSheet.state == BottomSheetBehavior.STATE_EXPANDED) {
                        mapBottomSheet.state = ViewPagerBottomSheetBehavior.STATE_COLLAPSED
                    } else {
                        mapBottomSheet.state = ViewPagerBottomSheetBehavior.STATE_EXPANDED
                    }
                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }
        swipeTop.visibility = View.GONE
        swipeRight.visibility = View.GONE
        visibleFragment(true)
    }

    private fun navigateToMap() {
        if (mapboxMap != null && iaLatLng != null) {
            val position = CameraPosition.Builder()
                    .target(LatLng(0.0, 0.0)) // Sets the new camera position
                    .bearing(15.0)
                    .build() // Creates a CameraPosition from the builder
            mapboxMap?.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position))
        }
    }

    private fun startSpeak() {
        mPresenter.setMaxVolume()

        btnMute.visibility = View.VISIBLE
        btnSpeak.visibility = View.GONE
    }

    private fun stopSpeak() {
        mPresenter.muteVolume()

        btnMute.visibility = View.GONE
        btnSpeak.visibility = View.VISIBLE
    }


    override fun showMessage(message: String) {
        if (progressBarShare.isShown) {
            progressBarShare.visibility = View.GONE
            imgShare.visibility = View.VISIBLE
            shareText.visibility = View.VISIBLE
        }
    }

    private fun animateCamera(bearing: Double = 0.0) {
        if (mapboxMap != null && latLngUser != null) {
            val position = CameraPosition.Builder()
                    .target(latLngUser) // Sets the new camera position
                    .bearing(bearing)
                    .build() // Creates a CameraPosition from the builder
            mapboxMap?.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position))
        }
    }

    private fun animateCamera(bearing: Double = 0.0, latLng: LatLng, zoom: Double) {
        if (mapboxMap != null) {
            val position = CameraPosition.Builder()
                    .target(latLng) // Sets the new camera position
                    .bearing(bearing)
                    .zoom(zoom)
                    .build() // Creates a CameraPosition from the builder
            mapboxMap?.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position))
        }
    }

    private fun stopNavigation() {
        try {
            stopNavigationLayout.visibility = View.GONE
            bottomSheetLayoutMap.visibility = View.VISIBLE
            bottomSheetNavigation.visibility = View.GONE
            layoutEta.visibility = View.GONE
            llBottom.visibility = View.VISIBLE
            removePolyLine()
            //change marker
            if (marker != null) {
                marker?.icon = iaMarkerIcon
//            updateLayerMarker(R.drawable.ia_dot)
                mapboxMap?.updateMarker(marker!!)
            }
            isAnimateCamera = false
            isStartNavigation = false
            translateRecenterLayout(0)
            changeStatusBarColor(R.color.colorStatusBar)
            //Toast.makeText(this.context, "You have reached your destination", Len)
        } catch (ise: java.lang.IllegalStateException) {
            ise.printStackTrace()
        }

    }

    private fun showNotification(message: String, id: Int) {
        layoutNotification.visibility = View.VISIBLE
        txtMsg.text = message
        imgMsg.setImageResource(id)
        Handler().postDelayed({
            try {
                layoutNotification.visibility = View.GONE
            } catch (ise: IllegalStateException) {
                ise.printStackTrace()
            }

        }, 2000)
    }

    private fun removePolyLine() {
        mapboxMap?.polylines!!.forEach { it -> mapboxMap?.removePolyline(it) }
        mapboxMap?.getStyle { style ->
            style.removeSource("simplifiedLine")
            style.removeLayer("simplifiedLine")
            style.removeSource("otherLevelLine")
            style.removeLayer("otherLevelLine")

        }
    }

    private fun startNavigation() {
        //change the dotted linelayer to polyline
        mapboxMap?.addPolyline(optCurrent!!)
        stopNavigationLayout.visibility = View.VISIBLE
        startNavigationLayout.visibility = View.GONE

        // change the marker
        if (marker != null) {
            if (dist < 1200) {
//                updateLayerMarker(R.drawable.ia_arrow_dot)
                marker?.icon = iaArrowMarker
            }
            mapboxMap?.updateMarker(marker!!)
        }
        isAnimateCamera = true
        isStartNavigation = true

    }

    private fun searchWeight(weight: Float) {
        val pram = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                weight
        )
        if (etSearchMap.text.toString().isEmpty() && weight == 0f) {
            etSearchMap.layoutParams = pram
        } else if (weight == 1f) {
            etSearchMap.layoutParams = pram
        }

    }

    var valueAnim: ValueAnimator? = null
    var valueAnimWidth: ValueAnimator? = null

    private fun animate() {
        valueAnim = ValueAnimator.ofInt(floorLayout.measuredHeight, CommonUtils.pxFromDp(context, 112f).toInt())
        valueAnim?.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val pram = floorLayout.layoutParams
            pram.height = animatedValue
            floorLayout.layoutParams = pram
        }
        valueAnim?.duration = 150
        valueAnim?.start()

        valueAnimWidth = ValueAnimator.ofInt(floorLayout.measuredWidth, CommonUtils.pxFromDp(context, 136f).toInt())
        valueAnimWidth?.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val pram = floorLayout.layoutParams
            pram.width = animatedValue
            floorLayout.layoutParams = pram
        }
        valueAnimWidth?.duration = 150
        valueAnimWidth?.start()
    }

    private fun animateReverse() {
        recenterLayout.visibility = View.VISIBLE
        if (startNavigationLayout.isShown || isStartNavigation) {

        } else {
            llBottom.visibility = View.VISIBLE
            bottomSheetLayoutMap.visibility = View.VISIBLE
        }
        logText.alpha = 1f
        floorContainer.alpha = 0f
        valueAnim = ValueAnimator.ofInt(floorLayout.measuredHeight, CommonUtils.pxFromDp(context, 56f).toInt())
        valueAnim?.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val pram = floorLayout.layoutParams
            pram.height = animatedValue
            floorLayout.layoutParams = pram
        }
        valueAnim?.duration = 150
        valueAnim?.start()

        valueAnimWidth = ValueAnimator.ofInt(floorLayout.measuredWidth, CommonUtils.pxFromDp(context, 56f).toInt())
        valueAnimWidth?.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            val pram = floorLayout.layoutParams
            pram.width = animatedValue
            floorLayout.layoutParams = pram
        }
        valueAnimWidth?.duration = 150
        valueAnimWidth?.start()
    }

    private fun searchInputListener() {
        etSearchMap.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().trim().isNotEmpty()) {
                    searchText(editable.toString())
                } else if (poiList.isNotEmpty()) {
                    searchText("")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(value: CharSequence, p1: Int, p2: Int, p3: Int) =
                    if (value.isNotEmpty()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            etSearchMap.compoundDrawableTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBloody))
                        }
                        textDirectory.text = "Result for " + value//l.makePartialTextsBold()
                        mapBottomSheet.state = ViewPagerBottomSheetBehavior.STATE_EXPANDED
                        imgCancelMap.visibility = View.VISIBLE
                        etSearchLayout.setBackgroundResource(R.drawable.edit_txt_pressed)
                        filterGroup.visibility = View.GONE
                        bottomMapGroup.visibility = View.VISIBLE
                        tabLayoutFilter.alpha = 1f
                        when (value.length) {
                            1 -> {
                                searchWeight(1f)
                            }
                            else -> {

                            }
                        }


                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            etSearchMap.compoundDrawableTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorCoolGrey))
                        }
                        textDirectory.text = getString(R.string.directory)
                        etSearchLayout.setBackgroundResource(R.drawable.edit_txt_default)
                        imgCancelMap.visibility = View.GONE
                        filterGroup.visibility = View.VISIBLE
                        bottomMapGroup.visibility = View.GONE
                        tabLayoutFilter.alpha = 0f
                        mapBottomSheet.state = ViewPagerBottomSheetBehavior.STATE_COLLAPSED

                        searchWeight(0f)
                    }
        })

    }


    private fun configureSearch() {
        flightSearchPublishSubject.debounce(250, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()//no back to back
                .map { t -> search(t) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result ->
                    for (i in 0..(pagerAdapter.count - 1)) {
                        try {
                            val poiFragment = pagerAdapter.getItem(i) as PoiFragment
                            when (i) {
                                0 -> {
                                    poiFragment.setPoiList(this, result as ArrayList<PoiDetail>, true)
                                }
                                1 -> {
                                    val filteredList = result.filter {
                                        it.category.equals("food & drink", true)
                                    }
//                                    if (filteredList.isNotEmpty())
                                    poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>, true)
                                }
                                2 -> {
                                    val filteredList = result.filter {
                                        it.category.equals("shopping", true)
                                    }
//                                    if (filteredList.isNotEmpty())
                                    poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>, true)
                                }

                                3 -> {
                                    val filteredList = result.filter {
                                        it.category.equals("services", true)
                                    }
//                                    if (filteredList.isNotEmpty())
                                    poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>, true)
                                }
                                4 -> {
                                    val filteredList = result.filter {
                                        it.category.equals("utilities", true)
                                    }
//                                    if (filteredList.isNotEmpty())
                                    poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>, true)
                                }
                            }
                        } catch (e: IndexOutOfBoundsException) {
                            e.printStackTrace()
                        }

                    }

                }
    }

    fun showPoi(poiDetail: PoiDetail) {

    }


    private var optCurrent: PolylineOptions? = null
    private fun setPoiBottomSheet(poiDetail: PoiDetail, stringProperty: String) {

        val bottomSheetDialog = BottomSheetDialog(context)
        val poiDetailView = layoutInflater.inflate(R.layout.poi_detail, null)
        bottomSheetDialog.setContentView(poiDetailView)
        val params = (poiDetailView.parent as View)
                .layoutParams as CoordinatorLayout.LayoutParams

        val view = poiDetailView.parent as View
        view.post {
            val parent = view.parent as View
            val behavior = params.behavior
            val bottomSheetBehavior = behavior as BottomSheetBehavior
            bottomSheetBehavior.peekHeight = view.measuredHeight
            parent.setBackgroundColor(Color.TRANSPARENT)
        }
        (poiDetailView.parent as View).setBackgroundColor(Color.TRANSPARENT)


        val pairList = ArrayList<Pair<Int, String>>()
        if (poiDetail.mobile_number.isNotEmpty() && poiDetail.mobile_number.length > 1) {
            val mobileNo = poiDetail.mobile_number.split(",")
            mobileNo.forEach { pairList.add(Pair(R.drawable.ic_phone, it)) }
        }
        if (poiDetail.email.isNotEmpty()) {
            pairList.add(Pair(R.drawable.ic_mail, poiDetail.email))
        }
        if (pairList.isEmpty()) {
            poiDetailView.recyclerViewPoiInfo.visibility = View.GONE
        } else {
            poiDetailView.recyclerViewPoiInfo.layoutManager = LinearLayoutManager(context)
            poiDetailView.recyclerViewPoiInfo.adapter = PoiContactAdapter(pairList)
        }

        val pairText = when (LocaleManager.getLanguage(context)) {
            LocaleManager.LANGUAGE_ENGLISH -> {
                Pair(poiDetail.poiMultilingual?.en?.name,
                        poiDetail.poiMultilingual?.en?.description)
            }
            LocaleManager.LANGUAGE_ARABIC -> {
                Pair(poiDetail.poiMultilingual?.ar?.name,
                        poiDetail.poiMultilingual?.ar?.description)
            }
            LocaleManager.LANGUAGE_HINDI -> {
                Pair(poiDetail.poiMultilingual?.hi?.name,
                        poiDetail.poiMultilingual?.hi?.description)
            }
            LocaleManager.LANGUAGE_RUSSSIAN -> {
                Pair(poiDetail.poiMultilingual?.ru?.name,
                        poiDetail.poiMultilingual?.ru?.description)
            }
            else -> {
                Pair(poiDetail.poiMultilingual?.en?.name,
                        poiDetail.poiMultilingual?.en?.description)
            }
        }

        pairText.first?.let {
            view.dialogTxtShopName.text = it
        } ?: kotlin.run {
            view.dialogTxtShopName.text = poiDetail.poiMultilingual?.en?.name
        }

        pairText.second?.let {
            view.dialogTxtDesc.text = it
        } ?: kotlin.run {
            view.dialogTxtDesc.text = poiDetail.poiMultilingual?.en?.description
        }

        poiDetailView.dialogTxtCategoryName.text = poiDetail.category

        poiDetailView.txt_Show_map.text = getString(R.string.route_preview)
        Glide.with(context).load(poiDetail.poiUrl)
                .into(poiDetailView.imgShop)
        when (poiDetail.category?.toLowerCase()) {
            "food & drink" -> {
                poiDetailView.digImgCategory.setImageResource(R.drawable.ic_food)
                poiDetailView.imgFacility.visibility = View.INVISIBLE
            }
            "shopping" -> {
                poiDetailView.digImgCategory.setImageResource(R.drawable.ic_shop)
                poiDetailView.imgFacility.visibility = View.INVISIBLE
            }
            else -> {
                if (poiDetail.poiUrl.isNullOrEmpty()) {
                    poiDetailView.imgFacility.visibility = View.VISIBLE
                    poiDetailView.imgFacility.setImageResource(stringProperty.toInt())
                } else {
                    poiDetailView.imgFacility.visibility = View.INVISIBLE
                }
                poiDetailView.dialogTxtCategoryName.visibility = View.GONE
                poiDetailView.digImgCategory.visibility = View.GONE
            }
        }
        poiDetailView.btnShowMap.setOnClickListener {
            bottomSheetDialog.dismiss()
            preview(poiDetail)

        }
        bottomSheetDialog.show()
    }

    fun preview(poiDetail: PoiDetail) {

        poiDetailRouting = poiDetail
        if (latLngUser != null) {
            smartNavigation(poiDetail)
            calculateEta(poiDetail)
            showNavigationLayout()
        } else {
            Toast.makeText(activity, "Location not available for routing", Toast.LENGTH_SHORT).show()
        }

    }

    private var path: JSONArray? = null
    private fun smartNavigation(poiDetail: PoiDetail) {
        // implement your navigation
        if (locManager != null && latLngUser != null) {
            val latLng = LatLng(poiDetail.lat!!, poiDetail.lng!!)
            path = locManager!!.getShortestRoute(latLngUser!!.wrap(), latLng, level, poiDetail.levelid)

            if (path == null || path?.length() == 0) {
                Toast.makeText(context, "No Route Found", Toast.LENGTH_SHORT).show()
            } else {
                optCurrent = PolylineOptions().color(ContextCompat.getColor(context, R.color.colorBloody))
                val points = ArrayList<Point>()
                val pointsOtherLevel = ArrayList<Point>()

                //points.add(Point.fromLngLat(latLngUser?.longitude!!, latLngUser?.latitude!!))
                for (i in 0 until path?.length()!!) {
                    val latLngLeg = LatLng(path!!.getJSONObject(i).getDouble("lat"), path!!.getJSONObject(i).getDouble("lng"))
                    Log.d("TAG", "loop count $i")
//                    if (i < path.length() - 1) {
                    val item = path!!.getJSONObject(i)
//
                    Log.d("TAG", " levels are ${item.getString("level")} \n other lvel $level path length ${path?.length()}")
                    if (item.getString("level") == level) {
                        points.add(Point.fromLngLat(latLngLeg.longitude, latLngLeg.latitude))
//
                    } else {
                        pointsOtherLevel.add(Point.fromLngLat(latLngLeg.longitude, latLngLeg.latitude))
                    }
                }

                if (points.size > 0) {
                    mapboxMap?.getStyle { style ->
                        style.removeSource("simplifiedLine")
                        style.removeLayer("simplifiedLine")
                        style.removeSource("otherLevelLine")
                        style.removeLayer("otherLevelLine")

                    }
                    if (pointsOtherLevel.size > 0) {
                        points.add(0, pointsOtherLevel.last())

                    }
                    //val listOfPoint = PolylineUtils.simplify(points)
                    addLine("simplifiedLine", Feature.fromGeometry(LineString.fromLngLats(points)), "#D13216")
                    if (pointsOtherLevel.size > 0) {

                        addLine("otherLevelLine", Feature.fromGeometry(LineString.fromLngLats(pointsOtherLevel)), "#E675787B")
                    }
                    /* removePolyLine()
                    latLngPolyline.forEachIndexed { index, polylineOptions ->
                        if (index > 0) {
                            polylineOptions.alpha(0.5f)
                        }
                        mapboxMap?.addPolyline(polylineOptions)
                    }*/
                } else {
                    showNotification("We are sorry! not able to find any route please try after some time", R.drawable.ic_compass)
                    return
                }
            }
        }
    }

    private fun addLine(layerId: String, feature: Feature, lineColorHex: String) {
        mapboxMap?.getStyle { style ->
            style.removeSource(layerId)
            style.removeLayer(layerId)
            style.addSource(GeoJsonSource(layerId, feature))
            style.addLayer(LineLayer(layerId, layerId).withProperties(
                    lineColor(com.mapbox.mapboxsdk.utils.ColorUtils.colorToRgbaString(Color.parseColor(lineColorHex))),
                    lineWidth(6f)))
        }
    }

    private fun showNavigationLayout() {
        bottomSheetLayoutMap.visibility = View.GONE
        startNavigationLayout.visibility = View.VISIBLE
        llBottom.visibility = View.GONE
        translateRecenterLayout(CommonUtils.pxFromDp(context, 90f).toInt())
        changeStatusBarColor(R.color.colorStatusBar)
    }

    private fun reRouting(latLng: LatLng?) {
        if (path == null || path?.length() == 0) {
            Toast.makeText(context, "No route to get projection.Make sure user is in navigation mode", Toast.LENGTH_LONG).show()
            return
        }
        if (latLng == null) {
            Toast.makeText(context, "No location", Toast.LENGTH_LONG).show()
            return
        }
        var d = 999999999.0
        val p = GeoCentricCordinates(latLng.latitude, latLng.longitude)
        var index = -1
        path?.let {
            for (i in 0 until it.length() - 1) {
                val p1 = LatLng(it.getJSONObject(i).getDouble("lat"), it.getJSONObject(i).getDouble("lng"))
                val l1 = GeoCentricCordinates(p1.latitude, p1.longitude)
                val p2 = LatLng(it.getJSONObject(i + 1).getDouble("lat"), it.getJSONObject(i + 1).getDouble("lng"))
                val l2 = GeoCentricCordinates(p2.latitude, p2.longitude)
                val dp: Double = p.distanceToLineSegMtrs(l1, l2)
                if (dp < d) {
                    d = dp
                    index = i
                }
            }
        }


        if (d >= 5) { //distance of current location from the route is greater than x meters.From trial and error set the x value
            //Toast.makeText(context, "Possible reroute trigger", Toast.LENGTH_LONG).show()
            smartNavigation(poiDetailRouting)
        }
    }

    private fun changeStatusBarColor(color: Int) {
        val window = activity!!.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(context, color)
    }

    private fun hideNavigationLayout() {
        bottomSheetLayoutMap.visibility = View.VISIBLE
        startNavigationLayout.visibility = View.GONE
        bottomSheetNavigation.visibility = View.GONE
        layoutEta.visibility = View.GONE
        llBottom.visibility = View.VISIBLE
        changeStatusBarColor(R.color.colorStatusBar)
        removePolyLine()
        translateRecenterLayout(0)
    }

    private fun calculateEta(poiDetail: PoiDetail) {
        if (latLngUser != null) {
            try {
                layoutEta.visibility = View.VISIBLE
                imgDirection.visibility = View.GONE
                txtTurnDes.visibility = View.GONE
                txtGoing.text = getString(R.string.going_to)
                txtDestination.visibility = View.VISIBLE

                var name = ""
                name = if (LocaleManager.getLanguage(context) == "ar") {
                    if (poiDetail.poiMultilingual?.ar?.name != null) {
                        poiDetail.poiMultilingual!!.ar?.name!!
                    } else {
                        poiDetail.poiMultilingual?.en!!.name!!
                    }
                } else {
                    poiDetail.poiMultilingual?.en!!.name!!
                }
                txtDestination.text = name
                destinationLatLng = LatLng(poiDetail.lat!!, poiDetail.lng!!)
                val distanceKM = CommonUtils.getDistanceFromLatLonInMeters(latLngUser?.latitude!!, latLngUser?.longitude!!,
                        poiDetail.lat!!, poiDetail.lng!!)
                val speedIs60MetersPerMinute = 60
                var estimatedDriveTimeInMinutes = distanceKM / speedIs60MetersPerMinute
                var meter = " meter"
                if (distanceKM > 1) {
                    meter = " meters"
                }
                var timeUnit = " mins"
                if (estimatedDriveTimeInMinutes < 1) {
                    timeUnit = " seconds"
                    estimatedDriveTimeInMinutes *= 60
                }
                txtTime.text = (String.format(Locale.US, "%.0f", estimatedDriveTimeInMinutes) + timeUnit + " • "
                        + String.format(Locale.US, "%.0f", distanceKM) + meter)
            } catch (e: Exception) {// risied when app is in background
                e.printStackTrace()
            }

        }

    }


    fun searchText(query: String) {
        flightSearchPublishSubject.onNext(query)

    }

    fun visibleFragment(isVisibleToUser: Boolean) {
        if (isVisibleToUser) {
            val packageManager = activity?.packageManager

            CommonUtils.hideKeyboard(activity!!, view!!)

            if (::mapBottomSheet.isInitialized) {
                removeArrrowAnimatoin()
            }
        }
    }

    private fun search(query: String): List<PoiDetail> {
        if (query.isEmpty()) {
            if (poiList.isNotEmpty())
                return poiList
        }
        val regex = ".*" + query.trim().toLowerCase() + ".*"
        val pattern = Pattern.compile(regex)
        return poiList.filter {

            if (it.poiMultilingual?.ar == null) {
                val locale = ShjLocal(it.poiMultilingual!!.en.locationText,
                        it.poiMultilingual!!.en.description, it.poiMultilingual!!.en.name)
                it.poiMultilingual = PoiMultilingual(locale,
                        it.poiMultilingual!!.en, locale, locale)
            }
            pattern.matcher(it.poiMultilingual!!.en.name?.toLowerCase()).matches() ||
                    pattern.matcher(it.poiMultilingual!!.ar?.name?.toLowerCase()).matches()
        }
    }

    private fun setupTabIcons() {
        val tabText = resources.getStringArray(R.array.map_filter_tabs)
        val tabIcons = arrayOf(0, R.drawable.ic_food, R.drawable.ic_shop,
                R.drawable.ic_services, R.drawable.ic_facilities)
        val tabTextColor = arrayOf(R.color.tab_all_places_txt_color, R.color.food_filter_selector_color, R.color.shop_filter_selector_color,
                R.color.services_filter_selector_color, R.color.facility_filter_selector_color)

        tabLayoutFilter.setSelectedIndicatorColors(ContextCompat.getColor(context, R.color.colorInsuidePurple),
                ContextCompat.getColor(context, R.color.colorOrange), ContextCompat.getColor(context, R.color.colorBlue),
                ContextCompat.getColor(context, R.color.colorBlueDark), ContextCompat.getColor(context, R.color.colorBloodyDry))
        val inflater = LayoutInflater.from(tabLayoutFilter.context)
        tabLayoutFilter.setCustomTabView { container, position, adapter ->
            val view = inflater.inflate(R.layout.tab, container, false)
            view.tabImage.setImageResource(tabIcons[position])
            view.txtTabName.setTextColor(resources.getColorStateList(tabTextColor[position]))//ContextCompat.getColor(context, tabTextColor[position]))
            view.txtTabName.text = tabText[position]
            view
        }
    }

    private fun setNavigationBottomSheet() {
        val colorCoolGray = ContextCompat.getColor(context, R.color.colorCoolGrey)
        val colorBloodyRed = ContextCompat.getColor(context, R.color.colorBloody)
        bottomSheetBehaviorNavigation = BottomSheetBehavior.from(bottomSheetNavigation)
        bottomSheetBehaviorNavigation.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (slideOffset in 0.0..1.0) {
                    textDirection.setTextColor(ColorUtils.blendARGB(colorCoolGray, colorBloodyRed, slideOffset))
                    textDirection.scaleX = 0.67f + (0.33 * (slideOffset)).toFloat()
                    textDirection.scaleY = 0.67f + (0.33 * (slideOffset)).toFloat()
//                    textDirection.alpha = 0.6f + (0.4 * (slideOffset)).toFloat()
                }
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        filterGroup.visibility = View.GONE
                        if (swipeTop.isShown) {
                            swipeTop.visibility = View.GONE

                        }
                    }
                }
            }
        })
    }

    private fun setupMapBottomSheet() {
        try {
            mapBottomSheet = ViewPagerBottomSheetBehavior.from(bottomSheetLayoutMap)
            pagerAdapter = MapPagerAdapter(childFragmentManager, getFragments())
            mapViewPager.adapter = pagerAdapter
            translateHeight = CommonUtils.pxFromDp(context, 30f)
            tabLayoutFilter.setViewPager(mapViewPager)
            mapViewPager.offscreenPageLimit = 5
            val colorCoolGray = ContextCompat.getColor(context, R.color.colorCoolGrey)
            val colorBloodyRed = ContextCompat.getColor(context, R.color.colorBloody)
            BottomSheetUtils.setupViewPager(mapViewPager)
            mapBottomSheet.setBottomSheetCallback(object : ViewPagerBottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {

                    llBottom.translationY = translateHeight * slideOffset

                    if (slideOffset in 0.0..1.0) {

                        textDirectory.setTextColor(ColorUtils.blendARGB(colorCoolGray, colorBloodyRed, slideOffset))
                        textDirectory.scaleX = 1 + (0.5 * (slideOffset)).toFloat()
                        textDirectory.scaleY = 1 + (0.5 * (slideOffset)).toFloat()
                        tabLayoutFilter.alpha = slideOffset
                        btnShopping.alpha = (1 - slideOffset)
                        btnFoodDrink.alpha = (1 - slideOffset)
                        mapBackgroundOverlay.alpha = 0.4f * slideOffset
                    }
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        ViewPagerBottomSheetBehavior.STATE_COLLAPSED -> {

                            mapBackgroundOverlay.alpha = 0f
                            filterGroup.visibility = View.VISIBLE
                            btnShopping.alpha = 1f
                            btnFoodDrink.alpha = 1f
                            mapBottomSheet.peekHeight = CommonUtils.pxFromDp(context, 179f).toInt()
                            removeArrrowAnimatoin()
                        }
                        ViewPagerBottomSheetBehavior.STATE_EXPANDED -> {
                            filterGroup.visibility = View.GONE
                            if (swipeTop.isShown) {
                                swipeTop.visibility = View.GONE

                            }

                            removeArrrowAnimatoin()
                        }
                    }

                }
            })
        } catch (ise: IllegalArgumentException) {
            ise.printStackTrace()
        }

        btnFoodDrink.isActivated = true
        btnShopping.isActivated = true
        btnShopping.setOnClickListener {
            if (mapboxMap != null) {
                btnShopping.isActivated = !btnShopping.isActivated
                mPresenter.filterSymbolLayerPoi(mapboxMap!!, "layer_floor_0", "shop", !btnShopping.isActivated)
            } else {
                showNotification("map not loader yet", R.drawable.ic_alert_triangle)
            }
        }
        btnFoodDrink.setOnClickListener {
            if (mapboxMap != null) {
                btnFoodDrink.isActivated = !btnFoodDrink.isActivated
                mPresenter.filterSymbolLayerPoi(mapboxMap!!, "layer_floor_0", "food", !btnFoodDrink.isActivated)
            } else {
                showNotification("map not loader yet", R.drawable.ic_alert_triangle)
            }
        }
    }

    private fun removeArrrowAnimatoin() {
        if (arrowValueAnimator != null) {
            arrowValueAnimator?.reverse()
            arrowValueAnimator?.removeAllListeners()
        }
    }

    private fun startArrow(rotation: Int): ValueAnimator {
        val heightBottomSheet = CommonUtils.pxFromDp(context, 179f).toInt()
        val arrowAnim = ValueAnimator.ofInt(handleRight.rotation.toInt(), rotation)
        arrowAnim?.addUpdateListener { valueAnimator ->
            val animatedValue = valueAnimator.animatedValue as Int
            handleRight.rotation = animatedValue.toFloat()
            handleLeft.rotation = -animatedValue.toFloat()
            mapBottomSheet.peekHeight = heightBottomSheet + animatedValue

        }
        arrowAnim?.duration = 150
        arrowAnim?.start()
        return arrowAnim
    }

    var translateTopHeader = 0f
    private lateinit var animator: ValueAnimator
    lateinit var animatorRecenter: ValueAnimator


    @SuppressLint("RestrictedApi")
    override fun onCameraMoveStarted(reason: Int) {
        if (reason == MapboxMap.OnCameraMoveStartedListener.REASON_API_GESTURE) {
            isAnimateCamera = false
            if (!fabRecenter.isShown)
                fabRecenter.visibility = View.VISIBLE
            if (shareText.isShown) {

                try {
                    shareLocationBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
                } catch (uipa: UninitializedPropertyAccessException) {
                    uipa.printStackTrace()
                }
            }
        }
    }

    private fun translateRecenterLayout(translateHeight: Int) {
        animatorRecenter = mPresenter.animate(recenterLayout, translateHeight)
        //mPresenter.animate(floorLayout, translateHeight)
    }

    private fun hasPermissions(permissions: Array<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(this.context!!, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    private fun getFragments(): List<Fragment> {
        val fragments = ArrayList<Fragment>()
        fragments.add(PoiFragment())
        fragments.add(PoiFragment())
        fragments.add(PoiFragment())
        fragments.add(PoiFragment())
        fragments.add(PoiFragment())
        return fragments
    }

    override var mPresenter: MapPresenter = MapPresenter()

    override fun showAllPoi(poiList: List<PoiDetail>) {
        this.poiList = poiList as ArrayList<PoiDetail>
        val count = pagerAdapter.count - 1
        Log.d("TAG", "adapter count $count")
        for (i in 0..count) {
            try {
                val poiFragment = pagerAdapter.getItem(i) as PoiFragment
                when (i) {
                    0 -> {
                        poiFragment.setPoiList(this, poiList)
                    }
                    1 -> {
                        val filteredList = poiList.filter {
                            it.category.equals("food & drink", true)
                        }
                        poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>)
                    }
                    2 -> {
                        val filteredList = poiList.filter { it ->
                            it.category.equals("shopping", true)
                        }
                        poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>)
                    }

                    3 -> {
                        val filteredList = poiList.filter {
                            it.category.equals("services", true)
                        }
                        poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>)
                    }
                    4 -> {
                        val filteredList = poiList.filter { it ->
                            it.category.equals("utilities", true)
                        }
                        poiFragment.setPoiList(this, filteredList as ArrayList<PoiDetail>)
                    }
                }
            } catch (e: IndexOutOfBoundsException) {

                Log.d("TAG", "Out of bound exceptions -----")
                e.printStackTrace()
            }

        }

        val gson = Gson()
        val features = ArrayList<Feature>()
        poiList.forEach { poi ->
            val feature = Feature.fromGeometry(
                    Point.fromLngLat(poi.lng!!, poi.lat!!))
            feature.addProperty(poi.id, gson.toJsonTree(poi))
            feature.addStringProperty("level", poi.levelid!!)

            val localeData = when (LocaleManager.getLanguage(context)) {
                LocaleManager.LANGUAGE_ENGLISH -> {
                    poi.poiMultilingual?.en
                }
                LocaleManager.LANGUAGE_ARABIC -> {
                    poi.poiMultilingual?.ar?.let {
                        it
                    } ?: run {
                        poi.poiMultilingual?.en
                    }
                }
                LocaleManager.LANGUAGE_RUSSSIAN -> {
                    poi.poiMultilingual?.ru?.let {
                        it
                    } ?: run {
                        poi.poiMultilingual?.en
                    }
                }
                LocaleManager.LANGUAGE_HINDI -> {
                    poi.poiMultilingual?.hi?.let {
                        it
                    } ?: run {
                        poi.poiMultilingual?.en
                    }
                }
                else -> {
                    poi.poiMultilingual?.en
                }
            }
            /* if (poi.poiMultilingual?.ar == null) {
                 val locale = ShjLocal(poi.poiMultilingual!!.en.locationText,
                         poi.poiMultilingual!!.en.description, poi.poiMultilingual!!.en.name)*/

            poi.poiMultilingual = PoiMultilingual(localeData!!,
                    poi.poiMultilingual!!.en, localeData, localeData)
//            }
            when (poi.category?.toLowerCase()) {
                "food & drink" -> {
                    feature.addStringProperty("name", poi.poiMultilingual!!.en.name)
                    feature.addStringProperty("name_ar", poi.poiMultilingual!!.ar?.name)
                    feature.addStringProperty("name_hi", poi.poiMultilingual!!.hi?.name)
                    feature.addStringProperty("name_ru", poi.poiMultilingual!!.ru?.name)
                    feature.addStringProperty("color", "food")
                    feature.addStringProperty("image", R.drawable.food.toString())
                    features.add(feature)
                }
                "shopping" -> {
                    feature.addStringProperty("name", poi.poiMultilingual!!.en.name)
                    feature.addStringProperty("name_ar", poi.poiMultilingual!!.ar?.name)
                    feature.addStringProperty("name_hi", poi.poiMultilingual!!.hi?.name)
                    feature.addStringProperty("name_ru", poi.poiMultilingual!!.ru?.name)
                    feature.addStringProperty("color", "shop")
                    feature.addStringProperty("image", R.drawable.shopping.toString())
                    features.add(feature)
                }
                "utilities" -> {
                    feature.addStringProperty("color", "utility")
                    when (poi.subcategory?.toLowerCase()) {
                        "escalator" -> {
                            feature.addStringProperty("image", R.drawable.escalator.toString())
                            features.add(feature)
                        }
                        "lift" -> {
                            feature.addStringProperty("image", R.drawable.lift.toString())
                            features.add(feature)
                        }
                        "staircase" -> {
                            feature.addStringProperty("image", R.drawable.stairs.toString())
                            features.add(feature)
                        }
                        "drinking water" -> {
                            feature.addStringProperty("image", R.drawable.water.toString())
                            features.add(feature)
                        }
                        "toilet" -> {
                            feature.addStringProperty("image", R.drawable.toilet.toString())
                            features.add(feature)
                        }
                        "prayer room" -> {
                            feature.addStringProperty("image", R.drawable.prayer_room.toString())
                            features.add(feature)
                        }
                        "smoking room" -> {
                            feature.addStringProperty("image", R.drawable.smoking.toString())
                            features.add(feature)
                        }
                        "baby changing room" -> {
                            feature.addStringProperty("image", R.drawable.baby.toString())
                            features.add(feature)
                        }
                        "service desk" -> {
                            feature.addStringProperty("image", R.drawable.info_helpdesk.toString())
                            features.add(feature)
                        }
                        "help desk" -> {
                            feature.addStringProperty("image", R.drawable.info_helpdesk.toString())
                            features.add(feature)
                        }
                        "fids" -> {
                            feature.addStringProperty("image", R.drawable.fids.toString())
                            features.add(feature)
                        }
                        "customs" -> {
                            feature.addStringProperty("image", R.drawable.customs.toString())
                            features.add(feature)
                        }
                        "baggage claim" -> {
                            feature.addStringProperty("name", poi.poiMultilingual!!.en.name)
                            feature.addStringProperty("name_ar", poi.poiMultilingual!!.ar?.name)
                            feature.addStringProperty("name_hi", poi.poiMultilingual!!.hi?.name)
                            feature.addStringProperty("name_ru", poi.poiMultilingual!!.ru?.name)
                            feature.addStringProperty("image", R.drawable.baggage.toString())
                            features.add(feature)
                        }
                        "passport control" -> {
                            feature.addStringProperty("image", R.drawable.passport.toString())
                            features.add(feature)
                        }
                        "security screening" -> {
                            feature.addStringProperty("image", R.drawable.security.toString())
                            features.add(feature)
                        }
                        "check-in counters" -> {
                            feature.addStringProperty("name", poi.poiMultilingual!!.en.name)
                            feature.addStringProperty("name_ar", poi.poiMultilingual!!.ar?.name)
                            feature.addStringProperty("name_hi", poi.poiMultilingual!!.hi?.name)
                            feature.addStringProperty("name_ru", poi.poiMultilingual!!.ru?.name)
                            feature.addStringProperty("image", R.drawable.check_in.toString())
                            features.add(feature)

                        }
                        "gates" -> {
                            feature.addStringProperty("name", poi.poiMultilingual!!.en.name)
                            feature.addStringProperty("name_ar", poi.poiMultilingual!!.ar?.name)
                            feature.addStringProperty("name_hi", poi.poiMultilingual!!.hi?.name)
                            feature.addStringProperty("name_ru", poi.poiMultilingual!!.ru?.name)
                            feature.addStringProperty("image", R.drawable.gate.toString())
                            features.add(feature)
                        }
                        "transit" -> {
                            feature.addStringProperty("image", R.drawable.transit.toString())
                            features.add(feature)
                        }
                        "lost and found" -> {
                            feature.addStringProperty("image", R.drawable.lost_found.toString())
                            features.add(feature)
//                            poi.poiUrl = R.drawable.lost_found.toString()
                        }
                        "charging" -> {
                            feature.addStringProperty("image", R.drawable.power.toString())
                            features.add(feature)
                        }
                        "exit" -> {
                            feature.addStringProperty("image", R.drawable.exit.toString())
                            features.add(feature)
                        }
                        "seating" -> {
                            feature.addStringProperty("image", R.drawable.seating.toString())
                            features.add(feature)
                        }
                        "bus" -> {
                            feature.addStringProperty("image", R.drawable.bus.toString())
                            features.add(feature)
                        }
                        "car" -> {
                            feature.addStringProperty("image", R.drawable.car.toString())
                            features.add(feature)
                        }
                        "metro" -> {
                            feature.addStringProperty("image", R.drawable.metro.toString())
                            features.add(feature)
                        }
                        "train" -> {
                            feature.addStringProperty("image", R.drawable.metro.toString())
                            features.add(feature)
                        }
                        "medical" -> {
                            feature.addStringProperty("image", R.drawable.medical.toString())
                            features.add(feature)
                        }
                        "disabled toilet" -> {
                            feature.addStringProperty("image", R.drawable.disabled_toilet.toString())
                            features.add(feature)
                        }
                        "female toilet" -> {
                            feature.addStringProperty("image", R.drawable.female_toilet.toString())
                            features.add(feature)
                        }
                    }
                }
                "services" -> {
                    feature.addStringProperty("name", poi.poiMultilingual!!.en.name)
                    feature.addStringProperty("name_ar", poi.poiMultilingual!!.ar?.name)
                    feature.addStringProperty("name_hi", poi.poiMultilingual!!.hi?.name)
                    feature.addStringProperty("name_ru", poi.poiMultilingual!!.ru?.name)
                    feature.addStringProperty("color", "service")
                    when (poi.subcategory?.toLowerCase()) {
                        "lounge" -> {
                            feature.addStringProperty("image", R.drawable.lounge.toString())
                            features.add(feature)
                        }
                        "currency exchange" -> {
                            feature.addStringProperty("image", R.drawable.exchange.toString())
                            features.add(feature)
                        }
                        "ticket counter" -> {
                            feature.addStringProperty("image", R.drawable.ticket.toString())
                            features.add(feature)
                        }
                        "children's play area" -> {
                            feature.addStringProperty("image", R.drawable.play.toString())
                            features.add(feature)
                        }
                        "atm" -> {
                            feature.addStringProperty("image", R.drawable.atm.toString())
                            features.add(feature)
                        }
                        "telephone" -> {
                            feature.addStringProperty("image", R.drawable.phone.toString())
                            features.add(feature)
                        }

                    }
                }
            }

        }
        level0Features = features.filter { it.getStringProperty("level") == "0" } as ArrayList<Feature>
        level1Features = features.filter { it.getStringProperty("level") == "1" } as ArrayList<Feature>
        level2Features = features.filter { it.getStringProperty("level") == "2" } as ArrayList<Feature>
        if (mapboxMap != null) {
            if (mapUrl != "") {
                addImagesToMap()
                if (level == "1") {
                    mPresenter.addSymbolLayer(mapboxMap!!, "source_floor_1", "layer_floor_1", level1Features)
                } else {
                    mPresenter.addSymbolLayer(mapboxMap!!, "source_floor_2", "layer_floor_2", level2Features)
                }
            } else {
                addImagesToMap()
                if (showing == 1) {
                    mPresenter.addSymbolLayer(mapboxMap!!, "source_floor_1", "layer_floor_1", level1Features)
                } else {
                    mPresenter.addSymbolLayer(mapboxMap!!, "source_floor_2", "layer_floor_2", level2Features)
                }
            }

        }

    }

    private fun getMarkerIcon(iconName: Int): Bitmap {
        return if (resources != null) {
            BitmapFactory.decodeResource(resources, iconName)
        } else {
            null!!
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()

        if (::mIALocationManager.isInitialized) {
            mIALocationManager!!.requestLocationUpdates(IALocationRequest.create(), this)

        } else {
            reInitIA()
        }


    }

    fun reInitIA() {
        mIALocationManager = IALocationManager.create(this.context)
        //mLocationManager.setLocation(IALocation.from(IARegion.floorPlan(getString(R.string.indoor_atlas_api_key_floor0))))
//        mLocationManager.requestLocationUpdates(IALocationRequest.create(), getPendingIntent())
//        mLocationManager.registerOrientationListener(IAOrientationRequest(5.0, 5.0), this)
//        mLocationManager.registerRegionListener(mRegionListener)
    }


    override fun onPause() {
        super.onPause()
        mapView.onPause()
        mPresenter.setMaxVolume()
        if (mIALocationManager != null) {
            mIALocationManager!!.removeLocationUpdates(this)
        }

    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroyView() {
        if (mIALocationManager != null) {
            mIALocationManager!!.destroy()
        }
        mapView.onDestroy()
        isRunning = false
        super.onDestroyView()
    }

    override fun onDestroy() {
        proximityManager?.disconnect()
        proximityManager = null


        if (tts != null) {
            tts?.stop()
            tts?.shutdown()
        }
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    var marker: Marker? = null


    override fun onInit(status: Int) {
        when (status) {
            TextToSpeech.SUCCESS -> {
                tts?.language = Locale.US
            }
        }
    }


    override fun onClick(v: View) {
        //this.activity!!.runOnUiThread(Runnable { Toast.makeText(this.context, "Converge complete", Toast.LENGTH_SHORT).show() })
        when (v.id) {
            R.id.groundFloor -> {
                animateReverse()
                showFirstFloor()
                showNotification(getString(R.string.msg_first_floor), R.drawable.ic_compass)
            }
            R.id.firstFloor -> {

                animateReverse()
                showSecondFloor()
                showNotification("You are at second floor", R.drawable.ic_compass)
            }
            R.id.logText -> {
                animate()
                logText.alpha = 0f
                floorContainer.alpha = 1f
                recenterLayout.visibility = View.GONE
                llBottom.visibility = View.GONE
                bottomSheetLayoutMap.visibility = View.GONE
//                genericAnimator = mPresenter.animate(llBottom, CommonUtils.pxFromDp(context, 86f).toInt())
//                valueAnimPeekHeight = mPresenter.animate(bottomSheetLayoutMap, CommonUtils.pxFromDp(context, 192f).toInt())
            }
        }

    }

    private fun addImagesToMap() {
        val imageMap = HashMap<String, Bitmap>()
        mPresenter.poiImageMap().forEach { iconName ->
            imageMap[iconName.toString()] = getMarkerIcon(iconName)
        }
        mapboxMap?.style?.addImages(imageMap)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap

        translateTopHeader = CommonUtils.pxFromDp(context, 48f)
        mapboxMap.addOnMapClickListener(this)
        mapboxMap.addOnCameraMoveStartedListener(this)
        if (mapUrl == "") {
            showFirstFloor()
        } else {
            setMap()
        }
        mPresenter.loadPoi(getString(R.string.token), getString(R.string.appname))
    }

    override fun reload() {
    }

    override fun onMapClick(point: LatLng): Boolean {

        if (!startNavigationLayout.isShown && !stopNavigationLayout.isShown) {
//            //headerHide()
        }

        val pointF = mapboxMap?.projection?.toScreenLocation(point)
        val features = mapboxMap?.queryRenderedFeatures(pointF!!)
        if (features?.isNotEmpty()!! && poiList.isNotEmpty()) {
            for (feature in features) {
                for (poi in poiList) {
                    if (feature.getProperty(poi.id) != null) {
                        val poiDetailDialog = PoiDetailDialog.newInstance(this, poi)
                        poiDetailDialog.show(fragmentManager!!, "PoiDetailDialog")
//                        setPoiBottomSheet(poi, feature.getStringProperty("image"))
                        break
                    }
                }
            }
        }
        // reversing the animation on floor layout
        if (valueAnim != null)
            animateReverse()
        return false
    }


    private fun convertSphericalToCartesian(latitude: Double, longitude: Double): DoubleArray {
        val earthRadius = 6371000.0 //radius in m
        val lat = Math.toRadians(latitude)
        val lon = Math.toRadians(longitude)
        val x = earthRadius * cos(lat) * cos(lon)
        val y = earthRadius * cos(lat) * sin(lon)
        val z = earthRadius * sin(lat)
        return doubleArrayOf(x, y, z)
    }


    fun getFloorPlan(): Boolean {
        var found = false
        for (i in 0 until floorplans.length()) {
            try {
                val obj = floorplans.getJSONObject(i)
                val terminalval = obj.getString("terminal")
                val sliceid = obj.getString("sliceid")
                val levelval = obj.getString("level")
                if (terminal == terminalval && slice == sliceid && level == levelval) {
                    floorPlanObject = obj
                    mapUrl = obj.getString("mapboxurl")
                    found = true
                    return true
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        return found
    }

    private fun setMap() {
        mapboxMap?.setStyle(mapUrl, Style.OnStyleLoaded {
            styleLoaded = true
            // Custom map style has been loaded and map is now ready

            val compassNeedleSymbolLayerIcon = BitmapFactory.decodeResource(
                    resources, R.mipmap.user_location_marker)
            this.mapboxMap?.style?.addImage(AIRCRAFT_MARKER_ICON_ID, compassNeedleSymbolLayerIcon)
            val resources: Resources
            try {
                resources = getResources()
            } catch (e: NullPointerException) {
                e.printStackTrace()
                return@OnStyleLoaded
            }

            this.mapboxMap?.style?.addImage("magnetic", getMarkerIcon(resources, R.mipmap.mappingpoint))

//            mPresenter.loadPoi(getString(R.string.token), getString(R.string.appname))


        })
    }

    private fun getMarkerIcon(resources: Resources, marker_shopping: Int): Bitmap {
        return BitmapFactory.decodeResource(resources, marker_shopping)
    }

    override fun onNeighboursLocation(location: DoubleArray?) {
        var donotupdate: Boolean = false
        val firstLatLng = convertCartesianToSpherical(doubleArrayOf(location!![0], location[1], location[2]))
        if (firstLatLng[0] < 0 || firstLatLng[0] == Double.NaN) {
            print("catch this")
            donotupdate = true
        }
        if (!regionDataAvailable) {
            return
        }
        if (!donotupdate) {
            val opts = generatePerimeter(
                    LatLng(firstLatLng[0], firstLatLng[1]), location[6] / 1000.0, 64)

            if (!converged) {
                if (location[4] > 0) {
                    converged = true
                    this.activity!!.runOnUiThread(Runnable { Toast.makeText(this.context, "Converge complete", Toast.LENGTH_SHORT).show() })
                }
            }
            val deviceId: String = Settings.Secure.getString(context.contentResolver,
                    Settings.Secure.ANDROID_ID)
            val timestamp = System.currentTimeMillis()
            var degrees = toDegrees(location[3])
            this.activity!!.runOnUiThread {
                //var degrees = toDegrees(location[3])
                val mapHeading = mapboxMap!!.cameraPosition.bearing
                if (degrees > 360) {
                    degrees -= 360
                }
                if (degrees < 0) {
                    degrees += 360
                }
                currHeading = degrees


                latLngUser = LatLng(firstLatLng[0], firstLatLng[1])
                updateMarkerPosition(firstLatLng[0], firstLatLng[1], degrees - mapHeading)

                if (location[5] == -99999.0 && converged) {
                    this.activity!!.runOnUiThread(Runnable {
                        Toast.makeText(activity, "Re init Here Probably?", Toast.LENGTH_SHORT).show()
                    })
                }
                if (circlePolygon == null) {
                    circlePolygon = mapboxMap!!.addPolygon(opts)
                } else {
                    circlePolygon!!.points = opts.points
                }


            }
            sendDataToServer(firstLatLng[0], firstLatLng[1], degrees, deviceId, timestamp, location[6], location[7])
        }


        /*update location zoom based on accuracy
        * 0 means 100% accuracy beyond 25-50 0% accuracy
        * min zoom 17 max zoom 18.5*/
//        var accuracy = location[6] // location radius in meters
//        if (accuracy > 25) accuracy = 25.0 // base on Prathyush input
//        //val zoomLevel = 17 + (accuracy * (18.5 - 17) / (accuracy+1) )// zoomLevel in between 17-18.5
//        //at zero I considered 19 zoom level
//        val zoomLevel = 19 - ((accuracy/25.0)*(19-17))
//        animateCamera(latLng = LatLng(firstLatLng[0], firstLatLng[1]), zoom = zoomLevel)


        this.activity!!.runOnUiThread {
            /*update location zoom based on accuracy
            * 0 means 100% accuracy beyond 25-50 0% accuracy
            * min zoom 17 max zoom 18.5*/
            var accuracy = location[6] // location radius in meters
            if (accuracy > 25) accuracy = 25.0 // base on Prathyush input
            //val zoomLevel = 17 + (accuracy * (18.5 - 17) / (accuracy+1) )// zoomLevel in between 17-18.5
            //at zero I considered 19 zoom levels
            val zoomLevel = 18.5 - ((accuracy / 25.0) * (18.5 - 17))
            animateCamera(latLng = LatLng(firstLatLng[0], firstLatLng[1]), zoom = zoomLevel)

            if (location[5] == -99999.0 && converged) {
                this.activity!!.runOnUiThread(Runnable {
                    Toast.makeText(activity, "Reinit Here Probably?", Toast.LENGTH_SHORT).show()
                })
            }
        }


    }

    private fun sendDataToServer(latitude: Double, longitude: Double, headDirection: Double, deviceId: String, time: Long, confidence: Double, measurementAccuracy: Double) {
        val obj: JSONObject = JSONObject()
        obj.put("lat", latitude)
        obj.put("lng", longitude)
        obj.put("heading", headDirection)
        obj.put("deviceid", deviceId)
        obj.put("timestamp", time)
        obj.put("confidenceinmetres", confidence)
        obj.put("level", level)
        //obj.put("measurementaccuracypercentage", measurementAccuracy)
        obj.put("trailId", trailId)

        NetworkManager.sendUserLocation(obj, null)
    }


    private var markerAnimator: ValueAnimator? = null
    private var currentPosLatLng: LatLng? = null
    private fun updateMarkerPosition(latitude: Double, longitude: Double, headDirection: Double) {
        if (mapboxMap != null && mapboxMap!!.style != null && mapboxMap!!.style!!.getSource(GEOJSON_SOURCE_ID) != null) {
            val source: GeoJsonSource? = mapboxMap?.style?.getSourceAs(GEOJSON_SOURCE_ID)
            if (source != null) {
                markerAnimator?.let {
                    if (it.isStarted) {
                        currentPosLatLng = markerAnimator?.animatedValue as LatLng
                        animator.cancel()
                    }
                }

                markerAnimator = ObjectAnimator.ofObject(latLngEvaluator, currentPosLatLng, LatLng(latitude, longitude))
                markerAnimator?.addUpdateListener(animatorUpdateListener)
                markerAnimator?.start()
                currentPosLatLng = LatLng(latitude, longitude)

//                source.setGeoJson(Feature.fromGeometry(Point.fromLngLat(longitude, latitude)))


                layer!!.setProperties(
                        PropertyFactory.iconRotate(headDirection.toFloat())
                )
            }
        }

    }

    private val animatorUpdateListener = ValueAnimator.AnimatorUpdateListener { animation ->
        val animatedPosition = animation!!.animatedValue as LatLng
        geoJsonSource?.setGeoJson(Point.fromLngLat(animatedPosition.longitude, animatedPosition.latitude))
    }

    // Class is used to interpolate the marker animation.
    private val latLngEvaluator = object : TypeEvaluator<LatLng> {
        private val latLng = LatLng()
        override fun evaluate(fraction: Float, startValue: LatLng?, endValue: LatLng?): LatLng {
            latLng.latitude = startValue!!.latitude
            +((endValue!!.latitude - startValue.latitude) * fraction)
            latLng.longitude = startValue.longitude + ((endValue.longitude - startValue.longitude) * fraction)
            return latLng
        }

    }


    override fun particlesLocation(particles: Array<out DoubleArray>?) {

    }

    override fun onInitialLocation(location: DoubleArray?) {
        val firstLatLng = convertCartesianToSpherical(doubleArrayOf(location!![0], location!![1], location!![2]))

        val firstLat = LatLng(firstLatLng[0], firstLatLng[1])


        val heading = location[3]

        latLngUser = firstLat
        startSense(firstLat)
    }

    private fun convertCartesianToSpherical(cartesian: DoubleArray): DoubleArray {
        val r = sqrt(cartesian[0] * cartesian[0] + cartesian[1] * cartesian[1] + cartesian[2] * cartesian[2])
        val lat = 180 / PI * asin(cartesian[2] / r)
        val lon = 180 / PI * atan2(cartesian[1], cartesian[0])


        return doubleArrayOf(lat, lon)
    }


    private fun generatePerimeter(centerCoordinates: LatLng,
                                  radiusInKilometers: Double, numberOfSides: Int): PolygonOptions {
        val positions = java.util.ArrayList<LatLng>()
        val distanceX = radiusInKilometers / (111.319 * cos(centerCoordinates.latitude * PI / 180))
        val distanceY = radiusInKilometers / 110.574

        val slice = 2 * PI / numberOfSides

        var theta: Double
        var x: Double
        var y: Double
        var position: LatLng
        for (i in 0 until numberOfSides) {
            theta = i * slice
            x = distanceX * cos(theta)
            y = distanceY * sin(theta)

            position = LatLng(centerCoordinates.latitude + y,
                    centerCoordinates.longitude + x)
            positions.add(position)
        }
        return PolygonOptions()
                .addAll(positions)
                .fillColor(Color.parseColor("#4DD0E1"))
                .alpha(0.4f)
    }

    private fun startSense(point: LatLng) {
        var stepLengthString: String? = "165"

        if (stepLengthString != null) {
            try {
                stepLengthString = stepLengthString.replace(",", ".")
                val savedBodyHeight = java.lang.Float.parseFloat(stepLengthString)
                if (savedBodyHeight < 241 && savedBodyHeight > 119) {
                    StepAndAzimutDetector.stepLength = savedBodyHeight / 222
                } else if (savedBodyHeight < 95 && savedBodyHeight > 45) {
                    StepAndAzimutDetector.stepLength = (savedBodyHeight * 2.54 / 222).toFloat()
                }
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }

        }
        //currentTime = System.currentTimeMillis()
        setInitialPosition(point.latitude, point.longitude)
        this.activity!!.runOnUiThread(Runnable { addUserMarker(point.latitude, point.longitude, heading) })


    }

    private fun addUserMarker(latitude: Double, longitude: Double, headDirection: Double) {
        try {
            if (mapboxMap != null && mapboxMap!!.style != null) {
                if (mapboxMap!!.style!!.getSource(GEOJSON_SOURCE_ID) != null) {
                    mapboxMap!!.style!!.removeSource(GEOJSON_SOURCE_ID)
                    //geoJsonSource?.let { mapboxMap!!.getStyle()!!.removeSource(it) }

                }
                if (mapboxMap!!.style!!.getSource(POLYGON_SOURCE_ID) != null) {
                    mapboxMap!!.style!!.removeSource(POLYGON_SOURCE_ID)

                }
                if (mapboxMap!!.style!!.getLayer(AIRCRAFT_LAYER_ID) != null) {
                    mapboxMap!!.style!!.removeLayer(AIRCRAFT_LAYER_ID)
                }

                if (mapboxMap!!.style!!.getLayer(SWEEP_LAYER_ID) != null) {
                    mapboxMap!!.style!!.removeLayer(SWEEP_LAYER_ID)
                }

                val OUTER_POINTS = java.util.ArrayList<Point>()
                val POINTS = java.util.ArrayList<List<Point>>()
                OUTER_POINTS.add(Point.fromLngLat(longitude, latitude))
                val pos = convertSphericalToCartesian(latitude, longitude)
                val latLng1 = nextPosition(5.0, 0.0, pos[0], pos[1], pos[2])
                val latLng2 = nextPosition(5.0, 360.0, pos[0], pos[1], pos[2])
                OUTER_POINTS.add(Point.fromLngLat(latLng1[1], latLng1[0]))
                OUTER_POINTS.add(Point.fromLngLat(latLng2[1], latLng2[0]))
                OUTER_POINTS.add(Point.fromLngLat(longitude, latitude))
                POINTS.add(OUTER_POINTS)

                geoJsonSource = GeoJsonSource(GEOJSON_SOURCE_ID, Feature.fromGeometry(
                        Point.fromLngLat(longitude, latitude)))

                currentPosLatLng = LatLng(latitude, longitude)
                headingErrorSource = GeoJsonSource(POLYGON_SOURCE_ID, Polygon.fromLngLats(POINTS))

                if (mapboxMap!!.style!!.getSource(GEOJSON_SOURCE_ID) == null) {
                    mapboxMap!!.style!!.addSource(geoJsonSource!!)
                    println("this has been set here")
                    println(latitude.toString())
                    println(longitude.toString())

                } else {
                    println("this has been why here?")
                }

                if (mapboxMap!!.style!!.getSource(POLYGON_SOURCE_ID) == null) {
                    mapboxMap!!.style!!.addSource(headingErrorSource!!)

                }

                layer = SymbolLayer(AIRCRAFT_LAYER_ID, GEOJSON_SOURCE_ID)
                        .withProperties(
                                PropertyFactory.iconImage(AIRCRAFT_MARKER_ICON_ID),
                                PropertyFactory.iconRotate(headDirection.toFloat()),
                                PropertyFactory.iconIgnorePlacement(true),
                                PropertyFactory.iconAllowOverlap(true)
                        )
                mapboxMap!!.getStyle()!!.addLayer(layer!!)

                sweepLayer = FillLayer(SWEEP_LAYER_ID, POLYGON_SOURCE_ID)
                        .withProperties(PropertyFactory.fillColor(Color.parseColor("#f3e5f5")))
                mapboxMap!!.style!!.addLayer(layer!!)
                mapboxMap!!.style!!.addLayerBelow(sweepLayer!!, "sweep-label")

                initialPointSet = true

            } else {
                println("this has been error here")
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun setInitialPosition(startLat: Double,
                                   startLon: Double) {
        val altitude = 0.0
        val middleLat = startLat * 0.01745329252
        val distanceLongitude = 111.3 * cos(middleLat)

        StepAndAzimutDetector.initialize(startLat, startLon, distanceLongitude, altitude)
        StepAndAzimutDetector.setLocation(startLat, startLon)

        sensorController!!.startSensors(true)

    }

    private fun nextPosition(meters: Double, heading: Double, mX: Double, mY: Double, mZ: Double): DoubleArray {
        val position = Commonutils.convertCartesianToSpherical(doubleArrayOf(mX, mY, mZ))
        return Commonutils.distance(position, meters, heading)
    }

    override fun onStepUpdate(event: Int, pitchChange: Double, rollChange: Double, yawChange: Double, directionVector: Int) {
        if (event == 1) {
            steps += 1
        }
        getNeighbourLocation(directionVector, pitchChange, rollChange, yawChange, event)

    }

    private fun getNeighbourLocation(directionVector: Int, pitchChange: Double, rollChange: Double, yawChange: Double, event: Int) {

        var beacons = java.util.ArrayList<FilteredBeacon>()
        beacons = modifiedList.clone() as kotlin.collections.ArrayList<FilteredBeacon>

        val clonedMagValues = this.magValues.clone() as ArrayList<DoubleArray>
        this.magValues.clear()
        if (clonedMagValues.size == 0) {
            clonedMagValues.add(doubleArrayOf(0.0, 0.0, 0.0))
        }

        beaconBar = beacons.size
        activity!!.runOnUiThread {
            //textView!!.setText("Number of beacons : " + beaconBar + " Num of steps: " + steps)
            setImageBeacon(beaconBar)
        }

    }

    private fun setImageBeacon(beaconBar: Int) {
        if (beaconBar < 1) {
            beaconSignalImageView.setImageResource(R.mipmap.wifi_0)
        } else if (beaconBar < 2) {
            beaconSignalImageView.setImageResource(R.mipmap.wifi_1)
        } else if (beaconBar < 3) {
            beaconSignalImageView.setImageResource(R.mipmap.wifi_2)
        } else {
            beaconSignalImageView.setImageResource(R.mipmap.wifi_4)
        }

    }

    private fun getTransmissionPower(txPower: Int): Int {
        if (txPower == 0) {
            return -115
        } else if (txPower == 1) {
            return -84
        } else if (txPower == 2) {
            return -81
        } else if (txPower == 3) {
            return -77
        } else if (txPower == 4) {
            return -72
        } else if (txPower == 5) {
            return -69
        } else if (txPower == 6) {
            return -65
        } else if (txPower == 7) {
            return -59
        }
        return -77
    }
}