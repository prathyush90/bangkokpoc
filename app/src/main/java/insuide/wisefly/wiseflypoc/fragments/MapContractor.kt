package insuide.wisefly.wiseflypoc.models



import com.mapbox.geojson.Feature
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.netpiper.sia.base.BaseMvpPresenter
import com.netpiper.sia.base.BaseMvpView
import java.util.*

/**
 * Created by Furqan on 22-01-2018.
 */
object MapContractor {
    interface View : BaseMvpView {
        fun showAllPoi(poiList: List<PoiDetail>)

        fun reload()

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun loadPoi(token: String, appname: String)


        fun poiImageMap(): IntArray
        fun addSymbolLayer(mapboxMap: MapboxMap, sourceId: String, layerId: String, features: ArrayList<Feature>)
    }
}