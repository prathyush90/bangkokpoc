package insuide.wisefly.wiseflypoc.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.models.PoiDetail
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.fragment_poi.*


/**
 * A simple [Fragment] subclass.
 *
 */
class PoiFragment : Fragment() {
    lateinit var poiAdapter: MapPoiAdapter
    lateinit var mapFragment: Fragment

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_poi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerViewPoi.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        poiAdapter = MapPoiAdapter(this, ArrayList())
        recyclerViewPoi.adapter = poiAdapter
//        recyclerViewPoi.addItemDecoration(SpacesItemDecoration(CommonUtils.pxFromDp(this.context!!, 160f).toInt()))
    }


    fun setPoiList(mapFragment: Fragment, poiList: ArrayList<PoiDetail>, isSearch: Boolean = false) {

        this.mapFragment = mapFragment
        if (::poiAdapter.isInitialized) {
            if (isSearch && poiList.isEmpty()) {
                recyclerViewPoi.visibility = View.GONE
                txtNoResult.text = "No Result found"
                txtNoResult.visibility = View.VISIBLE
            } else {
                recyclerViewPoi.visibility = View.VISIBLE
                txtNoResult.visibility = View.GONE
            }
            poiAdapter.updateList(poiList)
        }
    }

    fun drawPreview(poiDetail: PoiDetail) {

        (mapFragment as BangkokMapFragment).preview(poiDetail)

    }

    fun collapse(poiDetail: PoiDetail) {
//        mapFragment.mapBottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED
        when (mapFragment) {

            is BangkokMapFragment -> {
                (mapFragment as BangkokMapFragment).showPoi(poiDetail)
                mapFragment.etSearchMap.setText("")
                //(mapFragment as BangkokMapFragment).navigateToGate(poiDetail)
            }
        }

    }

}
