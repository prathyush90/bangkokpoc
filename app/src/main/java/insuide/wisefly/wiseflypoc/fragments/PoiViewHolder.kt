package com.netpiper.sia.ui.map

import android.graphics.Color
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.netpiper.sia.manager.LocaleManager
import insuide.wisefly.wiseflypoc.DemoActivity
import insuide.wisefly.wiseflypoc.R
import insuide.wisefly.wiseflypoc.fragments.PoiContactAdapter
import insuide.wisefly.wiseflypoc.models.PoiDetail
import kotlinx.android.synthetic.main.poi_detail.view.*

/**
 * Created by Furqan on 09-07-2018.
 */
class PoiViewHolder(v: View, val activity: AppCompatActivity) : RecyclerView.ViewHolder(v), View.OnClickListener {
    val view = v
    lateinit var poiDetail: PoiDetail

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        showPoiDetail(poiDetail)

    }

    private fun showPoiDetail(poiDetail: PoiDetail) {
        val bottomSheetDialog = BottomSheetDialog(itemView.context)
        val poiDetailView = activity.layoutInflater.inflate(R.layout.poi_detail, null)
        bottomSheetDialog.setContentView(poiDetailView)
        val params = (poiDetailView.parent as View)
                .layoutParams as CoordinatorLayout.LayoutParams

        val view = poiDetailView.parent as View
        view.post {
            val parent = view.getParent() as View
            val behavior = params.behavior
            val bottomSheetBehavior = behavior as BottomSheetBehavior
            bottomSheetBehavior.peekHeight = view.measuredHeight

            parent.setBackgroundColor(Color.TRANSPARENT)
        }
        (poiDetailView.parent as View).setBackgroundColor(Color.TRANSPARENT)

        val pairList = ArrayList<Pair<Int, String>>()
        if (poiDetail.mobile_number.isNotEmpty() && poiDetail.mobile_number.length > 1) {
            val mobileNo = poiDetail.mobile_number.split(",")
            mobileNo.forEach { pairList.add(Pair(R.drawable.ic_phone, it)) }
        }
        if (poiDetail.email.isNotEmpty()) {
            pairList.add(Pair(R.drawable.ic_mail, poiDetail.email))
        }
        if (pairList.isEmpty()) {
            poiDetailView.recyclerViewPoiInfo.visibility = View.GONE
        } else {
            poiDetailView.recyclerViewPoiInfo.layoutManager = LinearLayoutManager(itemView.context)
            poiDetailView.recyclerViewPoiInfo.adapter = PoiContactAdapter(pairList)
        }
        val pairText = when (LocaleManager.getLanguage(itemView.context)) {
            LocaleManager.LANGUAGE_ENGLISH -> {
                Pair(poiDetail.poiMultilingual?.en?.name,
                        poiDetail.poiMultilingual?.en?.description)
            }
            LocaleManager.LANGUAGE_ARABIC -> {
                Pair(poiDetail.poiMultilingual?.ar?.name,
                        poiDetail.poiMultilingual?.ar?.description)
            }
            LocaleManager.LANGUAGE_HINDI -> {
                Pair(poiDetail.poiMultilingual?.hi?.name,
                        poiDetail.poiMultilingual?.hi?.description)
            }
            LocaleManager.LANGUAGE_RUSSSIAN -> {
                Pair(poiDetail.poiMultilingual?.ru?.name,
                        poiDetail.poiMultilingual?.ru?.description)
            }
            else -> {
                Pair(poiDetail.poiMultilingual?.en?.name,
                        poiDetail.poiMultilingual?.en?.description)
            }
        }

        pairText.first?.let {
            view.dialogTxtShopName.text = it
        } ?: kotlin.run {
            view.dialogTxtShopName.text = poiDetail.poiMultilingual?.en?.name
        }

        pairText.second?.let {
            view.dialogTxtDesc.text = it
        } ?: kotlin.run {
            view.dialogTxtDesc.text = poiDetail.poiMultilingual?.en?.description
        }

        poiDetailView.dialogTxtCategoryName.text = poiDetail.category
//        poiDetailView.dialogTxtDesc.movementMethod = ScrollingMovementMethod()
        Glide.with(itemView.context).load(this.poiDetail.poiUrl)
                .into(poiDetailView.imgShop)
        when (poiDetail.category?.toLowerCase()) {
            "food & drink" -> poiDetailView.digImgCategory.setImageResource(R.drawable.ic_food)
            "shopping" -> poiDetailView.digImgCategory.setImageResource(R.drawable.ic_shop)
            else -> {

                if (poiDetail.poiUrl.isNullOrEmpty()) {

                    poiDetailView.imgFacility.visibility = View.VISIBLE
                    try {
                        poiDetailView.imgFacility.setImageResource(poiDetail.poiUrl?.toInt()!!)
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                } else {
                    poiDetailView.imgFacility.visibility = View.INVISIBLE
                }
                poiDetailView.digImgCategory.visibility = View.INVISIBLE
                poiDetailView.dialogTxtCategoryName.visibility = View.GONE
            }
        }
        poiDetailView.btnShowMap.setOnClickListener {

             if (activity is DemoActivity) {
                //activity.showMap(poiDetail)
                bottomSheetDialog.dismiss()
            }

        }
        bottomSheetDialog.show()
    }

//    fun bindPoi(poi: PoiDetail) {
//        this.poiDetail = poi
//        Glide.with(itemView.context).load(this.poiDetail.poiUrl)
//                .into(view.imgBackground)
//        val category: String = if (poiDetail.category.equals("shopping", true)) {
//            view.imgGradient.setImageResource(R.drawable.gradient_shopping)
//            view.imgCategory.setImageResource(R.drawable.ic_shop)
//            itemView.context.getString(R.string.shopping)
//        } else if (poiDetail.category.equals("utilities", true)) {
//            view.imgCategory.setImageResource(R.drawable.ic_facilities)
//            itemView.context.resources.getStringArray(R.array.map_filter_tabs)[4]
//        } else if (poiDetail.category.equals("services", true)) {
//            view.imgCategory.setImageResource(R.drawable.ic_services)
//            itemView.context.resources.getStringArray(R.array.map_filter_tabs)[3]
//        } else {
//            view.imgGradient.setImageResource(R.drawable.gradient_food)
//            view.imgCategory.setImageResource(R.drawable.ic_food)
//            itemView.context.getString(R.string.food_drink)
//        }
//        CommonUtils.setCatName(itemView.context, view.txtShopName, poiDetail.poiMultilingual)
//
//        /* if (LocaleManager.getLanguage(itemView.context) == LocaleManager.LANGUAGE_ARABIC) {
//             if (poiDetail.poiMultilingual?.ar != null) {
//                 view.txtShopName.text = poiDetail.poiMultilingual!!.ar.name
//             } else {
//                 view.txtShopName.text = poiDetail.poiMultilingual?.en!!.name
//             }
//         } else {
//             view.txtShopName.text = poiDetail.poiMultilingual?.en!!.name
// //            view.txtCategoryName.text = poiDetail.category
//         }*/
//        view.txtCategoryName.text = category
//        view.imgBackground.clipToOutline = true
//    }
}