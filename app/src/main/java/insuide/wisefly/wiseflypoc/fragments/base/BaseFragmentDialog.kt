package com.netpiper.sia.base


import android.content.Context

import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 * A simple [Fragment] subclass.
 */
abstract class BaseFragmentDialog<in V : BaseMvpView, T : BaseMvpPresenter<V>> : BottomSheetDialogFragment(), BaseMvpView {

    protected abstract var mPresenter: T


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this as V)
    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView()
    }

    override fun showLoading() {

    }

    override fun hideLoading() {
    }

    override fun onError(resId: Int) {
    }

    override fun onError(message: String) {
    }

    override fun showMessage(message: String) {

    }

    override fun showMessage(resId: Int) {
    }

    override fun isNetworkConnected(): Boolean {
        return false
    }

    override fun getContext(): Context = super.getContext()!!
}// Required empty public constructor
