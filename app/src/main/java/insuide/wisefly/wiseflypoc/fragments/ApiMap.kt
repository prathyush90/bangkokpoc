package insuide.wisefly.wiseflypoc.fragments



import com.netpiper.sia.model.FlightResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Furqan on 18-04-2018.
 */
interface ApiMap {
    @POST("/poisroute/getpois")
    fun fetchPois(
            @Body body: Map<String, String>
    ): Observable<FlightResponse>


}