package insuide.wisefly.wiseflypoc

import android.app.Application
import com.kontakt.sdk.android.common.KontaktSDK
import com.mapbox.mapboxsdk.Mapbox
import io.socket.client.Socket


/**
 * Created by Furqan on 24-01-2018.
 */
class DemoApp : Application() {

    private var mSocket: Socket? = null
    private var mSocketAds: Socket? = null

    init {

//        try {
//            mSocket = IO.socket(AppConstant.SERVER_URL)
//            mSocketAds = IO.socket(AppConstant.SERVER_URL_ADS + "mobile")
//
//        } catch (e: URISyntaxException) {
//            e.printStackTrace()
//        }
    }

     companion object {
        public var isRunning:Boolean = false;
    }

    override fun onCreate() {
        super.onCreate()
        KontaktSDK.initialize(this)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        isRunning = true;

    }



}