package insuide.wisefly.wiseflypoc;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import insuide.wisefly.wiseflypoc.helpers.AppConstant;
import insuide.wisefly.wiseflypoc.manager.NetworkManager;
import insuide.wisefly.wiseflypoc.receiver.GeofenceBroadcastReceiver;
import insuide.wisefly.wiseflypoc.receiver.LocationUpdatesBroadcastReceiver;
import insuide.wisefly.wiseflypoc.services.ForeGroundService;
import insuide.wisefly.wiseflypoc.services.WiseFlyLocationService;
import io.socket.client.IO;
import io.socket.engineio.client.transports.WebSocket;

public class DemoActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,
        OnCompleteListener<Void> {

    IO.Options opts = new IO.Options();
    private static String TAG = "DemoActivity";
    private LocationBroadcastReceiver locationBroadcastReceiver;
    private WiseFlyLocationService wiseFlyLocationService = null;

    private static Long UPDATE_INTERVAL = 10 * 1000L;
    private static Long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2;
    private static Long MAX_WAIT_TIME = UPDATE_INTERVAL * 2;

    private GoogleApiClient mGoogleApiClient = null;

    private GeofencingClient mGeofencingClient;
    private ArrayList<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent = null;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest = null;

    // private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        locationBroadcastReceiver = new LocationBroadcastReceiver();
        opts.transports = new String[]{WebSocket.NAME};

        // Empty list for storing geofences.
        mGeofenceList = new ArrayList<>();
        populateGeofenceList();
        mGeofencingClient = LocationServices.getGeofencingClient(this);
//        try {
//            mSocket = IO.socket("http://development.wisefly.in/");
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//
//        mSocket.on(Socket.EVENT_CONNECT, args -> System.out.print("xxx"));
//        mSocket.on(Socket.EVENT_DISCONNECT, args -> System.out.print("Socket Disconnect"));
//        mSocket.on(Socket.EVENT_ERROR, args -> System.out.print("sss"));
//        //getInitialLocation();
//
//        mSocket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Transport transport = (Transport) args[0];
//                transport.on(Transport.EVENT_ERROR, new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Exception e = (Exception) args[0];
//                        Log.e("xxx", "Transport error " + e);
//                        e.printStackTrace();
//                        e.getCause().printStackTrace();
//                    }
//                });
//            }
//        });
    }


    @Override
    protected void onStart() {
        super.onStart();
//        Intent serviceIntent = new Intent(this.getApplicationContext(), WiseFlyLocationService.class);
//        bindService(serviceIntent, wiseServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check is geofence already added
        if (getGeofencesAdded()) {
            removeGeofences();
        }
//        addGeofences();
//        updateGeofencesAdded(true);
//
//
////        mSocket.connect();
//        buildGoogleApiClient();
//        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(
//                locationBroadcastReceiver,new
//                        IntentFilter(
//                        WiseFlyLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST)
//        );
        if (!ForeGroundService.isRunning) {
            Intent serviceIntent = new Intent(this, ForeGroundService.class);
            serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
            ContextCompat.startForegroundService(this, serviceIntent);
        }

    }

    private void addGeofences() {
        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnCompleteListener(this);
    }

    private void removeGeofences() {

        mGeofencingClient.removeGeofences(getGeofencePendingIntent()).addOnCompleteListener(this);
    }

    private boolean      getGeofencesAdded() {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                AppConstant.GEOFENCES_ADDED_KEY, false);
    }

    /**
     * Stores whether geofences were added ore removed in {@link };
     *
     * @param added Whether geofences were added or removed.
     */
    private void updateGeofencesAdded(boolean added) {
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putBoolean(AppConstant.GEOFENCES_ADDED_KEY, added)
                .apply();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    @Override
    public void onComplete(@NonNull Task<Void> task) {
        if (task.isSuccessful()) {
            updateGeofencesAdded(!getGeofencesAdded());
        }
    }

    public void emitSocketMessage(JSONObject data, String event) {
//        if (mSocket.connected()) {
//
//            mSocket.emit(event, data);
//            //}
//        }

        try {
            NetworkManager.sendPositionData(getString(R.string.token), data, null);
        } catch (Exception e) {
            Toast.makeText(DemoActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdates();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void requestLocationUpdates() {
        try {
//            Log.i(TAG, "Starting location updates")
            //LocationRequestHelper.setRequesting(this, true)
//            LocationServices.FusedLocationApi.requestLocationUpdates(
//                    mGoogleApiClient, mLocationRequest, getPendingIntent());
        } catch (SecurityException e) {
            //LocationRequestHelper.setRequesting(this, false)
            e.printStackTrace();
        }
    }

    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(this.getApplicationContext(), LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.Companion.getACTION_PROCESS_UPDATES());
        return PendingIntent.getBroadcast(this.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    }


    private void logResultsToScreen(String output) {
        Log.d(TAG, "logResultsToScreen: " + output);
    }

    private void populateGeofenceList() {
        for (Map.Entry<String, LatLng> entry : AppConstant.AREA_LANDMARKS.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(entry.getKey())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            entry.getValue().getLatitude(),
                            entry.getValue().getLongitude(),
                            AppConstant.GEOFENCE_RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(AppConstant.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)

                    // Create the geofence.
                    .build());
        }
    }

    private ServiceConnection wiseServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            WiseFlyLocationService.LocalBinder localBinder = (WiseFlyLocationService.LocalBinder) service;
//            wiseFlyLocationService = localBinder.getService$app();
//            wiseFlyLocationService.startTrackingLocation();
            Log.d(TAG, "onServiceConnected: ");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected: ");

        }

    };

    private class LocationBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(WiseFlyLocationService.EXTRA_LOCATION);


            if (location != null) {
                logResultsToScreen("Foreground only location: " + location.getLatitude());
            }
        }
    }
}
