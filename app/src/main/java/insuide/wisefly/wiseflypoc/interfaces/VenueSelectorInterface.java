package insuide.wisefly.wiseflypoc.interfaces;

import org.json.JSONObject;

public interface VenueSelectorInterface {
    public void onItemClicked(JSONObject obj);
}
