package insuide.wisefly.wiseflypoc.interfaces;

/**
 * Created by prathyush on 02/12/18.
 */

public interface LocationUpdaterInterface {
    public void onNeighboursLocation(double location[]);
    public void particlesLocation(double[][] particles);
    public void onInitialLocation(double location[]);
}
