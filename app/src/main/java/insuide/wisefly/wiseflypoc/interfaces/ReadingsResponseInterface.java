package insuide.wisefly.wiseflypoc.interfaces;

import org.json.JSONObject;

public interface ReadingsResponseInterface {
    public void onSuccess(JSONObject model);
    public void onError(Object error);
}
