package insuide.wisefly.wiseflypoc.interfaces;

import insuide.wisefly.wiseflypoc.models.SimpleResponsePojo;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AnalyticsService {

    @POST("/userdata")
    Call<SimpleResponsePojo> sendLocData(@Body RequestBody key);
    @POST("/notificationdata")
    Call<SimpleResponsePojo> sendNotificationData(@Body RequestBody key);





}
