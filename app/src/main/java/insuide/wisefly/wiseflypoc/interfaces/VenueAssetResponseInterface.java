package insuide.wisefly.wiseflypoc.interfaces;


import insuide.wisefly.wiseflypoc.models.VenueAndAssetsPojo;

public interface VenueAssetResponseInterface {
    public void onSuccess(VenueAndAssetsPojo model);
    public void onError(Object error);
}
