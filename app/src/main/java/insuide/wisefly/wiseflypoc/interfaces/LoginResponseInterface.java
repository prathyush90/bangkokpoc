package insuide.wisefly.wiseflypoc.interfaces;


import insuide.wisefly.wiseflypoc.models.LoginPojo;

public interface LoginResponseInterface {
    public void onSuccess(LoginPojo model);
    public void onError(Object error);
}
