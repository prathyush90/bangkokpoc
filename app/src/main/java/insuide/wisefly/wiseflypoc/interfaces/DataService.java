package insuide.wisefly.wiseflypoc.interfaces;

import com.google.gson.JsonElement;

import java.util.Map;

import insuide.wisefly.wiseflypoc.models.LoginPojo;
import insuide.wisefly.wiseflypoc.models.SimpleResponsePojo;
import insuide.wisefly.wiseflypoc.models.VenueAndAssetsPojo;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface DataService {

    @POST("/wisemapperroute/loginassetuser")
    Call<LoginPojo> authenticateAssetInputUser(@Body RequestBody key);

    @POST("/wisemapperroute/getVenuesAndBeacons")
    Call<VenueAndAssetsPojo> getVenuesAndAssets(@Body RequestBody key);

    @POST("/wisemapperroute/updatebeacondata")
    Call<SimpleResponsePojo> updateBeacon(@Body RequestBody key);

    @POST("/loginroute/uploadimage")
    Call<SimpleResponsePojo> uploadImage(@Body RequestBody key);

    @POST("/wisemapperroute/updatepoi")
    Call<SimpleResponsePojo> updatePOI(@Body RequestBody key);

    @POST("/wisemapperroute/addwaypoint")
    Call<SimpleResponsePojo> addWayPoint(@Body RequestBody key);

    @POST("/wisemapperroute/getmappedrouteslines")
    Call<JsonElement> getMappedRoutes(@Body RequestBody key);

    @POST("/wisemapperroute/getallwaypoints")
    Call<SimpleResponsePojo> getAllWayPoints(@Body RequestBody key);

    @POST("/wisemapperroute/deletewaypoint")
    Call<SimpleResponsePojo> deleteWayPoint(@Body RequestBody key);

    @POST("/wisemapperroute/insertmapreading")
    Call<SimpleResponsePojo> insertFingerPrintReading(@Body RequestBody key);

    @POST("/wisemapperroute/getallmapreadings")
    Call<JsonElement> getReadingsFromServer(@Body RequestBody key);

    @POST("/wisemapperroute/addfenceformapping")
    Call<JsonElement> addFence(@Body RequestBody key);

    @POST("/wisemapperroute/getallwaypoints")
    Call<SimpleResponsePojo> getAddedWayPoints(@Body Map<String, String> key);

    @POST("/wisemapperroute/deletewaypoint")
    Call<SimpleResponsePojo> deleteWayPoint(@Body Map<String, String> key);

    @POST("/wisemapperroute/positiondata")
    Call<SimpleResponsePojo> updatePositionData(@Body RequestBody key);

    @GET()
    @Streaming
    Call<ResponseBody> downloadFile(@Url String urlString);

}
