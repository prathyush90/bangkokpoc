package insuide.wisefly.wiseflypoc.interfaces;


import insuide.wisefly.wiseflypoc.models.SimpleResponsePojo;

public interface SimpleNetworkInterface {
    public void onSuccess(SimpleResponsePojo model);
    public void onError(Object error);
}
