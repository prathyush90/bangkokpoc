package insuide.wisefly.wiseflypoc.interfaces;


import insuide.wisefly.wiseflypoc.models.BeaconsPojo;

/**
 * Created by Furqan on 22-09-2019.
 */
public interface BeaconCallBack {

    public void showBeacon(BeaconsPojo beaconsPojo);
}
