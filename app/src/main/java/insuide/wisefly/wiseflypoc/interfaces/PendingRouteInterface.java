package insuide.wisefly.wiseflypoc.interfaces;

import org.json.JSONObject;

public interface PendingRouteInterface {
    public void onUploadRequested(JSONObject obj, int index);
    public void onDeleteSelected(JSONObject obj, int index);
}
