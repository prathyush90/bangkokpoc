//
// Created by prathyush on 02/05/19.
//

#ifndef WISEMAP_INTERVALTREE_H
#define WISEMAP_INTERVALTREE_H

#include "IntervalTreeNode.h"
#include <vector>
class IntervalTree {
public:
    Node* root = nullptr;
    Node* insertNode(Node* tmpNode, Node* newNode){
        if(!tmpNode){
            tmpNode = newNode;
            return tmpNode;
        }

        if(newNode->getEnd() > tmpNode->getMax()){
            tmpNode->setMax(newNode->getEnd());
        }

        if(tmpNode->compareTo(newNode) <= 0){
            tmpNode->right = insertNode(tmpNode->right, newNode);
        } else{
//            if(!tmpNode->getLeft()){
//                tmpNode->setLeft(newNode);
//            } else{
//                insertNode(tmpNode->getRight(), newNode);
//            }
            tmpNode->left = insertNode(tmpNode->left, newNode);
        }
        return tmpNode;
    }

    IntervalTree() {}

    void intersectingInterval(Node* tmp, Node* queryNode, vector<vector<double>> *res){
        if(tmp == nullptr){
            return;
        }
        if(tmp->left != nullptr) {
            if (!((tmp->getStart() > queryNode->getEnd()) ||
                  (tmp->left->getMax() < queryNode->getStart()))) {
                vector<double> wayPointLoc;
                wayPointLoc.push_back(tmp->getWayX());
                wayPointLoc.push_back(tmp->getWayY());
                wayPointLoc.push_back(tmp->getWayZ());
                res->push_back(wayPointLoc);
            }
        }

        if( (tmp->left != nullptr) && (tmp->left->getMax() >= queryNode->getStart()) ){
            this->intersectingInterval(tmp->left, queryNode, res);
        }
        this->intersectingInterval(tmp->right, queryNode, res);
    }


};


#endif //WISEMAP_INTERVALTREE_H
