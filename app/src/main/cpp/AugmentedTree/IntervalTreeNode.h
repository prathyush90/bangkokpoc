//
// Created by prathyush on 02/05/19.
//

#ifndef WISEMAP_INTERVALTREENODE_H
#define WISEMAP_INTERVALTREENODE_H


class Node{
    private:
    double start;
    double end;
    double max;

    double wayX;
    double wayY;
    double wayZ;
public:
    Node* left;
    Node* right;
    double getStart() const {
        return start;
    }

    void setStart(double start) {
        Node::start = start;
    }

    double getEnd() const {
        return end;
    }

    void setEnd(double end) {
        Node::end = end;
    }

    double getMax() const {
        return max;
    }

    void setMax(double max) {
        Node::max = max;
    }

    Node *getLeft()  {
        return left;
    }

    void setLeft(Node *left) {
        Node::left = left;
    }

    Node *getRight()  {
        return right;
    }

    void setRight(Node *right) {
        Node::right = right;
    }

    Node(double start, double end, double wayx, double wayy, double wayz){
        this->start = start;
        this->end   = end;
        this->max   = end;
        this->wayX  = wayx;
        this->wayY  = wayy;
        this->wayZ  = wayz;
        this->left  = nullptr;
        this->right = nullptr;
    }

    int compareTo(Node* otherNode){
        if(this->start < otherNode->start){
            return -1;
        } else if (this->start == otherNode->start){
            return this->end <= otherNode->end ? -1 : 1;
        } else{
            return 1;
        }
    }

    double getWayX() const {
        return wayX;
    }

    void setWayX(double wayX) {
        Node::wayX = wayX;
    }

    double getWayY() const {
        return wayY;
    }

    void setWayY(double wayY) {
        Node::wayY = wayY;
    }

    double getWayZ() const {
        return wayZ;
    }

    void setWayZ(double wayZ) {
        Node::wayZ = wayZ;
    }
};
#endif //WISEMAP_INTERVALTREENODE_H