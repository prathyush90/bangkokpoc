#include <jni.h>
#include <string>

#include <iostream>


#include "json/json.h"
#include "LocProvider.h"
#include "SlamManager.h"
#include "NeighbourMeasurements.h"

#include "models/Beaconsdata.h"
#include "models/Wifidetails.h"
#include "models/MagneticMap.h"
#include "models/SensorReadings.h"
#include <android/log.h>

extern "C"
JNIEXPORT jlong JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocationManager_nativeInitlocProvider(JNIEnv *env,
                                                                                        jobject instance) {

    // TODO
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "---> nativeInitlocProvider");
    LocProvider* locProvider = new LocProvider();
    return (long) locProvider;
}
extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocationManager_deleteNativeReference(JNIEnv *env,
                                                                              jobject thiz,
                                                                              jlong address) {
    // TODO: implement deleteNativeReference()
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "---> delete loc provider called");
    LocProvider* locProvider = (LocProvider*) address;
    delete locProvider;
}extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocationManager_initNativeLocalizerAndMapper(
        JNIEnv *env, jobject instance, jlong address, jint dimensions, jobject measurements,
        jint particleSize, jobjectArray geofencearray, jobject _walkregion, jboolean use1beacon) {
    // TODO: implement initNativeLocalizerAndMapper()
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "---> data init called");
    LocProvider* locProvider = (LocProvider*) address;

    int len1         = env -> GetArrayLength(geofencearray);
    vector<vector<double>> geofenceVector;
    vector<vector<vector<double>>> walkregion;



    for(int i=0; i<len1; ++i){
        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
        vector<double> inside_elem;
        inside_elem.push_back(element[0]);
        inside_elem.push_back(element[1]);
        geofenceVector.push_back(inside_elem);

        env->ReleaseDoubleArrayElements(oneDim, element, 0);

        env->DeleteLocalRef(oneDim);
    }





    jclass alCls     = env->FindClass("java/util/ArrayList");
    jclass pojoCls = env->FindClass("insuide/wisefly/wiseflypoc/models/MapReadingsPojo");
    //jclass magMapPojoCls = env->FindClass("com/example/tarak/wisemap/models/MagneticMapPojo");
    jclass rssidetailsCls = env->FindClass("insuide/wisefly/wiseflypoc/models/BeaconReadingPojo");


    if (alCls == nullptr || pojoCls == nullptr || rssidetailsCls == nullptr) {
        return;
    }
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem class");
    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");
    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");


    jmethodID getBeaconX        = env->GetMethodID(pojoCls, "getBeaconDistX", "()D");
    jmethodID getBeaconY        = env->GetMethodID(pojoCls, "getBeaconDistY", "()D");
    jmethodID getBeaconZ        = env->GetMethodID(pojoCls, "getBeaconDistZ", "()D");
    jmethodID getParticleX        = env->GetMethodID(pojoCls, "getPosx", "()D");
    jmethodID getParticleY        = env->GetMethodID(pojoCls, "getPosy", "()D");
    jmethodID getParticleZ        = env->GetMethodID(pojoCls, "getPosz", "()D");
    jmethodID getBeaconMeanX        = env->GetMethodID(pojoCls, "getBeacon_dist_x_mean", "()D");
    jmethodID getBeaconMeanY       = env->GetMethodID(pojoCls, "getBeacon_dist_y_mean", "()D");
    jmethodID getBeaconMeanZ        = env->GetMethodID(pojoCls, "getBeacon_dist_z_mean", "()D");

    jmethodID getXMag        = env->GetMethodID(pojoCls, "getMagX", "()D");
    jmethodID getYMag        = env->GetMethodID(pojoCls, "getMagY", "()D");
    jmethodID getZMag        = env->GetMethodID(pojoCls, "getMagZ", "()D");
    jmethodID hasBeaconsReadings = env->GetMethodID(pojoCls, "getHasBeaconReading", "()Z");
    jmethodID getMeanRssiBeacon        = env->GetMethodID(pojoCls, "getBeaconMeanRssi", "()D");
    jmethodID getStdRssiBeacon        = env->GetMethodID(pojoCls, "getBeaconStdRssi", "()D");
    jclass doubleClass = env->FindClass("java/lang/Double");
    jmethodID doubleValueMid = env->GetMethodID(doubleClass, "doubleValue", "()D");







    int measurementCount = static_cast<int>(env->CallIntMethod(measurements, alSizeId));
    vector<NeighbourMeasurements> neighMeasurements;

    int shapesSize  = static_cast<int>(env->CallIntMethod(_walkregion, alSizeId));

    for(int i=0; i<shapesSize; ++i){
        jobject shape = (jobject)env->CallObjectMethod(_walkregion, alGetId, i);
        int points         =  static_cast<int>(env->CallIntMethod(shape, alSizeId));
        vector<vector<double>>polygon;
        for(int k=0;k<points;k++){
            jobject point = (jobject)env->CallObjectMethod(shape, alGetId, k);
            int pointsize = 2;
            jvalue arg;
            vector<double> inside_elem;
            for(int m=0;m<pointsize;m++){
                arg.i = i;
                jobject element = env->CallObjectMethod(point, alGetId, m);
                jdouble value   = env->CallDoubleMethodA(element, doubleValueMid, &arg);
                inside_elem.push_back(value);
                env->DeleteLocalRef(element);
            }
            polygon.push_back(inside_elem);
            env->DeleteLocalRef(point);
        }
        walkregion.push_back(polygon);
        env->DeleteLocalRef(shape);




    }

    double beacon_x;
    double beacon_y;
    double beacon_z;
    double particle_x;
    double particle_y;
    double particle_z;
    double x_mag;
    double y_mag;
    double z_mag;
    bool hasBeacon;
    double mean_rssi_beacon;
    double std_rssi_beacon;
    double beacon_mean_x;
    double beacon_mean_y;
    double beacon_mean_z;
    for(int j=0; j<measurementCount; j++) {
        jobject point = env->CallObjectMethod(measurements, alGetId, j);

        beacon_x = static_cast<double>(env->CallDoubleMethod(point, getBeaconX));
        beacon_y = static_cast<double>(env->CallDoubleMethod(point, getBeaconY));
        beacon_z = static_cast<double>(env->CallDoubleMethod(point, getBeaconZ));
        particle_x = static_cast<double>(env->CallDoubleMethod(point, getParticleX));
        particle_y = static_cast<double>(env->CallDoubleMethod(point, getParticleY));
        particle_z = static_cast<double>(env->CallDoubleMethod(point, getParticleZ));
        x_mag = static_cast<double>(env->CallDoubleMethod(point, getXMag));
        y_mag = static_cast<double>(env->CallDoubleMethod(point, getYMag));
        z_mag = static_cast<double>(env->CallDoubleMethod(point, getZMag));
        hasBeacon = static_cast<bool >(env->CallBooleanMethod(point, hasBeaconsReadings));
        mean_rssi_beacon = static_cast<double>(env->CallDoubleMethod(point, getMeanRssiBeacon));
        std_rssi_beacon = static_cast<double>(env->CallDoubleMethod(point, getStdRssiBeacon));
        beacon_mean_x = static_cast<double>(env->CallDoubleMethod(point, getBeaconMeanX));
        beacon_mean_y = static_cast<double>(env->CallDoubleMethod(point, getBeaconMeanY));
        beacon_mean_z = static_cast<double>(env->CallDoubleMethod(point, getBeaconMeanZ));
        if(beacon_mean_y == 0){
            __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "---> something strange");
        }
        neighMeasurements.push_back(NeighbourMeasurements(beacon_x, beacon_y, beacon_z, particle_x, particle_y, particle_z, x_mag, y_mag, z_mag, hasBeacon, mean_rssi_beacon, std_rssi_beacon, beacon_mean_x, beacon_mean_y, beacon_mean_z));
        env->DeleteLocalRef(point);

    }

    locProvider->initLocalizerAndMapper(dimensions, particleSize, geofenceVector, neighMeasurements, walkregion, use1beacon);


    env->DeleteLocalRef(alCls);
    env->DeleteLocalRef(pojoCls);
    env->DeleteLocalRef(doubleClass);
    env->DeleteLocalRef(rssidetailsCls);


}

extern "C"
JNIEXPORT jdoubleArray JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocationManager_nativeMoveSlaves(JNIEnv *env,
                                                                         jobject instance,
                                                                         jdouble distance,
                                                                         jdouble turn,
                                                                         jdoubleArray magobservations_,
                                                                         jlong address,
                                                                         jint maglength,
                                                                         jdoubleArray beaconsobservations_,
                                                                         jboolean isBeacon,
                                                                         jdoubleArray wifiobservations_,
                                                                         jboolean isWifi,
                                                                         jint size,
                                                                         jboolean isCached
){
    // TODO: implement nativeMoveSlaves()
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "---> move called");
    jdouble *magobservations = env->GetDoubleArrayElements(magobservations_, NULL);
    jdouble *beaconsobservations = env->GetDoubleArrayElements(beaconsobservations_, NULL);
    jdouble *wifiobservations = env->GetDoubleArrayElements(wifiobservations_, NULL);

    jclass alCls     = env->FindClass("java/util/ArrayList");
    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");
    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");

    vector<double>beacon;
    vector<double>wifi;

    vector<double> magnetic(magobservations,magobservations+maglength);

    if(isBeacon) {
        beacon  = vector<double>(beaconsobservations, beaconsobservations + 5);
    }
    if(isWifi){
        wifi    = vector<double>(wifiobservations, wifiobservations+2);
    }

//    jclass beaconCls = env->FindClass("insuide/wisefly/wiseflypoc/models/FilteredBeacon");
//    jmethodID rssiMethod            = env->GetMethodID(beaconCls, "getRssi", "()D");
//    jmethodID uniqueIdMethod            = env->GetMethodID(beaconCls, "getUniqueId", "()Ljava/lang/String;");

    vector<vector<double>> geofenceVector;




    // TODO
    LocProvider* locProvider = (LocProvider*) address;
    double userpos[8];
    locProvider->move(distance, turn, magnetic, userpos, beacon, isBeacon, wifi, isWifi, size, isCached);
    jdoubleArray  returnarray = env->NewDoubleArray(8);
    env->SetDoubleArrayRegion(returnarray,0,8,userpos);


    env->ReleaseDoubleArrayElements(magobservations_, magobservations, 0);
    env->ReleaseDoubleArrayElements(beaconsobservations_, beaconsobservations, 0);
    env->ReleaseDoubleArrayElements(wifiobservations_, wifiobservations, 0);
    return returnarray;

}





extern "C"
JNIEXPORT jobjectArray JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocationManager_getParticlesLocations(JNIEnv *env,
                                                                              jobject instance,
                                                                              jlong address,
                                                                              jint particleSize) {
    // TODO: implement getParticlesLocations()

    LocProvider* locProvider = (LocProvider*) address;
    double particlePos[particleSize][10];
    locProvider->getParticleLocations(particlePos);

    jclass doubleArray1DClass = env->FindClass("[D");
    jobjectArray retData = env->NewObjectArray(
            particleSize, doubleArray1DClass, NULL);;
    for(int i=0;i<particleSize;i++){
        jdoubleArray particleloc = env->NewDoubleArray(10);
        env->SetDoubleArrayRegion(particleloc,0,10,particlePos[i]);
        env->SetObjectArrayElement(retData, i, particleloc);
    }
    env->DeleteLocalRef(doubleArray1DClass);
    return retData;

}extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocationManager_nativeInitParticles(JNIEnv *env,
                                                                            jobject instance,
                                                                            jlong address,
                                                                            jobjectArray geofencearray,
                                                                            jdouble heading, jdouble b_x, jdouble b_y, jdouble b_z, jint size ) {
    // TODO: implement nativeInitParticles()
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "---> particle filter initialized  called");
    LocProvider* locProvider = (LocProvider*) address;

    int len1         = env -> GetArrayLength(geofencearray);
    vector<vector<double>> geofenceVector;

    for(int i=0; i<len1; ++i){
        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
        vector<double> inside_elem;
        inside_elem.push_back(element[0]);
        inside_elem.push_back(element[1]);
        geofenceVector.push_back(inside_elem);

        env->ReleaseDoubleArrayElements(oneDim, element, 0);

        env->DeleteLocalRef(oneDim);
    }
    locProvider->initParticleFilter(geofenceVector, heading, b_x, b_y, b_z, size);

}
//extern "C"
//JNIEXPORT jdoubleArray JNICALL
//Java_insuide_wisefly_wiseflypoc_manager_LocationManager_nativeMoveSlaves(JNIEnv *env, jobject thiz,jint event,
//                                                                         jint axis,
//                                                                         jdouble pitch_change,
//                                                                         jdouble roll_change,
//                                                                         jdouble yaw_change,
//                                                                         jdoubleArray magobservations_,
//                                                                         jlong address,
//                                                                         jint maglength,
//                                                                         jdoubleArray beaconsobservations_,
//                                                                         jboolean isBeacon,
//                                                                         jdoubleArray wifiobservations_,
//                                                                         jboolean isWifi,
//                                                                         jint size) {
//    // TODO: implement nativeMoveSlaves()
//
//    jdouble *magobservations = env->GetDoubleArrayElements(magobservations_, NULL);
//    jdouble *beaconsobservations = env->GetDoubleArrayElements(beaconsobservations_, NULL);
//    jdouble *wifiobservations = env->GetDoubleArrayElements(wifiobservations_, NULL);
//
//    jclass alCls     = env->FindClass("java/util/ArrayList");
//    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");
//    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");
//
//    vector<double>beacon;
//    vector<double>wifi;
//
//    vector<double> magnetic(magobservations,magobservations+maglength);
//
//    if(isBeacon) {
//        beacon  = vector<double>(beaconsobservations, beaconsobservations + 5);
//    }
//    if(isWifi){
//        wifi    = vector<double>(wifiobservations, wifiobservations+2);
//    }
//
//    jclass beaconCls = env->FindClass("insuide/wisefly/wiseflypoc/models/FilteredBeacon");
//    jmethodID rssiMethod            = env->GetMethodID(beaconCls, "getRssi", "()D");
//    jmethodID uniqueIdMethod            = env->GetMethodID(beaconCls, "getUniqueId", "()Ljava/lang/String;");
//
//    vector<vector<double>> geofenceVector;
//
//
//
//
//    // TODO
//    LocProvider* locProvider = (LocProvider*) address;
//    double userpos[8];
//    locProvider->move(event, axis, pitch_change, roll_change, yaw_change, magnetic, userpos, beacon, isBeacon, wifi, isWifi, size);
//    jdoubleArray  returnarray = env->NewDoubleArray(8);
//    env->SetDoubleArrayRegion(returnarray,0,8,userpos);
//
//
//    env->ReleaseDoubleArrayElements(magobservations_, magobservations, 0);
//    env->ReleaseDoubleArrayElements(beaconsobservations_, beaconsobservations, 0);
//    env->ReleaseDoubleArrayElements(wifiobservations_, wifiobservations, 0);
//
//    env->DeleteLocalRef(beaconCls);
//    env->DeleteLocalRef(alCls);
//    return returnarray;
//
//
//}
extern "C"
JNIEXPORT jlong JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_nativeInitlocProvider(JNIEnv *env,
                                                                                  jobject thiz) {
    // TODO: implement nativeInitlocProvider()
    SlamManager* locProvider = new SlamManager();
    return (long) locProvider;
}extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_deleteNativeReference(JNIEnv *env,
                                                                                  jobject thiz,
                                                                                  jlong address) {
    // TODO: implement deleteNativeReference()
    SlamManager* locProvider = (SlamManager*) address;
    delete locProvider;
}extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_initNativeLocalizerAndMapper(
        JNIEnv *env, jobject thiz, jlong address, jint particle_size, jobjectArray geofencearray,
        jobject _walkregion) {
    // TODO: implement initNativeLocalizerAndMapper()
    SlamManager* locProvider = (SlamManager*) address;
    int len1         = env -> GetArrayLength(geofencearray);
    vector<vector<double>> geofenceVector;
    vector<vector<vector<double>>> walkregion;



    for(int i=0; i<len1; ++i){
        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
        vector<double> inside_elem;
        inside_elem.push_back(element[0]);
        inside_elem.push_back(element[1]);
        geofenceVector.push_back(inside_elem);

        env->ReleaseDoubleArrayElements(oneDim, element, 0);

        env->DeleteLocalRef(oneDim);
    }

    jclass alCls     = env->FindClass("java/util/ArrayList");
    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");
    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");
    jclass doubleClass = env->FindClass("java/lang/Double");
    jmethodID doubleValueMid = env->GetMethodID(doubleClass, "doubleValue", "()D");

    int shapesSize  = static_cast<int>(env->CallIntMethod(_walkregion, alSizeId));

    for(int i=0; i<shapesSize; ++i){
        jobject shape = (jobject)env->CallObjectMethod(_walkregion, alGetId, i);
        int points         =  static_cast<int>(env->CallIntMethod(shape, alSizeId));
        vector<vector<double>>polygon;
        for(int k=0;k<points;k++){
            jobject point = (jobject)env->CallObjectMethod(shape, alGetId, k);
            int pointsize = 2;
            jvalue arg;
            vector<double> inside_elem;
            for(int m=0;m<pointsize;m++){
                arg.i = i;
                jobject element = env->CallObjectMethod(point, alGetId, m);
                jdouble value   = env->CallDoubleMethodA(element, doubleValueMid, &arg);
                inside_elem.push_back(value);
                env->DeleteLocalRef(element);
            }
            polygon.push_back(inside_elem);
            env->DeleteLocalRef(point);
        }
        walkregion.push_back(polygon);
        env->DeleteLocalRef(shape);
    }



    locProvider->initLocalizerAndMapper(particle_size, geofenceVector, walkregion);


    env->DeleteLocalRef(alCls);
    env->DeleteLocalRef(doubleClass);
}extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_nativeInitParticles(JNIEnv *env,
                                                                                jobject thiz,
                                                                                jlong address,
                                                                                jobjectArray geofencearray,
                                                                                jdouble heading,
                                                                                jdouble b_x,
                                                                                jdouble b_y,
                                                                                jdouble b_z,
                                                                                jint size) {
    // TODO: implement nativeInitParticles()

    SlamManager* locProvider = (SlamManager*) address;

    int len1         = env -> GetArrayLength(geofencearray);
    vector<vector<double>> geofenceVector;

    for(int i=0; i<len1; ++i){
        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
        vector<double> inside_elem;
        inside_elem.push_back(element[0]);
        inside_elem.push_back(element[1]);
        geofenceVector.push_back(inside_elem);

        env->ReleaseDoubleArrayElements(oneDim, element, 0);

        env->DeleteLocalRef(oneDim);
    }
    locProvider->initParticleFilter(geofenceVector, heading, b_x, b_y, b_z, size);
}
//extern "C"
//JNIEXPORT void JNICALL
//Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_nativeMoveRobots(JNIEnv *env,
//                                                                             jobject thiz,
//                                                                             jlong address,
//                                                                             jdouble yaw, jint axis,
//                                                                             jint event) {
//    // TODO: implement nativeMoveRobots()
//    SlamManager* locProvider = (SlamManager*) address;
//    double userpos[8];
//    locProvider->move(distance, turn, magnetic, userpos, beacon, isBeacon, wifi, isWifi, size, isCached);
//    jdoubleArray  returnarray = env->NewDoubleArray(8);
//    env->SetDoubleArrayRegion(returnarray,0,8,userpos);
//    return returnarray
//
//}
extern "C"
JNIEXPORT jdoubleArray JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_nativeMoveRobots(JNIEnv *env,
                                                                             jobject thiz,
                                                                             jlong address,
                                                                             jdouble yaw, jint axis,
                                                                             jint event) {
    // TODO: implement nativeMoveRobots()
    SlamManager* locProvider = (SlamManager*) address;
    double userpos[8];
    locProvider->move(yaw, event, axis, userpos);
    jdoubleArray  returnarray = env->NewDoubleArray(8);
    env->SetDoubleArrayRegion(returnarray,0,8,userpos);
    return returnarray;
}extern "C"
JNIEXPORT jobjectArray JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_getParticlesLocations(JNIEnv *env,
                                                                                  jobject thiz,
                                                                                  jlong address,
                                                                                  jint particle_size) {
    // TODO: implement getParticlesLocations()
    SlamManager* locProvider = (SlamManager*) address;
    double particlePos[particle_size][3];
    locProvider->getParticleLocations(particlePos);

    jclass doubleArray1DClass = env->FindClass("[D");
    jobjectArray retData = env->NewObjectArray(
            particle_size, doubleArray1DClass, NULL);;
    for(int i=0;i<particle_size;i++){
        jdoubleArray particleloc = env->NewDoubleArray(3);
        env->SetDoubleArrayRegion(particleloc,0,3,particlePos[i]);
        env->SetObjectArrayElement(retData, i, particleloc);
    }
    env->DeleteLocalRef(doubleArray1DClass);
    return retData;
}extern "C"
JNIEXPORT void JNICALL
Java_insuide_wisefly_wiseflypoc_manager_LocalizationManager_nativeResampleRobots(JNIEnv *env,
                                                                                 jobject thiz,
                                                                                 jlong address,
                                                                                 jdoubleArray weights_) {
    // TODO: implement nativeResampleRobots()
    jdouble *weightsObs = env->GetDoubleArrayElements(weights_, NULL);
    int len1         = env -> GetArrayLength(weights_);
    vector<double> weights = vector<double>(weightsObs, weightsObs+len1);
    SlamManager* locProvider = (SlamManager*) address;
    locProvider->continueWithResample(weights);

    env->ReleaseDoubleArrayElements(weights_, weightsObs, 0);


}