//
// Created by prathyush on 2020-02-24.
//

#ifndef WISEFLYPOC_ROBOT_H
#define WISEFLYPOC_ROBOT_H


#include <vector>
#include "math.h"
#include "../Eigen/Dense"
#include "../Eigen/Eigenvalues"

using namespace std;
using namespace Eigen;
class Robot {
public:
    double mX;
    double mY;
    double mZ;
    double orientation;
    double forward_noise    = 0.0;
    double turn_noise       = 0.0;
    double weight = 1.0;
    double magYReading = 1.0;

    Robot(double x, double y, double z, double heading);
    Robot(const Robot& robot);
    void setNoise(double forward_noise, double bearing_noise);
    Robot move(double distance, double turn, vector<vector<vector<double>>> walkregion);
    double radToDeg(double radians);
    double degToRad(double degrees);

    void nextPosition(double meters, double heading, double returnValue[]);
    void convertSphericalToCartesian(double meters, double heading, double returnarray[]);
    void convertCartesianToSpherical(double * cartesian, double latlng[]);
    void distance(double * from, double distance, double heading, double latlng[]);
    double getX();
    double getY();
    double getZ();
    double getWeight();
    double getOrientation();
    double transformWeight(double weight);
    bool isInsideFence(vector<double> obs, vector<vector<vector<double>>> walkregion);
    bool rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b);
    void setMagYReading(double magYReading);
    double getMagYReading() const;
    void setWeight(double weight);

};


#endif //WISEFLYPOC_ROBOT_H
