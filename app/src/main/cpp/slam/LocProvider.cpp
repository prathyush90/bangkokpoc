//
// Created by noob on 5/3/19.
//

#include <vector>
#include "LocProvider.h"
#include <string>
#include <android/log.h>

#include <vector>
#include "math.h"

#include <chrono>
#include <sstream>
#include <iostream>
#include<cmath>
#include <random>

using namespace std;


void LocProvider::initLocalizerAndMapper(int dimensions, int particleSize,
                                         vector<vector<double>> geofence,
                                         vector<NeighbourMeasurements> magneticMapData, vector<vector<vector<double>>> _walkregion, bool useB) {


    this->dimensions     = dimensions;
    this->particleSize   = particleSize;
    //LocProvider::particlesizecopy = particleSize;
    this->geofence         = geofence;
    //insertMagneticMapIntoTree(magneticMapData);
    removeDuplicatesReverse(magneticMapData);
    //removeDuplicates(magneticMapData);
    //initParticleFilter(geofence, 360*this->randd());
    this->walkregion = _walkregion;
    this->useSingleBeacon = useB;

}

void LocProvider::removeDuplicatesReverse(vector<NeighbourMeasurements> measurements) {
    vector<point_t>values;
    for (unsigned long i=0;i<measurements.size();i++){
        NeighbourMeasurements measurement = measurements.at(i);
        double  x = measurement.getParticle_x();
        double  y = measurement.getParticle_y();
        double  z = measurement.getParticle_z();
        double hasBeacon = measurement.isHasBeacon();
        double beacon_rssi_std = measurement.getBeacon_rssi_std();
        string  key = to_string(x) +"->"+to_string(y)+"->"+to_string(z);
        point_t point;
        point = {x, y, z};
        if(hasBeacon && beacon_rssi_std != 0) {
            if (measurementsHash.find(key) == measurementsHash.end()) {
                values.push_back(point);
                measurementsHash.insert(pair<string, NeighbourMeasurements>(key, measurement));
            }
        }

    }


    buildTreeReverse(values);

}


//try using this for inital location and when the user hits geofence *fingers crossed*
void LocProvider::findClosestBeaconReading(double b_x, double b_y, double b_z, double result[]){
//    double minimum = DBL_MAX;
//    std:basic_string key;
//    for (auto const& x : measurementsHash)
//    {
//        NeighbourMeasurements mst = x.second;
//        double bx = mst.getBeacon_x();
//        double by = mst.getBeacon_y();
//        double bz = mst.getBeacon_z();
//        double mse_diff = sqrt(pow(b_x- bx, 2) +  pow(b_y- by, 2) + pow(b_z- bz, 2));
//        if(minimum > mse_diff) {
//            minimum = mse_diff;
//            key = x.first;
//        }
//    }
//    NeighbourMeasurements mst = measurementsHash.at(key);
//    result[0]               = mst.getParticle_x();
//    result[1]                = mst.getParticle_y();
//    result[2]                = mst.getParticle_z();
//    result[3]                = minimum;


}


void LocProvider::removeDuplicates(vector<NeighbourMeasurements> measurements) {
    vector<point_t>values;
    for (unsigned long i=0;i<measurements.size();i++){
        NeighbourMeasurements measurement = measurements.at(i);
        double beacon_x                   = measurement.getBeacon_x();
        double beacon_y                   = measurement.getBeacon_y();
        double beacon_z                   = measurement.getBeacon_z();
        //double mag_world                  = measurement.getMagnitude_world();

        //string  key = to_string(beacon_x) +"->"+to_string(beacon_y)+"->"+to_string(beacon_z)+"->"+to_string(mag_world);;
        string  key = to_string(beacon_x) +"->"+to_string(beacon_y);
        point_t point;
        point = {beacon_x, beacon_y};

        if(beaconHash.find(key) == beaconHash.end()){
            values.push_back(point);
            beaconHash.insert(pair<string,NeighbourMeasurements>(key,measurement));
        }

    }
    buildTree(values);

}



void LocProvider::buildTree(vector<point_t> values) {
    this->beaconNeigboursTree = KDTree(values);
}

void LocProvider::initParticleFilter(vector<vector<double>> geofence, double heading, double b_x, double b_y, double b_z, int size) {
//    double x     = ((geofence.at(0).at(1) + geofence.at(0).at(0))/2);
//    double y     = ((geofence.at(1).at(1) + geofence.at(1).at(0))/2);
//    double z     = ((geofence.at(2).at(1) + geofence.at(2).at(0))/2);
    orientation = M_PI*heading/180;
    if(orientation < 0){
        orientation += 2*M_PI;
    }
    converged = false;
    orientation = fmod(orientation, 2*M_PI);
    this->slaves.clear();

    vector<double> weights;
    //weights.reserve(particleSize);

    std::random_device rd_x;
    std::random_device rd_y;
    std::random_device rd_z;
    std::random_device rd_or;


    std::mt19937 e2_x(rd_x());
    std::mt19937 e2_y(rd_y());
    std::mt19937 e2_z(rd_z());
    std::mt19937 e2_or(rd_or());

    std::uniform_real_distribution<> dist_x(0.0, 1.0);
//    std::uniform_real_distribution<> dist_y(0.0, 1.0);
//    std::uniform_real_distribution<> dist_z(0.0, 1.0);
//    std::uniform_real_distribution<> dist_or(0.0, 1.0);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator_x (seed);
//    std::default_random_engine generator_y (seed);
//    std::default_random_engine generator_z (seed);
//    std::default_random_engine generator_or (seed);
    double std_dev = 22.5 * M_PI/180;
    std::normal_distribution<double> turn_distribution(0, std_dev);

    std::default_random_engine generator (seed);

    for(int i=0;i<particleSize;i++){

        double x = (dist_x(generator_x))*((geofence.at(0).at(1) - geofence.at(0).at(0)))+geofence.at(0).at(0);
        double y = (dist_x(generator_x))*((geofence.at(1).at(1) - geofence.at(1).at(0)))+geofence.at(1).at(0);
        double z = (dist_x(generator_x))*((geofence.at(2).at(1) - geofence.at(2).at(0)))+geofence.at(2).at(0);
        vector<double>obs;
        obs.push_back(x);
        obs.push_back(y);
        obs.push_back(z);
        while (!isInsideFence(obs)){
            x = (dist_x(generator_x))*((geofence.at(0).at(1) - geofence.at(0).at(0)))+geofence.at(0).at(0);
            y = (dist_x(generator_x))*((geofence.at(1).at(1) - geofence.at(1).at(0)))+geofence.at(1).at(0);
            z = (dist_x(generator_x))*((geofence.at(2).at(1) - geofence.at(2).at(0)))+geofence.at(2).at(0);
            obs.clear();
            obs.push_back(x);
            obs.push_back(y);
            obs.push_back(z);
        }



        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

        double ori = (this->randd()*2)*M_PI;

        Particle slave(x, y, z, ori,this->geofence);
        slave.setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        this->slaves.push_back(slave);
        weights.push_back(1);
    }
    xPos     = ((geofence.at(0).at(1) + geofence.at(0).at(0))/2);
    yPos     = ((geofence.at(1).at(1) + geofence.at(1).at(0))/2);
    zPos     = ((geofence.at(2).at(1) + geofence.at(2).at(0))/2);

//    int cluster = particleSize/4;
//    for(int i=0;i<4;i++){
//        for(int k=0;k<cluster;k++){
//            int index = cluster*i + k;
//            double min_ori = i*M_PI/2;
//            double rand_ori = this->randd()*(M_PI/2) + min_ori;
//            this->slaves.at(index).setOrientation(rand_ori);
//        }
//    }

//if(size > 1) {
//    transformParticles(xPos, yPos, zPos, b_x, b_y, b_z);
//}



}

void LocProvider::move(double distance, double turn, vector<double> magneticmeasurements, double position[], vector<double> beaconmeasurements, bool isBeacon, vector<double> wifimeasurements, bool isWifi, int bSize, bool isCached) {

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    if(this->reInitinitiated){
        position[0] = xPos;
        position[1] = yPos;
        position[2] = zPos;
        position[3] = orientation;
    }

    vector<double> weights;
    vector<double>weights_now;


    //int size = beaconDetails.size();



    std::default_random_engine generator (seed);

    //double std_dev = this->TURN_NOISE * M_PI/180;
    std::normal_distribution<double> turn_distribution(0, this->TURN_NOISE);
    std::normal_distribution<double> forward_distribution(0, this->FORWARD_NOISE);
    std::normal_distribution<double> random_angle_distribution(0, 1);
    std::normal_distribution<double> random_distance_distribution(0, 1);

    bool posteriorInside = false;
    for (int i = 0; i < particleSize; i++) {
        Particle slave = this->slaves.at(i);

        double degreeTurn = turn+turn_distribution(generator);
        double distance_forward = 0;
        if(distance > 0) {
            distance_forward = distance + forward_distribution(generator);
        }
        double shouldBeRandomHeading = fabs(random_angle_distribution(generator));
        double distanceTravelledShouldBeRandom = fabs(random_distance_distribution(generator));
        //roughly 10-13 percent particles random movement

//        if(shouldBeRandomHeading < 2 && shouldBeRandomHeading > 1.5){
//            degreeTurn = (this->randd()*2-1)*5;
//        }
        double distance_RANDOM = 1.5;

        if(distance > 0) {
            if (distanceTravelledShouldBeRandom < 2 && distanceTravelledShouldBeRandom > 1.5) {
                distance_forward = this->randd() * distance_RANDOM;
            }
        }

        this->slaves.at(i) = slave.move(distance_forward, degreeTurn, turn_distribution, generator, this->TURN_NOISE,this->walkregion);
        if(this->slaves.at(i).getMagYReading()){
            posteriorInside = true;
        }
    }

    if(distance == 0  || bSize < 1){
//        for(int k=0;k<particleSize;k++){
//            Particle slave = this->slaves.at(k);
//            if(!slave.getMagYReading()){
//                slave.transformWeight(1.1927909110297572e-20);
//            }
//            this->slaves.at(k) = slave;
//        }
        getUserPosition(weights, slaves, position);
        position[5] = 0;
        return;
    }


    double magnitude_sensed = magneticmeasurements.at(3);
//    vector<double> predictedLoc;
//    if(isBeacon){
//        vector<double> beaconVals;
//        beaconVals.push_back(beaconmeasurements.at(0));
//        beaconVals.push_back(beaconmeasurements.at(1));
//        predictedLoc = getNearestReadingsBeacon(beaconVals);
//    }

    for (int i = 0; i < particleSize; i++) {
        Particle slave = this->slaves.at(i);
        vector<double> location;
        location.push_back(slave.getX());
        location.push_back(slave.getY());
        location.push_back(slave.getZ());
        double weight = 1;

        double measurements[8] = {0, 0, 0, 0, 0, 0, 0, 0};
        bool ismagnetic= true;
        if (magneticmeasurements.at(0) == 0 && magneticmeasurements.at(1) == 0 &&
            magneticmeasurements.at(2) == 0) {
            ismagnetic = false;
        }

        if (magnitude_sensed > 80 || magnitude_sensed < 25) {
            ismagnetic = false;
        }




//
        double weight_sim = 1;
        if(isBeacon) {

            weight_sim = getNearestReadings_Similarity(location,
                                                       bSize, ismagnetic,
                                                       magneticmeasurements.at(1),
                                                       magneticmeasurements.at(2),
                                                       magneticmeasurements.at(0),
                                                       beaconmeasurements,
                                                       magnitude_sensed, slave.getMagYReading(), isCached);
//            weight_sim = getNearestReadings_Similarity_Interpolated(location,
//                                                       isBeacon, ismagnetic,
//                                                       magneticmeasurements.at(1),
//                                                       magneticmeasurements.at(2),
//                                                       magneticmeasurements.at(0),
//                                                       beaconmeasurements.at(0),
//                                                       beaconmeasurements.at(1),
//                                                       beaconmeasurements.at(2),
//                                                       magnitude_sensed);

            //}
            if(this->neighbour.getBeacon_x() != 0) {
                //slave.setMagYReading(this->neighbour.getY_mag());
                slave.setMagXReading(this->neighbour.getX_mag());
                slave.setMagZReading(this->neighbour.getZ_mag());
                slave.setBeaconXReading(this->neighbour.getBx_mean());
                slave.setBeaconYReading(this->neighbour.getBy_mean());
                slave.setBeaconZReading(this->neighbour.getBz_mean());
            }else{
                //slave.setMagYReading(0);
                slave.setMagXReading(0);
                slave.setMagZReading(0);
                slave.setBeaconXReading(0);
                slave.setBeaconYReading(0);
                slave.setBeaconZReading(0);
            }
        }

        //weight_sim = exp(-500);
        double weight_slave = slave.transformWeight(weight_sim);
        weights.push_back(weight_slave);
        weights_now.push_back(weight_sim);
        this->slaves.at(i) = slave;

    }




    int min_index = returnMax(weights_now);
    double min_weights = weights_now.at(min_index)+exp(-80);
    int max_index = returnMax(weights);
    Particle bestParticle = this->slaves.at(min_index);
    bXPos = bestParticle.getX();
    bYPos = bestParticle.getY();
    bZPos = bestParticle.getZ();
    double max_weights = weights.at(max_index)+exp(-80);
    getUserPosition(weights, slaves, position);


    this->normalizeWeightsForSlaves();


    //double min_weights = returnMin(weights);
    double check = max_weights;
//
    double minCheck = exp(-200);



    this->checkNeedForResample();
    //this->TURN_NOISE = position[4]*180/M_PI;
    status = position[4];
    xPos = position[0];
    yPos = position[1];
    zPos = position[2];
    orientation = position[3];
//    if(isBeacon && bSize > 1) {
//        position[4] = predictedLoc.at(0);
//        position[5] = predictedLoc.at(1);
//        position[7] = predictedLoc.at(2);
//    }else{
//        position[4] = xPos;
//        position[5] = yPos;
//        position[7] = zPos;
//    }
    //position[5] = max_weights;

//    if(!isBeacon || bSize <2){
//        this->avoidReasample = true;
//    }


    reSample(weights, slaves);
    if(!converged && status > 0){
        converged = true;
    }


    double pdf = -1;

    if(isBeacon && bSize > 1) {
        vector<double>obse;
        obse.push_back(xPos);
        obse.push_back(yPos);
        obse.push_back(zPos);
        bool isInside = isInsideFence(obse);
        double pdf1 = getMSE(xPos, yPos, zPos, beaconmeasurements.at(0), beaconmeasurements.at(1), beaconmeasurements.at(2));
        double pdf2 = getMSE(bXPos, bYPos, bZPos, beaconmeasurements.at(0), beaconmeasurements.at(1), beaconmeasurements.at(2));

        if(pdf1 > 15.0 && isInside){
            //robot is kidnapped
            pdf = pdf1;
            resample_count += 1;
            //position[7] = 100 - (resample_count/5) * 100;

//                if(resample_count >= 5 && converged){
//                    transformParticles(bXPos, bYPos, bZPos, beaconmeasurements.at(0), beaconmeasurements.at(1), beaconmeasurements.at(2));
//                }

        }else{
            resample_count = 0;
            //position[7] = 100;

        }

    }else{
        resample_count = 0;
        //position[7] = 100;
        pdf = 0;
    }
    if(!posteriorInside){
        pdf = -99999.0;
    }
    position[5] = pdf;

//    if(resample_count >= 10){
//        reInitinitiated = true;
//        resample_count = 0;
//    }
//    if(converged) {
//        if(pdf<=20 && pdf>=10){
//            int numberToAdd = max_particles - (particleSize+100);
//            if(numberToAdd >= 0) {
//                addMoreParticles(100);
//            }
//        }else if(pdf <=10 && pdf > 0 && !this->avoidReasample){
//            int numberToSubtract = (particleSize-100) - min_particles;
//            if(numberToSubtract > 0){
//                removeParticles(100);
//            }
//        }
//    }
}



void LocProvider::getNearestReadings(vector<double> sensed, double returnValue[]) {

    point_t p(sensed);

    auto res2 = this->neighboursTree.neighborhood_points(p, 3.0);

//    point_t a = res2;
    //double diff = 0.1;//sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;

    vector<double>inverseWeights;
    vector<double>beaconWeights;
    double min_diff = 999999;
    if(res2.size() > 0) {
        for (point_t a : res2) {
            double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
            if(diff < min_diff){
                min_diff = diff;
            }
            double inverseDiff = 1.0/diff;
            inverseWeights.push_back(1.0/diff);
            string key = to_string(a.at(0)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(2));
            NeighbourMeasurements reading = measurementsHash.at(key);
            bool isBeacon  = reading.isHasBeacon();
            if(isBeacon){
                beaconWeights.push_back(1.0/diff);
            }
        }

        double sum_inverse = 0;
        for(int k=0;k<inverseWeights.size();k++){
            sum_inverse += inverseWeights.at(k);
        }
        for(int k=0;k<inverseWeights.size();k++){
            double weightUnormalized = inverseWeights.at(k);
            double weightNormalized  = weightUnormalized/sum_inverse;
            inverseWeights.at(k)     = weightNormalized;
        }


        double sum_beacon_inverse = 0;
        for(int k=0;k<beaconWeights.size();k++){
            sum_beacon_inverse += beaconWeights.at(k);
        }
        for(int k=0;k<beaconWeights.size();k++){
            double weightUnormalized = beaconWeights.at(k);
            double weightNormalized  = weightUnormalized/sum_beacon_inverse;
            beaconWeights.at(k)     = weightNormalized;
        }




        int index = 0;
        int bindex = 0;
        for (point_t a : res2){
            string key = to_string(a.at(0)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(2));
            NeighbourMeasurements reading = measurementsHash.at(key);
            double weight = inverseWeights.at(index);
            returnValue[0] += (weight * reading.getX_mag());
            returnValue[1] += (weight * reading.getY_mag());
            returnValue[2] += (weight * reading.getZ_mag());
            bool isBeacon  = reading.isHasBeacon();
            if(isBeacon) {
                double bWeight = beaconWeights.at(bindex);
                returnValue[3] += (bWeight * reading.getBeacon_x());
                returnValue[4] += (bWeight * reading.getBeacon_y());
                returnValue[5] += (bWeight * reading.getBeacon_z());
                returnValue[6] += (bWeight * reading.getBeacon_rssi_mean());
                returnValue[7] += (bWeight * reading.getBeacon_distance_std());
                bindex++;
            }



            index++;
        }


    }else{
        returnValue[0] = 9999999999;
        returnValue[1] = 9999999999;
        returnValue[2] = 9999999999;
        returnValue[3] = 9999999999;
        returnValue[4] = 9999999999;
        returnValue[5] = 9999999999;
        returnValue[6] = 9999999999;
        returnValue[7] = 9999999999;
    }



}

void LocProvider::getNearestReadings_Point(vector<double> sensed, double returnValue[]) {

    point_t p(sensed);

    point_t a = this->neighboursTree.nearest_point(p);

//    point_t a = res2;
    double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
    if(diff < 5.0){
        string key = to_string(a.at(0)) + "->" + to_string(a.at(1)) + "->" + to_string(a.at(2));
        NeighbourMeasurements reading = measurementsHash.at(key);
        returnValue[0] += (1 * reading.getX_mag());
        returnValue[1] += (1 * reading.getY_mag());
        returnValue[2] += (1 * reading.getZ_mag());
        bool isBeacon = reading.isHasBeacon();
        if (isBeacon) {
            //double bWeight = beaconWeights.at(bindex);
            returnValue[3] += (1 * reading.getBeacon_x());
            returnValue[4] += (1 * reading.getBeacon_y());
            returnValue[5] += (1 * reading.getBeacon_z());
            returnValue[6] += (1 * reading.getBeacon_rssi_mean());
            returnValue[7] += (1 * reading.getBeacon_distance_std());

        }
    } else {
        returnValue[0] = 9999999999;
        returnValue[1] = 9999999999;
        returnValue[2] = 9999999999;
        returnValue[3] = 9999999999;
        returnValue[4] = 9999999999;
        returnValue[5] = 9999999999;
        returnValue[6] = 9999999999;
        returnValue[7] = 9999999999;
    }




}

vector<double> LocProvider::getNearestReadingsBeacon(vector<double> sensed) {

    point_t p(sensed);


    auto res2 = this->beaconNeigboursTree.nearest_point(p);
    vector<double> prediction;
    //double magnitude=0,beacon_x=0,beacon_y=0,beacon_z=0,magnitude_world=0,xy_mag=0,beaconrssi_x=0,beaconrssi_y=0,beaconrssi_z = 0;
    double x = 0, y =0, z =0;
    int index =0;
    point_t a = res2;
    string key = to_string(a.at(0)) +"->"+to_string(a.at(1));
    NeighbourMeasurements mst   = beaconHash.at(key);
    double x_                   = mst.getParticle_x();
    double y_                   = mst.getParticle_y();
    double z_                   = mst.getParticle_z();



    x               += x_;
    y               += y_;
    z               += z_;
    prediction.push_back(x);
    prediction.push_back(y);
    prediction.push_back(z);
    return prediction;
}

void LocProvider::getUserPosition(vector<double> weights, vector<Particle> slaves, double position[]) {
    double xMean = 0;
    double yMean = 0;
    double zMean = 0;
    double count = 0;
    double sumWeight = 0;
    double orientation = 0;

//    for (int i = 0; i < particleSize; i++) {
//        sumWeight += weights[i];
//    }
    for (int i = 0; i < particleSize; i++) {
        Particle slave = slaves.at(i);
        sumWeight += slave.getWeight();
    }
    if(sumWeight == 0){
        sumWeight = 0.00000001;
        //avoidReasample = true;
    }
    double min_orientation = slaves.at(0).getOrientation();
    double max_orientation = slaves.at(0).getOrientation();
    for (int i = 0; i < particleSize; i++) {
        Particle slave = slaves.at(i);
        double weight = slave.getWeight();
        double weightFactor = weight / sumWeight;
        count += weightFactor;

        xMean += slave.getX() * weightFactor;
        yMean += slave.getY() * weightFactor;
        zMean += slave.getZ() * weightFactor;
        double particle_orientation = (fmod((slave.getOrientation() - slaves.at(0).getOrientation() + M_PI), (2.0 * M_PI))
                                       + slaves.at(0).getOrientation() - M_PI);
        if(particle_orientation < 0){
            particle_orientation += 2*M_PI;
        }
        particle_orientation = fmod(particle_orientation, 2*M_PI);
        orientation += particle_orientation;
        if(max_orientation < particle_orientation){
            max_orientation = particle_orientation;
        }
        if(min_orientation > particle_orientation){
            min_orientation = particle_orientation;
        }

    }
    if(count == 0){
        count = 0.0000000001;
    }
    xMean /= count;
    yMean /= count;
    zMean /= count;
    orientation /= this->particleSize;
    //count = -1;
    count = 0;
    double maxDiff = -1;
    double orientationdiff = 0;
    int orientationCount = 0;
    double highestDiff = -1;
    for (int i = 0; i < particleSize; i++) {
        Particle slave = slaves.at(i);
        double euclidean = sqrt(pow(slave.getX() - xMean, 2) +
                                pow(slave.getY() - yMean, 2) + pow(slave.getZ() - zMean, 2));

        if(euclidean > highestDiff){
            highestDiff = euclidean;
        }
        maxDiff += euclidean;

        double orientDiff = orientation - slave.getOrientation();
        orientDiff = fabs(fmod((orientDiff + M_PI),(2.0 * M_PI)) - M_PI);
        orientationdiff += orientDiff;
        if (euclidean < 3.0) {
            count++;
            //orientationCount += 1;
        }
        if(orientDiff < 0.25){
            orientationCount += 1;
        }
//
    }
    orientationdiff /= particleSize;

    //count /= particleSize;
//
    if(orientationCount > 0.9 * particleSize){
//        if(!orientationConfident){
        orientationConfident = true;
        //}
    }else{
        orientationConfident = false;
    }
    double status = 1;
    if (count < (0.90 * particleSize)) {
        status = 0;
    }


    position[0] = xMean;
    position[1] = yMean;
    position[2] = zMean;
    position[3] = orientation;
    position[4] = status;
    position[5] = orientationdiff;
    //position[6] = maxDiff / particleSize;
    if(converged) {
        position[6] = maxDiff / particleSize;
    }else{
        position[6] = highestDiff;
    }
    //position[6] = maxDiff/particleSize;
    position[7] = max_orientation*180/M_PI;
}

void LocProvider::reSample(vector<double> weights, vector<Particle> slaves) {
    if(avoidReasample){

//        for(int j=0;j<particleSize;j++){
//            Particle particle = this->slaves.at(j);
//
//            this->slaves.at(j) = particle;
//        }
        return;

    }
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "resample called");
    vector<Particle> newparticles;


    int rand_index                    = (int)(((double) rand() / (RAND_MAX))*(particleSize));
    double beta                       = 0.0;
    double max_weight = *max_element(weights.begin(),weights.end());

    for(int i=0; i<particleSize; i++){
        beta    += ((randd())*(2.0)*(max_weight));
        while (beta >= weights.at(rand_index)){
            beta -= weights.at(rand_index);
            rand_index = (rand_index+1) % particleSize;
        }

        Particle particle = this->slaves.at(rand_index);
        particle.setWeight(1);
        Particle copy_particle(particle.getX(), particle.getY(), particle.getZ(), particle.getOrientation(), this->geofence, this->FORWARD_NOISE, this->TURN_NOISE,1);

        copy_particle.setMagYReading(particle.getMagYReading());
        copy_particle.setMagZReading(particle.getMagZReading());
        copy_particle.setBeaconXReading(particle.getBeaconXReading());
        copy_particle.setBeaconYReading(particle.getBeaconYReading());
        copy_particle.setBeaconZReading(particle.getBeaconZReading());
        newparticles.push_back(copy_particle);
    }
    this->slaves.clear();
    this->slaves = newparticles;
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "resample ended");
    this->resampleCalled = true;
    this->convergenceConfidence = 1;
}

int LocProvider::returnMax(vector<double> weights) {
    return max_element(weights.begin(),weights.end())- weights.begin();
}

int LocProvider::returnMin(vector<double> weights) {
    return min_element(weights.begin(),weights.end())- weights.begin();
}



void LocProvider::reInitParticleFilter(double px, double py, double pz) {
    this->slaves.clear();
    this->reInitinitiated = true;
    this->converged = false;
    status = 0;

    for(int i=0;i<particleSize;i++){
        double x = (((this->randd()*2)-1)*5) + px;
        double y = (((this->randd()*2)-1)*5) + py;
        double z = (((this->randd()*2)-1)*5) + pz;
        Particle slave(x, y, z, this->randd()*2*M_PI,geofence);
        slave.setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        this->slaves.push_back(slave);
        //weights.push_back(1);
    }
    //divide equally into all quadrants of [0 2#]
    int cluster = particleSize/4;
    for(int i=0;i<4;i++){
        for(int k=0;k<cluster;k++){
            int index = cluster*i + k;
            double min_ori = i*M_PI/2;
            //double max_ori = min_ori + (M_PI/2);
            double rand_ori = this->randd()*(M_PI/2) + min_ori;
            this->slaves.at(index).setOrientation(rand_ori);
        }
    }

    this->reInitinitiated = false;

}

void LocProvider::calculateBias(double turnInDegrees) {
    double totalLeftOver = 0.0;
    for(int i=0;i<particleSize;i++){
        double particleHeadingInradians = this->slaves.at(i).getOrientation();
        double particleHeadingInDegrees = particleHeadingInradians*180/M_PI;
        double leftOver                 = particleHeadingInDegrees - turnInDegrees;
        totalLeftOver                   += (this->slaves.at(i).getWeight()*leftOver);
    }
    bias    = totalLeftOver/particleSize;
}

void LocProvider::getParticleLocations(double locs[2500][10]) {
    if(!this->reInitinitiated) {
        for (int i = 0; i < particleSize; i++) {
            Particle slave = this->slaves.at(i);
            locs[i][0] = slave.getX();
            locs[i][1] = slave.getY();
            locs[i][2] = slave.getZ();
            locs[i][3] = slave.getOrientation();
            locs[i][4] = slave.getWeight();

            locs[i][5] = slave.getMagYReading();
            locs[i][6] = slave.getMagZReading();
            locs[i][7] = slave.getBeaconXReading();
            locs[i][8] = slave.getBeaconYReading();
            locs[i][9] = slave.getBeaconZReading();
        }
    } else{
        for (int i = 0; i < particleSize; i++) {

            locs[i][0] = (this->randd()*(geofence.at(0).at(1) - geofence.at(0).at(0)))+geofence.at(0).at(0);
            locs[i][1] = (this->randd()*(geofence.at(1).at(1) - geofence.at(1).at(0)))+geofence.at(1).at(0);
            locs[i][2] = (this->randd()*(geofence.at(2).at(1) - geofence.at(2).at(0)))+geofence.at(2).at(0);
            locs[i][3] = this->randd()*M_PI;
            locs[i][4] = 99;

            locs[i][5] = 999999999999;
            locs[i][6] = 999999999999;
            locs[i][7] = 999999999999;
            locs[i][8] = 999999999999;
            locs[i][9] = 999999999999;
        }
    }

}

void LocProvider::buildTreeReverse(vector<point_t> vector) {
    this->neighboursTree = KDTree(vector);
}

//void LocProvider::insertMagneticMapIntoTree(vector<MagneticMap> magMapVector) {
//    this->augmentedTree = new IntervalTree();
//    for(unsigned long i=0;i<magMapVector.size();i++){
//        MagneticMap map = magMapVector.at(i);
//        Node* node = new Node(map.getMinimum(), map.getMaximum(), map.getMX(), map.getMY(), map.getMZ());
//        this->augmentedTree->root = this->augmentedTree->insertNode(this->augmentedTree->root, node);
//    }
//    cout<<"here";
//}



void LocProvider::normalizeWeightsForSlaves() {
    double sum = 0;
    for (int i = 0; i < particleSize; i++) {
        Particle slave = this->slaves.at(i);
        double weight = slave.getWeight();
        sum += weight;
    }
    if(sum == 0){
        sum = 0.00000001;
    }
    for (int i = 0; i < particleSize; i++) {
        Particle slave = this->slaves.at(i);
        double normalizedWeight = slave.getWeight()/sum;
        slave.setWeight(normalizedWeight);
        this->slaves.at(i) = slave;
    }
}

void LocProvider::checkNeedForResample() {
//        if(!converged) {
    double sumWeights = 0;
    for (int i = 0; i < particleSize; i++) {
        Particle slave = this->slaves.at(i);
        double val = slave.getWeight();
        val = pow(val, 2);
        sumWeights += val;
    }

    if (sumWeights == 0) {
        sumWeights = 0.0000000000000001;
    }
    double invWeight = 1 / sumWeights;
    this->avoidReasample = invWeight >= particleSize / 2;
//        }else{
//            this->avoidReasample = false;
//        }


}

/*
 * Add
 */
void LocProvider::addMoreParticles(int toAdd) {
    this->reInitinitiated = true;
    this->particleSize += toAdd;
    for (int i=0;i<toAdd;i++){
        double x = (this->randd()*2-1)*3+xPos;
        double y = (this->randd()*2-1)*3+yPos;
        double z = (this->randd()*2-1)*3+zPos;
        double ori = (this->randd()*2 -1)*M_PI/18+orientation;
        Particle p(x, y, z, ori, this->geofence);
        p.setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        p.setWeight(1);
        this->slaves.push_back(p);
    }
    this->reInitinitiated = false;
}
/*
 * Remove
 */

void LocProvider::removeParticles(int toRemove) {
    std::sort(this->slaves.begin(), this->slaves.end());
    if(this->slaves.size() > toRemove) {
        this->slaves.erase(this->slaves.begin() , this->slaves.begin()+toRemove);
        this->particleSize -= toRemove;

    }

}


//double LocProvider::getNearestReadings_Similarity(vector<double> obs, map<string,SensorReading>beaconDetails, bool isBeacon, bool isMagnitude, double magy, double magz,double magx,
//                                                  double b_x, double b_y, double b_z, double magObs) {
//    point_t p(obs);
//
//    point_t a = this->neighboursTree.nearest_point(p);
//
////    point_t a = res2;
//    double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
//    //double magnitude = getMagnitudeInterpolated(obs);
//    if(diff < 2.0){
//        string key = to_string(a.at(0)) + "->" + to_string(a.at(1)) + "->" + to_string(a.at(2));
//        NeighbourMeasurements reading = measurementsHash.at(key);
//        double magy_reading = reading.getMagy_mean();
//        double magy_std     = reading.getMagy_std()+1.5;
//        double magz_reading = reading.getMagz_mean();
//        double magz_std     = reading.getMagz_std()+1.5;
//        double magx_reading = reading.getMagx_mean();
//        double magx_std     = reading.getMagx_std()+1.5;
//        //this->neighbour = reading;
//
//        double weight = 1;
//        map<string,SensorReading> beaconreadings = reading.getBeacon_details();
//        double magsensed  = sqrt(pow(magy, 2)+pow(magz, 2)+pow(magx, 2));
//        double weight_wxp = 0.25;
//        double magnitude_err = reading.getMag_std();
////        double mag_vector_err = 3.0;
//
//        if(isMagnitude && converged){
//            //only uncomment magnitude if fingerprint is not in kalman mode
//            double magreading = sqrt(pow(magy_reading, 2)+ pow(magz_reading,2)+pow(magx_reading,2));
//
////            magsensed  = magsensed;
////            magreading = magreading;
//            double val = pow(gaussian(magsensed, magnitude_err, magreading),weight_wxp)+exp(-70);
//            //if(converged) {
//            weight *= val;
////            weight *= pow(gaussian(fabs(magx), magx_std, fabs(magx_reading)),weight_wxp)+exp(-70);
////            weight *= pow(gaussian(fabs(magy), magy_std, fabs(magy_reading)),weight_wxp)+exp(-70);
////            weight *= pow(gaussian(fabs(magz), magz_std, fabs(magz_reading)),weight_wxp)+exp(-70);
//
//            //}
//
//
//        }
//
//
//        double readings[beaconDetails.size()];
//        double sensedvals[beaconDetails.size()];
//        int i=0;
//        if(isBeacon){
//            for (std::map<string,SensorReading>::iterator it=beaconDetails.begin(); it!=beaconDetails.end(); ++it){
//                string key = it->first;
//                SensorReading sensreading = it->second;
//                readings[i] = (sensreading.getRssi()+120.0)/120.0;
//                if (beaconreadings.count(key)){
//                    SensorReading readingVal = beaconreadings.at(key);
//                    sensedvals[i] = (readingVal.getRssi()+120.0)/120.0;
//                }else{
//                    sensedvals[i] = (-150+120.0)/120.0;
//                }
//                i++;
//            }
//            double val = cosine_similarity(readings, sensedvals, beaconDetails.size());
//            double gaus  = gaussian(val, 0.01, 1);
//            double prob  = pow(gaus, weight_wxp)+exp(-70);
//            weight *= prob;
//
//            double b_x_read = reading.getBx_mean();
//            double b_y_read = reading.getBy_mean();
//            double b_z_read = reading.getBz_mean();
//
//
//            double bx_std   = 1.5;//reading.getBx_std();
//            double by_std   = 1.5;//reading.getBy_std();
//            double bz_std   = 1.5;//reading.getBz_std();
//
//            double distance_std = sqrt(pow(bx_std, 2)+pow(by_std, 2)+pow(bz_std, 2));
//
////
//            double prob_x = gaussian(b_x, bx_std, b_x_read);
//            prob_x  = pow(prob_x, weight_wxp);
//            weight *= prob_x+exp(-70);
//
//            double prob_y = gaussian(b_y, by_std, b_y_read);
//            prob_y  = pow(prob_y, weight_wxp);
//            weight *= prob_y+exp(-70);
//
//            double prob_z = gaussian(b_z, bz_std, b_z_read);
//            prob_z  = pow(prob_z, weight_wxp);
//            weight *= prob_z+exp(-70);
//
////            double beacons_dist_diff = sqrt(pow(b_x_read-b_x, 2)+pow(b_y_read-b_y, 2)+pow(b_z_read-b_z, 2));
////
////            double prob_dist = pow(gaussian(0.0, distance_std, beacons_dist_diff), weight_wxp);
////            weight *= prob_dist+exp(-70);
//        }
//
//        return weight;
//
//
//    } else {
//        return 1.1927909110297572e-100;
//    }
//
//
//
//
//}

double LocProvider::getNearestReadings_Similarity(vector<double> obs, bool isBeacon, bool isMagnitude, double magy, double magz,double magx,
                                                  vector<double>beaconmeasurements, double magObs, bool isInside, bool isCached) {
    point_t p(obs);
    //bool inside = isInsideFence(obs);

    point_t a = this->neighboursTree.nearest_point(p);
    if(!isInside || a.size() == 0){
        this->neighbour.setBeacon_x(0);
        return 1.1927909110297572e-150;
    }

//    point_t a = res2;
    double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
    //double magnitude = getMagnitudeInterpolated(obs);
    string key = to_string(a.at(0)) + "->" + to_string(a.at(1)) + "->" + to_string(a.at(2));
    NeighbourMeasurements reading = measurementsHash.at(key);
    double magy_reading = reading.getY_mag();
    double magz_reading = reading.getZ_mag();
    double magx_reading = reading.getX_mag();
    double mean_rssi_beacon = reading.getBeacon_rssi_mean();
    double std_rssi_beacon  = reading.getBeacon_rssi_std();

    this->neighbour = reading;
    this->neighbour.setBeacon_x(1);
    //if(diff < 10){
    //this->neighbour.setY_mag(inside);

    double weight = 1;

    map<string,SensorReading> beaconreadings = reading.getBeacon_details();
    double magsensed  = sqrt(pow(magy, 2)+pow(magz, 2)+pow(magx, 2));
    double weight_wxp = 0.15;
    double magnitude_err = 5.0;
    double mag_vector_err = 3.5;

    if(isMagnitude && converged){

        double magreading = sqrt(pow(magy_reading, 2)+ pow(magz_reading,2)+pow(magx_reading,2));
        double val = pow(gaussian(magsensed, magnitude_err, magreading),weight_wxp)+exp(-70);

        //weight *= val;

    }

    if(isBeacon){

        //weight *= 1.5/diff;
        double b_x_read = reading.getBx_mean();//(reading.getBeacon_x()-geofence.at(0).at(0))/(geofence.at(0).at(1) - geofence.at(0).at(0));
        double b_y_read = reading.getBy_mean();//(reading.getBeacon_y()-geofence.at(1).at(0))/(geofence.at(1).at(1) - geofence.at(1).at(0));
        double b_z_read = reading.getBz_mean();//(reading.getBeacon_z()-geofence.at(2).at(0))/(geofence.at(2).at(1) - geofence.at(2).at(0));

        double b_x = beaconmeasurements.at(0);
        double b_y = beaconmeasurements.at(1);
        double b_z = beaconmeasurements.at(2);

        double readingStd = 3.5;
        double distanceStd = 5.0;
        double reading_zStd = 3.5;
//    if(converged){
//        distanceStd = 5.0;
//    }
//        if(!converged){
//            readingStd = 3.0;
//        }
        if(!isBeacon){
            readingStd = 7.5;
            distanceStd = 25;
        }
//    if(isCached){
//        readingStd = 3.5;
//    }


//            if(!converged){
//                distanceStd = 7.5;
//            }

        double prob_x = pow(gaussian(b_x, readingStd, b_x_read), weight_wxp);
        weight *= prob_x+exp(-70);

        double prob_y = pow(gaussian(b_y, readingStd, b_y_read), weight_wxp);
        weight *= prob_y+exp(-70);

//            double prob_z = pow(gaussian(b_z, readingStd, b_z_read), weight_wxp);
//            weight *= prob_z+exp(-70);

//    double beacons_dist_diff = sqrt(pow(b_x_read-b_x, 2)+pow(b_y_read-b_y, 2));
////
//    double prob_dist = pow(gaussian(beacons_dist_diff, distanceStd, 0.0), weight_wxp);
//    weight *= prob_dist+exp(-70);
        //}
//        else {
//            double mean_beacon = reading.getBeacon_rssi_mean();
//            double prob_rssi = pow(gaussian(mean_beacon, beaconmeasurements.at(3), 1.5),
//                                   weight_wxp);
//            weight *= prob_rssi;
//
    }else{
        double  dist = beaconmeasurements.at(4);
        double b_x = beaconmeasurements.at(0);
        double b_y = beaconmeasurements.at(1);
        double b_z = beaconmeasurements.at(2);

        double distanceCalc = sqrt(pow(a.at(0)-b_x, 2)+pow(a.at(1)-b_y, 2)+pow(a.at(2)-b_z, 2));
        weight *=     pow(gaussian(dist, 2.5, distanceCalc),
                                   weight_wxp);

    }
    return weight;


//    } else {
//        this->neighbour.setBeacon_x(0);
//        return 1.1927909110297572e-50;
//    }




}




double LocProvider::getMagnitudeInterpolated(vector<double>obs){
    point_t p(obs);

    auto res2 = this->neighboursTree.neighborhood_points(p, 3.0);

    vector<double>inverseWeights;
    vector<double>beaconWeights;

    double min_diff = 999999;
    if(res2.size() > 0) {
        for (point_t a : res2) {
            double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
            if(diff < min_diff){
                min_diff = diff;
            }
            double inverseDiff = 1.0/diff;
            inverseWeights.push_back(1.0/diff);
        }

        double sum_inverse = 0;
        for(int k=0;k<inverseWeights.size();k++){
            sum_inverse += inverseWeights.at(k);
        }
        for(int k=0;k<inverseWeights.size();k++){
            double weightUnormalized = inverseWeights.at(k);
            double weightNormalized  = weightUnormalized/sum_inverse;
            inverseWeights.at(k)     = weightNormalized;
        }







        int index = 0;
        double magnitude_ = 0;
        for (point_t a : res2){
            string key = to_string(a.at(0)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(2));
            NeighbourMeasurements reading = measurementsHash.at(key);
            double weight = inverseWeights.at(index);
            double magy_reading = reading.getY_mag();
            double magz_reading = reading.getZ_mag();
            double magx_reading = reading.getX_mag();
            double mag          = sqrt(pow(magx_reading, 2) + pow(magy_reading, 2) + pow(magz_reading, 2));
            magnitude_          += weight * mag;

            index++;
        }

        return magnitude_;
    }else{
        return 0;
    }
}

double LocProvider::getNearestReadings_Similarity_Interpolated(vector<double> obs,  bool isBeacon, bool isMagnitude, double magy, double magz,double magx,
                                                               double b_x, double b_y, double b_z, double magObs) {
    point_t p(obs);

    auto res2 = this->neighboursTree.neighborhood_points(p, 3.0);

    vector<double>inverseWeights;

    double magsensed  = sqrt(pow(magy, 2)+pow(magz, 2)+pow(magx, 2));
    double weight     = 1;
    double min_diff = 999999;
    if(res2.size() > 0) {

        int min_index =0;
        int index_count =0;
        for (point_t a : res2) {
            double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
            if(diff < min_diff){
                min_diff = diff;
                min_index = index_count;
            }
            double inverseDiff = 1.0/diff;
            inverseWeights.push_back(1.0/diff);
            index_count += 1;
        }

        double sum_inverse = 0;
        for(int k=0;k<inverseWeights.size();k++){
            sum_inverse += inverseWeights.at(k);
        }
        for(int k=0;k<inverseWeights.size();k++){
            double weightUnormalized = inverseWeights.at(k);
            double weightNormalized  = weightUnormalized/sum_inverse;
            inverseWeights.at(k)     = weightNormalized;
        }







        int index = 0;
        int bindex = 0;
        double b_x_=0;
        double b_y_=0;
        double b_z_=0;

        double weight_exp = 0.2;

        for (point_t a : res2){
            string key = to_string(a.at(0)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(2));
            NeighbourMeasurements reading = measurementsHash.at(key);
            double weight = inverseWeights.at(index);
            double beacon_x     = reading.getBeacon_x();
            double beacon_y     = reading.getBeacon_y();
            double beacon_z     = reading.getBeacon_z();
            b_x_                += weight * beacon_x;
            b_y_                += weight * beacon_y;
            b_z_                += weight * beacon_z;
            bool isBeacon       = reading.isHasBeacon();
            if(index == min_index){
                this->neighbour = reading;
            }
            index++;
        }
        if(isBeacon){

            double readingStd = 3.0;
            double distanceStd = 3.0;

//
//            double prob_x = gaussian(b_x_, readingStd, b_x);
//            prob_x  = pow(prob_x, weight_exp);
//            weight *= prob_x+exp(-70);
//
//            double prob_y = gaussian(b_y_, readingStd, b_y);
//            prob_y  = pow(prob_y, weight_exp);
//            weight *= prob_y+exp(-70);
//
//            double prob_z = gaussian(b_z_, readingStd, b_z);
//            prob_z  = pow(prob_z, weight_exp);
//            weight *= prob_z+exp(-70);

            double beacons_dist_diff = sqrt(pow(b_x_-b_x, 2)+pow(b_y_-b_y, 2)+pow(b_z_-b_z, 2));
            double prob_dist = pow(gaussian(0.0, distanceStd, beacons_dist_diff), weight_exp);
            weight *= prob_dist+exp(-70);

        }

        return weight;

    }else{
        return 1.1927909110297572e-80;
    }






}



double LocProvider::gaussian(double mu, double sigma, double value) {
    double numerator = -1*pow((mu - value)/(sigma*0.5) ,2);
    numerator        = exp(numerator);
    double denominator      = sqrt(2 * M_PI)*sigma+0.00000000000000000000000000005;

    return numerator/denominator;
}
double LocProvider::cosine_similarity(double *A, double *B, unsigned int Vector_Length)
{
    double dot = 0.0, denom_a = 0.0, denom_b = 0.0 ;
    for(unsigned int i = 0u; i < Vector_Length; ++i) {
        dot += A[i] * B[i] ;
        denom_a += A[i] * A[i] ;
        denom_b += B[i] * B[i] ;
    }
    return dot / (sqrt(denom_a) * sqrt(denom_b)) ;
}

double LocProvider::angleFromLatLng(double lat1, double lng1, double lat2, double lng2) {
    lat1 = lat1 * (M_PI /180.0);
    lat2 = lat2 * (M_PI/180.0);
    lng1 = lng1 * (M_PI/180.0);
    lng2 = lng2 * (M_PI/180.0);

    double dlon = lng2 - lng1;
    double y    = sin(dlon) * cos(lat2);
    double x    = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dlon);

    double brng = atan2(y, x);
    brng        = (180 * brng) / M_PI;
    if(brng < 0){
        brng += 360;
    }
    brng = fmod(brng, 360);
    return brng;
}

void LocProvider::convertCartesianToSpherical(double *cartesian, double *latlng) {
    double radius = sqrt((cartesian[0] * cartesian[0])+(cartesian[1] * cartesian[1])+(cartesian[2] * cartesian[2]));
    double lat = radToDeg(asin(cartesian[2] / radius));
    double lng = radToDeg(atan2(cartesian[1], cartesian[0]));
    latlng[0] = lat;
    latlng[1] = lng;
    return;
}

double LocProvider::radToDeg(double deg) {
    if(deg < 0){
        deg += 2 * M_PI;
    }else if(deg > 2*M_PI){
        deg -= 2*M_PI;
    }
    double radians =  deg * 180 / M_PI;
    return radians;


}

double LocProvider::degToRad(double deg) {
    return (deg * M_PI)/180;
}

double LocProvider::getExpectedDensity(double x, double y, double z) {
    double avgx = 0;
    double avgy = 0;
    double avgz = 0;
    for(int k=0;k<particleSize;k++){
        Particle slave = this->slaves.at(k);
        avgx += slave.getBeaconXReading();
        avgy += slave.getBeaconYReading();
        avgz += slave.getBeaconZReading();
    }

    avgx /= particleSize;
    avgy /= particleSize;
    avgz /= particleSize;

    double pdf = 1;
    pdf *=  gaussian(x, 1.0, avgx);
    pdf *=  gaussian(y, 1.0, avgy);
    pdf *=  gaussian(z, 1.0, avgz);
    return pdf;
}

double LocProvider::getMSE(double x, double y, double z, double b_x, double b_y, double b_z) {

    vector<double>obs;
    obs.push_back(x);
    obs.push_back(y);
    obs.push_back(z);



    point_t p(obs);
    //1.5->30
    auto res2 = this->neighboursTree.neighborhood_points(p, 0.5);

    vector<double>inverseWeights;



    double min_diff = 999999;
    if(res2.size() > 0) {

        int min_index =0;
        int index_count =0;
        for (point_t a : res2) {
            double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2))+0.00000000001;
            if(diff < min_diff){
                min_diff = diff;
                min_index = index_count;
            }
            double inverseDiff = 1.0/diff;
            inverseWeights.push_back(1.0/diff);
            index_count += 1;
        }

        double sum_inverse = 0;
        for(int k=0;k<inverseWeights.size();k++){
            sum_inverse += inverseWeights.at(k);
        }
        for(int k=0;k<inverseWeights.size();k++){
            double weightUnormalized = inverseWeights.at(k);
            double weightNormalized  = weightUnormalized/sum_inverse;
            inverseWeights.at(k)     = weightNormalized;
        }







        int index = 0;
        int bindex = 0;
        double b_x_=0;
        double b_y_=0;
        double b_z_=0;



        for (point_t a : res2){
            string key = to_string(a.at(0)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(2));
            NeighbourMeasurements reading = measurementsHash.at(key);
            double weight = inverseWeights.at(index);
            double beacon_x     = reading.getBx_mean();
            double beacon_y     = reading.getBy_mean();
            double beacon_z     = reading.getBz_mean();
            b_x_                += weight * beacon_x;
            b_y_                += weight * beacon_y;
            b_z_                += weight * beacon_z;


            index++;
        }




        double rmse = pow(pow(b_x_-b_x, 2)+pow(b_y_-b_y, 2), 0.5);

        return rmse;


    }else{
        return 999999999999999999999999999.0;
    }
}

void LocProvider::transformParticles(double bxpos, double bypos, double bzpos, double b_x, double b_y, double b_z) {

    this->slaves.clear();
    //this->reInitinitiated = true;
    this->converged = false;
    status = 0;



    double currentBestPos[3] = {bxpos, bypos, bzpos};
    double dp[3]             = {1, 1, 1};
    double b_error           = getMSE(bxpos, bypos, bzpos, b_x, b_y, b_z);
    int n = sizeof(dp)/sizeof(dp[0]);
    double sum_dp = accumulate(dp, dp+3, 0.0);
    while(sum_dp > 0.1){
        for(int i=0;i<3;i++){
            currentBestPos[i] += dp[i];
            //currentBestPos[i] = adjustCoordinates(currentBestPos[i], i);
            double error      = getMSE(currentBestPos[0], currentBestPos[1], currentBestPos[2], b_x, b_y, b_z);
            if(error < b_error){
                b_error = error;
                dp[i] *= 1.1;

            }else{
                currentBestPos[i] -= 2*dp[i];
                //currentBestPos[i] = adjustCoordinates(currentBestPos[i], i);
                double error      = getMSE(currentBestPos[0], currentBestPos[1], currentBestPos[2], b_x, b_y, b_z);
                if(error < b_error){
                    b_error = error;
                    dp[i]   *= 1.1;
                }else{
                    currentBestPos[i] += dp[i];
                    //currentBestPos[i] = adjustCoordinates(currentBestPos[i], i);
                    dp[i] *= 0.9;
                }
            }

        }
        sum_dp = accumulate(dp, dp+3, 0.0);
    }
    xPos = currentBestPos[0];
    yPos = currentBestPos[1];
    zPos = currentBestPos[2];


    std::uniform_real_distribution<> dist_x(0.0, 1.0);
    std::uniform_real_distribution<> dist_y(0.0, 1.0);
    std::uniform_real_distribution<> dist_z(0.0, 1.0);
    std::uniform_real_distribution<> dist_or(0.0, 1.0);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator_x (seed);
    std::default_random_engine generator_y (seed);
    std::default_random_engine generator_z (seed);
    std::default_random_engine generator_or (seed);

    for(int i=0;i<particleSize;i++){
        double x = ((dist_or(generator_or)*2-1) * 3.0)+ xPos;
        double y = ((dist_or(generator_or)*2-1) * 3.0)+ yPos;
        double z = ((dist_or(generator_or)*2-1) * 3.0)+ zPos;
        vector<double>obs;
        obs.push_back(x);
        obs.push_back(y);
        obs.push_back(z);
        while (!isInsideFence(obs)){
            x = ((dist_or(generator_or)*2-1) * 3.5)+ xPos;
            y = ((dist_or(generator_or)*2-1) * 3.5)+ yPos;
            z = ((dist_or(generator_or)*2-1) * 3.5)+ zPos;
            obs.clear();
            obs.push_back(x);
            obs.push_back(y);
            obs.push_back(z);
        }

        double ori = this->randd()*2*M_PI;
        //double ori = (dist_or(generator_or)*2-1)*M_PI/4+orientation;
//        if(ori < 0){
//            ori += 2*M_PI;
//        }
        ori = fmod(ori, 2*M_PI);
        Particle slave(x, y, z, ori,geofence);
        slave.setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        this->slaves.push_back(slave);

    }

    this->reInitinitiated = false;
}

double LocProvider::adjustCoordinates(double value, int index) {
    double adjustedvalue = value;
    if (value < this->geofence.at(index).at(0)){
        adjustedvalue = this->geofence.at(index).at(0);
        __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "check called");
    } else if(value > this->geofence.at(index).at(1)){
        adjustedvalue = this->geofence.at(index).at(1);
        __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "check called");
    }

    return adjustedvalue;
}

bool LocProvider::isInsideFence(vector<double> obs){
    double testpoint[2];
    double coords[] = {obs.at(0), obs.at(1), obs.at(2)};
    this->convertCartesianToSpherical(coords,testpoint);

    bool inside = false;
    for(int m = 0; m<walkregion.size(); m++) {
        vector<vector<double>> polygon = walkregion.at(m);
        double y = testpoint[0];
        double x = testpoint[1];
        int j = 0;
        int crossings = 0;
        for (int i = 0; i < polygon.size(); i++) {


            vector<double> a = polygon.at(i);
            j = i + 1;
            if (j >= polygon.size()) {
                j = 0;
            }
            vector<double> b = polygon.at(j);
            vector<double> testpointvec;
            testpointvec.push_back(testpoint[0]);
            testpointvec.push_back(testpoint[1]);
            if (rayCrossesSegment(testpointvec, a, b)) {
                crossings++;
            }



        }
        inside = crossings % 2 == 1;
        if(inside){
            return inside;
        }
    }
    return inside;
}

bool LocProvider::rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b){
    double px = point.at(1),
            py = point.at(0),
            ax = a.at(1),
            ay = a.at(0),
            bx = b.at(1),
            by = b.at(0);
    if (ay > by) {
        ax = b.at(1);
        ay = b.at(0);
        bx = a.at(1);
        by = a.at(0);
    }
    // alter longitude to cater for 180 degree crossings
    if (px < 0) {
        px += 360;
    }
    if (ax < 0) {
        ax += 360;
    }
    if (bx < 0) {
        bx += 360;
    }

    if (py == ay || py == by) py += 0.00000001;
    if ((py > by || py < ay) || (px > max(ax, bx))) return false;
    if (px < min(ax, bx)) return true;

    double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
    double blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
    return (blue >= red);
}

//void LocProvider::move(double distance, double turn, vector<double> measurements, double *position,
//                       vector<double> beaconmsts, bool isBeacon, vector<double> wifimsts,
//                       bool isWifi, int beaconSize, vector<vector<double>> neighlocs
//                       ) {
//
//    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
//    if(this->reInitinitiated){
//        position[0] = xPos;
//        position[1] = yPos;
//        position[2] = zPos;
//        position[3] = orientation;
//    }
//
//    vector<double> weights;
//    vector<double>weights_now;
//
//
//    double std_dev = this->TURN_NOISE * M_PI/180;
//
//    for (int i = 0; i < particleSize; i++) {
//        Particle slave = this->slaves.at(i);
//        std::default_random_engine generator (seed);
//        std::normal_distribution<double> turn_distribution(0, bias);
//        double degreeTurn = turn;//turn+turn_distribution(generator);
//
//        this->slaves.at(i) = slave.move(distance, degreeTurn, turn_distribution, generator, this->TURN_NOISE);
//    }
//
//    if(distance == 0){
//        getUserPosition(weights, slaves, position);
//        return;
//    }
//
//    for (int i = 0; i < particleSize; i++) {
//        Particle slave = this->slaves.at(i);
//        vector<double> location;
//        location.push_back(slave.getX());
//        location.push_back(slave.getY());
//        location.push_back(slave.getZ());
//        double weight_sim = 1;
//        for(int k=0;k<neighlocs.size();k++){
//            vector<double> pos = neighlocs.at(k);
//            double rmse = sqrt(pow(pos.at(0)- location.at(0), 2) + pow(pos.at(1)- location.at(1), 2) + pow(pos.at(2)- location.at(2), 2));
//            double weight = gaussian(rmse, 5.0, 0)+exp(-70);
//            weight_sim *= pow(weight, 0.2);
//        }
//
//
//        //weight_sim = exp(-500);
//        double weight_slave = slave.transformWeight(weight_sim);
//        weights.push_back(weight_slave);
//        weights_now.push_back(weight_sim);
//        this->slaves.at(i) = slave;
//
//    }
//
//
//    getUserPosition(weights, slaves, position);
//
//
//    this->normalizeWeightsForSlaves();
//
//
//    this->checkNeedForResample();
//    //this->TURN_NOISE = position[4]*180/M_PI;
//    status = position[4];
//    xPos = position[0];
//    yPos = position[1];
//    zPos = position[2];
//    orientation = position[3];
//    position[4] = status;
//    //position[5] = max_weights;
//    position[7] = position[5];
//
//    steps = 1;
//    reSample(weights, slaves);
//    if(!converged && status > 0){
//        converged = true;
//    }
//
//
//}











