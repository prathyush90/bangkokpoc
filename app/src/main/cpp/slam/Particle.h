//
// Created by noob on 5/3/19.
//

#ifndef WISEMAP_PARTICLE_H
#define WISEMAP_PARTICLE_H
#include <vector>
#include "math.h"
#include <chrono>
#include <string>
#include <sstream>
#include <iostream>
#include<cmath>
#include <random>
#include "../Eigen/Dense"
#include "../Eigen/Eigenvalues"

using namespace std;
using namespace Eigen;
class Particle {
private:
    double mX;
    double mY;
    double mZ;
    double orientation;
    double forward_noise    = 0.0;
    double turn_noise       = 0.0;

    double magYReading = 1.0;
    double magXReading = 0.0;
    double magZReading = 0.0;
public:
    double getMagZReading() const;

    void setMagZReading(double magZReading);

private:
    double beaconXReading = 0.0;
    double beaconYReading = 0.0;
    double beaconZReading = 0.0;

public:
    double getWeight() const;

    double getMagYReading() const;

    void setMagYReading(double magYReading);

    double getMagXReading() const;

    void setMagXReading(double magXReading);

    double getBeaconXReading() const;

    void setBeaconXReading(double beaconXReading);

    double getBeaconYReading() const;

    void setBeaconYReading(double beaconYReading);

    double getBeaconZReading() const;

    void setBeaconZReading(double beaconZReading);

    void setOrientation(double orientation);

    void setWeight(double weight);


private:
    vector<vector<double> >geofence;
    bool isRobot;
    double weight = 1;
public:
    Particle(double x, double y, double z, double heading, vector<vector<double> > geofence);

    Particle(const Particle& robot);

    Particle(double x, double y, double z, double orientation, vector<vector<double> >geofence,
             double forward_noise, double turn_noise, double weight);

    void setValues(double x, double y, double z, double orientation);

    void setNoise(double forward_noise, double bearing_noise);

    double getX();
    double getY();
    double getZ();
    double adjustCoordinates(double value, int index);
    Particle move(double distance, double turn, std::normal_distribution<double> turn_distribution, std::default_random_engine generator,double turnNoise,vector<vector<vector<double>>> _walkregion);
    double randd() {
        return (double)rand() / (RAND_MAX + 1.0);
    }
    double radToDeg(double radians);
    double degToRad(double degrees);

    void nextPosition(double meters, double heading, double returnValue[]);
    void convertSphericalToCartesian(double meters, double heading, double returnarray[]);
    void convertCartesianToSpherical(double * cartesian, double latlng[]);
    void distance(double * from, double distance, double heading, double latlng[]);
    double gaussian(double mu, double sigma, double value);
    double measurementProbability(vector<double> sensed, double readings[]);
    double measurementProbabilityBeacon(vector<double> sensed, vector<double> readings);

    double getOrientation();

    double measurementProbabilityBeaconPseudo(vector<double> sensed, vector<double> readings);

    double measurementProbabilityMagnitude(vector<double> location, vector<vector<double>> result);
    double measurementProbabilityWithStd(double magVal, double sensedVal, double std);
    bool operator < (const Particle p) const
    {
        return (weight < p.weight);
    }
    double transformWeight(double weight);

    double getMagnitudeReading();
    bool isInsideFence(vector<double> obs, vector<vector<vector<double>>> walkregion);
    bool rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b);
};


#endif //WISEMAP_PARTICLE_H
