//
// Created by noob on 5/3/19.
//

#include "Particle.h"
#include <android/log.h>
#include "math.h"



Particle::Particle(double x, double y, double z, double heading, vector <vector<double>> geofence) {
    this->mX = x;
    this->mY = y;
    this->mZ = z;



    this->orientation = heading;
    this->geofence = geofence;
    mX = adjustCoordinates(mX, 0);
    mY = adjustCoordinates(mY, 1);
    mZ = adjustCoordinates(mZ, 2);
//    this->walkregion = _walkregion;
}

Particle::Particle(const Particle &robot) {
    this->mX          = robot.mX;
    this->mY          = robot.mY;
    this->mZ          = robot.mZ;
    this->orientation =  robot.orientation;
    this->geofence    = robot.geofence;
    this->forward_noise = robot.forward_noise;
    this->turn_noise  = robot.turn_noise;
    this->weight      = robot.weight;

    this->magYReading = robot.magYReading;
    this->magZReading = robot.magZReading;
    this->beaconXReading = robot.beaconXReading;
    this->beaconYReading = robot.beaconYReading;
    this->beaconZReading = robot.beaconZReading;
//    this->walkregion    = robot.walkregion;

}

void Particle::setValues(double x, double y, double z, double orientation) {
    if(x < this->geofence.at(0).at(1) || x > this->geofence.at(0).at(0)){
        this->mX = x;
    } else{
        throw "Index out of bounds: X";
    }

    if(y < this->geofence.at(1).at(1) || y > this->geofence.at(1).at(0)){
        this->mY = y;
    } else{
        throw "Index out of bounds: Y";
    }

    if(z < this->geofence.at(2).at(1) || z > this->geofence.at(2).at(0)){
        this->mZ = z;
    } else{
        throw "Index out of bounds: Z";
    }

    if (!(orientation < 0 ||  orientation > 2*M_PI)){
        this->orientation = orientation;
    } else{
        if(orientation < 0){
            orientation += 2 * M_PI;
            this->orientation = orientation;
        } else{
            orientation -= 2 * M_PI;
            this->orientation = orientation;
        }
    }

}

void Particle::setNoise(double forward_noise, double bearing_noise) {
    this->forward_noise = forward_noise;
    this->turn_noise = bearing_noise;
}

double Particle::getX() {
    return this->mX;
}

double Particle::getY() {
    return this->mY;
}

double Particle::getZ() {
    return this->mZ;
}

double Particle::getOrientation() {
    return this->orientation;
}

double Particle::adjustCoordinates(double value, int index) {
    double adjustedvalue = value;
//    if (value < this->geofence.at(index).at(0)){
//        adjustedvalue = this->geofence.at(index).at(0);
//        __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "check called");
//    } else if(value > this->geofence.at(index).at(1)){
//        adjustedvalue = this->geofence.at(index).at(1);
//        __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "check called");
//    }

    return adjustedvalue;
}

Particle Particle::move(double distance, double turn, std::normal_distribution<double> turn_distribution, std::default_random_engine generator, double turnNoise, vector<vector<vector<double>>> walkregion) {
//    std::normal_distribution<double> turn_distribut(0, turnNoise * M_PI/180);
//    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
//    std::default_random_engine generato (seed);
    double noise   = 0;//turn_distribut(generato);
    //double noise   = ((this->randd()*2)-1)*2*turnNoise * M_PI/180;

    //std::normal_distribution<double> forward_distribution(0, this->forward_noise);
    double noise2 = 0;//fabs(forward_distribution(generator));

    double heading = this->orientation + noise + degToRad(turn);
    if(heading <0){
        heading  += 2*M_PI;
    }
    heading        = fmod(heading,2 * M_PI);
    double distance_travelled = distance+noise2;
    double newPosition[3];
    if(distance_travelled > 0) {
        nextPosition(distance_travelled, radToDeg(heading), newPosition);
    }else{
        newPosition[0] = mX;
        newPosition[1] = mY;
        newPosition[2] = mZ;
    }
    //if(distance_travelled > 0) {
        vector<double> obs;
        obs.push_back(newPosition[0]);
        obs.push_back(newPosition[1]);
        obs.push_back(newPosition[2]);
        bool isInside = this->isInsideFence(obs, walkregion);
        if (isInside) {
            mX = newPosition[0];
            mY = newPosition[1];
            mZ = newPosition[2];
        }
        if (isInside) {
            //this is not magreading. Didn't use a new variable that's all. Don't confuse
            setMagYReading(1);
        } else {
            setMagYReading(0);
        }
    //}
    orientation = heading;
    Particle newRobot(*this);
    return newRobot;
}



double Particle::radToDeg(double deg) {
    if(deg < 0){
        deg += 2 * M_PI;
    }else if(deg > 2*M_PI){
        deg -= 2*M_PI;
    }
    double radians =  deg * 180 / M_PI;
    return radians;


}

double Particle::degToRad(double deg) {
    return (deg * M_PI)/180;
}

bool Particle::isInsideFence(vector<double> obs, vector<vector<vector<double>>> walkregion){
    //x:-1131984.749268518, y: 6084095.048049189, z: 1514009.7323876543
    double testpoint[2];
//    obs.at(0) = -1131983.4391005398;
//    obs.at(1) = 6084082.546061173;
//    obs.at(2) = 1513999.36139878;
    double coords[] = {obs.at(0), obs.at(1), obs.at(2)};
    this->convertCartesianToSpherical(coords,testpoint);

    bool inside = false;
    for(int m = 0; m<walkregion.size(); m++) {
        vector<vector<double>> polygon = walkregion.at(m);
        double y = testpoint[0];
        double x = testpoint[1];
        int j = 0;
        int crossings = 0;
        for (int i = 0; i < polygon.size(); i++) {


            vector<double> a = polygon.at(i);
            j = i + 1;
            if (j >= polygon.size()) {
                j = 0;
            }
            vector<double> b = polygon.at(j);
            vector<double> testpointvec;
            testpointvec.push_back(testpoint[0]);
            testpointvec.push_back(testpoint[1]);
            if (rayCrossesSegment(testpointvec, a, b)) {
                crossings++;
            }



        }
        inside = (crossings % 2 == 1);
        if(inside){
            return inside;
        }
    }
    return inside;
}

void Particle::nextPosition(double meters, double heading, double returnValue[]) {
    double position[2];
    double coords[] = {this->mX, this->mY, this->mZ};
    convertCartesianToSpherical(coords,position);
    double latLng[2];
    distance(position, meters, heading,latLng);

    convertSphericalToCartesian(latLng[0], latLng[1],returnValue);
}

void  Particle::convertSphericalToCartesian(double latitude, double longitude, double returnValue[]) {

    double earthRadius = 6371000; //radius in m
    double lat = degToRad(latitude);
    double lon = degToRad(longitude);
    double x = earthRadius * cos(lat) * cos(lon);
    double y = earthRadius * cos(lat) * sin(lon);
    double z = earthRadius * sin(lat);
    returnValue[0] =  x;
    returnValue[1] =  y;
    returnValue[2] =  z;
}

void Particle::distance(double *from, double distance, double heading, double latlng[]) {
    distance    /= 6371000;
    heading      = degToRad(heading);
    double fromLat = degToRad(from[0]);
    double fromLng = degToRad(from[1]);
    double cosdistance = cos(distance);
    double sindistance = sin(distance);


    double sinFromLat = sin(fromLat);
    double cosFromLat = cos(fromLat);
    double sinLat = cosdistance * sinFromLat + sindistance * cosFromLat * cos(heading);
    double dLng = atan2(
            sindistance * cosFromLat * sin(heading),
            cosdistance - sinFromLat * sinLat);
    latlng[0] = radToDeg(asin(sinLat));
    latlng[1] = radToDeg(fromLng + dLng);
}

void Particle::convertCartesianToSpherical(double *cartesian, double latlng[]) {
    double radius = sqrt((cartesian[0] * cartesian[0])+(cartesian[1] * cartesian[1])+(cartesian[2] * cartesian[2]));
    double lat = radToDeg(asin(cartesian[2] / radius));
    double lng = radToDeg(atan2(cartesian[1], cartesian[0]));
    latlng[0] = lat;
    latlng[1] = lng;
}

double Particle::gaussian(double mu, double sigma, double value) {
    double numerator = -1*pow((mu - value)/sigma ,2)*0.5;
    numerator        = exp(numerator);
    double denominator      = sqrt(2 * M_PI)*sigma+0.00000000000000000000000000005;

    return numerator/denominator;
}

double Particle::measurementProbability(vector<double> sensed, double readings[]) {
    double probability = 1.0;
//    bool notPresent = true;
//    if(readings[0] != 0){
//        notPresent = false;
//    }
//
//    if(notPresent){
//        return exp(-50);
//    }
    int length = sensed.size();
    double sigma = 1.5;

    double squaredDiff = 0.0;
    for (int i = 0; i < length; i++) {
        double val  = sensed.at(i);
        double val_ = readings[i];
        double diff = pow((sensed.at(i)-readings[i]),2);
        double gauss = gaussian(0.0,sigma,sqrt(diff))+exp(-70);
        double prob  = pow(gauss, 1.0/length);
        probability *= prob;
        squaredDiff += diff;
    }

//    double exponent = 1.0/length;
//
//
//    double gauss = gaussian(0.0,sigma,sqrt(squaredDiff))+exp(-70);
//    double prob  = pow(gauss, exponent);
//    probability *= prob;
    //probability += 0.0000000000001;
    this->weight *= probability;
    return probability;
}

double Particle::measurementProbabilityBeacon(vector<double> sensed, vector<double> readings) {
    //this->weight *= 1;
    double probability = 1.0;
    int length = readings.size();
    bool notPresent = true;
    int readingsSize = sensed.size();
    for(int i=0;i<readingsSize;i++){
        if(sensed.at(i) != 9999999999){
            notPresent = false;
        }
    }

    if(notPresent){
        this->weight *= exp(-70);
        return exp(-70);
    }

    double sigma = 0.5;

    double squaredDiff = 0.0;
    for (int i = 0; i < length; i++) {
        squaredDiff += pow(sensed[i]-readings[i],2);
    }

    double exponent = 0.1;


    double gauss = gaussian(0.0,sigma,sqrt(squaredDiff))+exp(-70);
    double prob  = pow(gauss, exponent);
    probability *= prob;
    this->weight *= probability;
    return probability;
}

double Particle::measurementProbabilityBeaconPseudo(vector<double> sensed, vector<double> readings) {
    double probability = 1.0;
    int length = readings.size();
    bool notPresent = true;
    int readingsSize = sensed.size();
    for(int i=0;i<readingsSize;i++){
        if(sensed.at(i) != 0){
            notPresent = false;
        }
    }

    if(notPresent){
        this->weight *= exp(-70);
        return exp(-10);
    }

    double sigma = 0.5;

    double squaredDiff = 0.0;
    for (int i = 0; i < length; i++) {
        squaredDiff += pow(sensed[i]-readings[i],2);
    }

    double exponent = 1.0/length;


    double gauss = gaussian(0.0,sigma,sqrt(squaredDiff))+exp(-70);
    double prob  = pow(gauss, exponent);
    probability *= prob;

    return probability;
}

double
Particle::measurementProbabilityMagnitude(vector<double> location, vector<vector<double>> result) {
    double minimumSqrt = INFINITY;
    double probability = 1;
    for(unsigned long i=0;i<result.size();i++){
        vector<double>waypoint = result.at(i);
        double dist = sqrt(pow(location.at(0)-waypoint.at(0), 2) + pow(location.at(1)-waypoint.at(1), 2) + pow(location.at(2)-waypoint.at(2), 2));
        if(dist < minimumSqrt){
            minimumSqrt = dist;
        }
    }
    double exponent = 0.8;

    double sigma = 1.5;
    double gauss = gaussian(0.0,sigma,sqrt(minimumSqrt))+exp(-70);
    double prob  = pow(gauss, exponent);
    probability *= prob;

    return probability;
}
double Particle::measurementProbabilityWithStd(double magVal, double sensedVal, double std) {

    double probability = 1;

    if(magVal == 9999999999){
        this->weight = numeric_limits<double >::min();
        return this->weight;
    }

    double gauss = gaussian(magVal,std,sensedVal)+exp(-70);
    double prob  = pow(gauss, 0.2);
    probability *= prob;
    this->weight *= probability;
    return this->weight;
}

double Particle::getWeight() const {
    return weight;
}

void Particle::setWeight(double weight) {
    this->weight = weight;
}

void Particle::setOrientation(double orientation) {
    Particle::orientation = orientation;
}

Particle::Particle(double x, double y, double z, double orientation,
                   vector<vector<double> > geofence, double forward_noise, double turn_noise,
                   double weight) {
    this->mX          = x;
    this->mY          = y;
    this->mZ          = z;
    this->orientation = orientation;
    this->geofence    = geofence;
    this->forward_noise = forward_noise;
    this->turn_noise  = turn_noise;
    this->weight      = weight;
//    this->walkregion = _walkregion;
}

double Particle::getMagYReading() const {
    return magYReading;
}

void Particle::setMagYReading(double magYReading) {
    Particle::magYReading = magYReading;
}

double Particle::getMagXReading() const {
    return magXReading;
}

void Particle::setMagXReading(double magXReading) {
    Particle::magXReading = magXReading;
}

double Particle::getBeaconXReading() const {
    return beaconXReading;
}

void Particle::setBeaconXReading(double beaconXReading) {
    Particle::beaconXReading = beaconXReading;
}

double Particle::getBeaconYReading() const {
    return beaconYReading;
}

void Particle::setBeaconYReading(double beaconYReading) {
    Particle::beaconYReading = beaconYReading;
}

double Particle::getBeaconZReading() const {
    return beaconZReading;
}

void Particle::setBeaconZReading(double beaconZReading) {
    Particle::beaconZReading = beaconZReading;
}

double Particle::getMagZReading() const {
    return magZReading;
}

void Particle::setMagZReading(double magZReading) {
    Particle::magZReading = magZReading;
}

double Particle::transformWeight(double weight) {
    this->weight *= weight;
    return this->weight;
}

double Particle::getMagnitudeReading() {
    return sqrt(pow(magXReading,2)+pow(magYReading, 2)+pow(magZReading, 2));
}

bool Particle::rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b) {
    double px = point.at(1),
            py = point.at(0),
            ax = a.at(1),
            ay = a.at(0),
            bx = b.at(1),
            by = b.at(0);
    if (ay > by) {
        ax = b.at(1);
        ay = b.at(0);
        bx = a.at(1);
        by = a.at(0);
    }
    // alter longitude to cater for 180 degree crossings
    if (px < 0) {
        px += 360;
    }
    if (ax < 0) {
        ax += 360;
    }
    if (bx < 0) {
        bx += 360;
    }

    if (py == ay || py == by) py += 0.00000001;
    if ((py > by || py < ay) || (px > max(ax, bx))) return false;
    if (px < min(ax, bx)) return true;

    double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
    double blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
    return (blue >= red);
}




