//
// Created by prathyush on 2020-02-24.
//

#include "Robot.h"

Robot::Robot(double x, double y, double z, double heading) {
    this->mX = x;
    this->mY = y;
    this->mZ = z;
    this->orientation = heading;
}

Robot::Robot(const Robot &robot) {
    this->mX          = robot.mX;
    this->mY          = robot.mY;
    this->mZ          = robot.mZ;
    this->orientation =  robot.orientation;
    this->forward_noise = robot.forward_noise;
    this->turn_noise  = robot.turn_noise;
    this->weight      = robot.weight;
    this->magYReading = robot.magYReading;

}

void Robot::setNoise(double forward_noise, double bearing_noise) {
    this->forward_noise = forward_noise;
    this->turn_noise = bearing_noise;
}

Robot Robot::move(double distance, double turn, vector<vector<vector<double>>> walkregion) {
    double heading = this->orientation + degToRad(turn);
    if(heading <0){
        heading  += 2*M_PI;
    }
    heading        = fmod(heading,2 * M_PI);
    double distance_travelled = distance;
    double newPosition[3];
    if(distance_travelled > 0) {
        nextPosition(distance_travelled, radToDeg(heading), newPosition);
    }else{
        newPosition[0] = mX;
        newPosition[1] = mY;
        newPosition[2] = mZ;
    }

    vector<double> obs;
    obs.push_back(newPosition[0]);
    obs.push_back(newPosition[1]);
    obs.push_back(newPosition[2]);
    bool isInside = this->isInsideFence(obs, walkregion);
    if (isInside) {
        mX = newPosition[0];
        mY = newPosition[1];
        mZ = newPosition[2];
    }
    if (isInside) {
        //this is not magreading. Didn't use a new variable that's all. Don't confuse
        setMagYReading(1);
    } else {
        setMagYReading(0);
    }

    orientation = heading;
    Robot newRobot(*this);
    return newRobot;
}

void Robot::setMagYReading(double magYReading) {
    Robot::magYReading = magYReading;
}

double Robot::radToDeg(double deg) {
    if(deg < 0){
        deg += 2 * M_PI;
    }else if(deg > 2*M_PI){
        deg -= 2*M_PI;
    }
    double radians =  deg * 180 / M_PI;
    return radians;
}

double Robot::degToRad(double deg) {
    return (deg * M_PI)/180;
}

void Robot::nextPosition(double meters, double heading, double *returnValue) {
    double position[2];
    double coords[] = {this->mX, this->mY, this->mZ};
    convertCartesianToSpherical(coords,position);
    double latLng[2];
    distance(position, meters, heading,latLng);

    convertSphericalToCartesian(latLng[0], latLng[1],returnValue);
}

void Robot::convertSphericalToCartesian(double latitude, double longitude, double returnValue[]) {
    double earthRadius = 6371000; //radius in m
    double lat = degToRad(latitude);
    double lon = degToRad(longitude);
    double x = earthRadius * cos(lat) * cos(lon);
    double y = earthRadius * cos(lat) * sin(lon);
    double z = earthRadius * sin(lat);
    returnValue[0] =  x;
    returnValue[1] =  y;
    returnValue[2] =  z;
}

void Robot::convertCartesianToSpherical(double *cartesian, double *latlng) {
    double radius = sqrt((cartesian[0] * cartesian[0])+(cartesian[1] * cartesian[1])+(cartesian[2] * cartesian[2]));
    double lat = radToDeg(asin(cartesian[2] / radius));
    double lng = radToDeg(atan2(cartesian[1], cartesian[0]));
    latlng[0] = lat;
    latlng[1] = lng;
}

void Robot::distance(double *from, double distance, double heading, double *latlng) {
    distance    /= 6371000;
    heading      = degToRad(heading);
    double fromLat = degToRad(from[0]);
    double fromLng = degToRad(from[1]);
    double cosdistance = cos(distance);
    double sindistance = sin(distance);


    double sinFromLat = sin(fromLat);
    double cosFromLat = cos(fromLat);
    double sinLat = cosdistance * sinFromLat + sindistance * cosFromLat * cos(heading);
    double dLng = atan2(
            sindistance * cosFromLat * sin(heading),
            cosdistance - sinFromLat * sinLat);
    latlng[0] = radToDeg(asin(sinLat));
    latlng[1] = radToDeg(fromLng + dLng);
}

double Robot::getX() {
    return this->mX;
}

double Robot::getY() {
    return this->mY;
}

double Robot::getWeight() {
    return this->weight;
}
double Robot::getZ() {
    return this->mZ;
}

double Robot::getOrientation() {
    return this->orientation;
}

double Robot::transformWeight(double weight) {
    this->weight *= weight;
    return this->weight;
}

bool Robot::isInsideFence(vector<double> obs, vector<vector<vector<double>>> walkregion){
    //x:-1131984.749268518, y: 6084095.048049189, z: 1514009.7323876543
    double testpoint[2];
//    obs.at(0) = -1131983.4391005398;
//    obs.at(1) = 6084082.546061173;
//    obs.at(2) = 1513999.36139878;
    double coords[] = {obs.at(0), obs.at(1), obs.at(2)};
    this->convertCartesianToSpherical(coords,testpoint);

    bool inside = false;
    for(int m = 0; m<walkregion.size(); m++) {
        vector<vector<double>> polygon = walkregion.at(m);
        double y = testpoint[0];
        double x = testpoint[1];
        int j = 0;
        int crossings = 0;
        for (int i = 0; i < polygon.size(); i++) {


            vector<double> a = polygon.at(i);
            j = i + 1;
            if (j >= polygon.size()) {
                j = 0;
            }
            vector<double> b = polygon.at(j);
            vector<double> testpointvec;
            testpointvec.push_back(testpoint[0]);
            testpointvec.push_back(testpoint[1]);
            if (rayCrossesSegment(testpointvec, a, b)) {
                crossings++;
            }



        }
        inside = (crossings % 2 == 1);
        if(inside){
            return inside;
        }
    }
    return inside;
}

bool Robot::rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b) {
    double px = point.at(1),
            py = point.at(0),
            ax = a.at(1),
            ay = a.at(0),
            bx = b.at(1),
            by = b.at(0);
    if (ay > by) {
        ax = b.at(1);
        ay = b.at(0);
        bx = a.at(1);
        by = a.at(0);
    }
    // alter longitude to cater for 180 degree crossings
    if (px < 0) {
        px += 360;
    }
    if (ax < 0) {
        ax += 360;
    }
    if (bx < 0) {
        bx += 360;
    }

    if (py == ay || py == by) py += 0.00000001;
    if ((py > by || py < ay) || (px > max(ax, bx))) return false;
    if (px < min(ax, bx)) return true;

    double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
    double blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
    return (blue >= red);
}

double Robot::getMagYReading() const {
    return magYReading;
}
void Robot::setWeight(double weight) {
    this->weight = weight;
}
