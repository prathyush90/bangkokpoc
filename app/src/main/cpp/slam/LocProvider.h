//
// Created by noob on 5/3/19.
//

#ifndef WISEMAP_LOCPROVIDER_H
#define WISEMAP_LOCPROVIDER_H

#include <map>
#include "Particle.h"
#include <math.h>
#include <NeighbourMeasurements.h>
#include "PCA.h"
#include "KDTree.h"
#include "IntervalTree.h"
#include "MagneticMap.h"
using namespace std;
using namespace Eigen;


class LocProvider {
private:
    int dimensions;
    int particleSize;
    vector<vector<double>>geofence;
    map<string,NeighbourMeasurements> measurementsHash;
    map<string,NeighbourMeasurements> beaconHash;
//    tree for magnetic and wifi readings
    KDTree neighboursTree;
//    tree for beacons
    KDTree beaconNeigboursTree;
    vector<NeighbourMeasurements>listMeasurements;
    NeighbourMeasurements neighbour;
    vector<Particle> slaves;
    double FORWARD_NOISE = 0.05;
    double TURN_NOISE    = 2.5;
    double status = 0;
    double unCertainity = 0;
    double previousHeading = 0;
    bool avoidReasample;
    int steps = 0;
    int randomCount = 0;
    double xPos,yPos,zPos,orientation;
    double bXPos,bYPos,bZPos;
    IntervalTree* augmentedTree;
    vector<double> beaconprediction;
    vector<double> magPrediction;
    double bias = 360;
    bool resampleCalled= false;
    bool orientationConfident = false;
    //static int particlesizecopy;
    //NeighbourMeasurements neighbour;
    int max_particles = 1000;
    int min_particles = 250;
    bool converged;
    int resample_count = 0;
    //bool orientationConfident;
    double convergenceConfidence = 1;
    vector<vector<vector<double>>> walkregion;
    bool useSingleBeacon = false;

public:
    void initLocalizerAndMapper(int dimensions, int particleSize, vector<vector<double>> geofence,
                                vector<NeighbourMeasurements> magneticMapData, vector<vector<vector<double>>> walkregion, bool usesingleBeacon);



    void initParticleFilter(vector<vector<double>> geofence, double heading, double b_x, double b_y, double b_z, int size);
    void reInitParticleFilter(double x, double y, double z);
    double randd() {
        return (double)rand() / (RAND_MAX + 1.0);
    }
    //void move(double distance, double turn, vector<double> measurements, double position[], vector<double> beaconmsts, bool isBeacon, vector<double> wifimsts, bool isWifi, int beaconSize, vector<vector<double>> neighlocs);
    void move(double distance, double turn, vector<double> measurements, double position[], vector<double> beaconmsts, bool isBeacon, vector<double> wifimsts, bool isWifi, int beaconSize, bool isCached);

    void getNearestReadings(vector<double> obs, double returnValue[]);
    void getNearestReadings_Point(vector<double> obs, double returnValue[]);
    double getNearestReadings_Similarity(vector<double> obs, bool isBeacon, bool isMagnitude, double magy, double magz, double magx, vector<double>beaconmeasurements, double magneticMagnitude, bool isInside, bool isCached);
    double getNearestReadings_Similarity_Interpolated(vector<double> obs, bool isBeacon, bool isMagnitude, double magy, double magz, double magx, double x, double y, double z, double magneticMagnitude);
    vector<double> getNearestReadingsBeacon(vector<double> sensed);
    void getUserPosition(vector<double> weights,vector<Particle>slaves, double position[]);
    void reSample(vector<double> weights,vector<Particle>slaves);
    int returnMax(vector<double> weights);
    int returnMin(vector<double> weights);
    void calculateBias(double turnInDegrees);
    void getParticleLocations(double locs[1500][10]);
    double cosine_similarity(double *A, double *B, unsigned int Vector_Length);



    void removeDuplicatesReverse(vector<NeighbourMeasurements> measurements);
    void removeDuplicates(vector<NeighbourMeasurements> measurements);

    void buildTreeReverse(vector<point_t> vector);
    void buildTree(vector<point_t> vector);

    void insertMagneticMapIntoTree(vector<MagneticMap> vector);
    //void getWayPointsLoc(double minimum, double maximum, vector<vector<double>> result);


    void getUserCorrectedPosition(double d, vector<double> beacon, vector<double> magnetic,
                                  double pDouble[2]);

    void normalizeWeightsForSlaves();

    void checkNeedForResample();

    bool reInitinitiated = false;

    void addMoreParticles(int number);
    void removeParticles(int number);

    double gaussian(double mu, double sigma, double value);
    double angleFromLatLng(double lat1, double lng1, double lat2, double lng2);
    void convertCartesianToSpherical(double * cartesian, double latlng[]);
    double radToDeg(double radians);
    double degToRad(double degrees);

    double getMagnitudeInterpolated(vector<double> obs);
    double getExpectedDensity(double x, double y, double z);
    double getMSE(double x, double y, double z, double b_x, double b_y, double b_z);
    void transformParticles(double x, double y, double z, double b_x, double b_y, double b_z);
    double adjustCoordinates(double value, int index);
    bool isInsideFence(vector<double> obs);
    bool rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b);

    void findClosestBeaconReading(double b_x, double b_y, double b_z, double result[]);
};


#endif //WISEMAP_LOCPROVIDER_H