//
// Created by prathyush on 2020-02-24.
//

#include <chrono>
#include <random>
#include "SlamManager.h"

void
SlamManager::initLocalizerAndMapper(int p_size, vector<vector<double>> geofence,
                                    vector<vector<vector<double>>> _walkregion) {
    this->particleSize   = p_size;
    this->geofence       = geofence;
    this->walkregion     = _walkregion;
}

void SlamManager::initParticleFilter(vector<vector<double>> geofence, double heading, double b_x,
                                     double b_y, double b_z, int size) {

    orientation = M_PI*heading/180;
    if(orientation < 0){
        orientation += 2*M_PI;
    }
    converged = false;
    orientation = fmod(orientation, 2*M_PI);
    this->slaves.clear();

    vector<double> weights;





    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator_x (seed);
    std::uniform_real_distribution<> dist_x(0.0, 1.0);

    for(int i=0;i<particleSize;i++){

        double x = (dist_x(generator_x))*((geofence.at(0).at(1) - geofence.at(0).at(0)))+geofence.at(0).at(0);
        double y = (dist_x(generator_x))*((geofence.at(1).at(1) - geofence.at(1).at(0)))+geofence.at(1).at(0);
        double z = (dist_x(generator_x))*((geofence.at(2).at(1) - geofence.at(2).at(0)))+geofence.at(2).at(0);
        vector<double>obs;
        obs.push_back(x);
        obs.push_back(y);
        obs.push_back(z);
        while (!isInsideFence(obs)){
            x = (dist_x(generator_x))*((geofence.at(0).at(1) - geofence.at(0).at(0)))+geofence.at(0).at(0);
            y = (dist_x(generator_x))*((geofence.at(1).at(1) - geofence.at(1).at(0)))+geofence.at(1).at(0);
            z = (dist_x(generator_x))*((geofence.at(2).at(1) - geofence.at(2).at(0)))+geofence.at(2).at(0);
            obs.clear();
            obs.push_back(x);
            obs.push_back(y);
            obs.push_back(z);
        }
        double ori = (this->randd()*2)*M_PI;
        Robot slave(x, y, z, ori);
        slave.setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        this->slaves.push_back(slave);
        weights.push_back(1);
    }
    xPos     = ((geofence.at(0).at(1) + geofence.at(0).at(0))/2);
    yPos     = ((geofence.at(1).at(1) + geofence.at(1).at(0))/2);
    zPos     = ((geofence.at(2).at(1) + geofence.at(2).at(0))/2);


}

bool SlamManager::isInsideFence(vector<double> obs) {
    double testpoint[2];
    double coords[] = {obs.at(0), obs.at(1), obs.at(2)};
    this->convertCartesianToSpherical(coords,testpoint);

    bool inside = false;
    for(int m = 0; m<walkregion.size(); m++) {
        vector<vector<double>> polygon = walkregion.at(m);
        double y = testpoint[0];
        double x = testpoint[1];
        int j = 0;
        int crossings = 0;
        for (int i = 0; i < polygon.size(); i++) {


            vector<double> a = polygon.at(i);
            j = i + 1;
            if (j >= polygon.size()) {
                j = 0;
            }
            vector<double> b = polygon.at(j);
            vector<double> testpointvec;
            testpointvec.push_back(testpoint[0]);
            testpointvec.push_back(testpoint[1]);
            if (rayCrossesSegment(testpointvec, a, b)) {
                crossings++;
            }



        }
        inside = crossings % 2 == 1;
        if(inside){
            return inside;
        }
    }
    return inside;
}

bool SlamManager::rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b) {
    double px = point.at(1),
            py = point.at(0),
            ax = a.at(1),
            ay = a.at(0),
            bx = b.at(1),
            by = b.at(0);
    if (ay > by) {
        ax = b.at(1);
        ay = b.at(0);
        bx = a.at(1);
        by = a.at(0);
    }
    // alter longitude to cater for 180 degree crossings
    if (px < 0) {
        px += 360;
    }
    if (ax < 0) {
        ax += 360;
    }
    if (bx < 0) {
        bx += 360;
    }

    if (py == ay || py == by) py += 0.00000001;
    if ((py > by || py < ay) || (px > max(ax, bx))) return false;
    if (px < min(ax, bx)) return true;

    double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
    double blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
    return (blue >= red);
}

void SlamManager::convertCartesianToSpherical(double *cartesian, double *latlng) {
    double radius = sqrt((cartesian[0] * cartesian[0])+(cartesian[1] * cartesian[1])+(cartesian[2] * cartesian[2]));
    double lat = radToDeg(asin(cartesian[2] / radius));
    double lng = radToDeg(atan2(cartesian[1], cartesian[0]));
    latlng[0] = lat;
    latlng[1] = lng;
    return;
}

double SlamManager::radToDeg(double deg) {
    if(deg < 0){
        deg += 2 * M_PI;
    }else if(deg > 2*M_PI){
        deg -= 2*M_PI;
    }
    double radians =  deg * 180 / M_PI;
    return radians;
}

void SlamManager::move(double turn, int event, int axis, double *position) {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    vector<double> weights;
    vector<double>weights_now;
    std::default_random_engine generator (seed);
    std::normal_distribution<double> turn_distribution(0, this->TURN_NOISE);
    std::normal_distribution<double> forward_distribution(0, this->FORWARD_NOISE);
    std::normal_distribution<double> random_angle_distribution(0, 1);
    std::normal_distribution<double> random_distance_distribution(0, 1);
    bool posteriorInside = false;
    double distance = event == 0 ? 0 : 0.65;
    for (int i = 0; i < particleSize; i++) {
        Robot slave = this->slaves.at(i);
        double degreeTurn = turn+turn_distribution(generator);

        double distance_forward = 0;
        if(distance > 0) {
            distance_forward = distance + forward_distribution(generator);
        }
        double shouldBeRandomHeading = fabs(random_angle_distribution(generator));
        double distanceTravelledShouldBeRandom = fabs(random_distance_distribution(generator));
        //roughly 10-13 percent particles random movement
        if(shouldBeRandomHeading < 2 && shouldBeRandomHeading > 1.5){
            degreeTurn = this->randd()*M_PI*2;
        }
        double distance_RANDOM = 1.0;
        if(distance == 0){
            distance_RANDOM = 0.4;
        }
        if(distanceTravelledShouldBeRandom < 2 && distanceTravelledShouldBeRandom > 1.5){
            distance_forward = this->randd()*distance_RANDOM;
        }
        this->slaves.at(i) = slave.move(distance_forward, degreeTurn,this->walkregion);

    }
    getUserPosition(slaves, position);
    status = position[4];
    if(!converged && status > 0){
        converged = true;
    }
}

void SlamManager::getUserPosition(vector<Robot> slaves, double position[]) {
    double xMean = 0;
    double yMean = 0;
    double zMean = 0;
    double count = 0;
    double sumWeight = 0;
    double orientation = 0;

    for (int i = 0; i < particleSize; i++) {
        Robot slave = slaves.at(i);
        sumWeight += slave.getWeight();
    }
    if(sumWeight == 0){
        sumWeight = 0.00000001;
        //avoidReasample = true;
    }
    double min_orientation = slaves.at(0).getOrientation();
    double max_orientation = slaves.at(0).getOrientation();
    for (int i = 0; i < particleSize; i++) {
        Robot slave = slaves.at(i);
        double weight = slave.getWeight();
        double weightFactor = weight / sumWeight;
        count += weightFactor;

        xMean += slave.getX() * weightFactor;
        yMean += slave.getY() * weightFactor;
        zMean += slave.getZ() * weightFactor;
        double particle_orientation = (fmod((slave.getOrientation() - slaves.at(0).getOrientation() + M_PI), (2.0 * M_PI))
                                       + slaves.at(0).getOrientation() - M_PI);
        if(particle_orientation < 0){
            particle_orientation += 2*M_PI;
        }
        particle_orientation = fmod(particle_orientation, 2*M_PI);
        orientation += particle_orientation;
        if(max_orientation < particle_orientation){
            max_orientation = particle_orientation;
        }
        if(min_orientation > particle_orientation){
            min_orientation = particle_orientation;
        }

    }
    if(count == 0){
        count = 0.0000000001;
    }
    xMean /= count;
    yMean /= count;
    zMean /= count;
    orientation /= this->particleSize;
    count = 0;
    double maxDiff = -1;
    double orientationdiff = 0;
    int orientationCount = 0;
    double highestDiff = -1;
    for (int i = 0; i < particleSize; i++) {
        Robot slave = slaves.at(i);
        double euclidean = sqrt(pow(slave.getX() - xMean, 2) +
                                pow(slave.getY() - yMean, 2) + pow(slave.getZ() - zMean, 2));

        if(euclidean > highestDiff){
            highestDiff = euclidean;
        }
        maxDiff += euclidean;

        double orientDiff = orientation - slave.getOrientation();
        orientDiff = fabs(fmod((orientDiff + M_PI),(2.0 * M_PI)) - M_PI);
        orientationdiff += orientDiff;
        if (euclidean < 3.0) {
            count++;
            //orientationCount += 1;
        }
        if(orientDiff < 0.25){
            orientationCount += 1;
        }
    }
    orientationdiff /= particleSize;

    double status = 1;
    if (count < (0.90 * particleSize)) {
        status = 0;
    }


    position[0] = xMean;
    position[1] = yMean;
    position[2] = zMean;
    position[3] = orientation;
    position[4] = status;
    position[5] = orientationdiff;
    position[6] = maxDiff / particleSize;
//    if(converged) {
//        position[6] = maxDiff / particleSize;
//    }else{
//        position[6] = highestDiff;
//    }
    position[7] = max_orientation*180/M_PI;
}

void SlamManager::getParticleLocations(double locs[4000][3]) {
    for (int i = 0; i < particleSize; i++) {
        Robot slave = this->slaves.at(i);
        locs[i][0] = slave.getX();
        locs[i][1] = slave.getY();
        locs[i][2] = slave.getZ();
    }
}

void SlamManager::continueWithResample(vector<double> weights) {
    for(int i=0;i<particleSize;i++){
        Robot slave = this->slaves.at(i);
        double weight = weights.at(i);
        if(slave.getMagYReading()) {
            weight = slave.transformWeight(weight);
        }else{
            weight = slave.transformWeight(1.1927909110297572e-150);
        }
        this->slaves.at(i) = slave;
        weights.at(i) = weight;
    }
    this->normalizeWeightsForSlaves();
    this->checkNeedForResample();
    reSample(weights, slaves);

}

void SlamManager::normalizeWeightsForSlaves() {
    double sum = 0;
    for (int i = 0; i < particleSize; i++) {
        Robot slave = this->slaves.at(i);
        double weight = slave.getWeight();
        sum += weight;
    }
    if(sum == 0){
        sum = 0.00000001;
    }
    for (int i = 0; i < particleSize; i++) {
        Robot slave = this->slaves.at(i);
        double normalizedWeight = slave.getWeight()/sum;
        slave.setWeight(normalizedWeight);
        this->slaves.at(i) = slave;
    }
}

void SlamManager::checkNeedForResample() {

    double sumWeights = 0;
    for (int i = 0; i < particleSize; i++) {
        Robot slave = this->slaves.at(i);
        double val = slave.getWeight();
        val = pow(val, 2);
        sumWeights += val;
    }

    if (sumWeights == 0) {
        sumWeights = 0.0000000000000001;
    }
    double invWeight = 1 / sumWeights;
    this->avoidReasample = invWeight >= particleSize / 2;



}

void SlamManager::reSample(vector<double> weights, vector<Robot> slaves) {
      vector<Robot> newparticles;
    if(avoidReasample){
        return;
    }


    int rand_index                    = (int)(((double) rand() / (RAND_MAX))*(particleSize));
    double beta                       = 0.0;
    double max_weight = *max_element(weights.begin(),weights.end());

    for(int i=0; i<particleSize; i++){
        beta    += ((randd())*(2.0)*(max_weight));
        while (beta >= weights.at(rand_index)){
            beta -= weights.at(rand_index);
            rand_index = (rand_index+1) % particleSize;
        }

        Robot particle = this->slaves.at(rand_index);
        particle.setWeight(1);
        Robot copy_particle(particle.getX(), particle.getY(), particle.getZ(), particle.getOrientation());

        copy_particle.setMagYReading(particle.getMagYReading());
        newparticles.push_back(copy_particle);
    }
    this->slaves.clear();
    this->slaves = newparticles;

}



