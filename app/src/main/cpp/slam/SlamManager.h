//
// Created by prathyush on 2020-02-24.
//

#ifndef WISEFLYPOC_SLAMMANAGER_H
#define WISEFLYPOC_SLAMMANAGER_H

#include<Robot.h>
#include <math.h>
#include <map>


using namespace std;
using namespace Eigen;

class SlamManager {
private:
    int particleSize;
    vector<vector<double>>geofence;
    vector<Robot>slaves;
    double FORWARD_NOISE = 0.05;
    double TURN_NOISE    = 3.0;
    double xPos,yPos,zPos,orientation;
    bool avoidReasample;
    bool converged;
    vector<vector<vector<double>>> walkregion;
    double status = 0;


public:
    void
    initLocalizerAndMapper(int i, vector<vector<double>> geofence,
                           vector<vector<vector<double>>> _walkregion);
    void initParticleFilter(vector<vector<double>> geofence, double heading, double b_x, double b_y, double b_z, int size);
    bool isInsideFence(vector<double> obs);
    bool rayCrossesSegment(vector<double> point, vector<double> a, vector<double> b);
    void convertCartesianToSpherical(double * cartesian, double latlng[]);
    double radToDeg(double radians);
    double randd() {
        return (double)rand() / (RAND_MAX + 1.0);
    }
    void move(double turn, int evnt, int axis, double position[]);
    void getUserPosition(vector<Robot>slaves, double position[]);
    void getParticleLocations(double pDouble[4000][3]);
    void continueWithResample(vector<double> vector);
    void normalizeWeightsForSlaves();
    void checkNeedForResample();
    void reSample(vector<double> weights,vector<Robot>slaves);
};


#endif //WISEFLYPOC_SLAMMANAGER_H
