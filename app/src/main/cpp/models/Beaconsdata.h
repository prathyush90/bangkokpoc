//
// Created by noob on 14/11/18.
//

#ifndef WISEMAP_BEACONSDATA_H
#define WISEMAP_BEACONSDATA_H


class Beaconsdata {
private:
    double beacon_distance;
    double beacon_pos_x;
    double beacon_pos_y;
public:
    double getKalman_distance() const {
        return kalman_distance;
    }

    void setKalman_distance(double kalman_distance) {
        Beaconsdata::kalman_distance = kalman_distance;
    }

private:
    double beacon_pos_z;
    int rssi;
    double kalman_distance;

public:
    Beaconsdata(double beacon_distance, double beacon_pos_x, double beacon_pos_y,
                double beacon_pos_z, int rssi, double kalman_distance) : beacon_distance(
            beacon_distance), beacon_pos_x(beacon_pos_x), beacon_pos_y(beacon_pos_y), beacon_pos_z(
            beacon_pos_z), rssi(rssi), kalman_distance(kalman_distance){}

    double getBeacon_distance() const {
        return beacon_distance;
    }

    void setBeacon_distance(double beacon_distance) {
        Beaconsdata::beacon_distance = beacon_distance;
    }

    double getBeacon_pos_x() const {
        return beacon_pos_x;
    }

    void setBeacon_pos_x(double beacon_pos_x) {
        Beaconsdata::beacon_pos_x = beacon_pos_x;
    }

    double getBeacon_pos_y() const {
        return beacon_pos_y;
    }

    void setBeacon_pos_y(double beacon_pos_y) {
        Beaconsdata::beacon_pos_y = beacon_pos_y;
    }

    double getBeacon_pos_z() const {
        return beacon_pos_z;
    }

    void setBeacon_pos_z(double beacon_pos_z) {
        Beaconsdata::beacon_pos_z = beacon_pos_z;
    }



    int getRssi() const {
        return rssi;
    }

    void setRssi(int rssi) {
        Beaconsdata::rssi = rssi;
    }
};


#endif //WISEMAP_BEACONSDATA_H
