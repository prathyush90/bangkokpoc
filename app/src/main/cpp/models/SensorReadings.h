//
// Created by prathyush on 08/08/19.
//

#ifndef WISEMAPPER_SENSORREADINGS_H
#define WISEMAPPER_SENSORREADINGS_H
#include "string"
using namespace std;
class SensorReading{
    private:
    double rssi;
    string bssid;
public:
    SensorReading(int rssi, const string &bssid) : rssi(rssi), bssid(bssid) {}

    double getRssi() const {
        return rssi;
    }

    void setRssi(double rssi) {
        SensorReading::rssi = rssi;
    }

    const string &getBssid() const {
        return bssid;
    }

    void setBssid(const string &bssid) {
        SensorReading::bssid = bssid;
    }


};



#endif //WISEMAPPER_SENSORREADINGS_H
