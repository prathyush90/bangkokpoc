//
// Created by noob on 5/3/19.
//

#ifndef WISEMAP_NEIGHBOURMEASUREMENTS_H
#define WISEMAP_NEIGHBOURMEASUREMENTS_H

#include "SensorReadings.h"
class NeighbourMeasurements {


private:
    double beacon_x;
    double beacon_y;
    double beacon_z;
    double particle_x;
    double particle_y;
    double particle_z;
    double wifi_mean;
    double wifi_std;
    double beacon_distance_mean;
    double beacon_distance_std;
    double beacon_rssi_mean;
    double beacon_rssi_std;
    double x_mag;
    double y_mag;
    double z_mag;
    bool hasBeacon;
    bool hasWifi;


    double mag_mean;
    double mag_std;
    double magx_mean;
    double magx_std;
    double magz_mean;
    double magz_std;
    double magy_mean;
    double magy_std;
    double bx_mean;
    double bx_std;
    double by_mean;
    double by_std;
    double bz_mean;
    double bz_std;
    double orientationDegrees;
public:
    double getOrientationDegrees() const {
        return orientationDegrees;
    }

    void setOrientationDegrees(double orientationDegrees) {
        NeighbourMeasurements::orientationDegrees = orientationDegrees;
    }

    NeighbourMeasurements(double b_x, double b_y, double b_z, double p_x, double p_y, double p_z,
                              double x_mag, double y_mag, double z_mag, bool hasbeacon, double mean_rssi, double std_rssi) {
        this->beacon_x = b_x;
        this->beacon_y = b_y;
        this->beacon_z = b_z;
        this->particle_x = p_x;
        this->particle_y = p_y;
        this->particle_z = p_z;
        this->x_mag      = x_mag;
        this->y_mag      = y_mag;
        this->z_mag      = z_mag;
        this->hasBeacon = hasbeacon;
        this->beacon_rssi_mean = mean_rssi;
        this->beacon_rssi_std = std_rssi;
    }

    NeighbourMeasurements(double b_x, double b_y, double b_z, double p_x, double p_y, double p_z,
                          double x_mag, double y_mag, double z_mag, bool hasbeacon, double mean_rssi, double std_rssi, double b_mean_x, double b_mean_y, double b_mean_z) {
        this->beacon_x = b_x;
        this->beacon_y = b_y;
        this->beacon_z = b_z;
        this->particle_x = p_x;
        this->particle_y = p_y;
        this->particle_z = p_z;
        this->x_mag      = x_mag;
        this->y_mag      = y_mag;
        this->z_mag      = z_mag;
        this->hasBeacon = hasbeacon;
        this->beacon_rssi_mean = mean_rssi;
        this->beacon_rssi_std = std_rssi;

        this->bx_mean = b_mean_x;
        this->by_mean = b_mean_y;
        this->bz_mean = b_mean_z;
    }

public:

    double getMag_mean() const {
        return mag_mean;
    }

    void setMag_mean(double mag_mean) {
        NeighbourMeasurements::mag_mean = mag_mean;
    }

    double getMag_std() const {
        return mag_std;
    }

    void setMag_std(double mag_std) {
        NeighbourMeasurements::mag_std = mag_std;
    }

    double getMagx_mean() const {
        return magx_mean;
    }

    void setMagx_mean(double magx_mean) {
        NeighbourMeasurements::magx_mean = magx_mean;
    }

    double getMagx_std() const {
        return magx_std;
    }

    void setMagx_std(double magx_std) {
        NeighbourMeasurements::magx_std = magx_std;
    }

    double getMagz_mean() const {
        return magz_mean;
    }

    void setMagz_mean(double magz_mean) {
        NeighbourMeasurements::magz_mean = magz_mean;
    }

    double getMagz_std() const {
        return magz_std;
    }

    void setMagz_std(double magz_std) {
        NeighbourMeasurements::magz_std = magz_std;
    }

    double getMagy_mean() const {
        return magy_mean;
    }

    void setMagy_mean(double magy_mean) {
        NeighbourMeasurements::magy_mean = magy_mean;
    }

    double getMagy_std() const {
        return magy_std;
    }

    void setMagy_std(double magy_std) {
        NeighbourMeasurements::magy_std = magy_std;
    }

    double getBx_mean() const {
        return bx_mean;
    }

    void setBx_mean(double bx_mean) {
        NeighbourMeasurements::bx_mean = bx_mean;
    }

    double getBx_std() const {
        return bx_std;
    }

    void setBx_std(double bx_std) {
        NeighbourMeasurements::bx_std = bx_std;
    }

    double getBy_mean() const {
        return by_mean;
    }

    void setBy_mean(double by_mean) {
        NeighbourMeasurements::by_mean = by_mean;
    }

    double getBy_std() const {
        return by_std;
    }

    void setBy_std(double by_std) {
        NeighbourMeasurements::by_std = by_std;
    }

    double getBz_mean() const {
        return bz_mean;
    }

    void setBz_mean(double bz_mean) {
        NeighbourMeasurements::bz_mean = bz_mean;
    }

    double getBz_std() const {
        return bz_std;
    }

    void setBz_std(double bz_std) {
        NeighbourMeasurements::bz_std = bz_std;
    }

public:
    const map<string, SensorReading> &getBeacon_details() const {
        return beacon_details;
    }

    void setBeacon_details(const map<string, SensorReading> &beacon_details) {
        NeighbourMeasurements::beacon_details = beacon_details;
    }

    const map<string, SensorReading> &getWifi_details() const {
        return wifi_details;
    }

    void setWifi_details(const map<string, SensorReading> &wifi_details) {
        NeighbourMeasurements::wifi_details = wifi_details;
    }

private:
    map<string,SensorReading> beacon_details;
    map<string,SensorReading> wifi_details;


public:
    double getBeacon_x() const {
        return beacon_x;
    }

    void setBeacon_x(double beacon_x) {
        NeighbourMeasurements::beacon_x = beacon_x;
    }

    double getBeacon_y() const {
        return beacon_y;
    }

    void setBeacon_y(double beacon_y) {
        NeighbourMeasurements::beacon_y = beacon_y;
    }

    double getBeacon_z() const {
        return beacon_z;
    }

    void setBeacon_z(double beacon_z) {
        NeighbourMeasurements::beacon_z = beacon_z;
    }

    double getParticle_x() const {
        return particle_x;
    }

    void setParticle_x(double particle_x) {
        NeighbourMeasurements::particle_x = particle_x;
    }

    double getParticle_y() const {
        return particle_y;
    }

    void setParticle_y(double particle_y) {
        NeighbourMeasurements::particle_y = particle_y;
    }

    double getParticle_z() const {
        return particle_z;
    }

    void setParticle_z(double particle_z) {
        NeighbourMeasurements::particle_z = particle_z;
    }

    double getWifi_mean() const {
        return wifi_mean;
    }

    void setWifi_mean(double wifi_mean) {
        NeighbourMeasurements::wifi_mean = wifi_mean;
    }

    double getWifi_std() const {
        return wifi_std;
    }

    void setWifi_std(double wifi_std) {
        NeighbourMeasurements::wifi_std = wifi_std;
    }

    double getBeacon_distance_mean() const {
        return beacon_distance_mean;
    }

    void setBeacon_distance_mean(double beacon_distance_mean) {
        NeighbourMeasurements::beacon_distance_mean = beacon_distance_mean;
    }

    double getBeacon_distance_std() const {
        return beacon_distance_std;
    }

    void setBeacon_distance_std(double beacon_distance_std) {
        NeighbourMeasurements::beacon_distance_std = beacon_distance_std;
    }

    double getBeacon_rssi_mean() const {
        return beacon_rssi_mean;
    }

    void setBeacon_rssi_mean(double beacon_rssi_mean) {
        NeighbourMeasurements::beacon_rssi_mean = beacon_rssi_mean;
    }

    double getBeacon_rssi_std() const {
        return beacon_rssi_std;
    }

    void setBeacon_rssi_std(double beacon_rssi_std) {
        NeighbourMeasurements::beacon_rssi_std = beacon_rssi_std;
    }

    double getX_mag() const {
        return x_mag;
    }

    void setX_mag(double x_mag) {
        NeighbourMeasurements::x_mag = x_mag;
    }

    double getY_mag() const {
        return y_mag;
    }

    void setY_mag(double y_mag) {
        NeighbourMeasurements::y_mag = y_mag;
    }

    double getZ_mag() const {
        return z_mag;
    }

    void setZ_mag(double z_mag) {
        NeighbourMeasurements::z_mag = z_mag;
    }

    bool isHasBeacon() const {
        return hasBeacon;
    }

    void setHasBeacon(bool hasBeacon) {
        NeighbourMeasurements::hasBeacon = hasBeacon;
    }

    bool isHasWifi() const {
        return hasWifi;
    }

    void setHasWifi(bool hasWifi) {
        NeighbourMeasurements::hasWifi = hasWifi;
    }

    NeighbourMeasurements(double beacon_x, double beacon_y, double beacon_z, double particle_x,
                          double particle_y, double particle_z, double wifi_mean, double wifi_std,
                          double beacon_distance_mean, double beacon_distance_std,
                          double beacon_rssi_mean, double beacon_rssi_std, double x_mag,
                          double y_mag, double z_mag, bool hasBeacon, bool hasWifi, map<string,SensorReading> bdetails, map<string,SensorReading>wdetails,
                          double mag_mean, double mag_std, double magx_mean, double magx_std, double magy_mean, double magy_std,double magz_mean, double magz_std,
                          double bx_mean, double bx_std, double by_mean, double by_std, double bz_mean, double bz_std, double headingDegrees) : beacon_x(
            beacon_x), beacon_y(beacon_y), beacon_z(beacon_z), particle_x(particle_x), particle_y(
            particle_y), particle_z(particle_z), wifi_mean(wifi_mean), wifi_std(wifi_std),
                                                                                      beacon_distance_mean(
                                                                                              beacon_distance_mean),
                                                                                      beacon_distance_std(
                                                                                              beacon_distance_std),
                                                                                      beacon_rssi_mean(
                                                                                              beacon_rssi_mean),
                                                                                      beacon_rssi_std(
                                                                                              beacon_rssi_std),
                                                                                      x_mag(x_mag),
                                                                                      y_mag(y_mag),
                                                                                      z_mag(z_mag),
                                                                                      hasBeacon(
                                                                                              hasBeacon),
                                                                                      hasWifi(hasWifi),beacon_details(bdetails),wifi_details(wdetails),mag_mean(mag_mean),mag_std(mag_std),magx_mean(magx_mean),magx_std(magx_std),magy_mean(magy_mean),magy_std(magy_std),magz_mean(magz_mean),magz_std(magz_std),bx_mean(bx_mean),bx_std(bx_std),by_mean(by_mean),by_std(by_std),bz_mean(bz_mean),bz_std(bz_std), orientationDegrees(headingDegrees){}

    NeighbourMeasurements() {}
};


#endif //WISEMAP_NEIGHBOURMEASUREMENTS_H
