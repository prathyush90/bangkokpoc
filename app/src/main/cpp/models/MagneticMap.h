//
// Created by prathyush on 06/05/19.
//

#ifndef WISEMAP_MAGNETICMAP_H
#define WISEMAP_MAGNETICMAP_H


class MagneticMap {
    double mag_x;
    double mag_y;
    double mX;
    double mY;
    double mZ;
    double mag_z;
public:
    MagneticMap(double mag_x, double mag_y, double mag_z, double mX, double mY, double mZ) : mag_x(
            mag_x), mag_y(mag_y), mX(mX), mY(mY), mZ(mZ), mag_z(mag_z) {}

    double getMag_x() const {
        return mag_x;
    }

    void setMag_x(double mag_x) {
        MagneticMap::mag_x = mag_x;
    }

    double getMag_y() const {
        return mag_y;
    }

    void setMag_y(double mag_y) {
        MagneticMap::mag_y = mag_y;
    }

    double getMX() const {
        return mX;
    }

    void setMX(double mX) {
        MagneticMap::mX = mX;
    }

    double getMY() const {
        return mY;
    }

    void setMY(double mY) {
        MagneticMap::mY = mY;
    }

    double getMZ() const {
        return mZ;
    }

    void setMZ(double mZ) {
        MagneticMap::mZ = mZ;
    }

    double getMag_z() const {
        return mag_z;
    }

    void setMag_z(double mag_z) {
        MagneticMap::mag_z = mag_z;
    }

};


#endif //WISEMAP_MAGNETICMAP_H
